![FatorCMS](http://www.fatordigital.com.br/logo_fatorcms_234x55.png "FatorCMS")

-------
# Framework: CakePHP 2.4.0

# Plugins:
- Acl
- AclExtras
- FdBanners
- FdBlocos
- FdCursos
- FdDashboard
- FdEmails
- FdEventos
- FdGalerias
- FdInstituicoes
- FdLogs
- FdMenus
- FdNoticias
- FdPaginas
- FdRotas
- FdSac
- FdSettings
- FdSites
- FdSvn
- FdUsuarios
- FilterResults
- PhpExcel
- Upload

# Libraries
- Error / AppExceptionRenderer
- Wideimage
- CustomUploadHandler
- GaleriaUploadHandler
- WSToken