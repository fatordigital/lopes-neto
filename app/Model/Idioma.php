<?php
App::uses('AppModel', 'Model');

class Idioma extends AppModel
{

    public $cacheData;

    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        $this->cache();
    }

    public function atual($key)
    {
        $return = $this->find('first',
            array(
                'recursive' => -1,
                'conditions' => array(
                    'Idioma.slug' => CakeSession::read('idioma')
                )
            )
        );

        return $key && isset($return['Idioma'][$key]) ? $return['Idioma'][$key] : $return;
    }

    public function exist($slug)
    {
        return $this->find('count', array('conditions' => array('Idioma.slug' => $slug)));
    }

    public function padrao($key)
    {
        $return = $this->find('first', array(
            'recursive' => -1,
            'conditions' => array(
                'Idioma.padrao' => 1
            )
        ));

        return $key ? $return['Idioma'][$key] : $return;
    }

    private function cache()
    {
        $cache = array();
        if (Cache::read('idiomas') === false) {
            $cache = $this->find('all',
                array(
                    'recursive' => -1,
                    'order' => array('Idioma.titulo')
                )
            );
            Cache::write('idiomas', $cache);
        } else {
            $cache = Cache::read('idiomas');
        }
        $this->cacheData = $cache;
    }

}