<?php

App::uses('ModelBehavior', 'Model');

class IdiomaBehavior extends ModelBehavior
{

    private $model;

    public function setup(Model $model, $settings = array())
    {
        $this->model = $model;
        $this->Idioma = ClassRegistry::init('Idioma');
    }

    private function arquivo_e_idioma($datas)
    {

        //idioma padrão
        $idioma_padrao_id = $this->Idioma->padrao('id');
        foreach ($datas as $data) {
            $current = $data[$this->model->alias . 'Atributo'];
            if ($current['idioma_id'] == $idioma_padrao_id && !empty($current['titulo'])) {
                $this->model->data[$this->model->alias]['file_name_idioma'] = Inflector::slug(strtolower($current['titulo']), '-');
                $exist = true;
            }
        }

        if (!isset($exist)) {
            foreach ($datas as $data) {
                $current = $data[$this->model->alias . 'Atributo'];
                if (!empty($current['titulo'])) {
                    $this->model->data[$this->model->alias]['file_name_idioma'] = Inflector::slug(strtolower($current['titulo']), '-');
                    $exist = true;
                }
            }
        }


        if (!isset($exist)) {
            $this->model->data[$this->model->alias]['file_name_idioma'] = time() . random_int(10, 20);
        }
    }

    public function afterSave(Model $model, $created, $options = array())
    {
        if (isset($model->data[$model->alias . 'Atributo'])) {
            $this->arquivo_e_idioma($model->data[$model->alias . 'Atributo']);
        }
    }

}