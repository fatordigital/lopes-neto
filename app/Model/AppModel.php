<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model
{

    public $lala = array();

    public function check_url_unica()
    {
        if (!isset($this->data[$this->alias]['seo_url']) || $this->data[$this->alias]['seo_url'] == "") {
            return true;
        }

        App::import('Model', 'FdRotas.Rota');
        $this->Rota = new Rota();

        if (isset($this->data[$this->alias]['sites']) && $this->data[$this->alias]['sites'] != "" && !is_array($this->data[$this->alias]['sites'])) {
            $rota = $this->Rota->find('first', array('conditions' => array('seo_url' => $this->data[$this->alias]['seo_url'], 'sites LIKE' => "%" . $this->data[$this->alias]['sites'] . "%")));
        } elseif (isset($this->data[$this->alias]['sites']) && is_array($this->data[$this->alias]['sites'])) {
            $sites = json_encode($this->data[$this->alias]['sites']);
            $rota = $this->Rota->find('first', array('conditions' => array('seo_url' => $this->data[$this->alias]['seo_url'], 'sites LIKE' => "%" . $sites . "%")));
        } else {
            $rota = $this->Rota->find('first', array('conditions' => array('seo_url' => $this->data[$this->alias]['seo_url'])));
        }
        //$rota = $this->Rota->find('first', array('conditions' => array('seo_url' => $this->data[$this->alias]['seo_url'])));

        if (empty($rota)) {
            return true;
        } else {
            if ($this->alias != 'Grades') {
                if ($rota['Rota']['row_id'] == $this->data[$this->alias]['id']) {
                    return true;
                } else {
                    return false;
                }
            } else {
                if ($rota['Rota']['params_value'] == $this->data[$this->alias]['id']) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    public function getLastQuery()
    {
        if (is_object($this->lala)) {
            $logs = $this->lala->getLog();
        } else {
            $dbo = $this->getDatasource();
            $dbo->fullDebug = true;
            $logs = $dbo->getLog();
        }

        $lastLog = end($logs['log']);
        if (is_object($this->lala)) {
            $this->lala->fullDebug = false;
        } else {
            $dbo->fullDebug = false;
        }
        return $lastLog['query'];

    }


    /**
     * beforeSave
     *
     * sobrecarga do metodo executado antes de salvar o registro
     */
    public function beforeSave($options = array())
    {

        $params = Router::getParams();

        if (isset($params['fatorcms']) && $params['fatorcms'] == true) {
            $this->lala = $this->getDatasource();
            // turn fullDebug on
            $this->lala->fullDebug = true;
        }

        //log de save de registro
        if (empty($this->data[$this->alias]['usuario_id'])) {
            $this->data[$this->alias]['usuario_id'] = CakeSession::read('Auth.User.id');
        }

        return true;
    }

    // public function beforeDelete($cascade = true) {
    //    	if(isset($params['fatorcms']) && $params['fatorcms'] == true){
    // 		$this->lala = $this->getDatasource();
    // 		// turn fullDebug on
    // 		$this->lala->fullDebug = true;
    // 	}
    // }

    /**
     * afterSave
     *
     * sobrecarga do metodo executado apos de salvar o registro
     */
    public function afterSave($created, $options = array())
    {

        $params = Router::getParams();
        if (isset($params['fatorcms']) && $params['fatorcms'] == true) {
            App::import('Model', 'FdLogs.Log');
            $Log = new Log();

            $model = Router::getParams();

            $log['id'] = null;
            $log['usuario_id'] = isset($_SESSION['Auth']['User']) ? $_SESSION['Auth']['User']['id'] : null;
            $log['info'] = $this->getLastQuery();
            $log['model'] = $model['controller'];
            $log['acao'] = ($created) ? 'Save' : 'Update';

            $Log->save($log, array('callbacks' => false));
        }
    }

    /**
     * afterDelete
     *
     * sobrecarga do metodo executado apos remover o registro
     */
    public function afterDelete()
    {
        $params = Router::getParams();
        if (isset($params['fatorcms']) && $params['fatorcms'] == true) {
            App::import('Model', 'FdLogs.Log');
            $Log = new Log();

            $model = Router::getParams();

            $log['id'] = null;
            $log['usuario_id'] = $_SESSION['Auth']['User']['id'];
            $log['info'] = $this->getLastQuery();
            $log['model'] = $model['controller'];
            $log['acao'] = 'Delete';

            $Log->save($log, array('callbacks' => false));
        }
    }


    public function unbindAll($params = array())
    {
        foreach ($this->__associations as $ass) {
            if (!empty($this->{$ass})) {
                $this->__backAssociation[$ass] = $this->{$ass};
                if (isset($params[$ass])) {
                    foreach ($this->{$ass} as $model => $detail) {
                        if (!in_array($model, $params[$ass])) {
                            $this->__backAssociation = array_merge($this->__backAssociation, $this->{$ass});
                            unset($this->{$ass}[$model]);
                        }
                    }
                } else {
                    $this->__backAssociation = array_merge($this->__backAssociation, $this->{$ass});
                    $this->{$ass} = array();
                }
            }
        }
        return true;
    }
}
