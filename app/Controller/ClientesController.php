<?php

App::uses('Controller', 'Controller');

class ClientesController extends AppController
{

    public $components = array('FdEmails.SendEmails');

    public function index()
    {
        if ($this->request->is('post')) {
            $this->enviar_contato();
            $this->redirect($this->referer());
        }
    }

    private function enviar_contato()
    {
        App::import('Model', 'FdSac.Sac');
        $this->Sac = new Sac();

        if ($this->request->data['Sac']['fd'] == "") {
            if (isset($this->request->data['Sac'])) {

                $this->request->data['Sac']['sac_tipo_id'] = 9;

                if ($this->Sac->save($this->request->data['Sac'])) {

                    $this->request->data['Sac']['lead_tipo'] = 'Cliente';
                    $this->SendEmails->_enviaSac($this->request->data['Sac']);

                    $this->Flash->success('Contato realizado com sucesso.', array('key' => 'success'));
                    $this->Flash->goal('/goal/contato-cliente', array('key' => 'goal'));
                } else {
                    $this->Flash->error('O contato não pode ser salvo. Tente novamente.');
                }
            }
        } else {
            $this->Flash->error('O contato não pode ser salvo. Tente novamente.', 'error');
        }

    }

}