<?php

App::uses('AppController', 'Controller');

/**
 * Share Controller
 *
 * @property Contact            $Contact
 * @property PaginatorComponent $Paginator
 */
class ShareController extends AppController{

    /**
     * Components
     *
     * @var array
     */
    public $components = array();

    /**
     * index method
     *
     * @return void
     */
    public function index(){
        if(isset($this->params->query['url']) && $this->params->query['url'] != ""){
            $share['url'] = $this->params->query['url'];
        }

        if(isset($this->params->query['title']) && $this->params->query['title'] != ""){
            $share['title'] = $this->params->query['title'];
        }

        if(isset($this->params->query['description']) && $this->params->query['description'] != ""){
            $share['description'] = $this->params->query['description'];
        }

        if(isset($this->params->query['image']) && $this->params->query['image'] != ""){
            $share['image'] = $this->params->query['image'];
        }

        $share['site_name'] = '';

        $this->layout = 'share';
        $this->rander = false;
        $this->set(compact('share'));
    }
}
