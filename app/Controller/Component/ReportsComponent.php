<?php

class ReportsComponent extends Component {

    var $components = array('PhpExcel.PhpExcel');
    var $controller = null;

	public function initialize(Controller $controller){
	    $this->controller = $controller;
	}

    public function xls($data, $name = 'Relatório', $label = true) {

    	ini_set('memory_limit', '-1');

    	$fields_exception[] = 'senha';

		// create new empty worksheet and set default font
	    $this->PhpExcel->createWorksheet()
	        ->setDefaultFont('Calibri', 12);

	    // define table cells
		$headers = array();
		if(is_array($data)){
			// debug($data);die;
			foreach($data[0] as $y => $dts){

				if(is_array($dts)){
					foreach($dts as $x => $dt){
						if(!is_array($dt)){

							//exceptions
							if(in_array($x, $fields_exception)) continue;

							if($label){
								$headers[] = array('label' => $y.'.'.$x, 'filter' => true, 'wrap' => true);	
							}else{
								$headers[] = array('label' => $x, 'filter' => true, 'wrap' => true);
							}
							
						}else{							
							foreach($dt as $d => $t){
								if($label){
									$headers[] = array('label' => $y.'.'.$x.'.'.$d, 'filter' => true, 'wrap' => true);
								}else{
									$headers[] = array('label' => $d, 'filter' => true, 'wrap' => true);
								}
							}
						}
					}
				}
				
			}

			// add heading with different font and bold text
	    	$this->PhpExcel->addTableHeader($headers, array('name' => 'Cambria', 'bold' => true));

	    	// debug($data);die;

	    	// add data
		    foreach ($data as $dts) {
				$row = array();
				foreach($dts as $x => $dt){
					if(count($row) > count($headers)){
						continue;
					}

					if(!is_array($dt)){
						$row[] = strip_tags($dt);
					}else{
						if(count($dt) > 0){
							foreach($dt as $d => $t){
								if(!is_array($t)){
									//exceptions
									if(in_array($d, $fields_exception)) continue;

									$row[] = strip_tags($t);
								}else{
									if(count($t) > 0){
										foreach ($t as $a => $b) {
											if($b != ""){
												$row[] = @strip_tags($b);
											}else{
												$row[] = $b;
											}
										}
									}
								}
							}
						}
					}
				}

				$this->PhpExcel->addTableRow($row);
		    }
		}

	    // close table and output
	    $this->PhpExcel->addTableFooter()
	        ->output($name);
    }
}
