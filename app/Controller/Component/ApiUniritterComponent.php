<?php
App::uses('WSToken', 'Lib');

class ApiUniritterComponent extends Component {
	
	private function commit($key){
		$s = curl_init();
		curl_setopt($s,CURLOPT_URL,"https://portal.uniritter.edu.br/manifestacao_interesse.php"); //ou "http://selecao.fadergs.edu.br/manifestacao_interesse.php" para a fadergs
		curl_setopt($s,CURLOPT_POST,true);
		curl_setopt($s,CURLOPT_POSTFIELDS,"key=".$key);
		curl_exec($s);
		curl_close($s);
		
		return $s;
	}
	
	public function getCampi(){
		return $this->commit(WSToken::getKey(array("sequencePage"=>"carregaCampi")));
	}

	public function getTipos(){
		return $this->commit(WSToken::getKey(array("sequencePage"=>"carregaTipos")));
	}
	
	public function getTurnos(){
		return $this->commit(WSToken::getKey(array("sequencePage"=>"carregaTurnos")));
	}
	
	public function getCursos($unid_id, $nens_id){
		if($nens_id != null){
			return $this->commit(WSToken::getKey(array("sequencePage"=>"carregaCursos", "unid_id"=> $unid_id, "nens_id" => $nens_id )));
		}else{
			return $this->commit(WSToken::getKey(array("sequencePage"=>"carregaCursos", "unid_id"=> $unid_id )));
		}
	}
	
	public function setData($data){
		return $this->commit(WSToken::getKey($data));
	}
}