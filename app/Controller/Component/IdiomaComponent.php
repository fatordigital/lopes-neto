<?php

class IdiomaComponent extends Component
{

    public function initialize(Controller $controller)
    {
        $this->controller = $controller;
        $this->Idioma = ClassRegistry::init('Idioma');
        $this->controller->set('idiomas', $this->Idioma->cacheData);
        if ($this->controller->params['idioma'] != 'fatorcms' && $this->controller->params['prefix'] != 'fatorcms') {
            $this->verificar();
        }
    }

    private function redirecionar_novo_idioma($idioma)
    {
        preg_match('/\/\/(?<link>[a-zA-Z0-9-.]+)\/(?<idioma>([a-zA-Z]{0,5}+)|([a-zA-Z]{0,6}+)\/)/', Router::url($this->here, true), $match);
        if (!empty($match['idioma']) && $match['idioma'] != $idioma) {
            $this->controller->redirect(str_replace($match['idioma'], $idioma, $this->controller->referer()));
        } else if (empty($match['idioma'])) {
            $this->controller->redirect('//' . $match['link'] . '/' . $idioma . '/');
        }
    }

    private function verificar()
    {
        $params = $this->controller->request->params;
        $session = $this->controller->Session;
        if (isset($params['idioma']) && $params['idioma'] == $this->Idioma->padrao('slug')) {
            $session->write('idioma', $params['idioma']);
            return true;
        };

        # Fix BUG Rotas CAKEPHP
        $only = array();
        foreach ($this->Idioma->cacheData as $idioma) {
            $only[] = $idioma['Idioma']['slug'];
        }
        # Erro nas Rotas do CAKEPHP para ajustar foi feito a seguinte condição
        if (!isset($params['idioma']) && in_array($params['controller'], $only)) {
            $old_idioma = $session->read('idioma');
            $session->write('idioma', $params['controller']);
            $this->controller->redirect(Router::url(str_replace('/' . $old_idioma . '/', '/' . $session->read('idioma') . '/', $this->controller->referer())));
        }
        # Fix BUG Rotas CAKEPHP
        if (!isset($params['idioma'])) {
            # Caso a variável de configuração esteja preenchida, significa que o cliente não trabalha com MULTI IDIOMAS
            if (Configure::read('Site.Idioma.Padrao') == '') {
                if ($session->read('idioma')) {
                    $this->redirecionar_novo_idioma($session->read('idioma'));
                } else {
                    $this->redirecionar_novo_idioma($this->Idioma->padrao('slug'));
                }
            }
        } else if (isset($params['idioma'])) {
            if ($this->Idioma->exist($params['idioma'])) {
                $session->write('idioma', $params['idioma']);
                $this->redirecionar_novo_idioma($params['idioma']);
            } else if ($session->read('idioma')) {
                $this->redirecionar_novo_idioma($this->Idioma->padrao('slug'));
            } else {
                $session->write('idioma', $this->Idioma->padrao('slug'));

                $this->redirecionar_novo_idioma($this->Idioma->padrao('slug'));
            }
        }
    }
}