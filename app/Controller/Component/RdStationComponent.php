<?php

class RdStationComponent extends Component {

    public $_rdstation_token 			= '975cdf028ac20021ab6d845d73847ef8';
    public $_rdstation_private_token 	= '62d128b442d452592aa049e93e5630b2';


	public function initialize(Controller $controller){
	    $this->controller = $controller;
	}

/**
 * RD Station - Integrações
 * addLeadConversionToRdstationCrm()
 * Envio de dados para a API de leads do RD Station
 *
 * Parâmetros:
 *     ($rdstation_token) - token da sua conta RD Station ( encontrado em https://www.rdstation.com.br/docs/api )
 *     ($identifier) - identificador da página ou evento ( por exemplo, 'pagina-contato' )
 *     ($data_array) - um Array com campos do formulário ( por exemplo, array('email' => 'teste@rdstation.com.br', 'nome' =>'Fulano') )
 */
	public function addLeadConversionToRdstationCrm( $identifier, $data_array ) {
	  $api_url = "http://www.rdstation.com.br/api/1.2/conversions";

	  $rdstation_token = $this->_rdstation_token;

	  try {
		if (empty($data_array["token_rdstation"]) && !empty($rdstation_token)) { $data_array["token_rdstation"] = $rdstation_token; }
		if (empty($data_array["identificador"]) && !empty($identifier)) { $data_array["identificador"] = $identifier; }

		// if (isset($_COOKIE["c_utmz"]) || (!isset($data_array["c_utmz"]) || empty($data_array["c_utmz"])) ) { $data_array["c_utmz"] = $_COOKIE["__utmz"]; }

		if(empty($data_array["c_utmz"]) && isset($_COOKIE["__utmz"])){
	      	$data_array["c_utmz"] = $_COOKIE["__utmz"];
	    }

	    if(empty($data_array["traffic_source"]) && isset($_COOKIE["__trf_src"])){
	      	$data_array["traffic_source"] = $_COOKIE["__trf_src"];
	    }

	    if(empty($data["client_id"]) && !empty($_COOKIE["rdtrk"])){
	    	$data["client_id"] = json_decode($_COOKIE["rdtrk"])->{'id'};	
	    } 

		unset($data_array["password"], $data_array["password_confirmation"], $data_array["senha"], 
			  $data_array["confirme_senha"], $data_array["captcha"], $data_array["_wpcf7"], 
			  $data_array["_wpcf7_version"], $data_array["_wpcf7_unit_tag"], $data_array["_wpnonce"], 
			  $data_array["_wpcf7_is_ajax_call"]);

		if ( !empty($data_array["token_rdstation"]) && !( empty($data_array["email"]) && empty($data_array["email_lead"]) ) ) {
      	  $data_query = http_build_query($data_array);
		  if (in_array ('curl', get_loaded_extensions())) {
			$ch = curl_init($api_url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_query);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$a = curl_exec($ch);
			curl_close($ch);
		  } else {
			$params = array('http' => array('method' => 'POST', 'content' => $data_query, 'ignore_errors' => true));
			$ctx = stream_context_create($params); 
			$fp = @fopen($api_url, 'rb', false, $ctx);
		  }
		}
	  } catch (Exception $e) { }
	}

/**
 * RD Station - Integrações
 * addWonLostConversionToRdstationCrm()
 * Envio de dados para a API de vendas do RD Station
 *
 * Parâmetros:
 *     ($status) - tipo de conversão (lost or won)
 *     ($data_array) - um Array com campos do formulário ( por exemplo, array('email' => 'teste@rdstation.com.br', 'nome' =>'Fulano') )
 */
	public function addWonLostConversionToRdstationCrm( $status, $data_array ) {
	  $api_url = "https://www.rdstation.com.br/api/1.2/services/{$this->_rdstation_private_token}/generic";

	  $data_array['status'] = $status;

	  try {
		if (!( empty($data_array["email"])) ) {
      	  $data_query = json_encode($data_array);

      	  if (in_array ('curl', get_loaded_extensions())) {
			$ch = curl_init($api_url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_query);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			    'Content-Type: application/json',                                                                                
			    'Content-Length: ' . strlen($data_query))                                                                       
			); 
			$a = curl_exec($ch);
			curl_close($ch);
		  } else {
			$params = array('http' => array('method' => 'POST', 'content' => $data_query, 'ignore_errors' => true));
			$ctx = stream_context_create($params); 
			$fp = @fopen($api_url, 'rb', false, $ctx);
		  }
		}
	  } catch (Exception $e) { }
	}


/**
 * RD Station - Integrações
 * funelRdstationCrm()
 * Envio de dados para a API de vendas do RD Station
 *
 * Parâmetros:
 *     ($email) - email do lead
 *	   (lifecycle) = 	
 *     Estágio no funil de vendas:
 *		0 - Lead;
 *		1 - Lead Qualificado; 
 *		2 - Cliente
 *
 *     ($opportunity) - se é ou não oportunidade
 */
	public function funelRdstationCrm( $email, $lifecycle_stage, $opportunity = false ) {
	  $api_url = "https://www.rdstation.com.br/api/1.2/leads/" . $email;

	  // var_dump($api_url);DIE;

	  $data_array = array();	
	  $data_array["auth_token"] = $this->_rdstation_private_token;
	  $data_array['lead'] = array('lifecycle_stage' => $lifecycle_stage, 'opportunity' => $opportunity);

	  try {
		if (!( empty($data_array["auth_token"])) ) {
      	  $data_query = json_encode($data_array);

      	  if (in_array ('curl', get_loaded_extensions())) {
			$ch = curl_init($api_url);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_query);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			// curl_setopt($ch, CURLOPT_VERBOSE, 0);  
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                          
			    'Content-Type: application/json',                                                                                
			    'Content-Length: ' . strlen($data_query))                                                                       
			); 
			$a = curl_exec($ch);
			curl_close($ch);
		  } else {
			$params = array('http' => array('method' => 'PUT', 'content' => $data_query, 'ignore_errors' => true));
			$ctx = stream_context_create($params); 
			$fp = @fopen($api_url, 'rb', false, $ctx);
		  }
		}
	  } catch (Exception $e) { }
	}
}
