<?php

App::uses('Controller', 'Controller');

class AssociadoController extends AppController
{

    public function index()
    {
        $this->loadModel('FdAssociados.Associados');
        $this->set(array(
            'associados' => $this->Associados->cacheData
        ));
    }

}