<?php

class HomeController extends AppController
{

    public $components = array('Cookie');
    public $helpers = array('Js');


    public function index()
    {
        $this->loadModel('FdBanners.Banner');
        $this->loadModel('FdEventos.Evento');
        $this->loadModel('FdAssociados.Associados');

        
        $this->set(array(
            'banners' => $this->Banner->cacheData,
            'eventos' => $this->Evento->cacheData,
            'associados' => $this->Associados->cacheData
        ));

    }

    public function ajax_noticias_artigos_home()
    {
        $this->layout = 'ajax';


        if (!isset($this->params['named']['tipo']) && empty($this->params['named']['tipo'])) {
            $tipo = 1;
        } else {
            $tipo = $this->params['named']['tipo'];
        }


        App::import('Model', 'FdNoticias.Noticia');
        $this->Noticia = new Noticia();

        $this->paginate = array(
            'limit' => 2,
            'fields' => array(
                'Noticia.data_publicacao', 'Noticia.noticia_tipo_id',
                'Noticia.noticia_categoria_id', 'Noticia.thumb',
                'Noticia.thumb_dir', 'Noticia.thumb_path'
            ),
            'contain' => array(
                'NoticiaAtributo' => array(
                    'fields' => array(
                        'NoticiaAtributo.idioma_id', 'NoticiaAtributo.titulo', 'NoticiaAtributo.resumo',
                    )
                ),
                'Rota' => array(
                    'fields' => array(
                        'Rota.seo_url'
                    )
                ),
                'NoticiaTipo' => array(
                    'fields' => array(
                        'NoticiaTipo.nome', 'NoticiaTipo.apresentacao', 'NoticiaTipo.seo_url'
                    )
                )
            ),
            'conditions' => array(
                'Noticia.status' => 1,
                'Noticia.destaque' => 1,
                'Noticia.noticia_tipo_id' => $tipo
            ),
            'order' => array('Noticia.id' => 'desc')
        );
        $noticias = $this->paginate('Noticia');

        $this->set(compact('noticias', 'tipo'));

        $this->render('/Elements/paginas/home_noticia_artigos_slider');
    }

}