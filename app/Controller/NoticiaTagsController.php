<?php

class NoticiaTagsController extends AppController {

	public $components = array('Cookie');
	
	public function index($date = null){

		//load do model afins de pegar o hasMany
		App::import('Model', 'FdNoticias.Noticia');
		$this->Noticia = new Noticia();

		App::import('Model', 'FdNoticias.NoticiaTag');
		$this->NoticiaTag = new NoticiaTag();

		App::import('Model', 'FdNoticias.NoticiaNoticiaTag');
		$this->NoticiaNoticiaTag = new NoticiaNoticiaTag();

		//existe?
		if (!$this->NoticiaTag->exists($this->params->params['pass'][0])) {
			throw new NotFoundException(__('NoticiaTag inválida.'));
		}

		//find tag
		$noticia_tag = $this->NoticiaTag->find('first', array('recursive' => -1, 'conditions' => array('NoticiaTag.id' => $this->params->params['pass'][0])));

		//find noticias com a tag
		$noticia_noticia_tags = $this->NoticiaNoticiaTag->find('list', array(
																			'recursive' 	=> -1, 
																			'fields' 		=> array('NoticiaNoticiaTag.noticia_id'), 
																			'conditions' 	=> array('NoticiaNoticiaTag.tag_id' => $this->params->params['pass'][0]),
																			'group'			=> array('NoticiaNoticiaTag.noticia_id')
																		));
		//conditions
		$options['conditions'] = array('Noticia.status' => true, 'Noticia.id' => $noticia_noticia_tags);
		$this->Noticia->recursive = -1;
		$options['order'] = 'Noticia.id DESC';
		$this->paginate = $options;

		//paginate noticias
		$this->set('noticias', $this->paginate('Noticia'));

		//_addBreadcrumb
		$this->_addBreadcrumb('Notícias', array('controller' => 'noticias', 'action' => 'index'));
		$this->_addBreadcrumb($noticia_tag['NoticiaTag']['nome']);
	}
}