<?php

class NoticiasController extends AppController
{

    public $components = array('Cookie');

    public function index($date = null)
    {
        //load do model afins de pegar o hasMany
        App::import('Model', 'FdNoticias.Noticia');
        $this->Noticia = new Noticia();

        //conditions
        if (is_null($date)) {
            $hoje = date('Y-m-d');
            $options['conditions'] = array('Noticia.noticia_tipo_id' => array(1, 2), 'Noticia.status' => true);

        } else {
            $hoje = date('Y-m-d', strtotime($date));
            $options['conditions'] = array('Noticia.noticia_tipo_id' => array(1, 2), 'Noticia.status' => true, 'Noticia.data_publicacao' => $hoje);
        }

        $options['order'] = 'Noticia.id DESC';
        $this->paginate = $options;
        $this->set('noticias', $this->paginate());

        //set dia de hoje (para o calendário)
        $this->set(compact('hoje'));

        //_addBreadcrumb
        $this->_addBreadcrumb('Notícias', array('controller' => 'noticias', 'action' => 'index'));
    }

    /**
     * detalhe method
     *
     * @return void
     */
    public function detalhe($id = null)
    {


        //load do model afins de pegar o hasMany
        App::import('Model', 'FdNoticias.Noticia');
        $this->Noticia = new Noticia();

        App::import('Model', 'FdNoticias.NoticiaCategoria');
        $this->NoticiaCategoria = new NoticiaCategoria();

        if (!$this->Noticia->exists($id)) {
            throw new NotFoundException(__('Notícia inválida.'));
        }

        App::import('Helper', 'Idioma');
        $this->IdiomaHelper = new IdiomaHelper(new View());

        //find
        $options = array('conditions' => array('Noticia.status' => true, 'Noticia.' . $this->Noticia->primaryKey => $id));
        $noticia = $this->Noticia->find('first', $options);

        $noticia_categoria = $this->NoticiaCategoria->find('first', array('contain' => array('NoticiaCategoriaAtributo'), 'conditions' => array('NoticiaCategoria.id' => $noticia['NoticiaCategoria']['id'])));
        $noticia['NoticiaCategoria'] = $noticia_categoria['NoticiaCategoria'];
        $noticia['NoticiaCategoriaAtributo'] = $noticia_categoria['NoticiaCategoriaAtributo'];


        //pageview
        //begin page_view
        //total
        $ranking = array();
        $ranking['modified'] = false;
        $ranking['views'] = $noticia['Noticia']['views'] + 1;
        $ranking['noticia_categoria_id'] = $noticia['Noticia']['noticia_categoria_id'];
        //semanal
        if ($noticia['Noticia']['views_week_number'] == date('z')) {
            $ranking['views_week'] = $noticia['Noticia']['views_week'] + 1;
        } else {
            $ranking['views_week'] = 1;
            $ranking['views_week_number'] = date('z');
        }
        //mensal
        if ($noticia['Noticia']['views_current_month'] == date('m')) {
            $ranking['views_mouth'] = $noticia['Noticia']['views_mouth'] + 1;
        } else {
            $ranking['views_mouth'] = 1;
            $ranking['views_current_month'] = date('m');
        }
        $this->Noticia->id = $noticia['Noticia']['id'];
        $this->Noticia->save($ranking, false);
        //end page_view

        //seo
        //begin page_view
        if ($noticia['Noticia']['thumb'] != "") {
            // $seo['og:imagem']				= $noticia['Noticia']['thumb_path'] .'/'. $noticia['Noticia']['thumb_dir'] . '/250x250-' . $noticia['Noticia']['thumb'];
            $seo['og:imagem'] = $noticia['Noticia']['thumb_path'] . '/' . $noticia['Noticia']['thumb_dir'] . '/700x400-' . $noticia['Noticia']['thumb'];
        }

        // $seo['og:image:width']			= '610';
        // $seo['og:image:height']			= '330';
        // $seo['og:image:width']			= '700';
        // $seo['og:image:height']			= '400';
        $seo['og:image:width'] = '250';
        $seo['og:image:height'] = '250';
        $seo['og:type'] = 'article';

        $seo['article:section'] = $this->IdiomaHelper->extrair($noticia['NoticiaCategoriaAtributo'], 'titulo');
        $seo['article:tag'] = $this->IdiomaHelper->extrair($noticia['NoticiaAtributo'], 'seo_keywords');
        $seo['article:published_time'] = date('Y-m-d\TH:m', strtotime($noticia['Noticia']['data_publicacao']));
        $this->set(compact('seo'));
        //end page_view

        //begin posts relacionados
        $noticias_relacionadas_db = array();
        $noticias_relacionadas = array();
        $tags = array();
        if (isset($noticia['NoticiaTag']) && count($noticia['NoticiaTag']) > 0) {
            $tags = Set::extract('/id', $noticia['NoticiaTag']);

            if (count($tags) > 0) {
                $noticias_ids = $this->Noticia->NoticiaNoticiaTag->find('list', array('fields' => array('NoticiaNoticiaTag.noticia_id'), 'conditions' => array('NoticiaNoticiaTag.tag_id' => $tags, 'NoticiaNoticiaTag.noticia_id <>' => $noticia['Noticia']['id'])));

                $noticias_relacionadas_db = $this->Noticia->find('all', array('limit' => 10, 'order' => 'Noticia.id DESC', 'fields' => array('*'), 'conditions' => array('Noticia.id' => $noticias_ids, 'Noticia.status' => 1)));
            }
        }

        if (count($noticias_relacionadas_db) == 0) {
            $noticias_relacionadas_db = $this->Noticia->find('all', array('limit' => 10, 'order' => 'Noticia.id DESC', 'fields' => array('*'), 'conditions' => array('Noticia.status' => 1, 'Noticia.noticia_categoria_id' => $noticia['Noticia']['noticia_categoria_id'], 'Noticia.id <>' => $noticia['Noticia']['id'])));
        }

        if (count($noticias_relacionadas_db) > 0) {
            foreach ($noticias_relacionadas_db as $key => $not) {
                $noticias_relacionadas[$not['Noticia']['data_publicacao']][] = $not;
            }
        }

        $this->set(compact('noticias_relacionadas'));
        //end posts relacionados

        //Begin Galeria
        if (isset($noticia['Noticia']['galeria_id']) && $noticia['Noticia']['galeria_id'] != "") {
            App::import('Model', 'FdGalerias.Galeria');
            $this->Galeria = new Galeria();

            //import do model apenas para carregar o atributo 'useTable'
            App::import('Model', 'FdGalerias.GaleriaImagem');

            $galeria = $this->Galeria->find('first', array('conditions' => array('Galeria.id' => $noticia['Noticia']['galeria_id'])));
            if (!empty($galeria)) {
                $noticia['GaleriaImagem'] = $galeria['GaleriaImagem'];
            }
        }
        //End Galeria

        //set
        $this->set(compact('noticia'));

        //_addBreadcrumb
        // $this->_addBreadcrumb('Notícias', array('controller' => 'noticias', 'action' => 'index'));
        $this->_addBreadcrumb($noticia['NoticiaTipo']['nome'], $noticia['NoticiaTipo']['seo_url']);
        $this->_addBreadcrumb($this->IdiomaHelper->extrair($noticia['NoticiaAtributo'], 'titulo'), '/' . $this->IdiomaHelper->extrair($noticia['NoticiaAtributo'], 'seo_url'));
    }
}