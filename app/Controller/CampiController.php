<?php

class CampiController extends AppController {

	public $components = array('FdEmails.SendEmails');

/**
 * detalhe method
 *
 * @return void
 */
	public function detalhe(){

		App::import('Model', 'FdCursos.Campus');
		$this->Campus = new Campus();

		//curso
		$campus = $this->Campus->find('first', array('contain' => array('CampusImagem', 'Curso' => array('CursoCategoria', 'CursoTipo')), 'conditions' => array('Campus.status' => true, 'Campus.seo_url' => $this->params->url)));

		if (!$campus) {
			throw new NotFoundException(__('Campus inválido.'));
		}else{
			if(isset($campus['Curso'])){
				if(!empty($campus['Curso'])){
					foreach ($campus['Curso'] as $key => $curso) {
						if(isset($curso['CursoCategoria']['seo_url'])){
							$campus['Cursos'][$curso['CursoCategoria']['seo_url']]['CursoCategoria'] = $curso['CursoCategoria'];
							$campus['Cursos'][$curso['CursoCategoria']['seo_url']]['Cursos'][] = $curso;
						}else{
							$campus['Cursos'][$curso['CursoTipo']['seo_url']]['CursoTipo'] = $curso['CursoTipo'];
							$campus['Cursos'][$curso['CursoTipo']['seo_url']]['Cursos'][] = $curso;
						}
					}
				}
			}

			//BEGIN: ordenação dos cursos
			if(isset($campus['Cursos']) && count($campus['Cursos']) > 0){
				ksort($campus['Cursos']);
				if(isset($campus['Cursos'])){
					if(!empty($campus['Cursos'])){
						foreach ($campus['Cursos'] as $key => $curso) {
							if(count($curso['Cursos']) > 0){
								$campus['Cursos'][$key]['Cursos'] = Set::sort($curso['Cursos'], '{n}.nome', 'asc');
							}
						}
					}
				}
			}
			//END: ordenação


			if(isset($campus['Campus']['diferenciais']) && count($campus['Campus']['diferenciais']) > 0){
				$this->loadModel('FdDiferenciais.Diferencial');
				$diferenciais = $this->Diferencial->find('all', array('recursive' => -1, 'conditions' => array('Diferencial.id' => $campus['Campus']['diferenciais'], 'Diferencial.status' => true)));
				$campus['Diferenciais'] = $diferenciais;
			}

			//campus
			$this->set(compact('campus'));

			if ($this->request->is('post')) {
				App::import('Model', 'FdSac.Sac');
				$this->Sac = new Sac();
				if(isset($this->request->data['SacCampus'])){
					$this->request->data['SacCampus']['campus_nome'] = $campus['Campus']['nome'];
					$this->request->data['SacCampus']['sac_tipo_id'] = 4;
					if($this->Sac->save($this->request->data['SacCampus'])){
						$this->SendEmails->_enviaSacCampus($this->request->data['SacCampus']);
						$this->request->data['SacCampus'] = array();
						$this->Session->setFlash('Contato realizado com sucesso.', 'default', array('class' => 'success_message'));
					}else{
						$this->Session->setFlash('O contato não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'));
					}
				}
			}

			//pageview
			//begin page_view
				//total
				$ranking['modified'] 	= false;
				$ranking['views'] 		= $campus['Campus']['views']+1;
				//semanal
				if($campus['Campus']['views_week_number'] == date('z')){
					$ranking['views_week'] = $campus['Campus']['views_week']+1;
				}else{
					$ranking['views_week'] = 1;
					$ranking['views_week_number'] = date('z');
				}
				//mensal
				if($campus['Campus']['views_current_month'] == date('m')){
					$ranking['views_mouth'] = $campus['Campus']['views_mouth']+1;
				}else{
					$ranking['views_mouth'] = 1;
					$ranking['views_current_month'] = date('m');
				}
				$this->Campus->id = $campus['Campus']['id'];
				if($campus['Campus']['id'] > 0){
					$this->Campus->save($ranking, false);
				}
			//end page_view
		}

	}

	public function ajax_get_cursos($campus_seo_url){
		App::import('Model', 'FdCursos.Campus');
		$this->Campus = new Campus();

		//curso
		$campus = $this->Campus->find('first', array('contain' => array('Curso'), 'conditions' => array('Campus.status' => true, 'Campus.seo_url' => $campus_seo_url)));

		// debug($campus);die;

		$retorno = array();
		if(count($campus['Curso'])){
			foreach ($campus['Curso'] as $key => $value) {
				$retorno[$value['id']] = $value['nome'];
			}
		}

		die(json_encode($retorno));
	}
}