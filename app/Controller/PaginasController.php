<?php

class PaginasController extends AppController {

	public $components = array('Cookie');

/**
 * detalhe method
 *
 * @return void
 */
	public function detalhe($id = null){

		App::import('Model', 'FdSac.Sac');
		$this->Sac = new Sac();
		if ($this->request->is('post')) {

			if(isset($this->request->data['Sac'])){
				if($this->request->data['Sac']['fd'] == ""){
					$this->request->data['Sac']['url'] = $this->params->url;
					$this->request->data['Sac']['sac_tipo_id'] = 1;
					if($this->Sac->save($this->request->data['Sac'])){
						$this->SendEmails->_enviaSac($this->request->data['Sac']);
						$this->request->data['Sac'] = array();
						$this->Session->setFlash('Contato realizado com sucesso.', 'default', array('class' => 'success_message'));
						$this->set('send_goal_sac', true);
					}else{
						$this->Session->setFlash('O contato não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'));
						$this->set('send_goal_sac', false);
					}
				}else{
					$this->Session->setFlash('O contato não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'));
					$this->set('send_goal_sac', false);
				}
			}

			if(isset($this->request->data['Hovet'])){
				App::import('Model', 'FdLandingPages.LpHovet');
				$this->LpHovet = new LpHovet();
				if($this->request->data['Hovet']['fd'] == ""){
					$this->request->data['Hovet']['url'] = $this->params->url;
					if($this->LpHovet->save($this->request->data['Hovet'])){
						// $this->SendEmails->_enviaHovet($this->request->data['Hovet']);
						$this->request->data['Hovet'] = array();
						$this->Session->setFlash('Cadastro realizado com sucesso.', 'default', array('class' => 'success_message'), 'hovet');
						$this->set('send_goal_sac', true);
					}else{
						$this->Session->setFlash('O contato não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'), 'hovet');
						$this->set('send_goal_sac', false);
					}
				}else{
					$this->Session->setFlash('O cadastro não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'), 'hovet');
					$this->set('send_goal_sac', false);
				}
			}
			
			if(isset($this->request->data['Reingresso'])){
				App::import('Model', 'FdLandingPages.LpReingresso');
				$this->LpReingresso = new LpReingresso();
				if($this->request->data['Reingresso']['fd'] == ""){
					$this->request->data['Reingresso']['url'] = $this->params->url;
					if($this->LpReingresso->save($this->request->data['Reingresso'])){
						$this->SendEmails->_enviaReingresso($this->request->data['Reingresso']);
						$this->request->data['Reingresso'] = array();
						$this->Session->setFlash('Cadastro realizado com sucesso.', 'default', array('class' => 'success_message'), 'reingresso');
						$this->set('send_goal_reingresso', true);
					}else{
						$this->Session->setFlash('O contato não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'), 'reingresso');
						$this->set('send_goal_reingresso', false);
					}
				}else{
					$this->Session->setFlash('O cadastro não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'), 'reingresso');
					$this->set('send_goal_reingresso', false);
				}
			}

			if(isset($this->request->data['LpNegociar'])){
				App::import('Model', 'FdLandingPages.LpNegociar');
				$this->LpNegociar = new LpNegociar();
				if($this->request->data['LpNegociar']['fd'] == ""){
					$this->request->data['LpNegociar']['url'] = $this->params->url;
					if($this->LpNegociar->save($this->request->data['LpNegociar'])){
						$this->SendEmails->_enviaNegociar($this->request->data['LpNegociar']);
						$this->request->data['LpNegociar'] = array();
						$this->Session->setFlash('Cadastro realizado com sucesso.', 'default', array('class' => 'success_message'), 'negociar');
						$this->set('send_goal_negociar', true);
					}else{
						$this->Session->setFlash('O contato não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'), 'negociar');
						$this->set('send_goal_negociar', false);
					}
				}else{
					$this->Session->setFlash('O cadastro não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'), 'negociar');
					$this->set('send_goal_negociar', false);
				}
			}

			if(isset($this->request->data['Wifi'])){
				App::import('Model', 'FdLandingPages.LpWifi');
				$this->LpWifi = new LpWifi();
				if($this->request->data['Wifi']['fd'] == ""){
					$this->request->data['Wifi']['url'] = $this->params->url;
					if($this->LpWifi->save($this->request->data['Wifi'])){
						$this->SendEmails->_enviaWifi($this->request->data['Wifi']);
						$this->request->data['Wifi'] = array();
						$this->Session->setFlash('Cadastro realizado com sucesso.', 'default', array('class' => 'success_message'));
						$this->redirect(array('controller' => 'home', 'action' => 'index'));
						$this->set('send_goal_wifi', true);
					}else{
						$this->Session->setFlash('O contato não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'), 'wifi');
						$this->set('send_goal_wifi', false);
					}
				}else{
					$this->Session->setFlash('O cadastro não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'), 'wifi');
					$this->set('send_goal_wifi', false);
				}
			}

			if(isset($this->request->data['SacFormularioBanner'])){
				App::import('Model', 'FdSac.Sac');
                $this->Sac = new Sac();
	            // die('a');
	            if($this->request->data['SacFormularioBanner']['fd'] == ""){
	                $this->request->data['SacFormularioBanner']['controller'] = $this->params->params['controller'];
	                // $this->request->data['SacFormularioBanner']['url'] = $this->params->url;
	                $this->request->data['SacFormularioBanner']['sac_tipo_id'] = 7;
	                 // debug($this->Sac->save($this->request->data['SacFormularioBanner']));die;
	                if($this->Sac->save($this->request->data['SacFormularioBanner'])){
	                    $this->SendEmails->_enviaSacInterna($this->request->data['SacFormularioBanner']);
	                    $this->request->data['SacFormularioBanner'] = array();
	                    $this->Session->setFlash('Demonstração de interesse realizada com sucesso.', 'default', array('class' => 'success_message'), 'banner-form');
	                    $this->set('send_goal_sac_formulario_banner', true);
	                }else{
	                    // debug($this->Sac->invalidFields());die;
	                    $this->Session->setFlash('A demonstração de interesse não pode ser salva. Tente novamente.', 'default', array('class' => 'error_message'), 'banner-form');
	                    $this->set('send_goal_sac_formulario_banner', false);
	                }
	            }else{
	                $this->Session->setFlash('A demonstração de interesse não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'), 'banner-form');
	                $this->set('send_goal_sac_formulario_banner', false);
	            }
	        }

	        if(isset($this->request->data['Indique'])){
				if($this->request->data['Indique']['fd'] == ""){
					if($this->SendEmails->_enviaIndiqueAmigoPagina($this->request->data['Indique'])){
						$this->Session->setFlash('Indicação enviada com sucesso.', 'default', array('class' => 'success_message'), 'indique');
					}else{
						$this->Session->setFlash('O contato não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'), 'indique');
					}
				}else{
					$this->Session->setFlash('O contato não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'), 'indique');
				}
			}
			
		}

		//load do model afins de pegar o hasMany
		App::import('Model', 'FdPaginas.Pagina');
		$this->Pagina = new Pagina();

		if (!$this->Pagina->exists($id)) {
			throw new NotFoundException(__('Página inválida.'));
		}
			
		$options = array('conditions' => array('Pagina.status' => true, 'Pagina.' . $this->Pagina->primaryKey => $id), 'contain' => array('PaginaTipo', 'Aba' => array('conditions' => array('status' => true), 'order' => 'aba_ordem ASC')));
		$pagina = $this->Pagina->find('first', $options);


		if (empty($pagina)) {
			throw new NotFoundException(__('Página inválida.'));
		}

		$this->set(compact('pagina'));

		//Begin Bloco
			if(isset($pagina['Pagina']['bloco_noticia_id']) && $pagina['Pagina']['bloco_noticia_id'] != ""){
				App::import('Model','FdBlocos.Bloco');
				$this->Bloco = new Bloco();
				$bloco = $this->Bloco->find('first', array('recursive' => -1, 'conditions' => array('Bloco.id' => $pagina['Pagina']['bloco_noticia_id'])));
				$this->set(compact('bloco'));

				App::import('Model','FdNoticias.NoticiaBloco');
				$this->NoticiaBloco = new NoticiaBloco();

				$noticias = $this->NoticiaBloco->find('list', array('recursive' => -1, 'fields' => array('NoticiaBloco.noticia_id'), 'conditions' => array('NoticiaBloco.status' => true, 'NoticiaBloco.bloco_id' => $pagina['Pagina']['bloco_noticia_id'])));
				if(count($noticias) > 0){
					App::import('Model', 'FdNoticias.Noticia');
					$this->Noticia = new Noticia();
					$bloco_noticias = $this->Noticia->find('all', array('recursive' => -1, 'fields' => array('Noticia.id', 'Noticia.titulo', 'Noticia.seo_url'), 'order' => array('Noticia.data_publicacao' => 'DESC'), 'conditions' => array('Noticia.status' => true, 'Noticia.id' => $noticias)));
					$this->set(compact('bloco_noticias'));
				}
			}
		//End Bloco

		$this->loadModel('FdCursos.Campus');
		$campi = $this->Campus->find('all', array('recursive' => -1, 'conditions' => array('Campus.status' => true)));
		$json_campi = array();
		if(count($campi)){
			foreach ($campi as $key => $campus) {
				$json_campi[$key]['Id'] 		= $campus['Campus']['id'];
				$json_campi[$key]['Latitude'] 	= $campus['Campus']['latitude'];
				$json_campi[$key]['Longitude'] 	= $campus['Campus']['longitude'];

				$endereco = "";
				$endereco .= "<strong>";
				$endereco .= $campus['Campus']['endereco'] . ", " . $campus['Campus']['numero'];
				$endereco .= "</strong>";
				$endereco .= "<br />";
				if($campus['Campus']['bairro'] != ""){
					$endereco .= "Bairro " . $campus['Campus']['bairro'];
				}
				$endereco .= " - " . $campus['Campus']['cidade'] . '/' . $campus['Campus']['uf'];
				$endereco .= "<br />";
				if($campus['Campus']['telefone1'] != ""){
					$endereco .= "Telefone: " . $campus['Campus']['telefone1'];
				}
				if($campus['Campus']['telefone2'] != ""){
					$endereco .= " / " . $campus['Campus']['telefone2'];
				}
				$json_campi[$key]['Descricao'] 	= $endereco;
			}
		}

		$json_campi = json_encode($json_campi);

		$this->set(compact('json_campi'));


		//international-office (faco o mesmo select e foreach no \app\Plugin\FdInstituicoes\Controller\FdInstituicoesController por questão de performance)
		$json_instituicoes = array();
		$json_obj = array();
		if($pagina['Pagina']['seo_url'] == 'international-office'){
			App::import('Model', 'FdInstituicoes.Instituicao');
			$this->Instituicao = new Instituicao();
			$instituicoes = $this->Instituicao->find('all', array(
																	'group' => 'Instituicao.endereco', 
																	'joins' => array(
																		array(
																	    	'table' => 'instituicao_cursos',
																	        'alias' => 'InstituicaoCurso',
																	        'type' => 'INNER',
																	        'conditions' => array(
																	            'InstituicaoCurso.instituicao_id = Instituicao.id',
																	        )
																	    )
    																),
																	'conditions' => array(
																			'Instituicao.status' => true, 
																			'Instituicao.latitude <>' => null
																	)
															));
			
			if(count($instituicoes)){
				foreach ($instituicoes as $key => $instituicao) {
					$json_instituicoes[$key]['id'] 			 = $instituicao['Instituicao']['id'];
					$json_instituicoes[$key]['nome'] 	     = $instituicao['Instituicao']['nome'];
					$json_instituicoes[$key]['email'] 	     = $instituicao['Instituicao']['email'];
					$json_instituicoes[$key]['apresentacao'] = $instituicao['Instituicao']['apresentacao'];
					$json_instituicoes[$key]['latitude'] 	 = $instituicao['Instituicao']['latitude'];
					$json_instituicoes[$key]['longitude'] 	 = $instituicao['Instituicao']['longitude'];
					$json_instituicoes[$key]['cidade'] 		 = $instituicao['Instituicao']['cidade'];
					$json_instituicoes[$key]['pais'] 		 = $instituicao['Instituicao']['pais'];
					// $json_instituicoes[$key]['url_do_curso'] = $instituicao['Instituicao']['url_do_curso'];
					$json_instituicoes[$key]['endereco'] 	 = $instituicao['Instituicao']['endereco'];
					$json_instituicoes[$key]['zipcode'] 	 = $instituicao['Instituicao']['zipcode'];
					$json_instituicoes[$key]['url_site'] 	 = $instituicao['Instituicao']['url_site'];
					$json_instituicoes[$key]['logo'] 	 	 = $instituicao['Instituicao']['logo'];
					$json_instituicoes[$key]['logo_dir'] 	 = $instituicao['Instituicao']['logo_dir'];
					$json_instituicoes[$key]['logo_size'] 	 = $instituicao['Instituicao']['logo_size'];
					$json_instituicoes[$key]['logo_type'] 	 = $instituicao['Instituicao']['logo_type'];
					$json_instituicoes[$key]['logo_path'] 	 = $instituicao['Instituicao']['logo_path'];

					if(isset($instituicao['InstituicaoCurso']) && count($instituicao['InstituicaoCurso']) > 0){
						foreach ($instituicao['InstituicaoCurso'] as $key => $curso) {
							$json_obj[trim($curso['nome_curso_br'])] = array(trim($curso['nome_curso_br']),$instituicao['Instituicao']['nome']);
							$json_obj[trim($curso['nome_curso_es'])] = array(trim($curso['nome_curso_es']),$instituicao['Instituicao']['nome']);
							$json_obj[trim($curso['nome_curso_en'])] = array(trim($curso['nome_curso_en']),$instituicao['Instituicao']['nome']);
						}
					}
				}
				$json_instituicoes = json_encode($json_instituicoes);
				ksort($json_obj);
				$json_obj = json_encode(array_values($json_obj));
			}
		}
		$this->set(compact('json_instituicoes', 'json_obj'));


		if($pagina['Pagina']['seo_url'] == 'escola-de-verao'){
			App::import('Model', 'FdEscolaVerao.EscolaVeraoArea');
			$this->EscolaVeraoArea = new EscolaVeraoArea();
			$escola_verao_areas = $this->EscolaVeraoArea->find('all', array('conditions' => array('EscolaVeraoArea.status' => true)));
			// debug($escola_verao_areas);die;
			$this->set(compact('escola_verao_areas'));
		}

		if($pagina['Pagina']['seo_url'] == 'wifi' || $pagina['Pagina']['seo_url'] == 'negociar'){
			$this->layout = 'wifi';
		}

		// $pagina['Pagina']['layout_novo'] = true;
		// if(isset($pagina['Pagina']['layout_novo']) && $pagina['Pagina']['layout_novo'] == true){
		//  	$this->render('FdGraduacaoCursos.Paginas/detalhe');
		// }
	}
}