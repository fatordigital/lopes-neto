<?php

class SacController extends AppController {

	public $components = array('FdEmails.SendEmails');

/**
 * index method
 *
 * @return void
 */
	public function add(){

		App::import('Model', 'FdSac.Sac');
		$this->Sac = new Sac();
		if ($this->request->is('post')) {

			if($this->request->data['Sac']['fd'] == ""){
				if(isset($this->request->data['Sac'])){					
					$this->request->data['Sac']['sac_tipo_id'] = 1;
					if($this->Sac->save($this->request->data['Sac'])){
						$this->SendEmails->_enviaSac($this->request->data['Sac']);
						$this->request->data['Sac'] = array();
						$this->Session->setFlash('Contato realizado com sucesso.', 'default', array('class' => 'success_message'));
						$this->set('send_goal_sac', true);
					}else{
						$this->Session->setFlash('O contato não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'));
						$this->set('send_goal_sac', false);
					}
				}
			}else{
				$this->Session->setFlash('O contato não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'));
				$this->set('send_goal_sac', false);
			}

		}

		$this->loadModel('FdCursos.Campus');
		$campi = $this->Campus->find('all', array('recursive' => -1, 'conditions' => array('Campus.status' => true)));
		$json_campi = array();
		if(count($campi)){
			foreach ($campi as $key => $campus) {
				$json_campi[$key]['Id'] 		= $campus['Campus']['id'];
				$json_campi[$key]['Latitude'] 	= $campus['Campus']['latitude'];
				$json_campi[$key]['Longitude'] 	= $campus['Campus']['longitude'];

				$endereco = "";
				$endereco .= "<strong>";
				$endereco .= $campus['Campus']['endereco'] . ", " . $campus['Campus']['numero'];
				$endereco .= "</strong>";
				$endereco .= "<br />";
				if($campus['Campus']['bairro'] != ""){
					$endereco .= "Bairro " . $campus['Campus']['bairro'];
				}
				$endereco .= " - " . $campus['Campus']['cidade'] . '/' . $campus['Campus']['uf'];
				$endereco .= "<br />";
				if($campus['Campus']['telefone1'] != ""){
					$endereco .= "Telefone: " . $campus['Campus']['telefone1'];
				}
				if($campus['Campus']['telefone2'] != ""){
					$endereco .= " / " . $campus['Campus']['telefone2'];
				}
				$json_campi[$key]['Descricao'] 	= $endereco;
			}
		}

		$json_campi = json_encode($json_campi);

		$this->set(compact('json_campi'));
	}
	
	
	public function funcionarios(){
		
		App::import('Model', 'FdSac.LpFuncionario');
		$this->LpFuncionario = new LpFuncionario();
		$this->layout = 'funcionarios';

		if ($this->request->is('post')) {
			if($this->request->data['LpFuncionario']['fd'] == ""){
				if(isset($this->request->data['LpFuncionario'])){
					if($this->LpFuncionario->save($this->request->data['LpFuncionario'])){
	//					$this->SendEmails->_enviaSac($this->request->data['Sac']);
						$this->request->data['LpFuncionario'] = array();
						$this->Session->setFlash('Indicação realizada com sucesso.', 'default', array('class' => 'success_message'));
						$this->set('send_goal_sac', true);
					}else{
						$this->Session->setFlash('A indicação não pode ser salva. Tente novamente.', 'default', array('class' => 'error_message'));
						$this->set('send_goal_sac', false);
					}
				}
			}else{
				$this->Session->setFlash('A indicação não pode ser salva. Tente novamente.', 'default', array('class' => 'error_message'));
				$this->set('send_goal_sac', false);
			}
		}/*
		Configure::write('debug', 2);
		CakeLog::write('post', var_export($this->request, true));

		App::import('Model', 'FdSac.LpFuncionario');
		$this->LpFuncionario = new LpFuncionario();

		$data['LpFuncionario'] = $this->request->data;
		$data['LpFuncionario']['id'] = null; 

		$data['LpFuncionario']['nome_funcionario'] 		= utf8_encode($data['LpFuncionario']['nome_funcionario']);
		$data['LpFuncionario']['nome_primeiro'] 		= utf8_encode($data['LpFuncionario']['nome_primeiro']);
		$data['LpFuncionario']['curso_ies_primeiro'] 	= utf8_encode($data['LpFuncionario']['curso_ies_primeiro']);
		$data['LpFuncionario']['nome_segundo'] 			= utf8_encode($data['LpFuncionario']['nome_segundo']);
		$data['LpFuncionario']['telefone_segundo'] 		= utf8_encode($data['LpFuncionario']['telefone_segundo']);
		$data['LpFuncionario']['curso_ies_segundo'] 	= utf8_encode($data['LpFuncionario']['curso_ies_segundo']);
		$data['LpFuncionario']['nome_terceiro'] 		= utf8_encode($data['LpFuncionario']['nome_terceiro']);
		$data['LpFuncionario']['curso_ies_terceiro'] 	= utf8_encode($data['LpFuncionario']['curso_ies_terceiro']);

		CakeLog::write('post', var_export($data, true));

		if(!$this->LpFuncionario->save($data['LpFuncionario'], false)){
			CakeLog::write('post', var_export($this->LpFuncionario->invalidFields(), true));
		}else{
			CakeLog::write('post', 'salvo: ' . $this->LpFuncionario->id);
		}

		CakeLog::write('post', '== end lead ==');*/
	}

	public function pos(){
		
		App::import('Model', 'FdSac.LpPos');
		$this->LpPos = new LpPos();
		$this->layout = 'pos';

		if ($this->request->is('post')) {
			if($this->request->data['LpPos']['fd'] == ""){
				if(isset($this->request->data['LpPos'])){
					if($this->LpPos->save($this->request->data['LpPos'])){
						$this->SendEmails->_enviaPos($this->request->data['LpPos']);
						$this->request->data['LpPos'] = array();
						$this->Session->setFlash('Registro salvo com sucesso.', 'default', array('class' => 'success_message'));
						$this->set('send_goal_sac', true);
					}else{
						$this->Session->setFlash('O registro não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'));
						$this->set('send_goal_sac', false);
					}
				}
			}else{
				$this->Session->setFlash('O registro não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'));
				$this->set('send_goal_sac', false);
			}
		}
	}

	public function fone(){
		App::import('Model', 'FdLandingPages.LpFone');
		$this->LpFone = new LpFone();
		$this->layout = 'fone';

		if ($this->request->is('post')) {
			if($this->request->data['LpFone']['fd'] == ""){
				if(isset($this->request->data['LpFone'])){

					if(isset($this->params->query['name'])){
						$this->request->data['LpFone']['nome'] = $this->params->query['name'];
					}
					if(isset($this->params->query['email'])){
						$this->request->data['LpFone']['email'] = $this->params->query['email'];
					}

					if($this->LpFone->save($this->request->data['LpFone'])){
						// $this->SendEmails->_enviaPos($this->request->data['LpFone']);
						$this->request->data['LpFone'] = array();
						$this->Session->setFlash('Registro salvo com sucesso.', 'default', array('class' => 'success_message'));
						$this->set('send_goal_sac', true);
					}else{
						$this->Session->setFlash('O registro não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'));
						$this->set('send_goal_sac', false);
					}
				}
			}else{
				$this->Session->setFlash('O registro não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'));
				$this->set('send_goal_sac', false);
			}
		}
	}

	public function set_data_download_apresentacao(){
		if(is_array($this->params->data) && count($this->params->data) > 0){

			//Begin: sac
				App::import('Model', 'FdSac.Sac');
				$this->Sac = new Sac();
				$data['Sac']['id'] 			= null;
				$data['Sac']['sac_tipo_id'] = 10;
				$data['Sac']['nome'] 		= $this->params->data['DownloadApresentacao']['nome'];
				$data['Sac']['email'] 		= $this->params->data['DownloadApresentacao']['email'];
				$data['Sac']['telefone'] 	= $this->params->data['DownloadApresentacao']['telefone'];
				$data['Sac']['curso'] 		= $this->params->data['DownloadApresentacao']['curso'];
				$data['Sac']['url'] 		= $this->params->data['DownloadApresentacao']['url'];

				$this->Sac->save($data, false);
			//End: sac

			//RdStation
			$this->RdStation->addLeadConversionToRdstationCrm('form_download_apresentacao', array(
																'email' 		 		=> $this->params->data['DownloadApresentacao']['email'], 
																'nome'			 		=> $this->params->data['DownloadApresentacao']['nome'], 
																'telefone'		 		=> $this->params->data['DownloadApresentacao']['telefone'], 
																'curso_de_interesse'	=> $this->params->data['DownloadApresentacao']['curso'], 
																'url' 					=> $this->params->data['DownloadApresentacao']['url'], 
															));
			// $this->RdStation->funelRdstationCrm($this->params->data['DownloadApresentacao']['email'], '1', true);
			die('1');
		}else{
			die('0');
		}
	}

	public function set_data_download_guia(){
		if(is_array($this->params->data) && count($this->params->data) > 0){
			

			//Begin: sac
				App::import('Model', 'FdSac.Sac');
				$this->Sac = new Sac();
				$data['Sac']['id'] 			= null;
				$data['Sac']['sac_tipo_id'] = 11;
				$data['Sac']['nome'] 		= $this->params->data['DownloadGuia']['nome'];
				$data['Sac']['email'] 		= $this->params->data['DownloadGuia']['email'];
				$data['Sac']['telefone'] 	= $this->params->data['DownloadGuia']['telefone'];
				$data['Sac']['url'] 		= $this->params->data['DownloadGuia']['url'];

				$this->Sac->save($data, false);
			//End: sac

			//RdStation
			$this->RdStation->addLeadConversionToRdstationCrm('form_download_guia', array(
																'email' 		 		=> $this->params->data['DownloadGuia']['email'], 
																'nome'			 		=> $this->params->data['DownloadGuia']['nome'], 
																'telefone'		 		=> $this->params->data['DownloadGuia']['telefone'], 
																'url' 					=> $this->params->data['DownloadGuia']['url'], 
															));
			// $this->RdStation->funelRdstationCrm($this->params->data['DownloadGuia']['email'], '1', true);
			die('1');
		}else{
			die('0');
		}
	}
}