<?php

class BuscasController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index(){

		App::import('Model', 'FdRotas.Rotas');
		$this->Rota = new Rota();
		
		//não veio paremetro?
		if(!isset($this->params['named']['item'])){
			$params_named['item'] = '';
		}else{
			$params_named['item'] = $this->params->params['named']['item'];
		}
		
		$options['recursive']	= -1;
		$options['limit']		= 10;
		$options['order']		= array('rating' => 'DESC', 'Rota.buscavel_ordem' => 'ASC', 'Rota.data' => 'DESC');
		$options['fields']		= array('Rota.nome', 'Rota.seo_url', 'Rota.model', 'Rota.controller', 'Rota.data', 'Rota.conteudo', 'Rota.seo_description', "MATCH(Rota.nome,Rota.conteudo,Rota.seo_keywords) AGAINST('" . addslashes($params_named['item']) . "') as rating");	
		$options['conditions']	= array('AND' => array(
													'Rota.model <>' => 'Curso',
													'Rota.buscavel' => true,
													'Rota.status' => true,
													"MATCH(Rota.nome,Rota.conteudo,Rota.seo_keywords) AGAINST('" . addslashes($params_named['item']) . "')"
												));
		$this->paginate = $options;

		$totais = array();
		$rotas = array();

		//se não for busca do tipo curso (url busca-curso), traço as rotas tbm, como noticias, paginas e afins
		if(!isset($this->params->params['curso']) || $this->params->params['curso'] == false){
			$rotas = $this->paginate('Rota');

			$counts = $this->Rota->find('list', array('fields' => array('Rota.id', 'Rota.model'), 'conditions' => array($options['conditions'])));
			foreach ($counts as $key => $count) {
				$totais[$count][] = $key;
			}
		}

		//primeiro select simplificado (mais direto)		
		$cursos = $this->Rota->find('all', array(
													'recursive' => -1, 
													'fields' => array('Rota.nome', 'Rota.seo_url', 'Rota.model', 'Rota.controller', 'Rota.data', 'Rota.conteudo', 'Rota.seo_description', "MATCH(Rota.nome) AGAINST('" . addslashes($params_named['item']) . "') as rating"), 
													'order' => $options['order'], 
													'conditions' => array('AND' => array(
																						'Rota.model' => 'Curso',
																						'Rota.buscavel' => true,
																						'Rota.status' => true,
																						'AND' => array(
																								'OR' => array(
																										"MATCH(Rota.nome) AGAINST('" . addslashes($params_named['item']) . "')",
																										"Rota.nome LIKE" => "%". addslashes($params_named['item']) . "%"
																									)
																							)
																					))
											));

		//não achou nada? então doulhe uma mais geral (inativo)
		// if(count($cursos) == 0){
		// 	$cursos = $this->Rota->find('all', array(
		// 											'recursive' => -1, 
		// 											'fields' => $options['fields'], 
		// 											'order' => $options['order'], 
		// 											'conditions' => array('AND' => array(
		// 																				'Rota.model' => 'Curso',
		// 																				'Rota.buscavel' => true, 
		// 																				'AND' => array(
		// 																						'OR' => array(
		// 																								"MATCH(Rota.nome,Rota.conteudo,Rota.seo_keywords) AGAINST('" . addslashes($params_named['item']) . "')",
		// 																								"Rota.nome LIKE" => "%". addslashes($params_named['item']) . "%"
		// 																							)
		// 																					)
		// 																			))
		// 									));
		// }

		// é busca por curso e só tem um curso? mando direto pra ele
		if((isset($this->params->params['curso']) && $this->params->params['curso'] == true) && count($cursos) == 1){
			$this->redirect('/' . $cursos[0]['Rota']['seo_url']);
		}
		
		$this->set(compact('rotas', 'totais', 'cursos'));


		$this->_addBreadcrumb('Resultado de busca por "'. $params_named['item'] .'"');
	}
}