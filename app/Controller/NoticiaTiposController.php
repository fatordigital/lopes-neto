<?php

class NoticiaTiposController extends AppController {

	public $components = array('Cookie');
	
	public function index($date = null){
		
		//load do model afins de pegar o hasMany
		App::import('Model', 'FdNoticias.Noticia');
		$this->Noticia = new Noticia();

		App::import('Model', 'FdNoticias.NoticiaTipo');
		$this->NoticiaTipo = new NoticiaTipo();

		//existe?
		if (!$this->NoticiaTipo->exists($this->params->params['pass'][0])) {
			throw new NotFoundException(__('NoticiaTipo inválida.'));
		}

		//find tipo
		$noticia_tipo = $this->NoticiaTipo->find('first', array('recursive' => -1, 'conditions' => array('NoticiaTipo.id' => $this->params->params['pass'][0])));
		$this->set(compact('noticia_tipo'));

		$date = (isset($this->params->params['pass'][2])) ? $this->params->params['pass'][2] : null;

		//find noticias
		//conditions
		if(is_null($date) || strlen($date) < 5){
			$hoje = date('Y-m-d');
			$options['conditions'] = array('Noticia.status' => true, 'Noticia.noticia_tipo_id' => $this->params->params['pass'][0]);
			
		}else{
			$hoje = date('Y-m-d', strtotime($date));
			$options['conditions'] = array('Noticia.status' => true, 'Noticia.noticia_tipo_id' => $this->params->params['pass'][0], 'Noticia.data_publicacao' => $hoje);
		}

		$this->Noticia->recursive = -1;
		$options['order'] = 'Noticia.id DESC';
		$this->paginate = $options;

		//paginate noticias
		$this->set('noticias', $this->paginate('Noticia'));

		//set dia de hoje (para o calendário)
		$this->set(compact('hoje'));

		//eu sei, é feio, mas foi preciso.
		if($noticia_tipo['NoticiaTipo']['id'] == $this->NoticiaTipo->inquietos){
			$this->render('inquietos');
		}

		//_addBreadcrumb
		$this->_addBreadcrumb('Notícias', array('controller' => 'noticias', 'action' => 'index'));
		$this->_addBreadcrumb($noticia_tipo['NoticiaTipo']['nome']);
	}
}