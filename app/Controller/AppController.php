<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package        app.Controller
 * @link        http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    public $components = array(
        'Acl',
        'Auth',
        'Idioma',
        'Session',
        'Flash',
        'Paginator',
        'ImportDb',
        'Reports',
        'Cookie',
        'FilterResults.FilterResults' => array(
            'auto' => array(
                'paginate' => false,
                'explode' => true,
            ),
            'explode' => array(
                'character' => ' ',
                'concatenate' => 'AND',
            ),
        ),
        'FdMenus.Menuu',
        'FdEmails.SendEmails',
        'RdStation'
    );

    public $helpers = array(
        'Session',
        'Time',
        'String',
        'Idioma',
        'Bloco',
        'FilterResults.FilterForm' => array(
            'operators' => array(
                'LIKE' => 'containing',
                'NOT LIKE' => 'not containing',
                'LIKE BEGIN' => 'starting with',
                'LIKE END' => 'ending with',
                '=' => 'equal to',
                '!=' => 'different',
                '>' => 'greater than',
                '>=' => 'greater or equal to',
                '<' => 'less than',
                '<=' => 'less or equal to'
            )
        ),
        'Html' => array('className' => 'MyHtml'),
        'FdMenus.Menus',
    );

    public function __construct(CakeRequest $cakeRequest, CakeResponse $cakeResponse)
    {
        // Load infos settings
        $this->loadModel('FdSettings.Setting');
        $this->Setting->loadSettings();
        
        parent::__construct($cakeRequest, $cakeResponse);
    }

    protected function _configureAcl()
    {

        //Configure AuthComponent
        $this->Auth->authenticate = array(
            AuthComponent::ALL => array(
                'userModel' => 'FdUsuarios.Usuario',
                'fields' => array('username' => 'email', 'password' => 'senha'),
                'scope' => array('Usuario.status' => true)
            ),
            'Form'
        );

        $this->Auth->authorize = array(
            'Actions' => array(
                'actionPath' => 'controllers',
                'userModel' => 'FdUsuarios.Usuario',
            ),
        );

        //Definicoes do methods de autenticação
        if ($this->_isAdminMode()) {
            $this->Auth->autoRedirect = true;
            $this->Auth->loginAction = array('fatorcms' => true, 'plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'login');
            $this->Auth->loginRedirect = array('fatorcms' => true, 'plugin' => 'fd_dashboard', 'controller' => 'fd_dashboard', 'action' => 'index');
            $this->Auth->logoutRedirect = array('fatorcms' => true, 'plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'login', 'prefix' => 'fatorcms');
//            $this->Auth->authError = 'Efetue login com permissão nessa sessão para prosseguir.';
            $this->Auth->authError = '';
        } else {
            $this->Auth->autoRedirect = false;
            $this->Auth->loginAction = array('fatorcms' => true, 'plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'login');
            $this->Auth->loginRedirect = array('fatorcms' => false, 'plugin' => 'fd_usuarios', 'controller' => 'home', 'action' => 'index');
            $this->Auth->logoutRedirect = array('fatorcms' => false, 'plugin' => 'fd_usuarios', 'controller' => 'home', 'action' => 'index');
//            $this->Auth->authError = 'Faça login para prosseguir.';
            $this->Auth->authError = '';
        }

        //Begin permissons
        if ($this->Auth->user() && $this->Auth->user('grupo_id') == 1) {
            // Role: Admin
            $this->Auth->allowedActions = array('*');
        } else {
            if ($this->Auth->user()) {
                $roleId = $this->Auth->user('grupo_id');
            } else {
                $roleId = 4; // Role: Public
            }

            $thisControllerNode = $this->Acl->Aco->node($this->Auth->actionPath . $this->name);

            //debug($thisControllerNode);die;

            if ($thisControllerNode) {
                $thisControllerNode = $thisControllerNode['0'];

                $thisControllerActions = $this->Acl->Aco->find('list', array(
                    'conditions' => array(
                        'Aco.parent_id' => $thisControllerNode['Aco']['id'],
                    ),
                    'fields' => array(
                        'Aco.id',
                        'Aco.alias',
                    ),
                    'recursive' => '-1',
                ));

                $thisControllerActionsIds = array_keys($thisControllerActions);

                $allowedActions = $this->Acl->Aco->Permission->find('list', array(
                    'conditions' => array(
                        'Permission.aro_id' => $roleId,
                        'Permission.aco_id' => $thisControllerActionsIds,
                        'Permission._create' => 1,
                        'Permission._read' => 1,
                        'Permission._update' => 1,
                        'Permission._delete' => 1,
                    ),
                    'fields' => array(
                        'id',
                        'aco_id',
                    ),
                    'recursive' => '-1',
                ));
                $allowedActionsIds = array_values($allowedActions);
            }

            $allow = array();
            if (isset($allowedActionsIds) &&
                is_array($allowedActionsIds) &&
                count($allowedActionsIds) > 0
            ) {
                foreach ($allowedActionsIds AS $i => $aId) {
                    $allow[] = $thisControllerActions[$aId];
                }
            }

            $this->Auth->allowedActions = $allow;
        }
        //End permissions

        // Allow all actions. CakePHP 2.0
//        $this->Auth->allow('*');
        // Allow all actions. CakePHP 2.1
//        $this->Auth->allow();
    }


    /**
     * Migalha
     *
     * @var array
     * @access protected
     */
    protected $_breadcrumb = array();

    /**
     * Adição de migalha
     *
     *
     * @param string $nome Nome da migalha
     * @param mixed $url Url de acesso (opcional). Deve seguir o padrão do cake para url
     * @access protected
     * @return AppController Próprio Objeto para encadeamento
     */
    protected function _addBreadcrumb($nome, $url = null)
    {
        if (empty($url)) {
            $url = 'javascript:void(0);';
        }
        $this->_breadcrumb[] = array('nome' => $nome, 'url' => $url);
        return $this;
    }

    /*
     * Verifico que o usuario está no admin
     *
     * @return bollean Resultado da operação
     */
    protected function _isAdminMode()
    {
        $adminRoute = Configure::read('Routing.prefixes');
        if (isset($this->params['prefix']) && in_array($this->params['prefix'], $adminRoute)) {
            return true;
        }
        return false;
    }

    /**
     * Mêtodo setUrlReferer
     *
     * @return Array
     */
    protected function _setUrlReferer()
    {
        $referer_tmp = $this->Session->read('referer');
        if ($referer_tmp == "" || $referer_tmp === FALSE) {
            $this->Session->write('referer', $this->referer());
        }
    }

    /**
     * Mêtodo _resetCaches
     *
     * @return Array
     */
    protected function _resetCaches()
    {
        Cache::clearGroup('site');
    }

    /**
     * Mêtodo _saveStatus
     *
     * @return Array
     */
    protected function _saveStatus($model, $id, $status)
    {
        $this->{$model}->id = $id;
        $this->{$model}->saveField('status', $status);
        if ($status == 1) {
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * Mêtodo _redirectFilter
     * responsavel por gerenciar os redirects dos filters
     * @return Array
     */
    protected function _redirectFilter($referer = null)
    {
        if ($referer == null) {
            $referer = $this->referer();
        }

        //custom do redirect para o FilterResults
        if ($referer && stripos($referer, '/fatorcms/')) {
            $this->redirect($referer);
        } else {
            $this->redirect(array('action' => 'index'));
        }
    }

    /**
     * Carrega os Banners
     * @return Array
     */
    private function _carregaBanners()
    {
//        App::import('Model', 'FdBanners.Banner');
//        $modelBanner = new Banner();
//        if (Cache::read('banners') === false) {
//            $banners = $modelBanner->find('all', array(
//                    'recursive' => 1,
//                    'conditions' => array('Banner.status' => true, 'Banner.sites LIKE "%' . Configure::read('site') . '%"'),
//                    'order' => 'coalesce(Banner.ordem,10000) ASC'
//                )
//            );
//            Cache::write('banners', $banners);
//        } else {
//            $banners = Cache::read('banners');
//        }

        //percorro o array, e mantenho só os que tiverem no intervalo de hoje
        // $data_agora = strtotime(date('Y-m-d H:i:s'));
        // foreach ($banners as $k => $banner) {
        //     $data_inicial = strtotime($banner['Banner']['data_inicio']);
        //     $data_fim = strtotime($banner['Banner']['data_fim']);
        //     if ($data_inicial != null || $data_fim != null) {
        //         if (($data_inicial <= $data_agora) || ($data_fim >= $data_agora)) {
        //             unset($banners[$k]);
        //         }
        //     }
        // }

        //banners_topo
//        if (count($banners) > 0) {
//            $this->set('banners_topo', $modelBanner->extraiBanners($banners, "site_topo"));
//            $this->set('banners_paginas', $modelBanner->extraiBanners($banners, "site_pagina"));
//            $this->set('banners_noticias', $modelBanner->extraiBanners($banners, "site_noticia"));
//            $this->set('banners_cursos', $modelBanner->extraiBanners($banners, "site_curso"));
//            $this->set('banners_sidebar', $modelBanner->extraiBanners($banners, "site_sidebar"));
//            $this->set('banners_floating', $modelBanner->extraiBanners($banners, "site_floating"));
//            $this->set('banners_curso_tipo', $modelBanner->extraiBanners($banners, "site_tipo_de_curso"));
//        }
    }

    private function _getEventos()
    {
        App::import('Model', 'FdEventos.Evento');
        $this->Evento = new Evento();
        $eventos_hoje = $this->Evento->find('all', array('recursive' => -1, 'fields' => array('Evento.id', 'Evento.nome', 'Evento.conteudo', 'Evento.data_inicio', 'Evento.data_fim', 'Evento.seo_url', 'Evento.noticia_seo_url'), 'conditions' => array('Evento.status' => true, 'OR' => array(array('Evento.data_inicio =' => date('Y-m-d')), array('Evento.data_inicio <' => date('Y-m-d'), 'Evento.data_fim >=' => date('Y-m-d'))))));
        $eventos_proximos = $this->Evento->find('all', array('recursive' => -1, 'order' => 'data_inicio ASC', 'fields' => array('Evento.id', 'Evento.nome', 'Evento.conteudo', 'Evento.data_inicio', 'Evento.data_fim', 'Evento.seo_url', 'Evento.noticia_seo_url'), 'conditions' => array('Evento.status' => true, 'Evento.data_inicio >' => date('Y-m-d'), 'Evento.evento_tipo' => 'evento'), 'limit' => 10));

        if (isset($this->params->params['tipo_calendario'])) {
            $conditions = array('Evento.status' => true, 'Evento.evento_tipo' => $this->params->params['tipo_calendario']);
        } else {
            $conditions = array('Evento.status' => true);
        }

        $all_eventos = $this->Evento->find('all', array('recursive' => -1, 'fields' => array('Evento.id', 'Evento.data_inicio', 'Evento.data_fim', 'Evento.evento_tipo', 'Evento.noticia_seo_url'), 'conditions' => $conditions));

        $eventos = array();
        foreach ($all_eventos as $key => $value) {
            if ($value['Evento']['data_fim'] != null) {
                $data_inicio = date('Y-n-j', strtotime($value['Evento']['data_inicio']));
                $eventos[$value['Evento']['evento_tipo']][$data_inicio] = $data_inicio;

                while ($value['Evento']['data_inicio'] < $value['Evento']['data_fim']) {
                    $value['Evento']['data_inicio'] = date('Y-m-d', strtotime($value['Evento']['data_inicio'] . ' + 1 day'));
                    $data_inicio = date('Y-n-j', strtotime($value['Evento']['data_inicio']));

                    $eventos[$value['Evento']['evento_tipo']][$data_inicio] = $data_inicio;
                }
            } else {
                $data_inicio = date('Y-n-j', strtotime($value['Evento']['data_inicio']));
                $eventos[$value['Evento']['evento_tipo']][$data_inicio] = $data_inicio;
            }
        }

        //oders
        foreach ($eventos as $key => $value) {
            ksort($eventos[$key]);
        }

        //set
        if (isset($eventos['evento'])) {
            $this->set('datas_eventos_eventos', $eventos['evento']);
        } else {
            $this->set('datas_eventos_eventos', array());
        }

        if (isset($eventos['academico'])) {
            $this->set('datas_eventos_academico', $eventos['academico']);
        } else {
            $this->set('datas_eventos_academico', array());
        }

        // debug($eventos);die;

        $this->set(compact('eventos_proximos', 'eventos_hoje'));
    }

    // sobrescrita de gancho para antes da execução das filtragens
    public function beforeFilter()
    {

//        debug($this->params);die;
        if (!session_id()) {
            session_start();
        }


        // $this->Session->destroy();
        // Configue ACL
        $this->_configureAcl();

        // Set user logado
        $this->set('auth', $this->Auth->user());



        if (!$this->request->is('ajax')) {

            //altero o layout se o usuário estiver no admin
            if ($this->_isAdminMode()) {
                //layout
                $this->layout = 'FdDashboard.fatorcms';
                //menu
                $this->set('menus_for_layout', $this->Menuu->carregaMenu(array('admin')));

                if (isset($this->params->query['reset']) && $this->params->query['reset'] == 'ok') {
                    $this->Session->setFlash(__('Dados atualizados com sucesso.'), 'fatorcms_success');
                    $this->redirect('/' . $this->params->url);
                }
            } else {


                //layout
                $this->layout = 'default';
                //menu
                $menus_for_layout = $this->Menuu->carregaMenu(array('site_institucional', 'site_area_do_aluno', 'site_footer'));

                $this->set('menus_for_layout', $menus_for_layout);

                // Lista os banners
                $this->_carregaBanners();

                if ((isset($_GET['noticia']) && $_GET['noticia'] != "") || (isset($this->params->query['noticia']) && $this->params->query['noticia'] != "")) {
                    throw new NotFoundException(__('Notícia inválida.'));
                }

                if ($this->request->is('post')) {
                    App::import('Model', 'FdSac.Sac');
                    $this->Sac = new Sac();

                    if (isset($this->request->data['SacInterna'])) {
                        // die('a');
                        if ($this->request->data['SacInterna']['fd'] == "") {
                            $this->request->data['SacInterna']['controller'] = $this->params->params['controller'];
                            $this->request->data['SacInterna']['url'] = $this->params->url;
                            $this->request->data['SacInterna']['sac_tipo_id'] = 2;
                            if ($this->Sac->save($this->request->data['SacInterna'])) {
                                $this->SendEmails->_enviaSacInterna($this->request->data['SacInterna']);
                                $this->request->data['SacInterna'] = array();
                                $this->Session->setFlash('Contato realizado com sucesso.', 'default', array('class' => 'success_message'));
                                $this->set('send_goal_sac_interna', true);
                            } else {
                                $this->Session->setFlash('O contato não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'));
                                $this->set('send_goal_sac_interna', false);
                            }
                        } else {
                            $this->Session->setFlash('O contato não pode ser salvo. Tente novamente.', 'default', array('class' => 'error_message'));
                            $this->set('send_goal_sac_interna', false);
                        }

                    }

                }
            }
        }

        if (isset($_GET['clear'])) {
            Cache::clear();
            $this->redirect('/');
        }
    }

    // sobrescrita de gancho para antes da renderização da view
    public function beforeRender()
    {
//        debug($this->params);die;
        // if($_SERVER['SERVER_NAME'] != "localhost" && $_SERVER['REMOTE_ADDR'] != "::1" && $_SERVER['SERVER_ADDR'] != "127.0.0.1" && $_SERVER['SERVER_ADDR'] != "192.168.0.221"){
        //     $url = env('SERVER_NAME') . env('REQUEST_URI');
        //     if (!preg_match('/www/', $url)) {
        //         $url = 'http://www.' . $url;
        //         $this->redirect($url, 301);
        //     }
        // }

        // envia a migalha para a visualização
        $this->set('breadcrumbs', $this->_breadcrumb);
    }


}