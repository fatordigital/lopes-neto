<?php

App::uses('Controller', 'Controller');

class EventosController extends AppController
{

    public $components = array('FdEmails.SendEmails');

    /**
     * index method
     *
     * @return void
     */
    public function index($date = null)
    {
        if ($this->request->is('post')) {
            App::import('Model', 'FdEventos.EventoPresenca');
            $this->EventoPresenca = new EventoPresenca();

            $status = $this->EventoPresenca->custom_save($this->request->data);
            if ($status == true) {
                $this->SendEmails->presenca($this->request->data['EventoPresenca']);
                $this->Flash->success('Sua inscrição foi efetuada com sucesso.', array('key' => 'success'));
                $this->Flash->goal('/goal/contato', array('key' => 'goal'));
            } else if ($status == false) {
                $this->Flash->error('Não foi possível realizar a inscrição, tente novamete.');
            } else if ($status == null) {
                $this->Flash->success('Você já efetuou sua inscrição.', array('key' => 'success'));
            }
            $this->redirect($this->referer());
        }

        //load do model afins de pegar o hasMany
        App::import('Model', 'FdEventos.Evento');
        $this->Evento = new Evento();

        //conditions
        if (is_null($date)) {
            $hoje = date('Y-m-d');
            // $options['conditions'] = array('Evento.status' => true, 'Evento.data_inicio >=' => $hoje);

        } else {
            $hoje = date('Y-m-d', strtotime($date));
            // $options['conditions'] = array('Evento.status' => true, 'OR' => array(array('Evento.data_inicio >=' => $hoje), array('Evento.data_fim <=' => $hoje)));
        }

        if (isset($this->params->params['tipo_calendario'])) {
            $options['conditions']['Evento.evento_tipo'] = $this->params->params['tipo_calendario'];
        }

        $options['conditions'] = array(
            'Evento.status' => true,
            // 'Evento.data_inicio' => $hoje,
            'OR' => array(
                array(
                    array('Evento.data_inicio <=' => $hoje),
                    array('Evento.data_fim >=' => $hoje)
                ),
                array(
                    array('Evento.data_inicio' => $hoje),
                    array('Evento.data_fim' => null)
                ),
                array(
                    array('Evento.data_inicio' => null)
                )

            )
        );

        $options['order'] = 'data_inicio ASC';
        $this->paginate = $options;
        $this->set('eventos', $this->paginate());

        $this->set(compact('hoje'));


        $meus_eventos = CakeSession::read('meus_eventos') ? CakeSession::read('meus_eventos') : array();
        $this->set(compact('meus_eventos'));

    }

    /**
     * detalhe method
     *
     * @return void
     */
    public function detalhe($id = null)
    {

        //load do model afins de pegar o hasMany
        App::import('Model', 'FdEventos.Evento');
        $this->Evento = new Evento();

        if (!$this->Evento->exists($id)) {
            throw new NotFoundException(__('Página inválida.'));
        }

        $options = array('conditions' => array('Evento.status' => true, 'Evento.' . $this->Evento->primaryKey => $id));
        $evento = $this->Evento->find('first', $options);
        $this->set(compact('evento'));

        // $this->set('hoje', date('Y-m-d', strtotime($evento['Evento']['data'])));
        $this->set('hoje', $evento['Evento']['data_inicio']);

        //begin: downloads
        App::import('Model', 'FdDownloads.Download');
        $this->Download = new Download();
        $downloads = $this->Download->find('all', array('conditions' => array('DownloadTipo.codigo' => 'calendario-academico', 'Download.status' => true)));
        $this->set(compact('downloads'));
        //end: downloads
    }
}