<?php

class EventoTagsController extends AppController {

	public $components = array('Cookie');
	
/**
 * index method
 *
 * @return void
 */
	public function index($date = null){
		//load do model afins de pegar o hasMany
		App::import('Model', 'FdEventos.Evento');
		$this->Evento = new Evento();
		
		App::import('Model', 'FdEventos.EventoTag');
		$this->EventoTag = new EventoTag();

		App::import('Model', 'FdEventos.EventoEventoTag');
		$this->EventoEventoTag = new EventoEventoTag();
		
		//existe?
		if (!$this->EventoTag->exists($this->params->params['pass'][0])) {
			throw new NotFoundException(__('EventoTag inválida.'));
		}

		//find tag
		$evento_tag = $this->EventoTag->find('first', array('recursive' => -1, 'conditions' => array('EventoTag.id' => $this->params->params['pass'][0])));

		//find eventos com a tag
		$evento_evento_tags = $this->EventoEventoTag->find('list', array(
																			'recursive' 	=> -1, 
																			'fields' 		=> array('EventoEventoTag.evento_id'), 
																			'conditions' 	=> array('EventoEventoTag.tag_id' => $this->params->params['pass'][0]),
																			'group'			=> array('EventoEventoTag.evento_id')
																		));

		//conditions
		$date = (isset($this->params->params['pass'][2])) ? $this->params->params['pass'][2] : null;
		if(is_null($date)){
			$hoje = date('Y-m-d');
			// $options['conditions'] = array('Evento.status' => true, 'Evento.id' => $evento_evento_tags, 'Evento.data >=' => $hoje);
			
		}else{
			$hoje = date('Y-m-d', strtotime($date));
			// $options['conditions'] = array('Evento.status' => true, 'Evento.id' => $evento_evento_tags, 'Evento.data' => $hoje);
		}

		$options['conditions'] = array('Evento.status' => true, 'Evento.id' => $evento_evento_tags, 'OR' => array(array('Evento.data_inicio >=' => $hoje), array('Evento.data_fim >=' => $hoje)));

		$this->Evento->recursive = 0;
		$options['order'] = 'data ASC';
		$this->paginate = $options;
		$this->set('eventos', $this->paginate('Evento'));
		$this->set(compact('hoje'));
	}
}