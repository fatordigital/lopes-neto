<?php
App::uses('AppController', 'Controller');

/**
 * Sacs Controller
 *
 * @property Sac $Sac
 */
class SitemapsController extends AppController {

	public $helpers = array('String');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = 'xml';

		$urls_fixas = array();
		$urls_fixas[] = 'fale-conosco';
		$urls_fixas[] = 'calendario';
		$urls_fixas[] = 'eventos';
		$urls_fixas[] = 'portal-aluno';
		
		$urls = array();
		foreach ($urls_fixas as $key => $url) {
			$urls[$key]['changefreq'] 	= 'daily';
			$urls[$key]['url'] 		 	= $url;
			$urls[$key]['priority'] 	= '0.85';
		}

		// $rotas = $this->Rota->find('all', array('recursive' => -1, 'fields' => array('Rota.id', 'Rota.seo_url', 'Rota.model'), 'conditions' => array('Rota.model <>' => 'PaginaTipo')));
	    App::import('Model', 'FdRotas.Rota');
		$modelRota = new Rota();
		$rotas = $modelRota->getRotas();

		if(!empty($rotas)){
			$idx = count($urls_fixas);
			foreach ($rotas as $key => $rota) {

				$idx += 1;

				$urls[$idx]['changefreq'] 	= 'daily';
				$urls[$idx]['url'] 		 	= $rota['seo_url'];

				switch ($rota['model']) {
					case 'CursoTipo':
						$urls[$idx]['priority'] = '1.0';
						break;

					case 'Curso':
						$urls[$idx]['priority'] = '1.0';
						break;
						
					case 'Pagina':
						$urls[$idx]['priority'] = '1.0';
						break;

					// case 'Campus':
						// $urls[$idx]['priority'] = '0.85';
						// break;

					// case 'CursoCategoria':
						// $urls[$idx]['priority'] = '0.85';
						// break;

					// case 'Evento':
						// $urls[$idx]['priority'] = '0.85';
						// break;

					default:
						$urls[$idx]['priority'] = '0.85';
						break;
				}
			}
		}
		$this->set('urls', $urls);
	}
}
