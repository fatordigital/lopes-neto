<?php

class BlocosController extends AppController {

	public function index($date = null){

		/*====================================
		#### CUIDAR OS RENDERS DO METODO  ####
		======================================*/
		
		//load do model afins de pegar o hasMany
		App::import('Model', 'FdNoticias.Noticia');
		$this->Noticia = new Noticia();

		App::import('Model', 'FdNoticias.Bloco');
		$this->Bloco = new Bloco();

		//existe?
		if (!$this->Bloco->exists($this->params->params['pass'][0])) {
			throw new NotFoundException(__('Bloco inválida.'));
		}

		//find bloco
		$bloco = $this->Bloco->find('first', array('recursive' => -1, 'contain' => array('BlocoTipo'), 'conditions' => array('Bloco.id' => $this->params->params['pass'][0])));
		$this->set(compact('bloco'));
		
		if($bloco['BlocoTipo']['slug'] == 'noticias'){
			$date = (isset($this->params->params['pass'][2])) ? $this->params->params['pass'][2] : null;

			App::import('Model','FdNoticias.NoticiaBloco');
			$this->NoticiaBloco = new NoticiaBloco();
			$noticias_id = $this->NoticiaBloco->find('list', array('recursive' => -1, 'fields' => array('NoticiaBloco.noticia_id'), 'conditions' => array('NoticiaBloco.status' => true, 'NoticiaBloco.bloco_id' => $this->params->params['pass'][0])));

			//find noticias
			//conditions
			//data é valida?
			if(is_null($date) || strlen($date) < 5){
				$hoje = date('Y-m-d');
				$options['conditions'] = array('Noticia.status' => true, 'Noticia.id' => $noticias_id);
				
			}else{
				$hoje = date('Y-m-d', strtotime($date));
				$options['conditions'] = array('Noticia.status' => true, 'Noticia.id' => $noticias_id, 'Noticia.data_publicacao' => $hoje);
			}

			$this->Noticia->recursive = -1;
			$options['order'] = 'Noticia.id DESC';
			$this->paginate = $options;

			//paginate noticias
			$this->set('noticias', $this->paginate('Noticia'));	

			//set dia de hoje (para o calendário)
			$this->set(compact('hoje'));

			//_addBreadcrumb
			$this->_addBreadcrumb('Notícias', array('controller' => 'noticias', 'action' => 'index'));
			$this->_addBreadcrumb($bloco['Bloco']['nome']);

			$this->render('noticias');
		}
	}
}