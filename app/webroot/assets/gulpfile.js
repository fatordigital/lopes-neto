/* file: gulpfile.js */

var gutil  = require('gulp-util'),
    uglify = require('gulp-uglify'),
    watch  = require('gulp-watch'),
    gulp   = require('gulp'),
    jshint = require('gulp-jshint'),
    sass   = require('gulp-sass'),
    browserSync = require('browser-sync').create();

var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');



/* jshint task would be here */
gulp.task('build-css', function() {
  return gulp.src('_source/scss/**/*.scss')
    .pipe(sass())
    .pipe(cssmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('public/assets/css'))
    .pipe(browserSync.stream());
});


// Static Server + watching scss/html files
gulp.task('serve', ['build-css'], function() {

    browserSync.init({
        server: ""
    });

    //gulp.watch('assets/_source/js/**/*.js', ['jshint']);
    gulp.watch("_source/scss/*.scss", ['build-css']);
    gulp.watch("*.html").on('change', browserSync.reload);
});

gulp.task('default', ['serve']);


