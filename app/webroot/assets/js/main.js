
var MaskBehavior = function (val) {
    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
maskOptions = {
    onKeyPress: function(val, e, field, options) {
        field.mask(MaskBehavior.apply({}, arguments), options);
    }
};

$(document).ready( function () {

    var $owl = $('.fd_carousel'),
        $owlHeader = $('.fd_carousel_topo'),
        nav = $('.navbar.navbar-default'),
        $eq = $('.fd_eq'),
        $tel = $('.fd_phone_mask'),
        $btnMobile = $('.navbar-toggle');

    /* BEGIN: MASK */
    $tel.mask(MaskBehavior, maskOptions);
    /* END: MASK */

    /* BEGIN: MENU MOBILE */
    $btnMobile.on( 'click', function () {
       $(this).toggleClass('opened');
       $('body, .fd_menu, .navbar-inverse').toggleClass('opened');
    });
    /* END: MENU MOBILE */

    /* BEGIN: OWL CAROUSEL CONFIGS */

    $owlHeader.owlCarousel({
        loop:true,
        margin:0,
        nav:false,
        navText: ["Prev","Next"],
        dots: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        autoplay: true,
        responsive:{
            0:{
                items:1
            }
        }
    });

    $owl.owlCarousel({
        loop:false,
        margin:0,
        nav:true,
        navText: ["Antigas","Próximas"],
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        autoplay: false,
        dots: true,
        responsive:{
            0:{
                items:1
            }
        }
    });
    /* END: OWL CAROUSEL CONFIG */

    /* BEGIN: TOOLTIP */
    $('[data-toggle="tooltip"]').tooltip()
    /* END: TOOLTIP */

    /* BEGIN: AJUSTE PAGINADOR */
    var owlNext = $('#fd_carousel_noticias .owl-nav .owl-next');
    var w = owlNext.parent().parent().find('.owl-dots').width();
    owlNext.css({'margin-left': w+'px'});

    var owlNext = $('#fd_carousel_artigos .owl-nav .owl-next');
    var w = owlNext.parent().parent().find('.owl-dots').width();
    owlNext.css({'margin-left': w+'px'});
    /* END: AJUSTE PAGINADOR */

    /* BEGIN: AFFIX */
    nav.affix({
       offset: {
           top: 200
       }
    });
    /* END: AFFIX */

    /* BEGIN: PAGE-SCROLL*/
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top - 100
        }, 1200, 'easeInOutExpo');
        event.preventDefault();
    });
    /* END: PAGE-SCROLL */

    /* BEGIN: SCROLLSPY */
    $('body').scrollspy({ target: '.navbar.navbar-default' });
    /* END SCROLLSPY */

    /* BEGIN: SHOW ARTICLES AND NEWS */
    $('.fd_articles .btn').on('click', function() {

        var $this = $(this);

        if($this.hasClass('inactive')){
            $this.removeClass('inactive');
            $this.siblings().addClass('inactive');

            //console.log($this.data('type'));
            $('#fd_carousel_'+$this.data('type')).siblings().addClass('inactive');
            $('#fd_carousel_'+$this.data('type')).removeClass('inactive');

        }
    });
    /* END: SHOW ARTICLES AND NEWS */


    /* BEGIN: TABS TO COLLAPSE */
    $('#fd_tab_collapse').tabCollapse({
        tabsClass: 'hidden-xs hidden-sm',
        accordionClass: 'visible-xs visible-sm'
    });
    /* END TABS TO COLLAPSE */

    /* BEGIN: SHOW RESUME */
    $('.fd_open_resume').on( 'click', function () {

        var _this       = $(this),
            _data       = _this.data('resume'),
            _resume     = $('.fd_open_resume'),
            _target     = $('.fd_content_resume'),
            _d          = $('.fd_open_resume[data-resume = ' + _data + ']');

        // console.log(_data);

        if(!_d.hasClass('opened') && !_resume.hasClass('opened') && !_target.hasClass('opened')){
            _this.addClass('opened');
            _d.addClass('opened');
            $('#'+_data).addClass('opened');
        } else {
            $('#'+_data).removeClass('opened');
            _resume.removeClass('opened');
            _target.removeClass('opened');
        }
    });
    /* END: SHOW RESUME */

    /* BEGIN: SHOW AREAS CONTENT */
    $('.fd_areas .fd_item .fd_miolo').on('click', function () {
        var $this = $(this),
            $data = $this.data('area'),
            $parent = $this.closest('.fd_item'),
            $item = $('.fd_areas .fd_item'),
            id = $('#'+$data);

        id.fadeIn(350);

        if($item.hasClass('active')){
            $parent.find('.button-close').fadeOut(300);
            $item.removeClass('active');
        }

        if(!$parent.hasClass('active')){
            $item.addClass('disabled');

            if($parent.hasClass('disabled')){
                $parent.removeClass('disabled');
            }

            $parent.find('.button-close').fadeIn(300);
            $parent.addClass('active');
            id.addClass('active');

            $('html, body').animate({
                scrollTop: id.offset().top - 200
            }, 750);

        }

        $parent.find('.button-close').on('click', function () {
            if($item.hasClass('active')){
                $item.removeClass('active');
            }
            if($item.hasClass('disabled')){
                $item.removeClass('disabled');
            }
            $(this).fadeOut(300);
            id.fadeOut(350);
        });
    });
    /* END: SHOW AREAS CONTENT */

    /* BEGIN: LABEL PARA INPUT */
    var inputs = $('input, textarea');
    inputs.blur(function () {
        if ($(this).val()) {
            $(this).addClass('used');
        }
        else {
            $(this).removeClass('used');
        }
    });
    /* END: LABEL PARA INPUT */

    /* BEGIN: EQUAL HEIGHT */
    var options = {
        byRow: true,
        property: 'height',
        target: null,
        remove: false
    };

    $eq.matchHeight(options);

    if(window.matchMedia('(min-width: 768px)').matches){
        $eq.matchHeight(options);
    }
    /* END: EQUAL HEIGHT */

    /* BEGIN: SEND FORM */
    var errorMessage = 'Preencha o campo<br /> corretamente';
    var validator = $("#form").validate({
        rules: {
            "nome" : {
                required: true
            },
            "telefone" : {
                required: true,
                minlength: 14,
                maxlength: 15
            },
            "email" : {
                required: true,
                email: true
            },
            "mensagem" : {
                required: true
            }
        },
        messages: {
            "nome" : {
                required: errorMessage
            },
            "email" : {
                required: errorMessage,
                email: errorMessage
            },
            "telefone" : {
                required: errorMessage,
                minlength: errorMessage
            },
            "mensagem" : {
                required: errorMessage
            },
            "cidade" : {
                required: errorMessage
            }
        },
        submitHandler: function( form ){
            var dados = $( form ).serialize(),
                btn = $('button[type="submit"]'),
                $msg = $('.fd_form_message');

            btn.html('<div class="fd_loading"><i></i><i></i><i></i><i></i></div>');

            btn.prop( "disabled", true);

            return true;
        }
    });

    /* END: SEND FORM */

});


var mapStyle = [
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e9e9e9"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dedede"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#333333"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f2f2f2"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    }
];


var baseIcon = baseUrl + '/assets/images/icons/',
    locations = [
        {
            mapa: {
                lat: -30.03174979,
                long: -51.23121651,
                zoom: 17,
                endereco: 'Rua Riachuelo, 1038 Sala 602 - Centro Histórico Porto Alegre - RS',
                enderecoHTML: 'Rua Riachuelo, 1038<br />Sala 602 - Centro Histórico<br />Porto Alegre - RS',
                controlPosition: google.maps.ControlPosition.RIGHT_BOTTOM,
                id: 'mapa'
            }
        }
    ];


// When the window has finished loading create our google map below
locations.map(function (obj, index) {
    // console.log(obj, index);
    google.maps.event.addDomListener(window, 'load', init(obj));
});


function init(locations) {
    // Basic options for a simple Google Map
    // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
    var mapOptions = {
        // How zoomed in you want the map to start at (always required)
        zoom: locations.mapa.zoom,
        scrollwheel: false,
        navigationControl: false,
        mapTypeControl: false,
        scaleControl: false,
        draggable: true,
        disableDoubleClickZoom: true,
        streetViewControl: false,
        zoomControl: true,
        zoomControlOptions: {
            position: locations.mapa.controlPosition
        },
        // The latitude and longitude to center the map (always required)
        center: new google.maps.LatLng(locations.mapa.lat, locations.mapa.long),
        // How you would like to style the map.
        // This is where you would paste any style found on Snazzy Maps.
        styles: mapStyle
    };
    var image = {
        url: baseIcon + 'pin.svg',
        size: new google.maps.Size(80, 80)
    };
    // Get the HTML DOM element that will contain your map
    // We are using a div with id="map" seen below in the <body>
    var mapElement = document.getElementById(locations.mapa.id);

    // Create the Google Map using our element and options defined above
    var map = new google.maps.Map(mapElement, mapOptions);

    // Let's also add a marker while we're at it
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations.mapa.lat, locations.mapa.long),
        map: map,
        title: locations.mapa.enderecoHTML,
        icon: image
    });

    var infowindow = new google.maps.InfoWindow({
        content: '<p>'+locations.mapa.endereco+'</p>',
        maxWidth: 250
    });


    /* REMOVE GOOGLE MAPS STYLE */
    google.maps.event.addListener(infowindow, 'domready', function() {

        // ReferÃªncia ao DIV que agrupa o fundo da infowindow
        var iwOuter = $('.gm-style-iw');

        /* Uma vez que o div pretendido estÃ¡ numa posiÃ§Ã£o anterior ao div .gm-style-iw.
         * Recorremos ao jQuery e criamos uma variÃ¡vel iwBackground,
         * e aproveitamos a referÃªncia jÃ¡ existente do .gm-style-iw para obter o div anterior com .prev().
         */
        var iwBackground = iwOuter.prev();

        // Remover o div da sombra do fundo
        iwBackground.children(':nth-child(2)').css({'display' : 'none'});

        // Remover o div de fundo branco
        iwBackground.children(':nth-child(4)').css({'display' : 'none'});

        // Desloca a infowindow 115px para a direita
        iwOuter.parent().parent().css({left: '0', top: '-15px'});

        // Desloca a sombra da seta a 76px da margem esquerda
        iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

        // Desloca a seta a 76px da margem esquerda
        iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

        // Altera a cor desejada para a sombra da cauda
        //iwBackground.children(':nth-child(3)').find('div').css({'box-shadow': 'none'});
        iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(255, 255, 255, 1) 0px 0px 5px', 'border' : 'none', 'z-index' : '1'});

        // ReferÃªncia ao DIV que agrupa os elementos do botÃ£o fechar
        var iwCloseBtn = iwOuter.next();

        // Aplica o efeito desejado ao botÃ£o fechar
        iwCloseBtn.css({opacity: '1', right: '38px', top: '3px', border: '7px solid #48b5e9', 'border-radius': '13px', 'box-shadow': '0 0 5px #3990B9', display: 'none'});

        // Se o conteÃºdo da infowindow nÃ£o ultrapassar a altura mÃ¡xima definida, entÃ£o o gradiente Ã© removido.
        if($('.iw-content').height() < 140){
            $('.iw-bottom-gradient').css({display: 'none'});
        }

        // A API aplica automaticamente 0.7 de opacidade ao botÃ£o apÃ³s o evento mouseout. Esta funÃ§Ã£o reverte esse evento para o valor desejado.
        iwCloseBtn.mouseout(function(){
            $(this).css({opacity: '1'});
        });
    });

    infowindow.open(infowindow, marker);
}