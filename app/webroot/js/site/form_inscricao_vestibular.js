$(document).ready(function(){

	$('#form-inscricao-vestibular').validate({ errorPlacement: function(){
        return false;  // suppresses error message text
    }});
		
	//begin: Formulário de Pré Inscrição

		var slug = function(str) {
		  str = str.replace(/^\s+|\s+$/g, ''); // trim
		  str = str.toLowerCase();

		  // remove accents, swap ñ for n, etc
		  var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
		  var to   = "aaaaaeeeeeiiiiooooouuuunc------";
		  for (var i=0, l=from.length ; i<l ; i++) {
		    str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
		  }

		  str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
		    .replace(/\s+/g, '-') // collapse whitespace and replace by -
		    .replace(/-+/g, '-'); // collapse dashes

		  return str;
		};

		// $('#form-inscricao-vestibular #nens_id').html('<option value="">Selecione o campus</option><option value="1">CAMPUS ZONA SUL</option><option value="2">CAMPUS CANOAS</option><option value="7">CAMPUS FAPA</option>');

		//carrega as informações dos campus
		$.ajax({
			dataType: "json",
			type: "GET",
			url: APP_ROOT + 'api/get_campi',
			success: function (data, textStatus){
				$('#form-inscricao-vestibular #nens_id').html("").append($('<option>', {
				    value: '',
				    text: 'Selecione o campus'
				}));
				$.each(data, function(index, itemData) {
				  	$('#form-inscricao-vestibular #nens_id').append($('<option>', {
					    value: index,
					    text: itemData
					}));
				});
			},
			// error: function(){
			// 	$('.form_pre_inscricao #ApiNensId').html('<option value="">Selecione o campus</option><option value="1">CAMPUS ZONA SUL</option><option value="2">CAMPUS CANOAS</option><option value="7">CAMPUS FAPA</option>');
			// }
		});

		if($('#form-inscricao-vestibular #nens_id:selected').val() != ""){
			get_cursos($('#form-inscricao-vestibular #nens_id:selected').val());
		}
		$('#form-inscricao-vestibular #nens_id').change(function(){
			get_cursos($(this).val());
			// if ($(this).val() == 1) {
			// 	$('#form-inscricao-vestibular #curs_id').html("").append('<option value="">Selecione o curso</option><option value="1|1">ADMINISTRAÇÃO - ZONA SUL</option><option value="1|39">ADMINISTRAÇÃO - ZONA SUL - GRADUAÇÃO EXECUTIVA</option><option value="1|9">ANÁLISE E DESENVOLVIMENTO DE SISTEMAS - ZONA SUL</option><option value="1|2">ARQUITETURA E URBANISMO - ZONA SUL</option><option value="1|20">BIOMEDICINA - ZONA SUL</option><option value="1|33">CIÊNCIA DA COMPUTAÇÃO - ZONA SUL</option><option value="1|18">CIÊNCIAS CONTÁBEIS - ZONA SUL</option><option value="1|74">CIÊNCIAS CONTÁBEIS - ZONA SUL - GRADUAÇÃO EXECUTIVA</option><option value="1|3">DESIGN - ZONA SUL</option><option value="1|5">DIREITO - ZONA SUL</option><option value="1|28">ENFERMAGEM - ZONA SUL</option><option value="1|31">ENGENHARIA AMBIENTAL E SANITÁRIA - ZONA SUL</option><option value="1|16">ENGENHARIA CIVIL - ZONA SUL</option><option value="1|40">ENGENHARIA DE CONTROLE E AUTOMAÇÃO - ZONA SUL</option><option value="1|25">ENGENHARIA DE PRODUÇÃO - ZONA SUL</option><option value="1|37">ENGENHARIA ELÉTRICA - ZONA SUL</option><option value="1|24">ENGENHARIA MECÂNICA  - ZONA SUL</option><option value="1|36">ENGENHARIA QUÍMICA - ZONA SUL</option><option value="1|27">FARMÁCIA - ZONA SUL</option><option value="1|21">FISIOTERAPIA - ZONA SUL</option><option value="1|26">JOGOS DIGITAIS - ZONA SUL</option><option value="1|22">JORNALISMO - ZONA SUL</option><option value="1|6">LETRAS - ZONA SUL</option><option value="1|32">NUTRIÇÃO - ZONA SUL</option><option value="1|7">PEDAGOGIA - ZONA SUL</option><option value="1|14">PSICOLOGIA - ZONA SUL</option><option value="1|23">PUBLICIDADE E PROPAGANDA - ZONA SUL</option><option value="1|17">RELAÇÕES INTERNACIONAIS - ZONA SUL</option><option value="1|41">RELAÇÕES PÚBLICAS - ZONA SUL</option><option value="1|8">SISTEMAS DE INFORMAÇÃO -  ZONA SUL</option>');
			// }
			// if ($(this).val() == 2) {
			// 	$('#form-inscricao-vestibular #curs_id').html("").append('<option value="">Selecione o curso</option><option value="1|15">ADMINISTRAÇÃO - CANOAS</option><option value="1|72">ADMINISTRAÇÃO - CANOAS - GRADUAÇÃO EXECUTIVA</option><option value="1|42">ANÁLISE E DESENVOLVIMENTO DE SISTEMAS - CANOAS</option><option value="1|64">ARQUITETURA E URBANISMO - CANOAS</option><option value="1|68">CIÊNCIA DA COMPUTAÇÃO - CANOAS</option><option value="1|73">CIÊNCIAS CONTÁBEIS - CANOAS - GRADUAÇÃO EXECUTIVA</option><option value="1|4">DIREITO - CANOAS</option><option value="1|38">ENFERMAGEM - CANOAS</option><option value="1|43">ENGENHARIA CIVIL - CANOAS</option><option value="1|66">ENGENHARIA DE PRODUÇÃO - CANOAS</option><option value="1|67">ENGENHARIA MECÂNICA - CANOAS</option><option value="1|65">FISIOTERAPIA - CANOAS</option><option value="1|29">GESTÃO DE RECURSOS HUMANOS - CANOAS</option><option value="1|30">MARKETING - CANOAS</option>');
			// }
			// if ($(this).val() == 7) {
			// 	$('#form-inscricao-vestibular #curs_id').html("").append('<option value="">Selecione o curso</option><option value="1|44">ADMINISTRAÇÃO - FAPA</option><option value="1|49">ANÁLISE E DESENVOLVIMENTO DE SISTEMAS - FAPA</option><option value="1|48">ARQUITETURA E URBANISMO - FAPA</option><option value="1|50">BIOMEDICINA - FAPA</option><option value="1|51">CIÊNCIA DA COMPUTAÇÃO - FAPA</option><option value="1|70">CIÊNCIAS BIOLÓGICAS - FAPA</option><option value="1|45">CIÊNCIAS CONTÁBEIS - FAPA</option><option value="1|52">DESIGN - FAPA</option><option value="1|53">ENFERMAGEM - FAPA</option><option value="1|54">ENGENHARIA CIVIL - FAPA</option><option value="1|55">ENGENHARIA DE PRODUÇÃO - FAPA</option><option value="1|56">FISIOTERAPIA - FAPA</option><option value="1|75">GASTRONOMIA - FAPA</option><option value="1|57">GESTÃO DE RECURSOS HUMANOS - FAPA</option><option value="1|58">HISTÓRIA - FAPA</option><option value="1|60">JORNALISMO - FAPA</option><option value="1|47">LETRAS - FAPA</option><option value="1|59">MARKETING - FAPA</option><option value="1|35">MEDICINA VETERINÁRIA - FAPA</option><option value="1|71">NUTRIÇÃO - FAPA</option><option value="1|46">PEDAGOGIA - FAPA</option><option value="1|62">PUBLICIDADE E PROPAGANDA - FAPA</option><option value="1|63">RELAÇÕES INTERNACIONAIS - FAPA</option>');
			// }
		});

		$("#form-inscricao-vestibular").submit(function(e) {

			$('#form-inscricao-vestibular .success_message').hide();
			$('#form-inscricao-vestibular .error_message').hide();

			if($("#form-inscricao-vestibular").valid()){
		        e.preventDefault();

		        $.ajax({
		                url: APP_ROOT + 'api/set_data_pre_inscricao',
		                type: 'post',
		                // dataType: 'json',
		                data: $(this).serialize(),
		                success: function(data) {
		                	if(data.slice(-1) == 1){
		                   		//goal
		                   		// var goal_tipo = 'contato';
	                   			// var goal_objetivo = 'vestibular-experience';

		                   		var goal_tipo = 'vestibular-experience';
		                   		var goal_objetivo = slug($('#ApiCursId option:selected').text());
		                   		var goal = '/goal/' + goal_tipo + '/' + goal_objetivo;
		                   		// console.log(goal);
								// ga('send','pageview', goal);
								dataLayer.push({
									'event':'VirtualPageview',
									'sendPageview': goal
								});

								//temporário
								// $('#inscricao-target').html('<select name="curso_interesse" class="control form-control" data-rule-required="true" data-msg-required="Selecione o curso de interesse" id="ApiCursId"> <option value="">Escolha o curso</option> </select>');

								//reset
		                   		$("#form-inscricao-vestibular")[0].reset();
		                   		$('#form-inscricao-vestibular .success_message').show();
		                   	}else{
		                   		$('#form-inscricao-vestibular .error_message').show();
		                   	}
		                }
		        });
			}	       

	    });


	//end: Formulário de Pré Inscrição

});

function get_cursos(campus){
	$('#form-inscricao-vestibular #curs_id').html("").append($('<option>', {
	    value: '',
	    text: 'Carregando...'
	}));
	$.ajax({
		dataType: "json",
		type: "GET",
		url: APP_ROOT + 'api/get_cursos/' + campus,
		success: function (data, textStatus){
			$('#form-inscricao-vestibular #curs_id').html("").append($('<option>', {
			    value: '',
			    text: 'Selecione o curso'
			}));
			$.each(data, function(index, itemData) {
			  	$('#form-inscricao-vestibular #curs_id').append($('<option>', {
				    value: index,
				    text: itemData
				}));
			});
		}
	});
}