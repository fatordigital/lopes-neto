<main class="blog interna">
    <section class="header-interna">
        <h1>
            BLOG<br/>
            <small><?php echo $this->Idioma->palavra('uma-orchestra-atualizada'); ?></small>
        </h1>
    </section>

    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2><span>Blog</span><span><?php echo $this->Idioma->palavra('ultimas-noticias'); ?></span></h2>
                </div>
            </div>
            <div class="row row-content">
                <?php if (isset($noticias)) { ?>
                    <?php foreach ($noticias as $key => $noticia) { ?>
                        <div class="col-xs-12 col-sm-6 col-md-4 <?php echo ($key+1) % 3 == 0 || ($key+1) % 3 == 1 ? '' : 'middle'; ?> equal-heights">
                            <article class="post-item">
                                <a href="<?php echo $this->Idioma->url('/' . $noticia['Rota']['seo_url'], true) ?>">
                                    <figure>
                                        <?php echo $this->Html->image('/files/noticia/banner/' . $noticia['Noticia']['banner_dir'] .'/' . $noticia['Noticia']['banner'], array("alt" => $this->Idioma->extrair($noticia['NoticiaAtributo'], 'titulo'))); ?>
                                    </figure>
                                </a>
                                <div class="post-content">
                                    <p>
                                        <a href="<?php echo $this->Idioma->url('/' . $noticia['Rota']['seo_url'], true) ?>">
                                            <?php echo $this->Idioma->extrair($noticia['NoticiaAtributo'], 'titulo') ?>
                                        </a>
                                    </p>
                                    <a href="<?php echo $this->Idioma->url('/' . $noticia['Rota']['seo_url'], true) ?>" class="btn btn-leia-mais">LEIA MAIS</a>
                                </div><!-- end of /.post-content -->
                            </article><!-- end of /article -->
                        </div><!-- end of /.cols -->
                    <?php } ?>
                <?php } ?>
            </div><!-- end of /.rwo.row-content -->
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <nav aria-label="Page navigation" class="text-center">
                        <ul class="pagination">
                            <li class="<?php echo $this->Paginator->hasPrev() ? '' : 'disabled' ?>">
                                <?php echo $this->Paginator->prev('<i class="fa fa-long-arrow-left" aria-hidden="true"></i>', array('escape' => false, 'tag' => ''), null, array()); ?>
                            </li>
                            <?php for ($i = 1; $i < $this->Paginator->param('pageCount') + 1; $i++) { ?>
                                <li <?php echo $this->Paginator->current() == $i ? 'class="active"' : '' ?>>
                                    <a href="<?php echo $this->Paginator->current() != $i ? $this->Idioma->url('blog/page:' . $i) : 'javascript:void(0)' ?>">
                                        <?php echo $i; ?>
                                    </a>
                                </li>
                            <?php } ?>
                            <li class="<?php echo $this->Paginator->hasNext() ? '' : 'disabled' ?>">
                                <?php echo $this->Paginator->next('<i class="fa fa-long-arrow-right" aria-hidden="true"></i>', array('escape' => false, 'tag' => ''), null, array()); ?>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div><!-- end of /.container -->
    </section>
</main>