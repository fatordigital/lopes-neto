<section class="fd_internal_article">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8">
                <article>
                    <figure>
                        <?php echo $this->Html->image('/files/noticia/banner/' . $noticia['Noticia']['banner_dir'] .'/' . $noticia['Noticia']['banner']) ?>
                    </figure>
                    <h1>
                        <?php echo $this->Idioma->extrair($noticia['NoticiaAtributo'], 'titulo') ?>
                    </h1>
                    <?php echo $this->Idioma->extrair($noticia['NoticiaAtributo'], 'conteudo') ?>
                </article>
            </div>
            <aside class="col-xs-12 col-sm-4 col-md-3">
                <a href="<?php echo $this->Html->Url('/#home') ?>" class="btn">Voltar para o site</a>
                <?php if (isset($noticias_relacionadas)) { ?>
                <h4>Últimas Notícias</h4>
                <ul>
                    <?php foreach ($noticias_relacionadas as $noticias_relacionada) { ?>
                        <?php foreach ($noticias_relacionada as $not) { ?>
                            <li>
                                <a href="<?php echo $this->Html->Url('/' . $not['Rota']['seo_url']) ?>">
                                    <?php echo $this->Idioma->extrair($not['NoticiaAtributo'], 'titulo') ?>
                                </a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
                <?php } ?>
            </aside>
        </div>
    </div>
</section>