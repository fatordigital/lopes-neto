<?php echo $this->assign('title', 'Calendário Acadêmico'); ?>

		<?php echo $this->element('site/banner_curso', array('banners' => $banners_noticias)); ?>
	</div>
</div>
<div class="row row-title">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h1>Calendário Acadêmico</h1>
			<div class="div-campus">
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
			</div>
		</div>
	</div>
</div>
<div class="row row-tabs-gray">
	<div class="container">
		<?php echo $this->element('site/breadcrumb') ?>
	</div>
</div>
<div class="row row-tab-body">
	<div class="container ">			
		<div role="tabpanel">
		  <!-- Nav tabs -->
		  <ul class="nav nav-tabs" role="tablist">
		    <li role="presentation" class="active"><a href="#calendario" aria-controls="home" role="tab" data-toggle="tab" >Calendário <span class="seta-tab glyphicon glyphicon-triangle-bottom pull-right triangle-red"></span></a></li>		
		  </ul>

		  <!-- Tab panes -->
		  <div class="tab-content">
		  	<div role="tabpanel" class="tab-pane active" id="calendario">
		  			<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 ">

		  				<?php if(isset($eventos) && count($eventos) > 0): ?>
		  					<?php foreach($eventos as $k => $evento): ?>
				    			<div class="box-day">	
				    				<h4><span class="glyphicon glyphicon-calendar"></span><?php echo date('d', strtotime($evento['Evento']['data'])); ?> de <?php echo $this->String->mes(date('m', strtotime($evento['Evento']['data']))); ?></h4>		    				
				    				<h3><?php echo $evento['Evento']['nome']; ?></h3>
				    				<p><?php echo $evento['Evento']['descricao_resumida']; ?></p>
				    				<a href="<?php echo $this->Html->Url('/' . $evento['Evento']['seo_url']); ?>" class="btn-red upper pull-right" title="<?php echo $evento['Evento']['nome']; ?>">Leia Mais</a>
				    			</div>
				    		<?php endForeach; ?>
			    		<?php else: ?>
			    			<br /><br />
			    			Nenhum evento agendado para hoje.
			    		<?php endIf; ?>

		    			<br/><br/><br/><br/>
	    			</div>	
		    		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 sidebar calendario-sidebar">	
		    			<?php echo $this->element('site/calendario') ?>

		    			<?php /*
		    			<div class="documents">

							<h4 class="title-color title-red">Documentos</h4>
							<p>Faça download dos calendários acadêmicos em formato PDF</p>
							<a href="#">
								<div class="icon-pdf">
									<img src="assets/images/icons/icon-pdf.png" class="img-responsive">
									<p>Calendário Acadêmico 2015</p>
								</div>
							</a>
							<a href="#">
								<div class="icon-pdf">
									<img src="assets/images/icons/icon-pdf.png" class="img-responsive">
									<p>Calendário Acadêmico 2014</p>
								</div>
							</a>
							<a href="#">
								<div class="icon-pdf">
									<img src="assets/images/icons/icon-pdf.png" class="img-responsive">
									<p>Calendário Acadêmico 2013</p>
								</div>
							</a>
							<a href="#">
								<div class="icon-pdf">
									<img src="assets/images/icons/icon-pdf.png" class="img-responsive">
									<p>Calendário Acadêmico 2012</p>
								</div>
							</a>								
						</div>
						*/?>

						<?php echo $this->element('site/redes-sociais-mini') ?>

					</div><!-- end sidebar-->
				</div>
		  	</div>				  			  			  	
		</div>						
	</div>		
</div>