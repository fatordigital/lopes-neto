<?php
App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class StringHelper extends Helper {

	const STATUS_ATIVO = 'Ativo';
	const STATUS_INATIVO = 'Inativo';

    public function list_controllers() {

        $aCtrlClasses = App::objects('controller');
        $controllers = array();
        foreach ($aCtrlClasses as $controller) {
            if ($controller != 'AppController') {
                // Load the controller
                App::import('Controller', str_replace('Controller', '', $controller));

                // Load its methods / actions
                $aMethods = get_class_methods($controller);

                foreach ($aMethods as $idx => $method) {

                    if ($method{0} == '_') {
                        unset($aMethods[$idx]);
                    }
                }

                // Load the ApplicationController (if there is one)
                App::import('Controller', 'AppController');
                $parentActions = get_class_methods('AppController');

                $controllers[$controller] = array_diff($aMethods, $parentActions);
            }
        }

        return $controllers;
    }

	public function getStatus($valor) {
	    switch ($valor) {
			case '1':
				return '<span class="btn btn-round btn-xs btn-primary">' . self::STATUS_ATIVO . '</span>';
				break;

			default:
				return '<span class="btn btn-round btn-xs btn-danger">' . self::STATUS_INATIVO . '</span>';
				break;
		}
	}

    /**
     * @param $date
     * @param string $format
     * @return bool
     *
     * Valida o formato da data
     */
    public function valid_date($date, $format = 'd/m/Y')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

	public function getEscola($codigo){
		$data = array(
			'43174159' => 'ESC EST DE ENS MEDIO MARIO QUINTANA-CAIC',
			'43183034' => 'ESCOLA DE ENSINO MEDIO SALVADOR JESUS CRISTO',
			'43013759' => 'IE NOSSA SENHORA DO CARMO',
			'43013872' => 'ESC EST ENS MEDIO NOSSA SENHORA APARECIDA',
			'43013740' => 'EEEM SENADOR SALGADO FILHO',
			'43013929' => 'EEEM CARLOS DRUMMOND DE ANDRADE',
			'43013937' => 'CE ERICO VERISSIMO',
			'43013732' => 'EEEB JULIO CESAR RIBEIRO DE SOUZA',
			'43013899' => 'EEEB PROF GENTIL VIEGAS CARDOSO',
			'43013910' => 'EEM VALE VERDE',
			'43013945' => 'COLEGIO ESTADUAL ANTONIO DE CASTRO ALVES',
			'43013856' => 'ESC EST ENS MED MAURICIO SIROTSKY SOBRINHO',
			'43013902' => 'ESCOLA ESTADUAL DE ENSINO MEDIO CAMPOS VERDES',
			'43003761' => 'EXITO SISTEMAS DE ENSINO LTDA',
			'43212034' => 'INSTITUTO DE EDUCACAO CRISTA NOVO HORIZONTE',
			'43029957' => 'COLEGIO ESTADUAL RODRIGUES ALVES',
			'43029787' => 'EEEM PRESIDENTE KENNEDY',
			'43181694' => 'ESC DE ENS MEDIO UNIVER CACH EDUC JOVENS ADULT',
			'43029990' => 'ETE MARECHAL MASCARENHAS DE MORAES',
			'43029817' => 'EEE BASICA LUIZ DE CAMOES',
			'43029825' => 'EEEM GUIMARAES ROSA',
			'43029914' => 'IEE PRINCESA ISABEL',
			'43029930' => 'CAE DANIEL DE OLIVEIRA PAIVA',
			'43029965' => 'EEEM OSVALDO CAMARGO',
			'43029981' => 'EEEM MARIO QUINTANA',
			'43030076' => 'EEEM NOSSA SENHORA DE FATIMA',
			'43174108' => 'EEEM FRANCISCO JOSE RODRIGUES',
			'43174116' => 'ESCOLA ESTADUAL DE ENSINO MEDIO NEUZA GOULART BRIZOLA',
			'43030009' => 'UNIDADE DE ENSINO SAO MATEUS',
			'43030017' => 'ESC LUTERANA DE ENSINO MEDIO MARTINHO LUTERO',
			'43030025' => 'COLEGIO INEDI',
			'43211984' => 'INSTITUTO SAO FRANCISCO-CACHOEIRINHA',
			'43038409' => 'EEEM AFFONSO CHARLIER',
			'43038433' => 'EEEM SAO FRANCISCO DE ASSIS',
			'43037690' => 'COLEGIO MARIA AUXILIADORA',
			'43037704' => 'COLEGIO LA SALLE NITEROI',
			'43037828' => 'EEEM VISCONDE DO RIO BRANCO',
			'43038204' => 'EEEM BARAO DO AMAZONAS',
			'43038425' => 'EEEM JOSE GOMES DE VASCONCELOS JARDIM',
			'43038441' => 'IEE DR CARLOS CHAGAS',
			'43038387' => 'COLEGIO ESPIRITO SANTO',
			'43210643' => 'COLEGIO UNIFICADO CANOAS',
			'43037682' => 'COLEGIO LA SALLE',
			'43038379' => 'COLEGIO LUTERANO CONCORDIA',
			'43037720' => 'COLEGIO ULBRA CRISTO REDENTOR',
			'43176976' => 'ESC ENS MEDIO O ACADEMICO',
			'43038174' => 'EEEM ANDRE LEAO PUENTE',
			'43038450' => 'COL ESTADUAL MARECHAL RONDON',
			'43038514' => 'EEEM BENTO GONCALVES',
			'43038662' => 'EEEM PROF MARGOT TEREZINHA NOAL GIACOMA',
			'43176364' => 'INSTITUTO PRO-UNIVERSIDADE CANOENSE - IPUC',
			'43037810' => 'EEEM ERICO VERISSIMO',
			'43038280' => 'EEEM GUILHERME DE ALMEIDA',
			'43038360' => 'COL ESTADUAL TEREZA FRANCESCUTTI',
			'43038603' => 'COL ESTADUAL MIGUEL LAMPERT',
			'43038646' => 'COL ESTADUAL JUSSARA MARIA POLIDORO',
			'43038727' => 'EEEM CONEGO JOSE LEAO HARTMANN',
			'43001483' => 'INSTITUTO FEDERAL DE EDUCACAO CIENCIA E TECNOLOGIA DO RIO GRANDE DO SUL CAMPUS CANOAS',
			'43037763' => 'UNIDADE ENSINO SAO JOAO',
			'43037780' => 'COLEGIO DA IMACULADA',
			'43054005' => 'ESC EST ENS MEDIO PROF AMERICO BRAGA',
			'43054072' => 'ESC EST ENS MEDIO ROSELI CORREIA DA SILVA',
			'43173438' => 'ESC EST DE ENS MEDIO ELDORADO DO SUL',
			'43361072' => 'ESCOLA CECILIA MEIRELES',
			'43060137' => 'COLEGIO ADVENTISTA DE ESTEIO',
			'43060315' => 'COLEGIO CORACAO DE MARIA',
			'43060102' => 'C E JOSE LOUREIRO DA SILVA',
			'43060129' => 'COLEGIO LA SALLE',
			'43060145' => 'COL ESTADUAL AUGUSTO MEYER',
			'43060170' => 'EEEM BERNARDO VIEIRA DE MELLO',
			'43060188' => 'EEEM CAETANO GONCALVES DA SILVA',
			'43060374' => 'EEEM JARDIM PLANALTO',
			'43174566' => 'ESCOLA ESTADUAL DE ENSINO MEDIO PROFª MARIA SIRLEY VARGAS FERRAZ',
			'43182127' => 'EEEM BAIRRO DO PARQUE',
			'43182135' => 'EEEM DYONELIO MACHADO',
			'43208738' => 'ESC ENS MEDIO GUSTAVO NORDLUND',
			'43105386' => 'INST EST PROFESSORA GEMA ANGELINA BELIA',
			'43106056' => 'EEEM CEARA',
			'43106277' => 'EEEM DR OSCAR TOLLENS',
			'43105351' => 'COLEGIO ROMANO SAO MATEUS',
			'43105505' => 'EMEB DR LIBERATO SALZANO VIEIRA DA CUNHA',
			'43104959' => 'COLEGIO MARISTA CHAMPAGNAT',
			'43107435' => 'COLEGIO ROMANO SANTA MARTA',
			'43107451' => 'INSTITUTO SAO FRANCISCO SANTA FAMILIA',
			'43211402' => 'COLEGIO ROMANO SENHOR BOM JESUS',
			'43107397' => 'ESC DE EDUC BASICA DON LUIS GUANELLA',
			'43107524' => 'COL METODISTA AMERICANO',
			'43107800' => 'COLEGIO ADVENTISTA DO PARTENON',
			'43108377' => 'COL LUTERANO SAO PAULO',
			'43109284' => 'CENTRO DE ESNINO MEDIO PASTOR DOHMS- UNIDADE DE ENSINO ZONA SUL',
			'43108059' => 'EEEM SANTOS DUMONT',
			'43105874' => 'EEEB APELES PORTO ALEGRE',
			'43106480' => 'EEEM PROFESSOR OSCAR PEREIRA',
			'43106765' => 'ESC EST DE ENS MEDIO MARIZ E BARROS',
			'43107214' => 'EEEM SANTA ROSA',
			'43107320' => 'EEEM JOSE DO PATROCINIO',
			'43108032' => 'COL EST PRESIDENTE ARTHUR DA COSTA E SILVA',
			'43108482' => 'CE PROF OTAVIO DE SOUZA',
			'43108180' => 'COLEGIO ESTADUAL RUBEN BERTA',
			'43108717' => 'COL EST CONEGO PAULO DE NADAL',
			'43105734' => 'COL SANTA DOROTEIA',
			'43001190' => 'INSTITUTO FEDERAL DE EDUCACAO CIENCIA E TECNOLOGIA DO RIO GRANDE DO SUL - CAMPUS RESTINGA',
			'43169481' => 'COLEGIO JOAO PAULO I',
			'43105114' => 'COLEGIO SAO JUDAS TADEU',
			'43105149' => 'COLEGIO BOM JESUS SEVIGNE',
			'43107370' => 'COLEGIO SALESIANO DOM BOSCO',
			'43203817' => 'COLEGIO MESQUITA',
			'43107460' => 'COLEGIO BOM JESUS SAO LUIZ',
			'43107567' => 'COLEGIO LUTERANO DA PAZ',
			'43238203' => 'COLEGIO JOAO PAULO I',
			'43105173' => 'ETE JOSE FEIJO',
			'43105785' => 'EEEM ALBERTO TORRES',
			'43105769' => 'EEEM AGRONOMO PEDRO PEREIRA',
			'43106595' => 'COLEGIO ESTADUAL JAPAO',
			'43106790' => 'EEEB MONSENHOR LEOPOLDO HOFF',
			'43106161' => 'EEEB DOLORES ALCARAZ CALDAS',
			'43108016' => 'COL EST MARECHAL FLORIANO PEIXOTO',
			'43108237' => 'CE PAULA SOARES',
			'43108105' => 'CEEM TIRADENTES',
			'43105130' => 'COLEGIO MARISTA SAO PEDRO',
			'43108202' => 'CE FRANCISCO A VIEIRA CALDAS JR',
			'43105017' => 'COL NOSSA SENHORA DO BOM CONSELHO',
			'43108962' => 'CE PADRE RAMBO',
			'43105211' => 'EEEM INFANTE DOM HENRIQUE',
			'43173306' => 'EEEM CRISTOVAO COLOMBO',
			'43105009' => 'COLEGIO MILITAR DE PORTO ALEGRE',
			'43107184' => 'EEEM ROQUE GONZALES',
			'43105670' => 'COLEGIO SINODAL DO SALVADOR',
			'43108768' => 'EEEB PRESIDENTE ROOSEVELT',
			'43107362' => 'COLEGIO MARISTA ASSUNCAO',
			'43107389' => 'COLEGIO VICENTINO SANTA CECILIA',
			'43107494' => 'CENTRO DE ENSINO MEDIO PASTOR DOHMS-UNIDADE DE ENSINO HIGIENOPOLIS',
			'43105076' => 'COLEGIO SANTA TERESA DE JESUS',
			'43107575' => 'INST EDUCAC JOAO XXIII-ESC DE 1 E 2 GRAUS',
			'43105190' => 'COL EST DOM JOAO BECKER',
			'43105580' => 'INSTITUTO ESTADUAL DOM DIOGO DE SOUZA',
			'43108091' => 'CE DR GLICERIO ALVES',
			'43108911' => 'COL EST ENG ILDO MENEGHETTI',
			'43173330' => 'COL JOAO PAULO I',
			'43107605' => 'COL KENNEDY',
			'43105084' => 'COL LA SALLE SANTO ANTONIO',
			'43104940' => 'COLEGIO BATISTA',
			'43104932' => 'COLEGIO DE APLICACAO UFRGS',
			'43105033' => 'COLEGIO LA SALLE DORES',
			'43107613' => 'INST VICENTE PALLOTTI ESCOLA EDUCACAO INFANTIL ENSINO FUNDAMENTAL E MEDIO',
			'43109330' => 'COL MONTEIRO LOBATO - ESC EDUC BASICA',
			'43108008' => 'CE INACIO MONTANHA',
			'43107982' => 'EEEM RAUL PILLA',
			'43107877' => 'INSTITUTO DE EDUCACAO SAO FRANCISCO',
			'43108067' => 'IEE PAULO DA GAMA',
			'43105025' => 'COLEGIO NOSSA SRA DA GLORIA',
			'43105165' => 'ETE IRMAO PEDRO',
			'43105181' => 'COL EST PROTASIO ALVES',
			'43108130' => 'COLEGIO ACM CENTRO',
			'43105203' => 'CE CANDIDO JOSE DE GODOI',
			'43105220' => 'CE JULIO DE CASTILHOS',
			'43105238' => 'ESC EST ENS MEDIO PADRE REUS',
			'43206387' => 'COLEGIO UNIVERSITARIO',
			'43104983' => 'COLEGIO ISRAELITA BRASILEIRO',
			'43105688' => 'ESCOLA TECNICA ESTADUAL PAROBE',
			'43105696' => 'ESCOLA TECNICA ESTADUAL SENADOR ERNESTO DORNELLES',
			'43108423' => 'COLEGIO PROVINCIA DE SAO PEDRO',
			'43106633' => 'EEEM PROFESSOR JULIO GRAU',
			'43107079' => 'EEEM PROFESSOR ALCIDES CUNHA',
			'43107583' => 'CE FORM DE PROFESSORES GENERAL FLORES DA CUNHA',
			'43105246' => 'EMEM EMILIO MEYER',
			'43108024' => 'COL EST PIRATINI',
			'43108040' => 'IE RIO BRANCO',
			'43108083' => 'COL EST ODILA GAY DA FONSECA',
			'43108113' => 'COLEGIO ESTADUAL FLORINDA TUBINO SAMPAIO',
			'43173349' => 'COL CONHECER',
			'43108733' => 'CE CEL AFONSO EMILIO MASSOT',
			'43000452' => 'ESC ENS MEDIO CESI ZONA SUL',
			'43104967' => 'COL CONCORDIA',
			'43104991' => 'COL MARIA IMACULADA',
			'43105050' => 'ESC DE EDUC BAS RAINHA DO BRASIL',
			'43105068' => 'COLEGIO SANTA INES',
			'43105106' => 'COL LA SALLE SAO JOAO',
			'43105408' => 'COLEGIO ADVENTISTA DE PORTO ALEGRE',
			'43105637' => 'COL ADVENTISTA MARECHAL RONDON',
			'43107419' => 'COLEGIO MAE DE DEUS',
			'43107559' => 'INSTITUTO SANTA LUZIA',
			'43108709' => 'COL EST ELPIDIO FERREIRA PAES',
			'43108903' => 'ESC EST DE ENS MEDIO PRESIDENTE COSTA E SILVA',
			'43109152' => 'COL SAO JOSE DE MURIALDO - ESC DE EDUCACAO BASICA',
			'43178740' => 'COLEGIO MARISTA IPANEMA',
			'43104975' => 'CENT ENS MEDIO FARROUPILHA',
			'43104924' => 'COLEGIO ANCHIETA',
			'43109187' => 'CE PROFESSOR ELMANO LAUFFER LEAL',
			'43108164' => 'COL MARISTA NOSSA SENHORA DO ROSARIO',
			'43172423' => 'COLEGIO LEONARDO DA VINCI-ALFA',
			'43105335' => 'ESC ENS MEDIO MARIA IMACULADA',
			'43172440' => 'COLEGIO LEONARDO DA VINCI-BETA',
			'43203876' => 'COL UNIFICADO LINDOIA',
			'43105572' => 'EEEM 1 DE MAIO',
			'43105807' => 'EEEB ALMIRANTE BACELAR',
			'43105815' => 'EEEM ALMIRANTE BARROSO',
			'43106048' => 'CE CARLOS FAGUNDES DE MELLO',
			'43106196' => 'EEEM RAFAELA REMIAO',
			'43106420' => 'ESC EST DE EDUCACAO BASICA FERNANDO GOMES',
			'43106439' => 'EEEM OSCAR COELHO DE SOUZA',
			'43106498' => 'ESC EST EDUC BASICA GOMES CARNEIRO',
			'43106587' => 'EEEM ITALIA',
			'43106943' => 'CE PARANA',
			'43107125' => 'EEEM PROFESSOR SARMENTO LEITE',
			'43107354' => 'EEEM VISCONDE DO RIO GRANDE',
			'43107850' => 'EEEF SENADOR PASQUALINI',
			'43107990' => 'EEEM REPUBLICA ARGENTINA',
			'43108318' => 'EEEM BALTAZAR DE OLIVEIRA GARCIA',
			'43108741' => 'CE GENERAL ALVARO ALVES DA SILVA BRAGA',
			'43108750' => 'EEEM OTAVIO ROCHA',
			'43180728' => 'EEEF TOM JOBIM',
			'43002803' => 'COLEGIO MARISTA IRMAO JAIME BIAZUS',
			'43105092' => 'ESC TEC SANTO INACIO',
			'43107400' => 'ESC DE EDUCACAO BASICA MAE ADMIRAVEL',
			'43109136' => 'ESCO PANAMERICANA EDUC BASICA',
			'43172393' => 'ESCOLA DE ENSINO MEDIO VINICIUS DE MORAIS',
			'43105718' => 'COLEGIO MAUA - COOPEM - COOPERATIVA EDUCACIONAL MAUA LTDA',
			'43166822' => 'EEEM ACORIANOS',
			'43167357' => 'ESCOLA ESTADUAL TECNICA DE AGRICULTURA',
			'43167365' => 'ESCOLA ESTADUAL DE ENSINO MEDIO BARAO DE LUCENA',
			'43167586' => 'EEEM GOVERNADOR WALTER JOBIM',
			'43170579' => 'ESC EST ENS MED AYRTON SENNA DA SILVA',
			'43167837' => 'COLEGIO ADVENTISTA DE VIAMAO - EDUC INF ENS FUNDAMENTAL E MEDIO',
			'43167403' => 'ESC EST DE ENS MEDIO SETEMBRINA',
			'43167543' => 'COLEGIO EST ALCEBIADES AZEREDO DOS SANTOS',
			'43167284' => 'COLEGIO STELLA MARIS',
			'43169929' => 'COLEGIO ESTADUAL CECILIA MEIRELES',
			'43167411' => 'EEEM PROF TOLENTINO MAIA',
			'43167489' => 'ESC EST DE ENS MEDIO FARROUPILHA',
			'43167330' => 'ESC EST ENS MEDIO FRANCISCO CANQUERINI',
			'43167446' => 'EEEM DR GENESIO PIRES',
			'43167764' => 'IEE ISABEL DE ESPANHA',
			'43378099' => 'EEEM SANTA ISABEL',
			'43180825' => 'E E M UNIENSINO',
			'43167772' => 'COLEGIO MARISTA NOSSA SENHORA DAS GRACAS',
			'43167306' => 'EEEM PROFESSORA CELIA FLORES LAVRA PINTO',
			'43167390' => 'EEEM MINUANO',
			'43167420' => 'EEEM NISIA FLORESTA',
			'43167810' => 'EEEM ORIETA',
			'43295258' => 'E E M VIAMOPOLIS'
		);


		return (isset($data[$codigo])) ? $data[$codigo] : false;

	}

	public function getStatusEvento($valor) {
	    switch ($valor) {
			case 'aberto':
				return 'Aguardando aprovação';
				break;

			case 'confirmado':
				return 'Aprovado';
				break;

			case 'cancelado':
				return 'Não Aprovado';
				break;
		}
	}

	public function getSacTipoLead($valor) {
	    switch ($valor) {
			case 'aluno':
				return 'Aluno';
				break;

			case 'professor':
				return 'Professor';
				break;

			case 'funcionario':
				return 'Funcionário';
				break;

			case 'publico':
				return 'Público Externo';
				break;
		}
	}

	//SEMPRE manter uma chave diferente (dificilmente será alterado, pois a ideia é haver apenas portal e hotsite)
	public function getSites() {
	    return array(
	    			'portal' 			=> 'Portal', 
	    			'hotsite' 			=> 'Hotsite', 
	    			'semipresencial' 	=> 'Semipresencial'
	    			);
	}

	public function getBtnBySite($site){
		switch ($site) {
			case 'portal':
				return '<span class="btn btn-xs btn-success">' . $site . '</span>';
				break;

			case 'hotsite':
				return '<span class="btn btn-xs btn-info">' . $site . '</span>';
				break;

			case 'semipresencial':
				return '<span class="btn btn-xs btn-primary">' . $site . '</span>';
				break;
		}
	}
	
	public function getDestinoIntercambio($valor) {
	    switch ($valor) {
			case 'alemanha':
				return 'Alemanha';
				break;

			case 'irlanda':
				return 'Irlanda';
				break;
		}
	}
	
	public function getDestinoIntercambioList() {
	    return array('alemanha' => 'Alemanha', 'irlanda' => 'Irlanda');
	}

	/**
	* Formata data para a string date passada
	*
	* @param string Formato (utilizar nota��o do PHP), se n�o informada gera DateTime Sem Timezone
	* @param string Data
	* @param string Data relativa (quando necess�rio para c�lculo, como "pr�xima segunda")
	*/
	public function DataFormatada($formato=null,$data=null,$relative=null){
		if(is_null($data)) $data = time();
		if(is_null($formato)) $formato = 'Y-m-d H:i:s';
		return date($formato,$this->DataToTime($data,$relative));
	}

	/**
	* Retorna o timestamp da data informada, é uma versão otimizada do strtotime
	*
	* @param string Data
	* @param string Data relativa (quando necessário para cálculo, como "próxima segunda")
	*/
	private function DataToTime($data,$relative=null){
		if(is_numeric($data)) return $data;
		if(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{4}|[0-9]{2})[ \t\n\r]*([0-9]{2})[ \t\n\r]*(:|h|hour|hours|horas|hr)[ \t\n\r]*([0-9]{2})[ \t\n\r]*(:|m|min|mins|minutos|minuto|mn)[ \t\n\r]*([0-9]{2})/i',$data,$m)){
			$hora = $m[4]; $minuto = $m[6]; $segundo = $m[8]; $dia = $m[1]; $mes = $m[2]; $ano = $m[3];
		}elseif(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{4}|[0-9]{2})[ \t\n\r]*([0-9]{2})[ \t\n\r]*(:|h|hour|hours|horas|hr)[ \t\n\r]*([0-9]{2})[ \t\n\r]*(:|m|min|mins|minutos|minuto|mn)/i',$data,$m)){
			$hora = $m[4]; $minuto = $m[6]; $segundo = 0; $dia = $m[1]; $mes = $m[2]; $ano = $m[3];
		}elseif(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{4}|[0-9]{2})[ \t\n\r]*([0-9]{2})[ \t\n\r]*(:|h|hour|hours|horas|hr)/i',$data,$m)){
			$hora = $m[4]; $minuto = 0; $segundo = 0; $dia = $m[1]; $mes = $m[2]; $ano = $m[3];
		}elseif(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{4}|[0-9]{2})/',$data,$m)){
			$hora = 0; $minuto = 0; $segundo = 0; $dia = $m[1]; $mes = $m[2]; $ano = $m[3];
		}elseif(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{1,2})/',$data,$m)){
			$hora = 0; $minuto = 0; $segundo = 0; $dia = $m[1]; $mes = $m[2]; $ano = date('Y',is_null($relative)?time():$relative);
		}elseif(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{4})/',$data,$m)){
			$hora = 0; $minuto = 0; $segundo = 0; $dia = 1; $mes = $m[1]; $ano = $m[2];
		}else{
			$tr = array(
				'hoje'=>'today','dia'=>'day', 'semana'=>'week', 'hora'=>'hour', 'minuto'=>'minute', 'segundo'=>'second',
				'meses'=>'months','mês'=>'month','mes'=>'month', 'ano'=>'year',
				'proxima'=>'next', 'próxima'=>'next',  'próximo'=>'next',  'próximo'=>'next', 
				'Última'=>'last', 'ultima'=>'last','ultimo'=>'last','Último'=>'last',
				'segunda-feira'=>'monday','segunda'=>'monday','terça-feira'=>'tuesday','terça'=>'tuesday',
				'quarta-feira'=>'wednesday','quarta'=>'wednesday', 'quinta-feira'=>'thursday','quinta'=>'thursday', 
				'sexta-feira'=>'friday','sexta'=>'friday', 'sábado'=>'saturday','sabado'=>'saturday', 'domingo'=>'sunday',
				'janeiro'=>'january', 'jan'=>'january', 'fevereiro'=>'february','fev'=>'february', 
				'março'=>'march','mar'=>'march', 'abril'=>'april', 'abr'=>'april',
				'maio'=>'may','mai'=>'may', 'junho'=>'june', 'jun'=>'june', 
				'julho'=>'july','jul'=>'july', 'agosto'=>'august', 'ago'=>'august', 
				'setembro'=>'september','set'=>'september', 'outubro'=>'october', 'out'=>'october', 
				'novembro'=>'november','nov'=>'november', 'dezembro'=>'december','dez'=>'december',
				'depois de amanhã'=>'+2 day','depois de amanha'=>'+2 day',
				'anteontem'=>'-2 day',
				'amanhã'=>'tomorrow','amanha'=>'tomorrow','ontem'=>'yesterday',' de '=>''
			);
			return strtotime(str_ireplace(array_keys($tr),array_values($tr),$data),is_null($relative)?time():($this->DataToTime($relative)));		
		}
		return mktime($hora,$minuto,$segundo,$mes,$dia,$ano);
	}

	public function diasemana($diasemana) {
		switch($diasemana) {
			case"0": $diasemana = "Domingo";       break;
			case"1": $diasemana = "Segunda-Feira"; break;
			case"2": $diasemana = "Terça-Feira";   break;
			case"3": $diasemana = "Quarta-Feira";  break;
			case"4": $diasemana = "Quinta-Feira";  break;
			case"5": $diasemana = "Sexta-Feira";   break;
			case"6": $diasemana = "Sábado";        break;
			case"7": $diasemana = "Domingo";        break;
		}
		return $diasemana;
	}

	public function mes($dia_mes, $abre = false) {
		switch($dia_mes) {
			case"1": 
				$dia_mes = ($abre == true) ? "JAN" : "Janeiro"; 	
				break;
			case"2": 
				$dia_mes = ($abre == true) ? "FEV" : "Fevereiro";  
				break;
			case"3": 
				$dia_mes = ($abre == true) ? "MAR" : "Março";  	
				break;
			case"4": 
				$dia_mes = ($abre == true) ? "ABR" : "Abril";  	
				break;
			case"5": 
				$dia_mes = ($abre == true) ? "MAI" : "Maio";   	
				break;
			case"6": 
				$dia_mes = ($abre == true) ? "JUN" : "Junho";      
				break;
			case"7": 
				$dia_mes = ($abre == true) ? "JUL" : "Julho";      
				break;
			case"8": 
				$dia_mes = ($abre == true) ? "AGO" : "Agosto";     
				break;
			case"9": 
				$dia_mes = ($abre == true) ? "SET" : "Setembro";   
				break;
			case"10": 
				$dia_mes = ($abre == true) ? "OUT" : "Outubro";   
				break;
			case"11": 
				$dia_mes = ($abre == true) ? "NOV" : "Novembro";  
				break;
			case"12": 
				$dia_mes = ($abre == true) ? "DEZ" : "Dezembro";  
				break;
		}
		return $dia_mes;
	}

	public function truncate_str($str, $maxlen) {
		if ( strlen($str) <= $maxlen ) return $str;

		$newstr = substr($str, 0, $maxlen);
		if ( substr($newstr,-1,1) != ' ' ) 
			$newstr = substr($newstr, 0, strrpos($newstr, " "));

		return $newstr.'...';
	}

	public function mask($val, $mask){
	 	$maskared = '';
	 	$k = 0;
	 	for($i = 0; $i<=strlen($mask)-1; $i++){
	 		if($mask[$i] == '#'){
	 			if(isset($val[$k])){
	 				$maskared .= $val[$k++];
	 			}
	 		}else{
	 			if(isset($mask[$i])){
	 				$maskared .= $mask[$i];
	 			}
	 		}
	 	}
	 	return $maskared;
	}

	public function moedaToBco($valor) {
        return (float) str_replace(",", ".", str_replace(".", "", $valor));
    }

    public function bcoToMoeda($valor, $prec = 2) {
        return number_format($valor, $prec, ",", ".");
    }
}
