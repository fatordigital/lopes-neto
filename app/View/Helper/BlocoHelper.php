<?php

class BlocoHelper extends Helper
{

    public $bloco;
    public $bloco_de_conteudo;

    private $idioma_id;

    public function __construct(View $view, $settings = array())
    {
        parent::__construct($view, $settings);

        $this->Bloco = ClassRegistry::init('FdBlocos.Bloco');
        $this->Idioma = ClassRegistry::init('Idioma');
    }

    public function traduzir_bloco($slug, $key = '')
    {
        $idioma_id = $this->Idioma->atual('id');
        $bloco = $this->Bloco->cacheData;

        //Procura o bloco pelo idioma que o usuário está
        foreach ($bloco[$slug] as $item) {
            if ($item['idioma_id'] == $idioma_id) {
                return $key && !empty($key) ? $item[$key] : $item;
            }
        }

        $idioma_padrao_id = $this->Idioma->padrao('id');

        //Procura o bloco pelo idioma padrão
        foreach ($bloco[$slug] as $item) {
            if ($item['idioma_id'] == $idioma_padrao_id) {
                return $key && !empty($key) ? $item[$key] : $item;
            }
        }

        //Procura o bloco que tem conteúdo
        foreach ($bloco[$slug] as $item) {
            if ($item['conteudo'] != '') {
                return $key && !empty($key) ? $item[$key] : $item;
            }
        }

    }

    public function bloco($slug = '', $key = '')
    {
        if (!empty($slug) && $this->Bloco->exist($slug)) {
            return $this->traduzir_bloco($slug, $key);
        }
        return Configure::read('debug') == 2 ? 'Bloco não fornecido' : '';
    }

}