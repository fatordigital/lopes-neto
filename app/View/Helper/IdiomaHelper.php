<?php
App::uses('Helper', 'View');

class IdiomaHelper extends Helper
{

    private $atual_idioma_id;

    public $slug;


    public function __construct(View $view, $settings = array())
    {
        parent::__construct($view, $settings);

        $this->Idioma = ClassRegistry::init('Idioma');
        $this->Palavra = ClassRegistry::init('FdBlocos.Palavra');
        $this->Setting = ClassRegistry::init('FdSettings.Setting');
        $this->Setting->loadSettings();

        if (Configure::read('Site.Idioma.Padrao') == '') {
            if (!preg_match('/fatorcms/', Router::url(null, true))) {
                $this->atual_idioma_id = $this->Idioma->atual('id');
                $this->slug = $this->Idioma->atual('slug');
            } else {
                $this->atual_idioma_id = $this->Idioma->padrao('id');
                $this->slug = $this->Idioma->atual('slug');
            }
        } else {
            $this->slug = '';
        }
    }

    public function url($path = NULL, $full = false)
    {
        return parent::url(($this->slug != '' ? '/' . $this->slug : '') . '/' . $path, true);
    }

    public function troca_idioma_url_atual($idioma)
    {
        return str_replace('/' . $this->slug . '/', '/' . $idioma . '/', Router::url(null, true));
    }

    public function palavra($slug)
    {
        $idioma_padrao_id = $this->Idioma->padrao('id');


        if (isset($this->Palavra->cacheData[$slug])) {

            foreach ($this->Palavra->cacheData[$slug] as $palavra) {
                if ($palavra['idioma_id'] == $this->atual_idioma_id && !empty($palavra['titulo'])) {
                    return $palavra['titulo'];
                }
            }

            foreach ($this->Palavra->cacheData[$slug] as $palavra) {
                if ($palavra['idioma_id'] == $this->atual_idioma_id && !empty($palavra['titulo'])) {
                    return $palavra['titulo'];
                }
            }

            foreach ($this->Palavra->cacheData[$slug] as $palavra) {
                if (!empty($palavra['titulo'])) {
                    return $palavra['titulo'];
                }
            }
        }

        return 'Palavra não existe';
    }

    public function extrair($dados, $indice, $alias = false)
    {
        if (!$alias) {
            if (isset($dados)) {
                foreach ($dados as $dado) {
                    if ($dado['idioma_id'] == $this->atual_idioma_id && !empty($dado[$indice])) {
                        return $dado[$indice];
                    }
                }

                foreach ($dados as $dado) {
                    if (!empty($dado[$indice])) {
                        return $dado[$indice];
                    }
                }
            }
        } else {
            if (isset($dados)) {
                foreach ($dados as $dado) {
                    if ($dado[$alias]['idioma_id'] == $this->atual_idioma_id && !empty($dado[$alias][$indice])) {
                        return $dado[$alias][$indice];
                    }
                }

                foreach ($dados as $dado) {
                    if (!empty($dado[$alias][$indice])) {
                        return $dado[$alias][$indice];
                    }
                }
            }
        }

        return 'Registro não encontrado.';
    }

}