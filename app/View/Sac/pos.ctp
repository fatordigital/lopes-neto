<?php echo $this->assign('title', 'Pós Graduação'); ?>
<div id="pagina">
    <div id="form" style="margin-top: 80px">
        <?php echo $this->Form->create('LpPos', array('url' => $this->Html->Url(null, true), 'class' => 'form-validate')); ?>
            <?php echo $this->Session->flash() ?>

            <div class="fd_key"><?php echo $this->Form->input('fd'); ?></div>
           
            <div class="border">
                <div id="primeiro_parente">
                    <?php echo $this->Form->input('nome', array(
                                'div'                   => false,
                                'type'                  => 'text',
                                'class'                 => 'form-control',
                                'data-rule-required'    => 'true',
                                'data-msg-required'     => 'Informe seu nome',
                                'required'              => false,
                                'label'                 => 'NOME COMPLETO *'
                            )
                    ); ?>
					
					<?php echo $this->Form->input('email', array(
                                'div'                   => false,
                                'type'                  => 'text',
                                'class'                 => 'form-control',
                                'data-rule-required'    => 'true',
                                'data-rule-email'       => 'true',
                                'data-msg-required'     => 'Informe o seu e-mail',
                                'data-msg-email'        => 'Informe um e-mail válido',
                                'required'              => false,
                                'label'                 => 'E-MAIL *'
                            )
                    ); ?>
					
					<?php echo $this->Form->input('email_confirma', array(
                                'div'                   => false,
                                'type'                  => 'text',
                                'class'                 => 'form-control',
                                'data-rule-required'    => 'true',
                                'data-rule-email'       => 'true',
                                'data-msg-required'     => 'Informe o seu e-mail',
                                'data-msg-email'        => 'Informe um e-mail válido',
								'data-msg-equalTo'     	=> 'E-mails não conferem',
								'equalTo' 			    => '[name="data[LpPos][email]"]',
                                'required'              => false,
                                'label'                 => 'CONFIRME O E-MAIL *'
                            )
                    ); ?>

                    <?php echo $this->Form->input('telefone', array(
                                'div'                   => false,
                                'type'                  => 'text',
                                'class'                 => 'form-control',
                                'data-rule-required'    => 'true',
                                'data-msg-required'     => 'Informe o seu telefone',
                                'required'              => false,
                                'label'                 => 'TELEFONE *'
                            )
                    ); ?>

					<?php echo $this->Form->input('empresa', array(
                                'div'                   => false,
                                'type'                  => 'text',
                                'class'                 => 'form-control',
                                'data-rule-required'    => 'true',
                                'data-msg-required'     => 'Informe a sua empresa',
                                'required'              => false,
                                'label'                 => 'EMPRESA *'
                            )
                    ); ?>

                   <?php echo $this->Form->input('contato_para', array(
                                'div'                   => false,
                                'type'                  => 'text',
                                'class'                 => 'form-control',
                                'data-rule-required'    => 'true',
                                'data-msg-required'     => 'Preenchimento obrigatório',
                                'required'              => false,
                                'label'                 => 'CONTATO PARA *'
                            )
                    ); ?>
					
					<?php echo $this->Form->input('comentario', array(
                                'div'                   => false,
                                'type'                  => 'textarea',
                                'class'                 => 'form-control',
                                'data-rule-required'    => 'true',
                                'data-msg-required'     => 'Preenchimento obrigatório',
                                'required'              => false,
                                'label'                 => 'DEIXE SEU COMENTÁRIO *'
                            )
                    ); ?>
					
                </div>
            </div>
			
            <button type="submit" class="pull-right" style="float: right;">ENVIAR</button>
        <?php echo $this->Form->end(); ?>
    </div>
</div>