<?php echo $this->assign('title', 'Indicação Pós'); ?>
<div id="pagina">
    <div id="form" style="margin-top: 80px">
        <?php echo $this->Form->create('LpFuncionario', array('url' => $this->Html->Url(null, true), 'class' => 'form-validate')); ?>
            <?php echo $this->Session->flash() ?>

            <div class="fd_key"><?php echo $this->Form->input('fd'); ?></div>
            <div class="border">
                <?php echo $this->Form->input('nome_funcionario', array(
                            'div'                   => false,
                            'type'                  => 'text',
                            'class'                 => 'form-control',
                            'data-rule-required'    => 'true',
                            'data-msg-required'     => 'Informe o nome do funcionário',
                            'required'              => false,
                            'label'                 => 'Nome do Funcionário'
                        )
                ); ?>
            </div>
            
            <div class="border">
                <p>PRIMEIRA INDICAÇÃO</p>
                <div id="primeiro_parente">
                    <?php echo $this->Form->input('nome_primeiro', array(
                                'div'                   => false,
                                'type'                  => 'text',
                                'class'                 => 'form-control',
                                'data-rule-required'    => 'true',
                                'data-msg-required'     => 'Informe o nome da indicação',
                                'required'              => false,
                                'label'                 => 'Nome da Indicação'
                            )
                    ); ?>

                    <?php echo $this->Form->input('telefone_primeiro', array(
                                'div'                   => false,
                                'type'                  => 'text',
                                'class'                 => 'form-control',
                                'data-rule-required'    => 'true',
                                'data-msg-required'     => 'Informe o telefone',
                                'required'              => false,
                                'label'                 => 'Telefone'
                            )
                    ); ?>

                    <?php echo $this->Form->input('curso_ies_primeiro', array(
                                'div'                   => false,
                                'type'                  => 'text',
                                'class'                 => 'form-control',
                                'data-rule-required'    => 'true',
                                'data-msg-required'     => 'Informe o curso',
                                'required'              => false,
                                'label'                 => 'Curso'
                            )
                    ); ?>

                    <?php echo $this->Form->input('instituicao_primeiro', array(
                                'div'                   => false,
                                'type'                  => 'text',
                                'class'                 => 'form-control',
                                'data-rule-required'    => 'true',
                                'data-msg-required'     => 'Informe a instituição',
                                'required'              => false,
                                'label'                 => 'Instituição/Campus'
                            )
                    ); ?>

                    <?php echo $this->Form->input('email_primeiro', array(
                                'div'                   => false,
                                'type'                  => 'text',
                                'class'                 => 'form-control',
                                'data-rule-required'    => 'true',
                                'data-rule-email'       => 'true',
                                'data-msg-required'     => 'Informe o seu e-mail',
                                'data-msg-email'        => 'Informe um e-mail válido',
                                'required'              => false,
                                'label'                 => 'E-mail'
                            )
                    ); ?>
                </div>
            </div>

            <?php /*
            <div class="border segundo_parente" rel="hide">
                <p>SEGUNDA INDICAÇÃO</p>
                <div id="segundo_parente">
                    <?php echo $this->Form->input('nome_segundo', array(
                                'div'                   => false,
                                'type'                  => 'text',
                                'class'                 => 'form-control',
                                'data-rule-required'    => 'true',
                                'data-msg-required'     => 'Informe o nome da indicação',
                                'required'              => false,
                                'label'                 => 'Nome da Indicação'
                            )
                    ); ?>

                    <?php echo $this->Form->input('telefone_segundo', array(
                                'div'                   => false,
                                'type'                  => 'text',
                                'class'                 => 'form-control',
                                'data-rule-required'    => 'true',
                                'data-msg-required'     => 'Informe o telefone',
                                'required'              => false,
                                'label'                 => 'Telefone'
                            )
                    ); ?>

                    <?php echo $this->Form->input('curso_ies_segundo', array(
                                'div'                   => false,
                                'type'                  => 'text',
                                'class'                 => 'form-control',
                                'data-rule-required'    => 'true',
                                'data-msg-required'     => 'Informe o curso',
                                'required'              => false,
                                'label'                 => 'Curso'
                            )
                    ); ?>

                    <?php echo $this->Form->input('instituicao_segundo', array(
                                'div'                   => false,
                                'type'                  => 'text',
                                'class'                 => 'form-control',
                                'data-rule-required'    => 'true',
                                'data-msg-required'     => 'Informe a instituição',
                                'required'              => false,
                                'label'                 => 'Instituição/Campus'
                            )
                    ); ?>

                    <?php echo $this->Form->input('email_segundo', array(
                                'div'                   => false,
                                'type'                  => 'text',
                                'class'                 => 'form-control',
                                'data-rule-required'    => 'true',
                                'data-rule-email'       => 'true',
                                'data-msg-required'     => 'Informe o seu e-mail',
                                'data-msg-email'        => 'Informe um e-mail válido',
                                'required'              => false,
                                'label'                 => 'E-mail'
                            )
                    ); ?>
                </div>
            </div>

            <div class="border terceira_parente" rel="hide">
                <p>TERCEIRA INDICAÇÃO</p>
                <div id="terceira_parente">
                    <?php echo $this->Form->input('nome_terceiro', array(
                                'div'                   => false,
                                'type'                  => 'text',
                                'class'                 => 'form-control',
                                'data-rule-required'    => 'true',
                                'data-msg-required'     => 'Informe o nome da indicação',
                                'required'              => false,
                                'label'                 => 'Nome da Indicação'
                            )
                    ); ?>

                    <?php echo $this->Form->input('telefone_terceiro', array(
                                'div'                   => false,
                                'type'                  => 'text',
                                'class'                 => 'form-control',
                                'data-rule-required'    => 'true',
                                'data-msg-required'     => 'Informe o telefone',
                                'required'              => false,
                                'label'                 => 'Telefone'
                            )
                    ); ?>

                    <?php echo $this->Form->input('curso_ies_terceiro', array(
                                'div'                   => false,
                                'type'                  => 'text',
                                'class'                 => 'form-control',
                                'data-rule-required'    => 'true',
                                'data-msg-required'     => 'Informe o curso',
                                'required'              => false,
                                'label'                 => 'Curso'
                            )
                    ); ?>

                    <?php echo $this->Form->input('instituicao_terceiro', array(
                                'div'                   => false,
                                'type'                  => 'text',
                                'class'                 => 'form-control',
                                'data-rule-required'    => 'true',
                                'data-msg-required'     => 'Informe a instituição',
                                'required'              => false,
                                'label'                 => 'Instituição/Campus'
                            )
                    ); ?>

                    <?php echo $this->Form->input('email_terceiro', array(
                                'div'                   => false,
                                'type'                  => 'text',
                                'class'                 => 'form-control',
                                'data-rule-required'    => 'true',
                                'data-rule-email'       => 'true',
                                'data-msg-required'     => 'Informe o seu e-mail',
                                'data-msg-email'        => 'Informe um e-mail válido',
                                'required'              => false,
                                'label'                 => 'E-mail'
                            )
                    ); ?>
                </div>
            </div>*/ ?>

            <button type="submit" class="pull-right" style="float: right; margin-top: 10px;">ENVIAR</button>
        <?php echo $this->Form->end(); ?>
    </div>
</div>