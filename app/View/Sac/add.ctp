<?php echo $this->assign('title', 'Fale Conosco'); ?>

	<?php echo $this->element('site/banner_curso'); ?>

	<div class="row row-title">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h1>Fale Conosco</h1>
			</div>
		</div>
	</div>
	<div class="row row-tabs-gray">
		<div class="container">
			<?php echo $this->element('site/breadcrumb') ?>
		</div>
	</div>
	<div class="row row-tab-body laurete">
		<div class="container">			
			<div role="tabpanel">
			  <!-- Nav tabs -->
			  <ul class="nav nav-tabs" role="tablist">
			    <li role="presentation" class="active"><a href="#apresentacao" aria-controls="home" role="tab" data-toggle="tab" >Fale Conosco <span class="seta-tab glyphicon glyphicon-triangle-bottom pull-right triangle-red"></span></a></li>
			  </ul>
			  <!-- Tab panes -->
			  <div class="tab-content">
			  	<div role="tabpanel" class="tab-pane active" id="apresentacao">
		  			
		  			<?php foreach($cursos_e_campi['campi_cursos'] as $key => $campi): ?>
			  			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 address">
			  				<div class="div-my-campi">
				  				<figure>
				  					<?php echo $this->Image->show($campi['Campus']['logo_red_path'] .'/'. $campi['Campus']['logo_red_dir'] . '/' . $campi['Campus']['logo_red'], array('alt' => $campi['Campus']['nome'], 'class' => 'img-responsive')); ?>
				  				</figure>
				    			<h4 class="red"><?php echo $campi['Campus']['nome']; ?></h4>
				    			<p>
				    				<?php echo $campi['Campus']['endereco']; ?>, <?php echo $campi['Campus']['numero']; ?>,  
			                        <?php if($campi['Campus']['bairro'] != ""): ?>
			                            <?php echo $campi['Campus']['bairro']; ?> 
			                        <?php endIf; ?>

			                        - <?php echo $campi['Campus']['cidade']; ?>/<?php echo $campi['Campus']['uf']; ?>.
			                        
			                        <?php if($campi['Campus']['cep'] != ""): ?>
			                        	CEP <?php echo $campi['Campus']['cep']; ?>
			                        <?php endIf; ?>
								</p>

								<?php if($campi['Campus']['telefone1'] != ""): ?>
									<h5 class="red">
										<?php echo $campi['Campus']['telefone1']; ?>

										<?php if($campi['Campus']['telefone2'] != ""): ?>
				                        	| <?php echo $campi['Campus']['telefone2']; ?>
				                        <?php endIf; ?>
									</h5>
								<?php endIf; ?>
							</div>
			    		</div>
		    		<?php endForeach; ?>

		    		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<?php if(isset($send_goal_sac) && $send_goal_sac == true): ?>
							<?php $goal_tipo = 'fale-conosco'; ?>
							<?php $goal_objetivo = strtolower($this->params->url); ?>
							<?php echo $this->element('site/goal', array('goal_tipo' => $goal_tipo, 'goal_objetivo' => $goal_objetivo)); ?>
						<?php endIf; ?>
		    			
		    			<?php echo $this->Form->create('Sac', array('url' => $this->Html->Url(null, true), 'class' => 'contact sac form-validate')); ?>
		    				<?php echo $this->Session->flash() ?>

		    				<div class="fd_key"><?php echo $this->Form->input('fd'); ?></div>
		    				<div class="div-radios">
			    				<p>Você é:</p>

			    				<?php echo $this->Form->input('lead_tipo', array(
																	'type' => 'radio', 
																	'options' => array('aluno' => 'Aluno', 'professor' => 'Professor', 'funcionario' => 'Funcionário', 'publico' => 'Público Externo'), 
																	'default' 				=> 'publico', 
																	'div' 					=> false,
																	'data-rule-required'    => 'true',
																	'data-msg-required'     => 'Informe quem você é',
																	'legend' => false)); ?>

			    			</div>
		    				<div class="form-group">
		    					<?php echo $this->Form->input('nome', array(
											'div'                   => false,
											'type'                  => 'text',
											'label'                 => false,
											'class'                 => 'form-control',
											'data-rule-required'    => 'true',
											'data-msg-required'     => 'Informe o seu nome',
											'required'              => false,
											'placeholder'           => 'Seu nome'
										)
								); ?>
		    				</div>
		    				<div class="form-group">
		    					<?php echo $this->Form->input('email', array(
											'div'                   => false,
											'type'                  => 'text',
											'label'                 => false,
											'class'                 => 'form-control',
											'data-rule-required'    => 'true',
											'data-rule-email'       => 'true',
											'data-msg-required'     => 'Informe o seu e-mail',
											'data-msg-email'        => 'Informe um e-mail válido',
											'required'              => false,
											'placeholder'           => 'Seu e-mail'
										)
								); ?>
		    				</div>
		    				<div class="form-group">
		    					<?php echo $this->Form->input('telefone', array(
															'div'                   => false,
															'type'                  => 'text',
															'label'                 => false,
															'class'                 => 'form-control phone_with_ddd',
															'data-rule-required'    => 'true',
															'data-msg-required'     => 'Informe o seu telefone',
															'required'              => false,
															'placeholder'           => 'Seu telefone'
														)
												); ?>
		    				</div>		    				
		    				<div class="form-group">
		    					<?php echo $this->Form->input('mensagem', array(
											'div'                   => false,
											'type'                  => 'textarea',
											'label'                 => false,
											'class'                 => 'form-control',
											'data-rule-required'    => 'true',
											'data-msg-required'     => 'Informe o sua mensagem',
											'required'              => false,
											'placeholder'           => 'Mensagem'
										)
								); ?>
		    				</div>
		    				<button type="submit" class="pull-left">Enviar</button>
		    			<?php echo $this->Form->end(); ?>
		    			<br/><br/><br/><br/><br/>
		    			<a href="http://www.uniritter.edu.br/ouvidoria" class="btn button-red btn-ouvidoria">Clique aqui para entrar em contato com a ouvidoria</a>		    			
		    		</div>

		    		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		    			<div id="map-canvas"></div>
		    		</div>

			  	</div>			  	
			</div>						
		</div>		
	</div>	
</div>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script src="assets/js/lib/infobox.js"></script>
<script src="assets/js/lib/markerclusterer.js"></script>
 <script>
	var map;
	var markers = [];
	var latlngbounds = new google.maps.LatLngBounds();
	var image = { url: 'assets/images/icons/icon-pin.png' };
	var pontos = <?php echo $json_campi; ?>;

	function initialize() {
		var mapOptions = {
			zoom: 8,
			scrollwheel: false,
			center: new google.maps.LatLng(-29.909912, -51.2176584),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
	    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	}

	var idInfoBoxAberto;
	var infoBox = [];
	 
	function abrirInfoBox(id, marker) {
	    if (typeof(idInfoBoxAberto) == 'number' && typeof(infoBox[idInfoBoxAberto]) == 'object') {
	        infoBox[idInfoBoxAberto].close();
	    }
	 
	    infoBox[id].open(map, marker);
	    idInfoBoxAberto = id;
	}

	function carregarPontos() { 		 
        $.each(pontos, function(index, ponto) {
         	 var marker = new google.maps.Marker({
                position: new google.maps.LatLng(ponto.Latitude,ponto.Longitude),
                title: "Uniritter",
                map: map,
                icon: image,
            });

         	markers.push(marker);
         	latlngbounds.extend(marker.position);

         	infoBox[ponto.Id] = new google.maps.InfoWindow(), marker; 
         	infoBox[ponto.Id].setContent(ponto.Descricao);
            infoBox[ponto.Id].marker = marker;
            infoBox[ponto.Id].listener = google.maps.event.addListener(marker, 'click', function(e) {
                abrirInfoBox(ponto.Id, marker);
            });
         	
            // var infowindow = new google.maps.InfoWindow(), marker; 
            // infowindow.setContent(ponto[i].Descricao);
			// google.maps.event.addListener(marker, 'click', (function(marker, x) {
			// 	console.log(i);
			//     return function() {
			//     	console.log(marker);
			//     	console.log(x);
			//     	console.log(i);
			//     	// infowindow.setContent(ponto[i].Descricao);
			//     	infowindow.open(map, marker);
			//     }
			// })(marker))	
        }); 	
        
        var markerCluster = new MarkerClusterer(map, markers);
        map.fitBounds(latlngbounds);           
	}

	initialize();
	carregarPontos();
</script>