<?php if(isset($send_goal_sac) && $send_goal_sac == true): ?>
    <?php $goal_tipo = 'lp'; ?>
    <?php $goal_objetivo = strtolower($this->params->url); ?>
    <?php echo $this->element('site/goal', array('goal_tipo' => $goal_tipo, 'goal_objetivo' => $goal_objetivo)); ?>
<?php endIf; ?>

<?php echo $this->assign('title', 'Venha Estudar na UniRitter'); ?>

<section class="venha-estudar">
    <h6>
        VENHA ESTUDAR<br>NA UNIRITTER
    </h6>
</section><!-- end .venha-estudar -->

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12 col-md-4 fundo">
            <div class="desconto">
                <h3>
                    PRIMEIRA
                </h3>
                <p class="bg-primary">MENSALIDADE</p>
                <p>ISENTA</p>
            </div><!-- end .desconto -->
        </div><!-- end .col-xs-12.col-sm-6 -->
        <div class="col-xs-12 col-md-8">
            <section class="form">
                <div class="form--content">
                    <?php echo $this->Form->create('LpFone', array('class' => 'form-validate')); ?>
                        <div class="fd_key"><?php echo $this->Form->input('fd'); ?></div>
                        <h3>LIGA AGORA</h3>
                        <p class="form--telefone">0800 642 4000</p>
                        <p class="form--telefone-texto">Ou preenche o formulário abaixo e entraremos em contato com você!</p>
                        <div class="form-group">
                             <?php echo $this->Form->input('telefone', array(
                                        'div'                   => false,
                                        'type'                  => 'text',
                                        'class'                 => 'form-control',
                                        'data-rule-required'    => 'true',
                                        'data-msg-required'     => 'Informe seu telefone',
                                        'required'              => false,
                                        'label'                 => false,
                                        'placeholder'           => 'Telefone'
                                    )
                            ); ?>

                             <?php echo $this->Form->input('horario', array(
                                        'div'                   => false,
                                        'type'                  => 'select',
                                        'class'                 => 'form-control',
                                        'data-rule-required'    => 'true',
                                        'data-msg-required'     => 'Informe o melhor horário de atendimento',
                                        'required'              => false,
                                        'label'                 => false,
                                        'escape'                => false,
                                        'options'               => array(
                                                ''                  => '<i>Melhor horário para ligarmos</i>',
                                                'entre 9h e 11h'    => 'entre 9h e 11h',
                                                'entre 12h e 14h'   => 'entre 12h e 14h',
                                                'entre 15h e 17h'   => 'entre 15h e 17h',
                                                'entre 18h e 20h'   => 'entre 18h e 20h'
                                            )
                                    )
                            ); ?>
                        </div>
                        <button type="submit" name="enviar" id="enviar" class="btn button-submit center-block">Enviar</button>
                    <?php echo $this->Form->end(); ?>
                    <p class="form--texto">
                        <strong>Excelência</strong> comprovada pelo MEC<br>
                        Parte da <strong>maior rede de universidades do mundo</strong><br>
                        <strong>3 campi</strong> com infraestrutura completa
                    </p>
                </div>
            </section><!-- end .form -->
        </div><!-- end .col-xs-12.col-sm-6 -->
    </div><!-- end .row -->
</div><!-- end .container-fluid -->

<section class="logo-uniritter">
    <figure>
        <a href="<?php echo $this->Html->Url('/', true); ?>" title="UniRitter">
            <img src="<?php echo $this->Html->Url('/assets/lp/fone/images/logo-uniritter.png') ?>" alt="Uniritter" />
        </a>
    </figure>
</section>

<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','1359aa933b48b754a2f54adb688bfa77']);
_tn.push(['_setAction','track-view']);
(function() {
  document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
  var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
  tss.src = '//tracker.tolvnow.com/js/tn.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
