<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset() ?>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-language" content="pt-br"/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php if (Configure::read('GooglePlus')) { ?>
    <!-- BEGIN: GOOGLE+ TAGS -->
    <link rel="author" href="https://plus.google.com/(Google+_Profile)/posts"/>
    <link rel="publisher" href="https://plus.google.com/(Google+_Page_Profile)"/>
    <meta itemprop="name" content="">
    <meta itemprop="description" content="">
    <meta itemprop="image" content="">
    <!-- END: GOOGLE+ TAGS -->
    <?php } ?>

    <?php if (Configure::read('GooglePlus')) { ?>
    <!-- BEGIN: TWITTER CARD -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="Conta do Twitter do site (incluindo arroba)">
    <meta name="twitter:title" content="Título da página">
    <meta name="twitter:description" content="Descrição da página. No máximo 200 caracteres">
    <meta name="twitter:creator" content="Conta do Twitter do autor do texto (incluindo arroba)">
    <!-- imagens largas para o Twitter Summary Card precisam ter pelo menos 280x150px -->
    <meta name="twitter:image" content="http://www.example.com/image.jpg">
    <!-- END: TWITTER CARD -->
    <?php } ?>

    <?php if (isset($seo)) { ?>
        <!-- BEGIN: OG TAGS -->
        <meta property="og:locale" content="<?php echo $this->Idioma->slug == 'pt-br' ? 'pt_BR' : 'en_US' ?>">
        <meta property="og:image" content="">
        <meta property="og:url" content="<?php echo Router::url(null, true); ?>">
        <meta property="og:site_name" content="<?php echo Configure::read('Site.Nome') ?>">
        <meta property="og:title" content="<?php echo isset($seo['title']) ? $seo['title'] : ''; ?>">
        <meta property="og:type" content="website">
        <meta property="og:description" content="<?php echo isset($seo['description']) ? $seo['description'] : '' ?>">
        <meta property="og:image" content="<?php echo $this->Html->Url('/assets/images/logo-facebook.png', true) ?>">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="800">
        <meta property="og:image:height" content="600">
        <!-- END: OG TAGS -->
    <?php } ?>

    <?php if (Configure::read('GooglePlus')) { ?>
    <!-- BEGIN: META TAGS DE GEOLOCALIZACAO -->
    <meta name="geo.region" content="BR-RS" />
    <meta name="geo.placename" content="Porto Alegre" />
    <meta name="geo.position" content="-29.560652;-49.904135" />
    <meta name="ICBM" content="-29.560652, -49.904135" />
    <!-- END: META TAGS DE GEOLOCALIZACAO -->
    <?php } ?>

    <?php if (Configure::read('GooglePlus')) { ?>
    <!-- BEIGN: META NAME -->
    <meta name="description" content="associação de organizações prestadoras de serviços de assessoria a empresas, de alta qualificação e experiência, com identidade de conceitos, critérios, procedimentos e crenças">
    <meta name="keywords" content="">
    <meta name="author" content="Fator Digital - http://www.fatordigital.com.br">
    <!-- END: META NAME -->
    <?php } ?>

    <?php if (Configure::read('GooglePlus')) { ?>
    <!-- BEGIN: META GEO LOCATION -->
    <meta name="geo.region" content="BR-SP" />
    <meta name="geo.position" content="-23.55974;-46.661485" />
    <meta name="ICBM" content="-23.55974, -46.661485" />
    <!-- END: META GEO LOCATION -->
    <?php } ?>

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo $this->Html->url('/assets/images/favicon/ms-icon-144x144.png') ?>">
    <meta name="theme-color" content="#ffffff">

    <title>
        <?php
        //seo_title
        if($this->fetch('title')){
            $seo['title'] = '';
            if(isset($this->params->params['page']) && $this->params->params['page'] > 1){
                $seo['title'] = $this->fetch('title') . ' | Página ' . $this->params->params['page'];
            }else{
                $seo['title'] = $this->fetch('title');
            }

            $seo['title'] .= ' | '. Configure::read('Site.Seo.Title');
        }elseif($title_for_layout != ""){
            $seo['title'] = $title_for_layout;

            if(isset($this->params->params['page']) && $this->params->params['page'] > 1){
                $seo['title'] .= ' | Página ' . $this->params->params['page'];
            }

            $seo['title'] .= ' | '. Configure::read('Site.Seo.Title');
        }else{
            $seo['title'] = Configure::read('Site.Seo.Title');
        }
        ?>
        <?php echo $seo['title']; ?>
    </title>

    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $this->Html->url('/assets/images/favicon/apple-icon-57x57.png') ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $this->Html->url('/assets/images/favicon/apple-icon-60x60.png') ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $this->Html->url('/assets/images/favicon/apple-icon-72x72.png') ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $this->Html->url('/assets/images/favicon/apple-icon-76x76.png') ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $this->Html->url('/assets/images/favicon/apple-icon-114x114.png') ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $this->Html->url('/assets/images/favicon/apple-icon-120x120.png') ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $this->Html->url('/assets/images/favicon/apple-icon-144x144.png') ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $this->Html->url('/assets/images/favicon/apple-icon-152x152.png') ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $this->Html->url('/assets/images/favicon/apple-icon-180x180.png') ?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $this->Html->url('/assets/images/favicon/android-icon-192x192.png') ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $this->Html->url('/assets/images/favicon/favicon-32x32.png') ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $this->Html->url('/assets/images/favicon/favicon-96x96.png') ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $this->Html->url('/assets/images/favicon/favicon-16x16.png') ?>">
    <link rel="manifest" href="<?php echo $this->Html->url('/assets/images/favicon/manifest.json') ?>">

    <?php echo $this->Html->meta('icon') ?>
    <?php
    //seo_keywords
    if($this->fetch('keywords')){
        $seo['keywords'] = $this->fetch('keywords');
    }else{
        $seo['keywords'] = Configure::read('Site.Seo.Keywords');
    }

    //seo_description
    if($this->fetch('description')){
        $seo['description'] = $this->fetch('description');
    }else{
        $seo['description'] = Configure::read('Site.Seo.Description');
    }

    //meta
    echo $this->Html->meta('keywords', $seo['keywords']);
    echo $this->Html->meta('description', $seo['description']);
    ?>

    <!-- BEGIN: FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700,800,900" rel="stylesheet">
    <!-- END: FONTS -->

    <!-- BEGIN: CSS -->
    <?php echo $this->Html->css('/assets/css/lib/reset.css') ?>
    <?php echo $this->Html->css('/assets/css/lib/bootstrap.min.css') ?>
    <?php echo $this->Html->css('/assets/css/lib/toastr.min.css') ?>
    <?php echo $this->Html->css('/assets/css/lib/owl.carousel.min.css') ?>
    <?php echo $this->Html->css('/assets/css/lib/owl.theme.default.css') ?>
    <?php echo $this->Html->css('/assets/css/main.css') ?>
    <!-- END: CSS -->

    <!-- BEGIN: SCRIPTS -->
    <script>
        var baseUrl = '<?php echo $this->Html->Url('/', true) ?>';
    </script>
    <?php if(Configure::read("Site.Google.GTM") != ""): ?>
        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=<?php echo Configure::read("Site.Google.GTM"); ?>"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','<?php echo Configure::read("Site.Google.GTM"); ?>');</script>
        <!-- End Google Tag Manager -->
    <?php elseif(Configure::read("Site.Google.Analytics") != ""): ?>
        <?php /*<script type="text/javascript">
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', '<?php echo Configure::read("Site.Google.Analytics"); ?>', 'auto');
            ga('require', 'displayfeatures');
            ga('send', 'pageview');
        </script> */ ?>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo Configure::read("Site.Google.Analytics"); ?>"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', '<?php echo Configure::read("Site.Google.Analytics"); ?>');
        </script>
    <?php endIf; ?>


    <?php if(Configure::read("Site.Google.GTM") != "" || Configure::read("Site.Google.Analytics") != ""): ?>
        <?php if(strtolower($this->params['controller']) == 'home'): ?>
            <script type="text/javascript">
                window._pt_lt = new Date().getTime();
                window._pt_sp_2 = [];
                _pt_sp_2.push('setAccount,67956ea2');
                var _protocol = (("https:" == document.location.protocol) ? " https://" : " http://");
                (function() {
                    var atag = document.createElement('script'); atag.type = 'text/javascript'; atag.async = true;
                    atag.src = _protocol + 'cjs.ptengine.com/pta_en.js';
                    var stag = document.createElement('script'); stag.type = 'text/javascript'; stag.async = true;
                    stag.src = _protocol + 'cjs.ptengine.com/pts.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(atag, s); s.parentNode.insertBefore(stag, s);
                })();
            </script>
        <?php endIf; ?>
    <?php endIf; ?>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLaAx8JweUAoiMwVIcgoeWvvxvflHAi7E&v=3.20&sensor=false"></script>
    <!-- END: SCRIPTS -->

    <?php echo $this->Flash->render('goal') ?>
</head>
<body>

<div id="lopes-neto">
    <?php echo $this->Element('site/menu') ?>

    <?php echo $this->fetch('content') ?>

    <?php echo $this->Element('site/footer') ?>
</div>

<?php echo $this->element('sql_dump'); ?>

<!-- BEGIN: SCRIPTS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.4/vue.js"></script>
<?php echo $this->Html->script('/assets/js/lib/jquery-2.2.4.min.js') ?>
<?php echo $this->Html->script('/assets/js/lib/bootstrap.min.js') ?>
<?php echo $this->Html->script('/assets/js/lib/mask.min.js') ?>
<?php echo $this->Html->script('/assets/js/lib/jquery.validate.min.js') ?>
<?php echo $this->Html->script('/assets/js/lib/owl.carousel.min.js') ?>
<?php echo $this->Html->script('/assets/js/lib/jquery.match-height.js') ?>
<?php echo $this->Html->script('/assets/js/lib/tabcollapse.js') ?>
<?php echo $this->Html->script('/assets/js/lib/swiffy-v7.4.js') ?>
<?php echo $this->Html->script('/assets/js/lib/fator.js') ?>
<?php echo $this->Html->script('/assets/js/lib/smoothscroll.js') ?>
<?php echo $this->Html->script('/assets/js/lib/toastr.min.js') ?>

<?php //echo $this->Html->script('/assets/componentes/noticia-sliders.js') ?>

<?php echo $this->Flash->render('success')  ?>
<?php echo $this->Flash->render('error')  ?>

<?php echo $this->Html->script('/assets/js/main.js') ?>

<script>
    $(document).ready(function(){
        var stage = new swiffy.Stage(document.getElementById('assinatura_fator'),
            swiffyobject, {});
        stage.setBackground(null);
        stage.start();
    });
</script>

<script>
    $(document).ready(function(){
        function teste () {
            $('#noticias').load(baseUrl + 'home/ajax_noticias_artigos_home/tipo:1');
//        $.get(APP_ROOT . 'home/ajax_noticias_artigos_home')
        }
        teste();
    })

</script>
<!-- END: SCRIPTS -->
</body>
</html>