<?php
$this->Paginator->options(array(
    'update' => '#noticias',
    'evalScripts' => true,
    'url' => array(
        'controller' => 'home',
        'action' => 'ajax_noticias_artigos_home'
    )
));
?>

<div class="container">
    <h2>
        <small>ARTIGOS E </small><br />
        Notícias
    </h2>

    <div class="row">
        <div class="col-xs-12 fd_flex_column">
            <?php echo $this->Paginator->link('Notícias', array('page' => 1, 'tipo' => 1), array('class' => $tipo == 2 ? 'btn inactive' : 'btn')); ?>
            <?php echo $this->Paginator->link('Artigos', array('page' => 1, 'tipo' => 2), array('class' => $tipo == 1 ? 'btn inactive' : 'btn')); ?>
        </div>
    </div>
</div>
<div class="fd_article_content container">

    <div class="container-fluid row-content" id="fd_carousel_noticias">
        <div class="row row-noticias">
            <div class="col-xs-12">
                <div class="fd_carousel">

                    <?php if (isset($noticias)) { ?>
                        <div class="item">
                            <?php foreach ($noticias as $i => $noticia) { ?>
                            <article class="fd_item">
                                <figure class="fd_eq" style="background-image: url(<?php echo $this->Html->Url('/'.$noticia['Noticia']['thumb_path'] . '/' . $noticia['Noticia']['thumb_dir'] .'/300x300-'. $noticia['Noticia']['thumb'], true) ?>)"></figure>
                                <div class="fd_content fd_eq">
                                    <h3>
                                        <a href="<?php echo $this->Html->Url('/' . $noticia['Rota']['seo_url'], true) ?>"><?php echo $this->Idioma->extrair($noticia['NoticiaAtributo'], 'titulo') ?></a>
                                    </h3>
                                    <p>
                                        <a href="<?php echo $this->Html->Url('/' . $noticia['Rota']['seo_url'], true) ?>">
                                            <?php echo $this->Idioma->extrair($noticia['NoticiaAtributo'], 'resumo') ?>
                                        </a>
                                    </p>
                                    <a href="<?php echo $this->Html->Url('/' . $noticia['Rota']['seo_url'], true) ?>" class="btn">LEIA MAIS</a>
                                </div>
                            </article>
                            <?php } ?>
                        </div>
                    <?php } ?>

                    <div class="owl-nav">
                        <?php $this->Paginator->options(array('url' => array('tipo' => $tipo))) ?>
                        <?php if ($this->Paginator->hasPrev()) { ?>
                            <div class="owl-prev">
                                <?php echo $this->Paginator->prev(__('Antigas', true),
                                    array(), null, array()
                                ); ?>
                            </div>
                        <?php } ?>
                        <div class="owl-dots">
                            <?php for ($i = 1; $i <= $this->Paginator->param('pageCount'); $i++) { ?>
                                <?php echo $this->Paginator->link($i, array('page' => $i, 'tipo' => $tipo), array('class' => 'owl-dot')); ?>
                            <?php } ?>
                        </div>
                        <?php if ($this->Paginator->hasNext()) { ?>
                            <div class="owl-next" style="margin-left: <?php echo ($this->Paginator->param('pageCount') ) ?>px;">
                                <?php echo $this->Paginator->next(__('Próximas', true),
                                    array(), null, array()); ?>
                            </div>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div><!-- end of /.row-noticias -->
    </div>

</div>

<?php echo $this->Js->writeBuffer(); ?>