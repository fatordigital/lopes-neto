<nav class="navbar navbar-default navbar-fixed-top navbar-post">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/#home">
                <?php echo $this->Html->image('/assets/images/lopes-neto-logo.png', array("alt" => Configure::read('Site.Nome')), Configure::read('Site.Nome')) ?>
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="fd_menu">
            <ul class="nav navbar-nav navbar-right">
                <?php if (isset($menus_for_layout['site_institucional']) && count($menus_for_layout['site_institucional'])) { ?>
                    <?php foreach ($menus_for_layout['site_institucional']['threaded'] as $link) { ?>
                        <li>
                            <a href="<?php echo $this->Idioma->url($link['Link']['seo_url']) ?>" class="page-scroll">
                                <?php echo $this->Idioma->extrair($link['LinkAtributo'], 'title'); ?>
                            </a>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
