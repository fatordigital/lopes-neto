<!-- <meta name="author" content="<?php //echo Configure::read('Site.Seo.Author'); ?>"> -->
<base href="<?php echo $this->Html->url('/') ?>">

<!-- Tags OG para compartilhamento em redes sociais -->
<meta property="og:locale" content="pt_BR">
<meta property="og:url" content="<?php echo $this->Html->Url(null, true); ?>">
<meta property="og:title" content="<?php echo $seo['title']; ?>">
<meta property="og:site_name" content="<?php echo Configure::read('Site.Nome'); ?>">
<meta property="og:description" content="<?php echo $seo['description']; ?>">

<?php if(isset($seo['og:type']) && $seo['og:type'] != ""): ?>
    <meta property="og:type" content="<?php echo $seo['og:type']; ?>">
<?php else: ?>
    <meta property="og:type" content="website">
<?php endIf; ?>

<?php if(isset($seo['article:section']) && $seo['article:section'] != ""): ?>
    <meta property="article:section" content="<?php echo $seo['article:section']; ?>">
<?php endIf; ?>

<?php if(isset($seo['article:tag']) && $seo['article:tag'] != ""): ?>
    <meta property="article:tag" content="<?php echo $seo['article:tag']; ?>">
<?php endIf; ?>

<?php if(isset($seo['article:published_time']) && $seo['article:published_time'] != ""): ?>
    <meta property="article:published_time" content="<?php echo $seo['article:published_time']; ?>">
<?php endIf; ?>

<meta property="og:image:type" content="image/jpeg">

<?php if(isset($seo['og:imagem']) && $seo['og:imagem'] != ""): ?>
<meta property="og:image" content="<?php echo $this->Html->Url('/'. $seo['og:imagem'], true); ?>">
<?php else: ?>
<meta property="og:image" content="<?php echo $this->Html->Url('/img/site/logo-facebook.png', true); ?>">
<?php endIf; ?>

<?php if(isset($seo['og:image:width']) && $seo['og:image:width'] != ""): ?>
<meta property="og:image:width" content="<?php echo $seo['og:image:width']; ?>">
<?php else: ?>
<meta property="og:image:width" content="700">
<?php endIf; ?>


<?php if(isset($seo['og:image:height']) && $seo['og:image:height'] != ""): ?>
<meta property="og:image:height" content="<?php echo $seo['og:image:height']; ?>">
<?php else: ?>
<meta property="og:image:height" content="400">
<?php endIf; ?>