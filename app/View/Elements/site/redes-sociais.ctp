<div class="social social-item">
  <h4 class="title-color title-red">Redes <span class="bold">sociais</span></h4>
  <div class="social-icons-big">
    <?php if(Configure::read('Site.Social.Facebook') != ""): ?>
      <a href="<?php echo Configure::read('Site.Social.Facebook'); ?>" title="Facebook" target="_blank">
        <img src="<?php echo $this->Html->Url('/assets/images/icons/icon-facebook-big.gif') ?>" title="Facebook" />
      </a>
    <?php endIf; ?>
    
    <?php if(Configure::read('Site.Social.Twitter') != ""): ?>
      <a href="<?php echo Configure::read('Site.Social.Twitter'); ?>" title="Twitter" target="_blank">
        <img src="<?php echo $this->Html->Url('/assets/images/icons/icon-twitter-big.gif') ?>" title="Twitter" />
      </a>
    <?php endIf; ?>
    
    <?php if(Configure::read('Site.Social.GooglePlus') != ""): ?>
      <a href="<?php echo Configure::read('Site.Social.GooglePlus'); ?>" title="Gplus" target="_blank">
        <img src="<?php echo $this->Html->Url('/assets/images/icons/icon-gplus-big.gif') ?>" title="Gplus" />
      </a>
    <?php endIf; ?>
    
    <?php if(Configure::read('Site.Social.Youtube') != ""): ?>
      <a href="<?php echo Configure::read('Site.Social.Youtube'); ?>" title="Youtube" target="_blank">
        <img src="<?php echo $this->Html->Url('/assets/images/icons/icon-youtube-big.gif') ?>" title="Youtube" />
      </a>
    <?php endIf; ?>
    
    <?php if(Configure::read('Site.Social.Instagram') != ""): ?>
      <a href="<?php echo Configure::read('Site.Social.Instagram'); ?>" title="Instagram" target="_blank">
        <img src="<?php echo $this->Html->Url('/assets/images/icons/icon-instagram-big.gif') ?>" title="Instagram" />
      </a>
    <?php endIf; ?>
  </div>
</div>