<div class="mp-container">
  <div class="mp-pusher" id="mp-pusher">

    <!-- mp-menu -->
    <nav id="mp-menu" class="mp-menu">
      <div class="mp-level">
        <ul>
          <?php if(isset($menus_for_layout['site_institucional']) && count($menus_for_layout['site_institucional']['threaded']) > 0): ?>
          <li>
            <a href="#">Institucional</a>
            <div class="mp-level">
              <h2>Institucional</h2>
              <a class="mp-back" href="javascript:void(0);">voltar</a>
              <ul>
                <?php foreach($menus_for_layout['site_institucional']['threaded'] as $k => $link): ?>
                  <li><a href="<?php echo $this->Html->Url('/'.$link['Link']['seo_url']); ?>" title="<?php echo $link['Link']['title']; ?>" target="<?php echo $link['Link']['target']; ?>"><?php echo $link['Link']['title']; ?></a></li>
                <?php endForeach; ?>
              </ul>
            </div>
          </li>
          <?php endIf; ?>

          <?php if(isset($cursos_e_campi['cursos_tipo']['graduacao']) && count($cursos_e_campi['cursos_tipo']['graduacao']) > 0): ?>
              
              <li>
                <a href="#" class="mp-course-item">Graduação</a>
                <div class="mp-level">
                  <h2>Graduação</h2>
                  <a class="mp-back" href="javascript:void(0);">voltar</a>                 
                      <ul class="mp-level-courses">
                      <?php foreach($cursos_e_campi['cursos_tipo']['graduacao'] as $k => $curso): ?>
                          <li class="cursos">
                              <?php if($curso['CursoTipo']['seo_url'] == 'graduacao-flex'): ?>
                                  <?php  $curso['Curso']['nome'] .= ' - ' . $curso['CursoTipo']['nome']; ?> 
                              <?php endIf; ?>

                              <a href="<?php echo $this->Html->Url('/' . $curso['Curso']['seo_url'], true); ?>" title="<?php echo $curso['Curso']['nome']; ?>">
                                  <?php echo $curso['Curso']['nome']; ?>
                              </a>
                          </li>
                      <?php endForeach; ?>
                    </ul>
                </div>
              </li>

          <?php endIf; ?>

          <?php if(isset($cursos_e_campi['cursos_tipo']['graduacao-flex']) && count($cursos_e_campi['cursos_tipo']['graduacao-flex']) > 0): ?>
              
              <li>
                <a href="<?php echo $this->Html->Url('/flex', true);?>" class="mp-course-item">Graduação Flex</a>
                <!--<div class="mp-level">
                  <h2>Graduação Flex</h2>
                  <a class="mp-back" href="javascript:void(0);">voltar</a>                 
                      <ul class="mp-level-courses">
                      <?php foreach($cursos_e_campi['cursos_tipo']['graduacao-flex'] as $k => $curso): ?>
                          <li class="cursos">
                              <a href="<?php echo $this->Html->Url('/' . $curso['Curso']['seo_url'], true); ?>" title="<?php echo $curso['Curso']['nome']; ?>">
                                  <?php echo $curso['Curso']['nome']; ?>
                              </a>
                          </li>
                      <?php endForeach; ?>
                    </ul>
                </div>-->
              </li>

          <?php endIf; ?>

          <?php if(isset($cursos_e_campi['cursos_tipo']['especializacao-mba']) && count($cursos_e_campi['cursos_tipo']['especializacao-mba']) > 0): ?>
              <li>
                <a href="#">Pós-graduação</a>
                <div class="mp-level">
                  <h2>Pós-graduação</h2>
                  <a class="mp-back" href="javascript:void(0);">voltar</a>
                  
                    <ul class="mp-level-courses">
                      <?php foreach($cursos_e_campi['cursos_tipo']['especializacao-mba'] as $k => $curso): ?>
                          <li>
                              <a href="<?php echo $this->Html->Url('/' . $curso['Curso']['seo_url'], true); ?>" title="<?php echo $curso['Curso']['nome']; ?>">
                                  <?php echo $curso['Curso']['nome']; ?>
                              </a>
                          </li>
                      <?php endForeach; ?>
                    </ul>
                </div>
              </li>
          <?php endIf; ?>
          
          <?php /*<li><a href="<?php echo $this->Html->Url('/international-office', true); ?>" title="International Offices">International Office</a></li>*/ ?>
          
          <?php if(isset($menus_for_layout['site_area_do_aluno']) && count($menus_for_layout['site_area_do_aluno']['threaded']) > 0): ?>
            <li>
              <a href="#" title="Área do Aluno">Área do Aluno</a>
              <div class="mp-level">
                  <h2>Área do Aluno</h2>
                  <a class="mp-back" href="javascript:void(0);">voltar</a>
                  <ul>
                    <li><a href="<?php echo $this->Html->Url('/portal-aluno'); ?>" title="PORTAL DO ALUNO" target="_blank">PORTAL DO ALUNO</a></li>  
                    <?php foreach($menus_for_layout['site_area_do_aluno']['threaded'] as $k => $link): ?>
                        <li class="col-md-<?php echo $link['Link']['class_estrutura']; ?>">
                            <a href="<?php echo $this->Html->Url('/'.$link['Link']['seo_url']); ?>" title="<?php echo $link['Link']['title']; ?>" target="<?php echo $link['Link']['target']; ?>"><?php echo $link['Link']['title']; ?></a>
                        </li>
                    <?php endForeach; ?>
                  </ul>
                </div>
            </li>
          <?php endIf; ?>
          <li><a href="<?php echo Configure::read('Site.Url.PortalProfessor'); ?>" title="Portal do Professor" target="_blank">Portal do Professor</a></li>


          <li>
              <a href="<?php echo $this->Html->Url('/vestibular',true);?>" title="Vestibular"> Vestibular</a>
          </li>


        </ul>
      </div>
    </nav>
    <!-- /mp-menu -->
  </div>
</div>
