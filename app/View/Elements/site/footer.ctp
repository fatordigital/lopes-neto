<footer id="contato">
    <div class="container-fluid fd_footer_top">
        <div class="row">
            <div class="col-xs-12 col-sm-6 fd_eq">
                <div class="fd_form pull-right">
                    <?php echo $this->Form->create('Sac', array('id' => 'form', 'url' => $this->Html->Url('/contato', true))); ?>
                        <h2>
                            <small>Entre em</small><br />
                            Contato
                        </h2>
                        <div class="form-group fd_group">
                            <?php echo $this->Form->text('nome', array('class' => 'form-control fd_input', 'data-rule-required' => 'true', 'data-msg-required' => 'Preencha o campo<br /> corretamente', 'pattern' => '([A-zÀ-ž\s]){1,}', 'required')) ?>
                            <label class="fd_label">Nome</label>
                        </div>
                        <div class="form-group fd_group">
                            <?php echo $this->Form->text('email', array('class' => 'form-control fd_input', 'data-msg-email' => 'Preencha o campo<br /> corretamente', 'data-rule-email' => 'true', 'data-rule-required' => 'true', 'data-msg-required' => 'Preencha o campo<br /> corretamente', 'pattern' => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$', 'required')) ?>
                            <label class="fd_label">E-mail</label>
                        </div>
                        <div class="form-group fd_group">
                            <?php echo $this->Form->text('telefone', array('class' => 'form-control fd_phone_mask fd_input', 'data-rule-required' => 'true', 'data-msg-required' => 'Preencha o campo<br /> corretamente','data-rule-minlength' => '14','data-msg-minlength' => 'Preencha o campo<br /> corretamente','data-rule-maxlength' => '15', 'data-msg-maxlength' => 'Preencha o campo<br /> corretamente', 'pattern' => '([\(]\d{2}[\)]) (\d{5}[\-]\d{4}|\d{4}[\-]\d{4})', 'required')) ?>
                            <label class="fd_label">Telefone</label>
                        </div>
                        <div class="form-group fd_group">
                            <?php echo $this->Form->textarea('mensagem', array('class' => 'form-control fd_input', 'data-rule-required' => 'true', 'data-msg-required' => 'Preencha o campo<br /> corretamente', 'pattern' => '([A-zÀ-ž\s]){1,}', 'required')) ?>
                            <label class="fd_label">Mensagem</label>
                        </div>
                        <button type="submit" class="btn button yellow inverse">enviar mensagem</button>
                        <div class="fd_form_message"></div>
                    <?php echo $this->Form->end() ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div id="mapa" class="fd_eq"></div>
            </div>
        </div>
    </div>

    <section class="fd_footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-5 fd_flex_column">
                    <figure>
                        <img src="<?php echo $this->Html->Url('/assets/images/icons/phone.png') ?>" alt="Telefone">
                    </figure>
                    <div class="fd_content">
                        <h4>
                            <?php echo $this->Idioma->palavra('fale-conosco'); ?><br />
                            <span class="thin"><?php echo $this->Idioma->palavra('ddd'); ?></span> <strong><?php echo $this->Idioma->palavra('telefones'); ?></strong>
                        </h4>
                    </div>
                </div><!-- end of /.cols -->
                <div class="col-xs-12 col-sm-6 col-md-4 fd_flex_column">
                    <figure>
                        <img src="<?php echo $this->Html->Url('/assets/images/icons/message.png') ?>" alt="Mensagem">
                    </figure>
                    <div class="fd_content">
                        <h4>
                            <?php echo $this->Idioma->palavra('envie-um-email'); ?><br />
                            <span class="thin">
                                <a href="mailto:<?php echo $this->Idioma->palavra('e-mail'); ?>"><?php echo $this->Idioma->palavra('e-mail'); ?></a>
                            </span>
                        </h4>
                    </div>
                </div><!-- end of /.cols -->
                <div class="col-xs-12 col-md-3 fd_flex_column">
                    <a href="<?php echo Configure::read('Site.Social.Facebook') ?>" title="Facebook" target="_blank" class="fd_social_link" data-toggle="tooltip">
                        <img src="<?php echo $this->Html->Url('/assets/images/icons/facebook.svg', true) ?>" alt="Facebook"/>
                    </a>
                    <a href="<?php echo Configure::read('Site.Social.Linkedin') ?>" title="LinkedIn" target="_blank" class="fd_social_link" data-toggle="tooltip">
                        <img src="<?php echo $this->Html->Url('/assets/images/icons/linkedin.svg', true) ?>" alt="LinkedIn" />
                    </a>
                </div><!-- end of /.cols -->
            </div><!-- end of /.row -->
        </div><!-- end of /.container -->
    </section><!-- end of /.fd_footer_bottom -->
    <section class="assinatura">
        <div class="container">
            <a href="https://www.fatordigital.com.br/" target="_blank" class="pull-right" title="Fator Digital">
                Fator Digital
                <span id="assinatura_fator" style="width: 200px; height: 50px; margin-top: -5px; float: right; margin-right: 20px;"></span>
            </a>
        </div>
    </section>

</footer>

