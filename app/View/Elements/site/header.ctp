<?php 
	//para ajuste de layout
    if($this->params['controller'] == 'home'):
?>

	<?php if(Configure::read('NAVEGACAO_TIPO') == 'ALUNO'): ?>	
		<?php echo $this->element('site/menu') ?>
	    <?php echo $this->element('site/menu_mobile') ?>
	    <?php echo $this->element('site/banner_topo_aluno'); ?>

	    <div class="row row-block-finder">
		    <div class="container">
		        <p>Encontre o seu curso na <span>UniRitter</span></p>
		    </div>
		</div>
	<?php else: ?>	    
		<div class="row fullscreen">
	    	<?php echo $this->element('site/menu') ?>
	    	<?php echo $this->element('site/menu_mobile') ?>
			<?php echo $this->element('site/banner_topo'); ?>

			<div class="row row-block-finder">
			    <div class="container">
			        <p>Encontre o seu curso na <span>UniRitter</span></p>
			    </div>
			</div>
		</div>
	<?php endIf; ?>
<?php else: ?>
	<div class="row fullscreen-curso">
		<?php echo $this->element('site/menu') ?>
	    <?php echo $this->element('site/menu_mobile') ?>
<?php endIf; ?>