<?php if(isset($banners_topo) && count($banners_topo) > 0): ?>

    <?php $banners_topo_tmp = $banners_topo; ?>
    <?php $banners_topo = array(); ?>
    <?php $banners_topo_mobile = array(); ?>
    <?php foreach($banners_topo_tmp as $k => $banner): ?>
        <?php if($banner['Banner']['visivel'] == 'GERAL' || $banner['Banner']['visivel'] == Configure::read('NAVEGACAO_TIPO')): ?>
            <?php $banners_topo[] = $banner; ?>

            <?php if($banner['Banner']['file_mobile'] != ""): ?>
                <?php $banners_topo_mobile[] = $banner; ?>
            <?php endIf; ?>
        <?php endIf; ?>
    <?php endForeach; ?>

<div class="row banner banner-home"  <?php echo (count($banners_topo_mobile) == 0) ? ' style="display: block !important;"' : ''; ?>>
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" >
        <!-- Indicators -->
        <?php if(count($banners_topo) > 1): ?>
        <ol class="carousel-indicators">
            <?php foreach($banners_topo as $k => $banner): ?>
                <li data-target="#carousel-example-generic" data-slide-to="<?php echo $k; ?>" <?php echo ($k==0) ? 'class="active"' : ''; ?>></li>
            <?php endForeach; ?>
        </ol>
        <?php endIf; ?>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
        <?php foreach($banners_topo as $k => $banner): ?>
            <div class="item <?php echo ($k==0) ? 'active' : ''; ?>" style="background: url(<?php echo $this->Html->Url('/' . $banner['Banner']['path'] . '/' . $banner['Banner']['dir'] . '/' . $banner['Banner']['file'], true); ?>) no-repeat scroll center center / cover;" >

                <?php if( $banner['Banner']['tem_formulario'] == true ): ?>
                    <div class="carousel-caption">                    
                        <?php echo $this->element('site/banner_form', array('title' => 'Cadastre-se para receber mais informações dos cursos')) ?>
                    </div>
                <?php endIf; ?>

                <?php if( $banner['Banner']['url'] != "" ): ?>
                    <a style="width: 100%; display: block; position: absolute; height: 100%;" data-href="<?php echo $banner['Banner']['url']; ?>" data-type="link">&nbsp;</a>
                <?php elseif( $banner['Banner']['url_video'] != "" ): ?>
                     <a style="width: 100%; display: block; position: absolute; height: 100%;" data-href="<?php echo $banner['Banner']['url_video']; ?>" data-type="youtube"><span class=
                        'glyphicon glyphicon-play-circle play'></span></a>
                <?php endIf; ?>
                <div class="player" id="<?php echo "player1".$k ?>" style="width: 100%; height: 100%; position: absolute; top: 0px; display: none"></div>

                
            </div>

        <?php endForeach; ?>
        </div>

        

        <!-- Controls -->
        <?php if(count($banners_topo) > 1): ?>
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
             <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
             <span class="sr-only">Anterior</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
             <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
             <span class="sr-only">Próximo</span>
            </a>
        <?php endIf; ?>
    </div>
</div>

    <?php if(isset($banners_topo_mobile) && count($banners_topo_mobile) > 0): ?>
    <div class="row banner banner-home_mobile">
        <div id="carousel-example-generic1" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->

            <!-- Indicators -->
            <?php if(count($banners_topo_mobile) > 1): ?>
            <ol class="carousel-indicators">
                <?php foreach($banners_topo_mobile as $k => $banner): ?>
                    <li data-target="#carousel-example-generic1" data-slide-to="<?php echo $k; ?>" <?php echo ($k==0) ? 'class="active"' : ''; ?>></li>
                <?php endForeach; ?>
            </ol>
            <?php endIf; ?>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">  
                <?php foreach($banners_topo_mobile as $k => $banner): ?>
                    <div class="item <?php echo ($k==0) ? 'active' : ''; ?>" style="background: url(<?php echo $this->Html->Url('/' . $banner['Banner']['path_mobile'] . '/' . $banner['Banner']['dir_mobile'] . '/' . $banner['Banner']['file_mobile'], true); ?>) no-repeat scroll center center / cover;">
                        <img src="<?php echo $this->Html->Url('/' . $banner['Banner']['path_mobile'] . '/' . $banner['Banner']['dir_mobile'] . '/' . $banner['Banner']['file_mobile'], true); ?>"/>
                        <?php if( $banner['Banner']['url'] != "" ): ?>
                            <a style="width: 100%; display: block; position: absolute; height: 100%;" href="<?php echo $banner['Banner']['url']; ?>">&nbsp;</a>
                        <?php endIf; ?>
                    </div>
                <?php endForeach; ?>
            </div>

            <!-- Controls -->
            <?php if(count($banners_topo_mobile) > 1): ?>
                <a class="left carousel-control" href="#carousel-example-generic1" role="button" data-slide="prev">
                     <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                     <span class="sr-only">Anterior</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic1" role="button" data-slide="next">
                     <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                     <span class="sr-only">Próximo</span>
                </a>
            <?php endIf; ?>      
        </div>
    </div>
    <?php endIf; ?>
<?php endIf; ?>

<?php if(isset($menus_for_layout['site_area_do_aluno']) && count($menus_for_layout['site_area_do_aluno']['threaded']) > 0): ?>
    <?php $quatro_primeiros = array(); ?>
    <?php $quatro_ultimos = array(); ?>
    <?php foreach($menus_for_layout['site_area_do_aluno']['threaded'] as $k => $link): ?>
        <?php
            if($k < 3){
                $quatro_primeiros[] = $link;
            }else{
                $quatro_ultimos[] = $link;
            }
        ?>
    <?php endForeach; ?>

    <div class="row nav-student-spot tur3">
        <div class="container vtoson-container">
            <ul>
                <?php foreach($quatro_primeiros as $k => $link): ?>
                    <li class="item <?php echo ($k == 0 || $k == 3) ? 'bd-gray-left' : ''; ?> <?php if($k == 0): echo 'tur4'; endif;?>">
                        <a href="<?php echo $this->Html->Url('/'.$link['Link']['seo_url']); ?>" title="<?php echo $link['Link']['title']; ?>" target="<?php echo $link['Link']['target']; ?>">
                            <span id="<?php echo $link['Link']['class_icon']; ?>" class="as-block"></span>
                            <span class="student--item-name"><?php echo $link['Link']['title']; ?></span>
                        </a>
                    </li>
                <?php endForeach; ?>

                <?php if(count($quatro_ultimos) > 0): ?>
                    <li class="list-to-break">
                        <ul>
                            <?php foreach($quatro_ultimos as $k => $link): ?>
                                <li class="item <?php echo ($k == 0 || $k == count($quatro_ultimos)-1) ? 'bd-gray-left' : ''; ?>">
                                    <a href="<?php echo $this->Html->Url('/'.$link['Link']['seo_url']); ?>" title="<?php echo $link['Link']['title']; ?>" target="<?php echo $link['Link']['target']; ?>">
                                        <span id="<?php echo $link['Link']['class_icon']; ?>" class="as-block"></span>
                                        <span class="<?php echo $link['Link']['class_icon']; ?>-active" style="display: none"></span>
                                        <span class="student--item-name"><?php echo $link['Link']['title']; ?></span>
                                    </a>
                                </li>
                            <?php endForeach; ?>     
                        </ul>
                    </li>
                <?php endIf; ?>
            </ul>
        </div>
    </div>
<?php endIf; ?>