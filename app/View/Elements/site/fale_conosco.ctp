<?php $formulario_titulo = ''; ?>
<?php if(isset($pagina['Pagina']['formulario_titulo']) && $pagina['Pagina']['formulario_titulo'] != ""): ?>
	   <?php $formulario_titulo = $pagina['Pagina']['formulario_titulo']; ?>
<?php elseif(isset($curso['Curso']['formulario_titulo']) && $curso['Curso']['formulario_titulo'] != ""): ?>
	<?php $formulario_titulo = $curso['Curso']['formulario_titulo']; ?>
<?php elseif(isset($tipo['NoticiaTipo']['formulario_titulo']) && $tipo['NoticiaTipo']['formulario_titulo'] != ""): ?>
	<?php $formulario_titulo = $tipo['NoticiaTipo']['formulario_titulo']; ?>
<?php elseif(isset($noticia['NoticiaTipo']['formulario_titulo']) && $noticia['NoticiaTipo']['formulario_titulo'] != ""): ?>
	<?php $formulario_titulo = $noticia['NoticiaTipo']['formulario_titulo']; ?>
<?php else: ?>
	<?php $formulario_titulo = "Fale <span>Conosco</span>"; ?>
<?php endIf; ?>

 <?php if(isset($send_goal_sac_interna) && $send_goal_sac_interna == true): ?>
 	<?php $goal_tipo = 'sidebar'; ?>
	<?php $goal_objetivo = strtolower($this->params->url); ?>
	<?php echo $this->element('site/goal', array('goal_tipo' => $goal_tipo, 'goal_objetivo' => $goal_objetivo)); ?>
<?php endIf; ?>
	
<div class="<?php echo ($this->params->url != 'international-office') ? 'contact-us-sidebar' : 'contact-us2-sidebar'; ?> ">
	<?php echo $this->Session->flash() ?>    

	<h4 class="<?php echo ($this->params->url == 'international-office') ? 'title-color title-red' : ''; ?>"><?php echo $formulario_titulo; ?></h4>

	<?php echo $this->Form->create('SacInterna', array('url' => $this->Html->Url(null, true), 'class' => 'form-validate')); ?>
		<div class="fd_key"><?php echo $this->Form->input('fd'); ?></div>
		<div class="form-group form-sidebar">
			 <?php //echo $this->Form->hidden('tipo', array('value' => strtolower(Inflector::slug(strip_tags($formulario_titulo), '-')))); ?>
			 <?php //echo $this->Form->hidden('objetivo', array('value' => strtolower($this->params->url))); ?>
			 <?php echo $this->Form->input('nome', array(
													'div'                   => false,
													'type'                  => 'text',
													'label'                 => false,
													'class'                 => 'form-control',
													'data-rule-required'    => 'true',
													'data-msg-required'     => 'Informe o seu nome',
													'required'              => false,
													'placeholder'           => 'Seu nome'
												)
										); ?>
		</div>
		<div class="form-group form-sidebar">
			<?php echo $this->Form->input('email', array(
													'div'                   => false,
													'type'                  => 'text',
													'label'                 => false,
													'class'                 => 'form-control',
													'data-rule-required' 	=> 'true',
					            					'data-rule-email' 		=> 'true',
					            					'data-msg-required' 	=> 'Informe o seu e-mail',
					            					'data-msg-email' 		=> 'Informe um e-mail válido',
													'required'              => false,
													'placeholder'           => 'Seu e-mail'
												)
										); ?>

		</div>
		<div class="form-group form-sidebar">
			<?php echo $this->Form->input('telefone', array(
													'div'                   => false,
													'type'                  => 'text',
													'label'                 => false,
													'class'                 => 'form-control phone_with_ddd',
													'data-rule-required'    => 'true',
													'data-msg-required'     => 'Informe o seu telefone',
													'required'              => false,
													'placeholder'           => 'Seu telefone'
												)
										); ?>
		</div>
		<div class="form-group form-sidebar">
			<?php echo $this->Form->input('assunto', array(
													'div'                   => false,
													'type'                  => 'text',
													'label'                 => false,
													'class'                 => 'form-control',
													'data-rule-required'    => 'true',
													'data-msg-required'     => 'Informe o assunto do contato',
													'required'              => false,
													'placeholder'           => 'Assunto do contato'
												)
										); ?>
		</div>
		<div class="form-group form-textarea">
			<?php echo $this->Form->input('mensagem', array(
								'div'                   => false,
								'type'                  => 'textarea',
								'label'                 => false,
								'data-rule-required'    => 'true',
								'data-msg-required'     => 'Preencha com sua mensagem',
								'required'              => false,
								'placeholder'           => 'Mensagem'
							)
					); ?>
		</div>		 
		<button type="submit" >Enviar</button>
	<?php echo $this->Form->end(); ?>
</div>