<?php echo $this->Html->script('/js/site/main.js'); ?>
<?php echo $this->assign('title', ($pagina['Pagina']['seo_title'] != "") ? $pagina['Pagina']['seo_title'] : $pagina['Pagina']['nome']); ?>
<?php echo $this->assign('description', ($pagina['Pagina']['seo_description'] != "") ? $pagina['Pagina']['seo_description'] : $this->String->truncate_str(strip_tags($pagina['Pagina']['conteudo']), 160)); ?>
<?php
    if($pagina['Pagina']['seo_keywords'] != ""){
        echo $this->assign('keywords', $pagina['Pagina']['seo_keywords']);
    }
?>
			
			<?php if(
					$pagina['Pagina']['seo_url'] == 'vestibular' || 
					$pagina['Pagina']['seo_url'] == 'vest' || 
					$pagina['PaginaTipo']['seo_url'] == 'vestibular-uniritter' || 
					$pagina['Pagina']['template_novo'] == true ): ?>
				<div class="vestibular">

					<div class="menu-bar">
					    <div class="container">
					        <ul class="menu">
					          <li>
					            <a href="<?php echo $this->Html->Url('/vestibular-prova',true);?>"><img src="assets/images/icon/prova.png" class="img-responsive">Prova</a>
					          </li>
					          <li>
					            <a href="<?php echo $this->Html->Url('/inscricao',true);?>"><img src="assets/images/icon/inscricoes.png" class="img-responsive">Inscrições Vestibular</a>
					          </li>
					          <li>
					            <a href="<?php echo $this->Html->Url('/outras-formas-de-ingresso',true);?>"><img src="assets/images/icon/ingressos.png" class="img-responsive">Outras Formas de Ingresso</a>
					          </li>
					          <li>
					            <a href="<?php echo $this->Html->Url('/bolsas-e-financiamentos-novos-alunos',true);?>"><img src="assets/images/icon/enade.png" class="img-responsive">Bolsas e Financiamentos</a>
					          </li>
					          <li>
					            <a href="<?php echo $this->Html->Url('/matricula-vestibular',true);?>"><img src="assets/images/icon/matricula.png" class="img-responsive">Matrícula</a>
					          </li>
					        </ul>
					    </div>
					</div>

				</div>
			<?php endIf; ?>

			<?php if(
			(isset($pagina['PaginaTipo']['seo_url']) && $pagina['PaginaTipo']['seo_url'] == 'vestibular-graduacao-executiva')
			|| 
			($pagina['Pagina']['seo_url'] == 'flex')
			
			): ?>
	        <div class="semipresencial">
	            <?php if(isset($menus_for_layout['hotsite_semipresencial']) && count($menus_for_layout['hotsite_semipresencial']['threaded']) > 0): ?>
	                <div class="menu-bar">
	                    <div class="container">
	                        <ul class="menu">
	                         <?php foreach($menus_for_layout['hotsite_semipresencial']['threaded'] as $k => $link): ?>
	                             <li>
	                                <a href="<?php echo $this->Html->Url('/'.$link['Link']['seo_url']); ?>" title="<?php echo $link['Link']['title']; ?>" target="<?php echo $link['Link']['target']; ?>"><?php echo $link['Link']['title']; ?></a>
	                            </li>
	                          <?php endForeach; ?>
	                        </ul>
	                    </div>
	                </div>
	            <?php endIf; ?>
	        </div>
	        <?php endIf; ?>

			<?php echo $this->element('site/banner_curso', array('banners' => $banners_paginas, 'data' => $pagina['Pagina'], 'pagina' => $pagina)); ?>
		</div>
	</div>
	
<?php echo $this->Session->flash('indique') ?>  

<?php if($pagina['Pagina']['seo_url'] == 'vestibular' || $pagina['Pagina']['seo_url'] == 'vest'): ?>

	<?php echo $this->element('paginas/vestibular'); ?>

<?php /*elseif($pagina['Pagina']['seo_url'] == 'hovet'): ?>

	<?php echo $this->element('paginas/hovet') */?>

	
<?php elseif($pagina['Pagina']['seo_url'] == 'flex'): ?>

	<?php echo $this->element('paginas/graduacao-flex') ?>

<?php else: ?>

	<div class="<?php echo ($pagina['PaginaTipo']['seo_url'] == 'vestibular-graduacao-executiva') ? 'graduacao-executiva' : ''; ?>">
		<div class="row row-title">
			<div class="container">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<?php if($pagina['Pagina']['template_novo'] != true): ?>
						<h1><?php echo $pagina['Pagina']['nome']; ?></h1>
						<div class="div-campus">
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
							<span class="glyphicon glyphicon-star"></span>
						</div>
					<?php endIf; ?>
				</div>
			</div>
		</div>

		<?php //if(isset($breadcrumbs) && is_array($breadcrumbs) && count($breadcrumbs) > 0): ?>
		<?php if($this->params->url != 'inscricoes-vestibular' && $this->params->url != 'inscricao'): ?>
		<div class="row row-tabs-gray">
			<div class="container">
				<?php echo $this->element('site/breadcrumb') ?>
			</div>
		</div>
		<?php endif; ?>
		<div class="row row-tab-body">
			<div class="<?php echo ($pagina['Pagina']['template_novo'] == true) ? 'container-fluid no-padding' : 'container' ?>">

				<?php if($this->params->url == 'inscricoes-vestibular' || $this->params->url == 'inscricao'): ?>

					<div class="col-xs-12 vestibular">
						<div class="box-prova">
							<?php if(count($pagina['Aba']) > 0): ?>
								<?php echo $pagina['Aba'][0]['conteudo']; ?>
							<?php endif; ?>

							<?php if(Configure::read('HotSite.Link.Inscricao-Manha') != ""): ?>

								<?php if(Configure::read('HotSite.Link.Inscricao-Manha') != ""): ?>
					  				<?php if(Configure::read('HotSite.Link.Inscricao-Manha') != ""): ?>
					  					<a href="<?php echo Configure::read('HotSite.Link.Inscricao-Manha') ?>" target="_blank" title="<?php echo Configure::read('HotSite.Link.Inscricao-Manha-Label') ?>" class="btn btn-danger <?php echo (Configure::read('HotSite.Link.BotaoExtra-Manha') != "") ? 'btn2' : ''; ?>"><?php echo Configure::read('HotSite.Link.Inscricao-Manha-Label') ?></a>
					  				<?php endIf; ?>
					  				<?php if(Configure::read('HotSite.Link.AlterarInscricao-Manha') != ""): ?>
										<a href="<?php echo Configure::read('HotSite.Link.AlterarInscricao-Manha') ?>" target="_blank" title="<?php echo Configure::read('HotSite.Link.AlterarInscricao-Manha-Label') ?>" class="btn btn-black <?php echo (Configure::read('HotSite.Link.BotaoExtra-Manha') != "") ? 'btn2' : ''; ?>"><?php echo Configure::read('HotSite.Link.AlterarInscricao-Manha-Label') ?></a>
									<?php endIf; ?>
									<?php if(Configure::read('HotSite.Link.BotaoExtra-Manha') != ""): ?>
										<a href="<?php echo Configure::read('HotSite.Link.BotaoExtra-Manha') ?>" target="_blank" title="<?php echo Configure::read('HotSite.Link.BotaoExtra-Manha-Label') ?>" class="btn btn-black btn2"><?php echo Configure::read('HotSite.Link.BotaoExtra-Manha-Label') ?></a>
									<?php endIf; ?>
								<?php endIf; ?>

								<?php if(Configure::read('HotSite.Link.Inscricao-Tarde') != ""): ?>
					  				<?php if(Configure::read('HotSite.Link.Inscricao-Tarde') != ""): ?>
					  					<a href="<?php echo Configure::read('HotSite.Link.Inscricao-Tarde') ?>" target="_blank" title="<?php echo Configure::read('HotSite.Link.Inscricao-Tarde-Label') ?>" class="btn btn-danger <?php echo (Configure::read('HotSite.Link.BotaoExtra-Tarde') != "") ? 'btn2' : ''; ?>"><?php echo Configure::read('HotSite.Link.Inscricao-Tarde-Label') ?></a>
					  				<?php endIf; ?>
					  				<?php if(Configure::read('HotSite.Link.AlterarInscricao-Tarde') != ""): ?>
										<a href="<?php echo Configure::read('HotSite.Link.AlterarInscricao-Tarde') ?>" target="_blank" title="<?php echo Configure::read('HotSite.Link.AlterarInscricao-Tarde-Label') ?>" class="btn btn-black <?php echo (Configure::read('HotSite.Link.BotaoExtra-Tarde') != "") ? 'btn2' : ''; ?>"><?php echo Configure::read('HotSite.Link.AlterarInscricao-Tarde-Label') ?></a>
									<?php endIf; ?>
									<?php if(Configure::read('HotSite.Link.BotaoExtra-Tarde') != ""): ?>
										<a href="<?php echo Configure::read('HotSite.Link.BotaoExtra-Tarde') ?>" target="_blank" title="<?php echo Configure::read('HotSite.Link.BotaoExtra-Tarde-Label') ?>" class="btn btn-black btn2"><?php echo Configure::read('HotSite.Link.BotaoExtra-Tarde-Label') ?></a>
									<?php endIf; ?>
								<?php endIf; ?>

								<?php if(Configure::read('HotSite.Link.Inscricao-Noite') != ""): ?>
					  				<?php if(Configure::read('HotSite.Link.Inscricao-Noite') != ""): ?>
					  					<a href="<?php echo Configure::read('HotSite.Link.Inscricao-Noite') ?>" target="_blank" title="<?php echo Configure::read('HotSite.Link.Inscricao-Noite-Label') ?>" class="btn btn-danger <?php echo (Configure::read('HotSite.Link.BotaoExtra-Noite') != "") ? 'btn2' : ''; ?>"><?php echo Configure::read('HotSite.Link.Inscricao-Noite-Label') ?></a>
					  				<?php endIf; ?>
					  				<?php if(Configure::read('HotSite.Link.AlterarInscricao-Noite') != ""): ?>
										<a href="<?php echo Configure::read('HotSite.Link.AlterarInscricao-Noite') ?>" target="_blank" title="<?php echo Configure::read('HotSite.Link.AlterarInscricao-Noite-Label') ?>" class="btn btn-black <?php echo (Configure::read('HotSite.Link.BotaoExtra-Noite') != "") ? 'btn2' : ''; ?>"><?php echo Configure::read('HotSite.Link.AlterarInscricao-Noite-Label') ?></a>
									<?php endIf; ?>
									<?php if(Configure::read('HotSite.Link.BotaoExtra-Noite') != ""): ?>
										<a href="<?php echo Configure::read('HotSite.Link.BotaoExtra-Noite') ?>" target="_blank" title="<?php echo Configure::read('HotSite.Link.BotaoExtra-Noite-Label') ?>" class="btn btn-black btn2"><?php echo Configure::read('HotSite.Link.BotaoExtra-Noite-Label') ?></a>
									<?php endIf; ?>
								<?php endIf; ?>

							<?php else: ?>

								<?php if(Configure::read('HotSite.Link.Inscricao') != ""): ?>
					  				<?php if(Configure::read('HotSite.Link.Inscricao') != ""): ?>
					  					<a href="<?php echo Configure::read('HotSite.Link.Inscricao') ?>" target="_blank" title="<?php echo Configure::read('HotSite.Link.Inscricao-Label') ?>" class="btn btn-danger <?php echo (Configure::read('HotSite.Link.BotaoExtra') != "") ? 'btn2' : ''; ?>"><?php echo Configure::read('HotSite.Link.Inscricao-Label') ?></a>
					  				<?php endIf; ?>
					  				<?php if(Configure::read('HotSite.Link.AlterarInscricao') != ""): ?>
										<a href="<?php echo Configure::read('HotSite.Link.AlterarInscricao') ?>" target="_blank" title="<?php echo Configure::read('HotSite.Link.AlterarInscricao-Label') ?>" class="btn btn-black <?php echo (Configure::read('HotSite.Link.BotaoExtra') != "") ? 'btn2' : ''; ?>"><?php echo Configure::read('HotSite.Link.AlterarInscricao-Label') ?></a>
									<?php endIf; ?>
									<?php if(Configure::read('HotSite.Link.BotaoExtra') != ""): ?>
										<a href="<?php echo Configure::read('HotSite.Link.BotaoExtra') ?>" target="_blank" title="<?php echo Configure::read('HotSite.Link.BotaoExtra-Label') ?>" class="btn btn-black btn2"><?php echo Configure::read('HotSite.Link.BotaoExtra-Label') ?></a>
									<?php endIf; ?>
								<?php endIf; ?>

							<?php endIf; ?>
							<?php /*

							<a href="<?php echo Configure::read('HotSite.Link.Inscricao') ?>" target="_blank" title="Inscrições" class="btn btn-danger">Inscrições</a>
							<a href="<?php echo Configure::read('HotSite.Link.AlterarInscricao') ?>" target="_blank" title="ALTERAR MINHA INSCRIÇÃO" class="btn btn-black">ALTERAR MINHA INSCRIÇÃO</a>
							*/ ?>

							<?php if(Configure::read('HotSite.Link.Inscricao-Agendada') != ""): ?>

							</div>
							<div class="box-prova box-prova-agendada">
								<?php if(Configure::read('HotSite.Link.Titulo-Agendada') != ""): ?>
									<h2><?php echo Configure::read('HotSite.Link.Titulo-Agendada') ?></h2>
								<?php endIf; ?>
								<?php if(Configure::read('HotSite.Link.Inscricao-Agendada') != ""): ?>
				  					<a href="<?php echo Configure::read('HotSite.Link.Inscricao-Agendada') ?>" target="_blank" title="<?php echo Configure::read('HotSite.Link.Inscricao-Agendada-Label') ?>" class="btn btn-danger"><?php echo Configure::read('HotSite.Link.Inscricao-Agendada-Label') ?>
				  					</a>
				  				<?php endIf; ?>
				  				<?php if(Configure::read('HotSite.Link.AlterarInscricao-Agendada') != ""): ?>
									<a href="<?php echo Configure::read('HotSite.Link.AlterarInscricao-Agendada') ?>" target="_blank" title="<?php echo Configure::read('HotSite.Link.AlterarInscricao-Agendada-Label') ?>" class="btn btn-black"><?php echo Configure::read('HotSite.Link.AlterarInscricao-Agendada-Label') ?>
									</a>
								<?php endIf; ?>
							<?php endIf; ?>

						</div>

						<div class="box-prova box-prova-2">
							<h2>OUTRAS FORMAS DE INGRESSO</h2>
							<?php if(Configure::read('HotSite.Link.FormaIngressoTransferencia') != ""): ?>
								<a href="<?php echo Configure::read('HotSite.Link.FormaIngressoTransferencia') ?>" target="_blank" title="TRANSFERÊNCIA" class="btn btn-black btn-small">TRANSFERÊNCIA</a>
							<?php endIf; ?>
							<?php if(Configure::read('HotSite.Link.FormaIngressoSegundaGraduacao') != ""): ?>
								<a href="<?php echo Configure::read('HotSite.Link.FormaIngressoSegundaGraduacao') ?>" target="_blank" title="SEGUNDA GRADUAÇÃO" class="btn btn-black btn-small">SEGUNDA GRADUAÇÃO</a>
							<?php endIf; ?>
							<?php if(Configure::read('HotSite.Link.FormaIngressoEnem') != ""): ?>
								<a href="<?php echo Configure::read('HotSite.Link.FormaIngressoEnem') ?>" target="_blank" title="ENEM" class="btn btn-black btn-small">ENEM</a>
							<?php endIf; ?>
						</div>

					</div>

				<?php elseif($this->params->url == 'inscricao-flex'): ?>

					<div class="col-xs-12">
						<div class="box-prova">
							<?php if(count($pagina['Aba']) > 0): ?>
								<?php echo $pagina['Aba'][0]['conteudo']; ?>
							<?php endif; ?>

							<?php if(Configure::read('Semipresencial.Link.Inscricao-Manha') != ""): ?>

								<?php if(Configure::read('Semipresencial.Link.Inscricao-Manha') != ""): ?>
					  				<?php if(Configure::read('Semipresencial.Link.Inscricao-Manha') != ""): ?>
					  					<a href="<?php echo Configure::read('Semipresencial.Link.Inscricao-Manha') ?>" target="_blank" title="<?php echo Configure::read('Semipresencial.Link.Inscricao-Manha-Label') ?>" class="btn btn-danger"><?php echo Configure::read('Semipresencial.Link.Inscricao-Manha-Label') ?></a>
					  				<?php endIf; ?>
					  				<?php if(Configure::read('Semipresencial.Link.AlterarInscricao-Manha') != ""): ?>
										<a href="<?php echo Configure::read('Semipresencial.Link.AlterarInscricao-Manha') ?>" target="_blank" title="<?php echo Configure::read('Semipresencial.Link.AlterarInscricao-Manha-Label') ?>" class="btn btn-black"><?php echo Configure::read('Semipresencial.Link.AlterarInscricao-Manha-Label') ?></a>
									<?php endIf; ?>
								<?php endIf; ?>

								<?php if(Configure::read('Semipresencial.Link.Inscricao-Tarde') != ""): ?>
					  				<?php if(Configure::read('Semipresencial.Link.Inscricao-Tarde') != ""): ?>
					  					<a href="<?php echo Configure::read('Semipresencial.Link.Inscricao-Tarde') ?>" target="_blank" title="<?php echo Configure::read('Semipresencial.Link.Inscricao-Tarde-Label') ?>" class="btn btn-danger"><?php echo Configure::read('Semipresencial.Link.Inscricao-Tarde-Label') ?></a>
					  				<?php endIf; ?>
					  				<?php if(Configure::read('Semipresencial.Link.AlterarInscricao-Tarde') != ""): ?>
										<a href="<?php echo Configure::read('Semipresencial.Link.AlterarInscricao-Tarde') ?>" target="_blank" title="<?php echo Configure::read('Semipresencial.Link.AlterarInscricao-Tarde-Label') ?>" class="btn btn-black"><?php echo Configure::read('Semipresencial.Link.AlterarInscricao-Tarde-Label') ?></a>
									<?php endIf; ?>
								<?php endIf; ?>

							<?php else: ?>

								<?php if(Configure::read('Semipresencial.Link.Inscricao') != ""): ?>
					  				<?php if(Configure::read('Semipresencial.Link.Inscricao') != ""): ?>
					  					<a href="<?php echo Configure::read('Semipresencial.Link.Inscricao') ?>" target="_blank" title="<?php echo Configure::read('Semipresencial.Link.Inscricao-Label') ?>" class="btn btn-danger"><?php echo Configure::read('Semipresencial.Link.Inscricao-Label') ?></a>
					  				<?php endIf; ?>
					  				<?php if(Configure::read('Semipresencial.Link.AlterarInscricao') != ""): ?>
										<a href="<?php echo Configure::read('Semipresencial.Link.AlterarInscricao') ?>" target="_blank" title="<?php echo Configure::read('Semipresencial.Link.AlterarInscricao-Label') ?>" class="btn btn-black"><?php echo Configure::read('Semipresencial.Link.AlterarInscricao-Label') ?></a>
									<?php endIf; ?>
								<?php endIf; ?>

							<?php endIf; ?>

							<?php /*
							<a href="<?php echo Configure::read('Semipresencial.Link.Inscricao') ?>" target="_blank" title="Inscrições" class="btn btn-danger">Inscrições</a>
							<a href="<?php echo Configure::read('Semipresencial.Link.AlterarInscricao') ?>" target="_blank" title="ALTERAR MINHA INSCRIÇÃO" class="btn btn-black">ALTERAR MINHA INSCRIÇÃO</a>
							*/ ?>

							<?php if(Configure::read('Semipresencial.Link.Inscricao-Agendada') != ""): ?>

							</div>
							<div class="box-prova box-prova-agendada">
								<?php if(Configure::read('Semipresencial.Link.Titulo-Agendada') != ""): ?>
									<h2><?php echo Configure::read('Semipresencial.Link.Titulo-Agendada') ?></h2>
								<?php endIf; ?>
								<?php if(Configure::read('Semipresencial.Link.Inscricao-Agendada') != ""): ?>
				  					<a href="<?php echo Configure::read('Semipresencial.Link.Inscricao-Agendada') ?>" target="_blank" title="<?php echo Configure::read('Semipresencial.Link.Inscricao-Agendada-Label') ?>" class="btn btn-danger"><?php echo Configure::read('Semipresencial.Link.Inscricao-Agendada-Label') ?>
				  					</a>
				  				<?php endIf; ?>
				  				<?php if(Configure::read('Semipresencial.Link.AlterarInscricao-Agendada') != ""): ?>
									<a href="<?php echo Configure::read('Semipresencial.Link.AlterarInscricao-Agendada') ?>" target="_blank" title="<?php echo Configure::read('Semipresencial.Link.AlterarInscricao-Agendada-Label') ?>" class="btn btn-black"><?php echo Configure::read('Semipresencial.Link.AlterarInscricao-Agendada-Label') ?>
									</a>
								<?php endIf; ?>
							<?php endIf; ?>

						</div>

						<div class="box-prova box-prova-2">
							<h2>OUTRAS FORMAS DE INGRESSO</h2>
							<?php if(Configure::read('Semipresencial.Link.FormaIngressoTransferencia') != ""): ?>
								<a href="<?php echo Configure::read('Semipresencial.Link.FormaIngressoTransferencia') ?>" target="_blank" title="TRANSFERÊNCIA" class="btn btn-black btn-small">TRANSFERÊNCIA</a>
							<?php endIf; ?>
							<?php if(Configure::read('Semipresencial.Link.FormaIngressoSegundaGraduacao') != ""): ?>
								<a href="<?php echo Configure::read('Semipresencial.Link.FormaIngressoSegundaGraduacao') ?>" target="_blank" title="DIPLOMADO" class="btn btn-black btn-small">DIPLOMADO</a>
							<?php endIf; ?>
							<?php if(Configure::read('Semipresencial.Link.FormaIngressoEnem') != ""): ?>
								<a href="<?php echo Configure::read('Semipresencial.Link.FormaIngressoEnem') ?>" target="_blank" title="ENEM" class="btn btn-black btn-small">ENEM</a>
							<?php endIf; ?>
						</div>

					</div>

				<?php else: ?>

					<div role="tabpanel">

						<?php if($pagina['Pagina']['exibir_abas'] == true): ?>
							<?php if(count($pagina['Aba']) > 0): ?>
								<?php if($pagina['Pagina']['template_novo'] != true): ?>
									<?php echo $this->element('paginas/abas') ?>
								<?php else: ?>
									<div class="container">
										<?php echo $this->element('paginas/abas') ?>
									</div>
								<?php endIf; ?>
							<?php endIf; ?>
						<?php endIf; ?>

						<!-- Tab panes -->
						<div class="tab-content">
							<?php $first = true; ?>
							<?php foreach($pagina['Aba'] as $k => $aba): ?>

								<?php
									if($aba['status'] == false){
										 continue;
									}
								?>

							  	<div role="tabpanel" class="tab-pane <?php echo ($first == true) ? 'active' : ''; ?>" id="<?php echo $aba['seo_url']; ?>">

							  		<?php if($aba['sidebar_status'] == true): ?>

							  				<?php //if($this->params->url == 'credenciais'): ?>
												<?php //echo $this->element('FdGraduacaoCursos.paginas/credenciais') ?>
											<?php //elseif($this->params->url == 'bolsas-e-financiamentos'): ?>
												<?php //echo $this->element('FdGraduacaoCursos.paginas/bolsas-e-financiamentos') ?>
											<?php //else: ?>

									  			<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 row-content-curso vestibular">

									  				<?php if ($aba['dinamico'] == 1 && $aba['dinamico_elemento'] != ""): ?>
									  					<?php $pagina_element = $this->element('paginas/' . $aba['dinamico_elemento'] . ''); ?>
									  					<?php
									  						if(stripos($aba['conteudo'], '{CONTEUDO_DINAMICO}') !== FALSE){
																$aba['conteudo'] = str_replace('{CONTEUDO_DINAMICO}', $pagina_element, $aba['conteudo']);
															}
									  					?>
									  				<?php endIf; ?>

									    			<?php echo $aba['conteudo']; ?>

									    		</div>

									    		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 sidebar">

									    			<?php if($this->params->url == 'hovet'): ?>
														<?php echo $this->element('paginas/form-cadastro-hovet') ?>

													<?php elseif($this->params->url == 'reingresso-uniritter'): ?>
														<?php echo $this->element('paginas/form-reingresso') ?>

													<?php //elseif($this->params->url == 'negociar'): ?>
														<?php //echo $this->element('paginas/form-negociar') ?>

													<?php else: ?>

										    			<?php if($pagina['PaginaTipo']['seo_url'] == "vestibular-uniritter"): ?>
										    				<div style="margin-bottom: -80px; display: inline-block; width: 100%;">
										    					<?php echo $this->element('site/vestibular') ?>
										    				</div>
										    			<?php endIf; ?>

										    			<?php if($pagina['PaginaTipo']['seo_url'] == "vestibular-graduacao-executiva"): ?>
										    				<div style="margin-bottom: -80px; display: inline-block; width: 100%;">
										    					<?php echo $this->element('site/vestibular-graduacao-executiva') ?>
										    				</div>
										    			<?php endIf; ?>

														<?php if($pagina['Pagina']['sidebar'] != ""): ?>
														    <?php echo $pagina['Pagina']['sidebar']; ?>
														<?php endIf; ?>

														<?php if($aba['sidebar'] != ""): ?>
														    <?php echo $aba['sidebar']; ?>
														<?php endIf; ?>

														<?php if(isset($bloco_noticias) && count($bloco_noticias) > 0): ?>
												          <div class="my-news" style="margin-bottom: 0px;">
												            <h4 class="title-total-red">Notícias</h4>
												            <ul>
												                <?php foreach($bloco_noticias as $k => $noticia): ?>
												                <li>
												                    <p>
												                        <a href="<?php echo $this->Html->Url('/' . $noticia['Noticia']['seo_url']); ?>" title="<?php echo $noticia['Noticia']['titulo'] ?>">
												                          <?php echo $noticia['Noticia']['titulo'] ?>
												                        </a>
												                    </p>
												                </li>
												                <?php endForeach; ?>
												            </ul>
												            <p class="todas-noticias"><a href="<?php echo $this->Html->Url('/' . $bloco['Bloco']['seo_url']); ?>" title="VEJA TODAS AS NOTÍCIAS">VEJA TODAS AS NOTÍCIAS</a></p>
												          </div>

												          <div class="clear"></div>
												      	<?php endIf; ?>

												      	<?php if($pagina['Pagina']['formulario']): ?>
														      <?php echo $this->element('site/fale_conosco') ?>
														<?php endIf; ?>
														<?php endIf; ?>
	                                                    <?php if($this->params->url == 'biblioteca'): ?>
	                                                        <h3>Horário de Atendimento da biblioteca</h3>
	                                                        <p>
	                                                            Segunda a sexta: das 07h45  às 22h30
	                                                        </p>
	                                                        <p>
	                                                            Sábado: das 8h às 12h.
	                                                        </p>
	                                                    <?php endIf; ?>

													<?php echo $this->element('site/redes-sociais-mini') ?>

												</div><!-- end sidebar-->

											<?php //endIf; ?>

									<?php else: ?>

										<?php if($this->params->url != 'international-office'): ?>
											<?php if ($aba['dinamico'] == 1 && $aba['dinamico_elemento'] != ""): ?>
												<?php if($pagina['Pagina']['template_novo'] == true): ?>
													<?php $pagina_element = $this->element('FdGraduacaoCursos.paginas/' . $aba['dinamico_elemento'] . ''); ?>
												<?php else: ?>
													<?php $pagina_element = $this->element('paginas/' . $aba['dinamico_elemento'] . ''); ?>
												<?php endIf; ?>
												<?php
													if(stripos($aba['conteudo'], '{CONTEUDO_DINAMICO}') !== FALSE){
														$aba['conteudo'] = str_replace('{CONTEUDO_DINAMICO}', $pagina_element, $aba['conteudo']);
													}
												?>
											<?php endIf; ?>

											<?php echo $aba['conteudo']; ?>
										<?php endIf; ?>

									<?php endIf; ?>

							  	</div>

							  	<?php $first = false; ?>

							<?php endForeach; ?>

						</div>

					</div><!-- end tabs -->

				<?php endIf; ?>

			</div>
			<?php if($this->params->url == 'international-office'): ?>
				<?php echo $this->element('paginas/international_office_map') ?>
			<?php endIf; ?>
		</div>
	</div>

	<?php if($this->params->url == 'escola-de-verao'): ?>
		<?php echo $this->element('paginas/escola-de-verao-modal') ?>
	<?php endIf; ?>

<?php endIf; ?>

<?php echo $this->element('FdGraduacaoCursos.paginas/modal-indique') ?>

<?php if($this->params->url == 'nra' || $pagina['PaginaTipo']['seo_url'] == 'vestibular-uniritter'): ?>
	<script type="text/javascript">
	var _tn = _tn || [];
	_tn.push(['_setAccount','1359aa933b48b754a2f54adb688bfa77']);
	_tn.push(['_setAction','track-view']);
	(function() {
	  document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
	  var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
	  tss.src = '//tracker.tolvnow.com/js/tn.js';
	  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
	})();
	</script>
<?php endIf; ?>
