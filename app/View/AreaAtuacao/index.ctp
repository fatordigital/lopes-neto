<main class="areas-de-atuacao interna">
    <section class="header-interna">
        <h1>
            <?php echo $this->Idioma->palavra('areas-da-atuacao'); ?><br /><small><?php echo $this->Idioma->palavra('uma-orchestra-de-grades-solucoes'); ?></small>
        </h1>
    </section>

    <section class="page-content">
        <div class="container-fluid">
            <div class="row row-content">
                <div class="col-xs-12 col-sm-4 col-sm-push-8 col-md-5 col-md-push-7 no-padding">
                    <div class="has-bg has-after equal-heights" style="background-image: url(<?php echo $this->Html->url('/assets/images/bgs/atuacao-onde-a-orchestra-esta-inserida.jpg') ?>);">

                    </div>
                </div><!-- end of /.cols.equal-heights -->
                <div class="col-xs-12 col-sm-8 col-sm-pull-4 col-md-7 col-md-pull-5 equal-heights">
                    <div class="text-content">
                        <h2><span><?php echo $this->Idioma->palavra('atuacao'); ?></span><span><?php echo $this->Idioma->palavra('onde-a-orchestra-esta-inserida'); ?></span></h2>
                        <p class="big">
                            <?php echo $this->Idioma->palavra('dentro-desse-enfoque-de-interdependencia'); ?>
                        </p>
                    </div><!-- end of /.text-content -->
                </div><!-- end of /.cols -->
            </div><!-- end of /.row.row-content -->

            <div class="row row-content">
                <div class="col-xs-12 col-sm-6 col-md-5 no-padding">
                    <div class="has-bg has-after-right equal-heights" style="background-image: url(<?php echo $this->Html->url('/assets/images/bgs/gestao.jpg') ?>);">

                    </div>
                </div><!-- end of /.cols.equal-heights -->
                <div class="col-xs-12 col-sm-6 col-md-7 equal-heights">
                    <div class="text-content">
                        <h2><span><?php echo $this->Idioma->palavra('gestao'); ?></span></h2>
                        <ul>
                            <?php echo $this->Bloco->bloco('lista-gestao', 'conteudo'); ?>
                        </ul>
                    </div><!-- end of /.text-content -->
                </div><!-- end of /.cols -->
            </div><!-- end of /.row.row-content -->


            <div class="row row-content">
                <div class="col-xs-12 col-sm-6 col-sm-push-6 col-md-5 col-md-push-7 no-padding">
                    <div class="has-bg has-after equal-heights" style="background-image: url(<?php echo $this->Html->url('/assets/images/bgs/financas.jpg') ?>);">

                    </div>
                </div><!-- end of /.cols.equal-heights -->
                <div class="col-xs-12 col-sm-6 col-sm-pull-6 col-md-7 col-md-pull-5 equal-heights">
                    <div class="text-content">
                        <h2 class="right"><span><?php echo $this->Idioma->palavra('financa'); ?></span></h2>
                        <ul>
                            <?php echo $this->Bloco->bloco('lista-financas', 'conteudo'); ?>
                        </ul>
                    </div><!-- end of /.text-content -->
                </div><!-- end of /.cols -->
            </div><!-- end of /.row.row-content -->


            <div class="row row-content">
                <div class="col-xs-12 col-sm-6 col-md-5 no-padding">
                    <div class="has-bg has-after-right equal-heights" style="background-image: url(<?php echo $this->Html->url('/assets/images/bgs/pessoas.jpg') ?>);">

                    </div>
                </div><!-- end of /.cols.equal-heights -->
                <div class="col-xs-12 col-sm-6 col-md-7 equal-heights">
                    <div class="text-content">
                        <h2 class="ln-100"><span><?php echo $this->Idioma->palavra('pessoas'); ?><br /><small style="font-size: 30%"><?php echo $this->Idioma->palavra('desenvolvimento-orgazicional'); ?></small></span></h2>
                        <ul>
                            <?php echo $this->Bloco->bloco('lista-pessoas', 'conteudo'); ?>
                        </ul>
                    </div><!-- end of /.text-content -->
                </div><!-- end of /.cols -->
            </div><!-- end of /.row.row-content -->



            <div class="row row-content">
                <div class="col-xs-12 col-sm-6 col-sm-push-6 col-md-5 col-md-push-7 no-padding">
                    <div class="has-bg has-after equal-heights" style="background-image: url(<?php echo $this->Html->url('/assets/images/bgs/assessoria.jpg') ?>);">

                    </div>
                </div><!-- end of /.cols.equal-heights -->
                <div class="col-xs-12 col-sm-6 col-sm-pull-6 col-md-7 col-md-pull-5 equal-heights">
                    <div class="text-content">
                        <h2 class="right ln-100"><span><?php echo $this->Idioma->palavra('assessoria-juridica'); ?></span></h2>
                        <ul>
                            <?php echo $this->Bloco->bloco('lista-assessoria-juridica', 'conteudo'); ?>
                        </ul>
                    </div><!-- end of /.text-content -->
                </div><!-- end of /.cols -->
            </div><!-- end of /.row.row-content -->




            <div class="row row-content">
                <div class="col-xs-12 col-sm-6 col-md-5 no-padding">
                    <div class="has-bg has-after-right equal-heights" style="background-image: url(<?php echo $this->Html->url('/assets/images/bgs/sistemas.jpg') ?>);">

                    </div>
                </div><!-- end of /.cols.equal-heights -->
                <div class="col-xs-12 col-sm-6 col-md-7 equal-heights">
                    <div class="text-content">
                        <h2><span><?php echo $this->Idioma->palavra('sistemas'); ?></span></h2>
                        <ul>
                            <?php echo $this->Bloco->bloco('lista-sistemas', 'conteudo'); ?>
                        </ul>
                    </div><!-- end of /.text-content -->
                </div><!-- end of /.cols -->
            </div><!-- end of /.row.row-content -->



            <div class="row row-content">
                <div class="col-xs-12 col-sm-6 col-sm-push-6 col-md-5 col-md-push-7 no-padding">
                    <div class="has-bg has-after equal-heights" style="background-image: url(<?php echo $this->Html->url('/assets/images/bgs/parceria.jpg') ?>);">

                    </div>
                </div><!-- end of /.cols.equal-heights -->
                <div class="col-xs-12 col-sm-6 col-sm-pull-6 col-md-7 col-md-pull-5 equal-heights">
                    <div class="text-content">
                        <h2 class="right"><span><?php echo $this->Idioma->palavra('parceiros'); ?></span></h2>
                        <p>
                            <?php echo $this->Idioma->palavra('para-ampliar-a-capacidade-de-solucao'); ?>
                        </p>
                        <p><?php echo $this->Idioma->palavra('outras-especialidades-providas'); ?></p>
                        <ul>
                            <?php echo $this->Bloco->bloco('lista-parceiros', 'conteudo'); ?>
                        </ul>
                    </div><!-- end of /.text-content -->
                </div><!-- end of /.cols -->
            </div><!-- end of /.row.row-content -->


        </div><!-- end of /.container-fluid -->
        <section>

</main>