<?php echo $this->assign('title', 'Busca por ' . $this->params['named']['item']); ?>

    <?php echo $this->element('site/banner_curso'); ?>

    </div>
</div>
</div>
    <div class="row row-title">
        <div class="container content-container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h1>
                    <span>Busca por "<?php echo $this->params['named']['item']; ?>"</span>
                </h1>
            </div>
        </div>
    </div>
    <div class="row row-tabs-gray">
        <div class="container">
            <span class="count_busca"><?php echo $this->params['paging']['Rota']['count']+count($cursos); ?> resultados encontrados</span>

            <?php echo $this->element('site/breadcrumb') ?>
        </div>
    </div>
    <div class="row row-tab-body">      
        <div class="container content-container">
            <div role="tabpanel">
                  
                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="apresentacao1">
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <br /><br />

                            <?php if( ($this->params['paging']['Rota']['page'] == 1 || (isset($this->params->params['curso']) && $this->params->params['curso'] == true) ) && isset($cursos) && count($cursos) > 0): ?>

                                <h4 class="title-color title-red"><?php echo count($cursos) ?> cursos encontrados</h4>

                                <?php foreach($cursos as $y => $rota): ?>

                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 box-news <?php echo ($y%2 == 0) ? 'clear' : ''; ?>">
                                        <a href="<?php echo $this->Html->Url('/' . $rota['Rota']['seo_url']); ?>" title="<?php echo $rota['Rota']['nome']; ?>" class="btn-box-curso">
                                            <div class="box-curso">
                                                <span class="curso-name"><?php echo $rota['Rota']['nome']; ?></span>
                                            </div>
                                        </a>
                                    </div>

                                <?php endForeach; ?>

                            <?php endIf; ?>



                            <?php if(isset($rotas) && count($rotas) > 0): ?>

                                <ul class="busca-list">

                                    <?php $ultimo_model = ''; ?>
                                    <?php foreach($rotas as $k => $rota): ?>
                                        
                                        <?php if($ultimo_model == "" || $ultimo_model != $rota['Rota']['model']): ?>
                                            <h4 class="title-color title-red"><?php echo count($totais[$rota['Rota']['model']]); ?> <?php echo $rota['Rota']['controller']; ?> <?php echo ($rota['Rota']['model'] == 'Noticia' || $rota['Rota']['model'] == 'Pagina') ? 'encontradas' : 'encontrados'; ?> </h4>
                                        <?php endIf; ?>

                                        <li>
                                            <?php if($rota['Rota']['data'] != ""): ?>
                                                <span class="data"><?php echo $this->Time->format($rota['Rota']['data'], '%d/%m/%Y') ?></span>
                                            <?php endIf; ?>
                                            <h2><?php echo $rota['Rota']['nome']; ?></h2>
                                            <?php if($rota['Rota']['seo_description'] != ""): ?>
                                                <p><?php echo $rota['Rota']['seo_description']; ?></p>
                                            <?php endIf; ?>
                                            <a href="<?php echo $this->Html->Url('/' . $rota['Rota']['seo_url']); ?>" class="btn-leia-mais" title="Leia mais">Leia mais »</a>
                                        </li>

                                        <?php $ultimo_model = $rota['Rota']['model']; ?>

                                    <?php endForeach; ?>

                                </ul>

                                <?php if(isset($this->params['paging']['Rota']['pageCount']) && $this->params['paging']['Rota']['pageCount'] > 1): ?>
                                    <div class="paginacao">
                                        <ul>
                                            <li class="voltar">
                                                <?php echo $this->Paginator->prev(__('« Voltar', true), array(), null, array()); ?>
                                            </li>
                                            <?php echo $this->Paginator->numbers(array('classs' => 'page', 'separator' => ' ', 'tag' => 'li')); ?>
                                            <?php if(($this->Paginator->current()+9) < $this->Paginator->counter(array('format' => '{:pages}'))): ?>
                                                <li>...</li>
                                                <li>
                                                    <?php echo $this->Paginator->last($this->Paginator->counter(array('format' => '{:pages}')), array(), null, array()); ?>
                                                </li>
                                            <?php endIf; ?>
                                            <li class="avancar">
                                                <?php echo $this->Paginator->next(__('Avançar »', true), array(), null, array()); ?>
                                            </li>
                                        </ul>
                                    </div>
                                <?php endIf; ?>

                                <br /><br /><br />

                                
                            <?php else: ?>
                                <div class="clear"></div>
                                <?php if(!isset($cursos) || count($cursos) == 0): ?>
                                    <p>Nenhum resultado encontrado.</p>
                                <?php endIf; ?>
                            <?php endIf ?>

                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 sidebar" >
                            <?php echo $this->element('site/fale_conosco') ?>
                            <?php echo $this->element('site/redes-sociais-mini') ?>
                        </div>                                     
                    </div>
                    
                </div>
            </div>
        </div> 
    </div>




<?php /*


<section class="interna">
    <div class="top">
        <strong class="pos">PÓS-GRADUAÇÃO</strong>
        <span class="nome">UNIRITTER</span>

        <img src="<?php echo $this->Html->Url('/assets'); ?>/img/uniritter-flag.png" alt="UniRitter" class="flag">
    </div>
    <div class="bar-curso">
        <div class="container">
            <div class="col-sm-6 col-md-8 wrapper-curso">
                <h1>
                    <span class="title-part1">&nbsp;</span>
                    <span class="title-part2">
                        Busca por cursos: "<?php echo $this->params['named']['item']; ?>"
                    </span>
                </h1>
            </div>
        </div>
    </div>

    <div class="space-bottom">

        <div class="container line">
            <div class="row">

				<div class="col-xs-12 col-sm-7 list-busca">

				    <div class="item">

						<?php if(count($cursos) > 0): ?>
						<div class="list-module">
							<ul>
						        <?php foreach($cursos as $k => $curso): ?>
								<li>
									<h4><?php echo $curso['Curso']['nome']; ?></h4>
									<p><?php echo $this->String->truncate_str(strip_tags($curso['Curso']['descricao']), 250); ?></p>
									<a href="<?php echo $this->Html->Url('/' . $curso['Curso']['seo_url'], true); ?>" title="<?php echo $curso['Curso']['nome']; ?>">Conheça o Curso >></a>
								</li>
						       <?php endForeach; ?>
							<ul>
						</div>

                        <?php if(isset($this->params['paging']['Curso']['pageCount']) && $this->params['paging']['Curso']['pageCount'] > 1): ?>
                            <ul class="col-xs-12 col-xs-offset-0 col-lg-6 col-lg-offset-4 col-md-12 paginacao">
                                <?php //echo $this->Paginator->prev('&larr; Anterior', array('tag' => 'li', 'escape' => false), null); ?>
                                <?php echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentClass' => 'active', 'modulus' => 4)); ?>
                                <?php //echo $this->Paginator->next('Próximo &rarr;', array('tag' => 'li', 'escape' => false), null); ?>
                            </ul>
                            <?php endIf; ?>

                        <?php else: ?>

                            Nenhum curso encontrado.

						<?php endIf; ?>

                    </div>

				</div>

				<?php echo $this->element('site/sidebar') ?>

            </div>
        </div>
    </div>
</section>

*/ ?>