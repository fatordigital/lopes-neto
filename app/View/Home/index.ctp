<header id="home">
    <div class="fd_carousel_topo owl-carousel owl-theme">
        <div class="item" style="background-image: url('<?php echo $this->Html->Url('/assets/images/backgrounds/poa3.jpg') ?>');">
        </div>
        <div class="item" style="background-image: url('<?php echo $this->Html->Url('/assets/images/backgrounds/poa.jpg') ?>');">
        </div>
        <div class="item" style="background-image: url('<?php echo $this->Html->Url('/assets/images/backgrounds/poa2.jpg') ?>');">
        </div>
    </div>
    <div class="header-content">
        <div class="container">
            <h1>
                <small>Lopes Neto, a sua</small><br />
                boutique jurídica
            </h1>
        </div><!-- end of /.container -->
    </div><!-- end of /.header-content -->
</header>
<section class="header-contact">
    <div class="container">
        <div class="fd_item">
            <figure>
                <img src="<?php echo $this->Html->Url('/assets/images/icons/phone.png') ?>" alt="Fale Conosco">
            </figure>
            <p>
                <small><?php echo $this->Idioma->palavra('fale-conosco') ?></small><br />
                <span class="thin"><?php echo $this->Idioma->palavra('ddd'); ?></span> <?php echo $this->Idioma->palavra('telefones'); ?>
            </p>
        </div><!-- end of /.fd_item -->
        <div class="fd_item">
            <figure><img src="<?php echo $this->Html->Url('/assets/images/icons/message.png') ?>" alt="Envie um E-mail"></figure>
            <p>
                <small><?php echo $this->Idioma->palavra('envie-um-email'); ?></small><br />
                <span class="thin"><?php echo $this->Idioma->palavra('e-mail'); ?></span>
            </p>
        </div><!-- end of /.fd_item -->
        <div class="fd_item">
            <figure><img src="<?php echo $this->Html->Url('/assets/images/icons/message.png') ?>" alt="Envie um e-mail"></figure>
            <p>
                <small><?php echo $this->Idioma->palavra('faca-uma-visita'); ?></small><br />
                <span class="thin"><?php echo $this->Idioma->palavra('endereco'); ?></span>
            </p>
        </div><!-- end of /.fd_item -->
    </div><!-- end of /.container -->
</section><!-- end of /.header-contact -->


<section class="institutional" id="institucional">
    <div class="institutional-content">
        <div class="container">

            <h2>
                <small>SOBRE A</small><br />
                Lopes Neto
            </h2>

            <?php echo $this->Bloco->bloco('sobre', 'conteudo'); ?>

        </div><!-- end of /.container -->
    </div><!-- end of/.about-text -->


    <div class="fd_wrap_team">
        <div class="container">
            <div class="fd_item">
                <div class="fd_content">
                    <a href="javascript:void(0)" class="fd_content_button fd_open_resume equal-height" data-resume="silvano-joaquin">
                        <figure>
                            <img src="<?php echo $this->Html->Url('/assets/images/user.jpg') ?>" alt="Silvino Joaquim Lopes Neto">
                        </figure>
                    </a>
                    <p>Silvino Joaquim Lopes Neto</p>
                    <div class="fd_buttons">
                        <a href="javascript:void(0)" target="_blank">
                            <img src="<?php echo $this->Html->Url('/assets/images/icons/linkedin.svg') ?>" alt="LinkedIn"/>
                        </a>
                        <a href="javascript:void(0)" class="fd_open_resume" data-resume="silvano-joaquin">
                            <img src="<?php echo $this->Html->Url('/assets/images/icons/plus.svg') ?>" alt="Mostrar Mais"/>
                        </a>
                    </div>
                </div><!-- end of /.fd_content -->
                <div class="fd_content_resume left equal-height" id="silvano-joaquin">
                    <p>
                        Advogado inscrito na OAB/RS sob o nº 2.369<br />
                        Graduado em Direito pela Universidade Federal do Rio Grande do Sul (Faculdade de Direito de Pelotas) 1957;<br />
                        Graduado em Filosofia pela Universidade Católica de Pelotas 1959;<br />
                        Livre Docente em Filosofia do Direito e Doutor em Direito pela UFRGS 1963;<br />
                        Diretor do Instituto de Sociologia e Política da Universidade Federal do Rio Grande do Sul no período de 1970 a 1974;<br />
                        Diretor da Faculdade de Direito de Pelotas (UFPEL) no período de 1975 a 1979;<br />
                        Desembargador Aposentado do TJRS;<br />
                        Secretário Geral do Ensino Superior (MEC) 1990;<br />
                        Conselheiro Federal de Educação 1992;<br />
                        Presidente do Instituto dos Advogados RS no período de 1989 a 1995;<br />
                        Diretor dos Cursos de Ciências Jurídicas da Universidade Bandeirante de São Paulo (UNIBAN);<br />
                        Vice-Reitor da Universidade Bandeirante de São Paulo (UNIBAN).<br />
                        <strong>DISTINÇÕES e CONDECORAÇÕES</strong><br />
                        Beca de Honor da Casa de Madri – Espanha;<br />
                        Colar da Sociedade de Geografia de Lisboa – Portugal;<br />
                        Colar do Conselho das Comunidades de Cultura Luso-Brasileira;<br />
                        Medalha do Mérito Universitário – UFPEL;<br />
                        Comenda Osvaldo Vergara – OAB/RS;<br />
                        Professor Insigne do Instituto dos Advogados do RS;<br />
                        Professor “honoris causa” da Fundação Universidade de Bagé;<br />
                        Professor Emérito da Faculdade de Direito de Curitiba;<br />
                        Professor Emérito da FUNBA – Bagé – RS.<br />
                        PALESTRAS e CONFERÊNCIAS INTERNACIONAIS<br />
                        Faculdade de Direito de Madri – Espanha;<br />
                        Faculdade de Direito de Madri – Espanha;<br />
                        Faculdade de Direito da Universidade de Barcelona – Espanha;<br />
                        Faculdade de Direito da Universidade de Valencia – Espanha;<br />
                        Faculdade de Direito da Universidade de Coimbra – Portugal;<br />
                        Faculdade de Direito da Universidade de Lisboa – Portugal;<br />
                        Faculdade de Filosofia de Braga – Portugal;<br />
                        Tribunal da Relação – Cidade do Porto – Portugal;<br />
                        Faculdade de Direito da Universidade de Córdoba – Argentina.<br />
                        EX-PROFESSOR DAS SEGUINTES INSTITUIÇÕES:<br />
                        Pós-Graduação da UFRGS e da PUC/RS;<br />
                        Faculdade de Direito da UFPEL;<br />
                        Faculdade de Direito da PUC/RS;<br />
                        Faculdade de Direito de Curitiba/PR;<br />
                        Escola Superior do Ministério Público/RS;<br />
                        Escola Nacional da Magistratura;<br />
                        Associação dos Juízes do RGS;<br />
                        Instituto dos Advogados do RGS;<br />
                        Idiomas: Português, Francês e Espanhol
                    </p>
                </div>
            </div><!-- end of /.fd_item -->
            <div class="fd_item">
                <div class="fd_content">
                    <a href="javascript:void(0)" class="fd_content_button fd_open_resume equal-height" data-resume="cesar-lopes">
                        <figure>
                            <img src="<?php echo $this->Html->Url('/assets/images/cesar.jpg') ?>" alt="César Pereira Lima Lopes">
                        </figure>
                    </a>
                    <p>César Pereira Lima Lopes</p>
                    <div class="fd_buttons">
                        <a href="javascript:void(0)" target="_blank">
                            <img src="<?php echo $this->Html->Url('/assets/images/icons/linkedin.svg') ;?>" alt="LinkedIn"/>
                        </a>
                        <a href="javascript:void(0)" class="fd_open_resume" data-resume="cesar-lopes">
                            <img src="<?php echo $this->Html->Url('/assets/images/icons/plus.svg') ;?>" alt="Mostrar Mais"/>
                        </a>
                    </div>
                </div><!-- end of /.fd_content -->
                <div class="fd_content_resume left equal-height" id="cesar-lopes">
                    <p>
                        Advogado inscrito na OAB/RS sob o nº 30.887<br />
                        Graduado em Direito na PUC/RS;<br />
                        Pós-graduado em Direito Romano e Direito da Integração Latino-Americana na PUC/RS;<br />
                        Mestre em Direito Processual Civil na PUC/RS;<br />
                        Professor de Direito Constitucional da PUC/RS;<br />
                        Professor de Direito Processual Civil da PUC/RS;<br />
                        Professor da Escola Superior do Ministério Público do RS;<br />
                        Professor de Direito Processual Civil do EAD – Ensino a Distância – na Pós-graduação da PUC/RS;<br />
                        Professor de Teoria Geral do Direito na Pós-Graduação de Fundamentos em Saúde Mental e Lei da Fundação Faculdade de Ciências Médicas do RS;<br />
                        Consultor Jurídico do IBGEN - Instituto Brasileiro de Gestão de Negócios;<br />
                        Professor de Direito Processual Civil e Direito Constitucional do IDC-Instituto de Desenvolvimento Cultural;<br />
                        Professor de Direito Processual Civil e Direito Civil no Instituto Brasileiro de Gestão de Negócios – IBGEN;<br />
                        Professor de Direito Processual Civil e Direito Civil na Faculdade São Judas Tadeu;<br />
                        Idiomas: Português, Inglês, Espanhol, Italiano.
                    </p>
                </div>
            </div><!-- end of /.fd_item -->
            <div class="fd_item">
                <div class="fd_content">
                    <a href="javascript:void(0)" class="fd_content_button fd_open_resume equal-height" data-resume="thiago-cervo">
                        <figure>
                            <img src="<?php echo $this->Html->Url('/assets/images/thiago.jpg') ;?>" alt="Thiago Cervo Veber">
                        </figure>
                    </a>
                    <p>Thiago Cervo Veber</p>
                    <div class="fd_buttons">
                        <a href="" target="_blank">
                            <img src="<?php echo $this->Html->Url('/assets/images/icons/linkedin.svg') ;?>" alt="LinkedIn"/>
                        </a>
                        <a href="javascript:void(0)" class="fd_open_resume" data-resume="thiago-cervo">
                            <img src="<?php echo $this->Html->Url('/assets/images/icons/plus.svg') ;?>" class="fd_open_resume" alt="Mostrar Mais"/>
                        </a>
                    </div>
                </div><!-- end of /.fd_content -->
                <div class="fd_content_resume right equal-height" id="thiago-cervo">
                    <p>
                        Advogado, inscrito na OAB/RS sob n. 107.668;<br />
                        Graduado em Direito pelo Centro Universitário Metodista, IPA;<br />
                        Graduado em Ciências Contábeis pela Universidade Federal de Santa Maria – UFSM;<br />
                        Pós-graduado (MBA) em Gestão de Negócios pela Universidade Federal de Santa Maria – UFSM;<br />
                        Pós-graduando em Direito Tributário pela Pontifícia Universidade Católica do Rio Grande do Sul – PUC/RS;
                    </p>
                </div>
            </div><!-- end of /.fd_item -->
        </div><!-- end of /.container -->
        <div class="container">
            <div class="fd_item">
                <div class="fd_content">
                    <a href="javascript:void(0)" class="fd_content_button fd_open_resume equal-height" data-resume="bernardo-gnieslaw">
                        <figure>
                            <img src="<?php echo $this->Html->Url('/assets/images/bernardo.jpg') ;?>" alt="Bernardo Gnieslaw">
                        </figure>
                    </a>
                    <p>Bernardo Gnieslaw</p>
                    <div class="fd_buttons">
                        <a href="javascript:void(0)" target="_blank">
                            <img src="<?php echo $this->Html->Url('/assets/images/icons/linkedin.svg') ;?>" alt="LinkedIn"/>
                        </a>
                        <a href="javascript:void(0)" class="fd_open_resume" data-resume="bernardo-gnieslaw">
                            <img src="<?php echo $this->Html->Url('/assets/images/icons/plus.svg') ;?>" alt="Mostrar Mais"/>
                        </a>
                    </div>
                </div><!-- end of /.fd_content -->
                <div class="fd_content_resume left equal-height" id="bernardo-gnieslaw">
                    <p><strong>Estagiário</strong></p>
                    <p>
                        Graduando em Direito pelo Instituto Brasileiro de Gestão de Negócios - IBGEN
                    </p>
                </div>
            </div><!-- end of /.fd_item -->
            <div class="fd_item">
                <div class="fd_content">
                    <a href="javascript:void(0)" class="fd_content_button fd_open_resume equal-height" data-resume="cristiane-muller">
                        <figure>
                            <img src="<?php echo $this->Html->Url('/assets/images/cristiane.jpg') ?>" alt="Cristiane Müller">
                        </figure>
                    </a>
                    <p>Cristiane Müller</p>
                    <div class="fd_buttons">
                        <a href="javascript:void(0)" target="_blank">
                            <img src="<?php echo $this->Html->Url('/assets/images/icons/linkedin.svg'); ?>" alt="LinkedIn"/>
                        </a>
                        <a href="javascript:void(0)" class="fd_open_resume" data-resume="cristiane-muller">
                            <img src="<?php echo $this->Html->Url('/assets/images/icons/plus.svg'); ?>" alt="Mostrar Mais"/>
                        </a>
                    </div>
                </div><!-- end of /.fd_content -->
                <div class="fd_content_resume right equal-height" id="cristiane-muller">
                    <p><strong>Estagiária</strong></p>
                    <p>
                        Graduanda em Direito pela Fundação Escola Superior do Ministério Público - FMP<br />
                        Graduanda em Letras Língua Inglesa pela PUCRS
                    </p>
                </div>
            </div><!-- end of /.fd_item -->
        </div><!-- end of /.container -->
    </div><!-- end of /.team -->

    <div class="fd_wrap_mission">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-xs-offset-0 col-md-10 col-md-offset-1">
                    <div class="row">
                        <div class="col-xs-12 col-sm-5">
                            <div class="fd_item">
                                <figure>
                                    <img src="<?php echo $this->Html->Url('/assets/images/icons/mission.png') ?>" alt="Missão">
                                </figure>
                                <h3>Missão</h3>
                                <p>
                                    <?php echo $this->Bloco->bloco('missao', 'conteudo'); ?>
                                </p>
                            </div><!-- end of /.fd_item -->
                        </div><!-- end of /.cols -->
                        <div class="col-xs-12 col-sm-7">
                            <div class="fd_item">
                                <figure>
                                    <img src="<?php echo $this->Html->Url('/assets/images/icons/vision.png') ?>" alt="Visão">
                                </figure>
                                <h3>Visão</h3>
                                <p>
                                    <?php echo $this->Bloco->bloco('visao', 'conteudo'); ?>
                                </p>
                            </div><!-- end of /.fd_item -->
                            <div class="fd_item">
                                <figure>
                                    <img src="<?php echo $this->Html->Url('/assets/images/icons/values.png') ?>" alt="Valores">
                                </figure>
                                <h3>Valores</h3>
                                <p>
                                    <?php echo $this->Bloco->bloco('valores', 'conteudo'); ?>
                                </p>
                            </div><!-- end of /.fd_item -->
                        </div><!-- end of /.cols -->
                    </div><!-- end of /.row -->
                </div><!-- end of /.cols -->
            </div><!-- end of /.row -->
        </div><!-- end of /.container -->
    </div><!-- end of /.fd_wrap_mission -->
</section>


<section class="fd_areas" id="areas-de-atuacao">
    <div class="container">
        <h2>
            <small>Áreas de</small><br />
            Atuação
        </h2>

        <div class="fd_flex_column">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="fd_item direito-civil">
                    <div class="fd_miolo" data-area="direito-civil">
                        <div>
                            <figure>
                                <img src="assets/images/icons/direito-civil.png" alt="Direito Civil">
                            </figure>
                            <h4>Direito <strong>Civil</strong></h4>
                            <a href="javascript:void(0)" class="btn">Saiba mais</a>
                        </div>
                    </div><!-- end of /.fd_miolo -->
                    <a href="javascript:void(0)" class="button-close">X</a>
                </div><!-- end of /.fd_item -->
            </div><!-- end of /.cols-->

            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="fd_item direito-consumidor">
                    <div class="fd_miolo" data-area="direito-consumidor">
                        <div>
                            <figure>
                                <img src="assets/images/icons/direito-do-consumidor.png" alt="Direito do Consumidor">
                            </figure>
                            <h4>Direito do <strong>Consumidor</strong></h4>
                            <a href="javascript:void(0)" class="btn">Saiba mais</a>
                        </div>
                    </div><!-- end of /.fd_miolo -->
                    <a href="javascript:void(0)" class="button-close">X</a>
                </div><!-- end of /.fd_item -->
            </div><!-- end of /.cols -->

            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="fd_item direito-empresarial">
                    <div class="fd_miolo" data-area="direito-empresarial">
                        <div>
                            <figure>
                                <img src="assets/images/icons/direito-empresarial.png" alt="Direito Empresarial">
                            </figure>
                            <h4>Direito <strong>Administrativo</strong></h4>
                            <a href="javascript:void(0)" class="btn" >Saiba mais</a>
                        </div>
                    </div><!-- end of /.fd_miolo -->
                    <a href="javascript:void(0)" class="button-close">X</a>
                </div><!-- end of /.fd_item -->
            </div><!-- end of /.cols -->

            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="fd_item direito-imobiliario">
                    <div class="fd_miolo" data-area="direito-imobiliario">
                        <div>
                            <figure>
                                <img src="assets/images/icons/direito-imobiliario.png" alt="Direito Imobiliário">
                            </figure>
                            <h4>Direito <strong>Imobiliário</strong></h4>
                            <a href="javascript:void(0)" class="btn" >Saiba mais</a>
                        </div>
                    </div><!-- end of /.fd_miolo -->
                    <a href="javascript:void(0)" class="button-close">X</a>
                </div><!-- end of /.fd_item -->
            </div><!-- end of /.cols -->

            <div class="col-xs-12 col-sm-6 col-md-4">
                <div class="fd_item direito-eleitoral">
                    <div class="fd_miolo" data-area="direito-eleitoral">
                        <div>
                            <figure>
                                <img src="assets/images/icons/direito-eleitoral.png" alt="Direito Eleitoral">
                            </figure>
                            <h4>Direito <strong>Eleitoral</strong></h4>
                            <a href="javascript:void(0)" class="btn" >Saiba mais</a>
                        </div>
                    </div><!-- end of /.fd_miolo -->
                    <a href="javascript:void(0)" class="button-close">X</a>
                </div><!-- end of /.fd_item -->
            </div><!-- end of /.cols -->

        </div>

    </div><!-- end of /.container -->

    <div class="fd_tabs" id="direito-civil">
        <div class="container">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist" id="fd_tab_collapse">
                <li role="presentation" class="active">
                    <a href="#familias-e-sucessoes" aria-controls="familias-e-sucessoes" role="tab" data-toggle="tab">FAMÍLIAS E SUCESSÕES</a>
                </li>
                <li role="presentation">
                    <a href="#coisas-reais" aria-controls="coisas-reais" role="tab" data-toggle="tab">COISAS/REAIS</a>
                </li>
                <li role="presentation">
                    <a href="#obrigacoes" aria-controls="obrigacoes" role="tab" data-toggle="tab">OBRIGAÇÕES</a>
                </li>
                <li role="presentation">
                    <a href="#contratos" aria-controls="contratos" role="tab" data-toggle="tab">CONTRATOS</a>
                </li>
                <li role="presentation">
                    <a href="#empresas" aria-controls="empresas" role="tab" data-toggle="tab">EMPRESAS</a>
                </li>
                <li role="presentation">
                    <a href="#resp-civil" aria-controls="resp-civil" role="tab" data-toggle="tab">RESPONSABILIDADE CIVIL</a>
                </li>
            </ul><!-- end of /.nav-tabs -->

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="familias-e-sucessoes">
                    <div class="row">
                        <div class="fd_flex_column">
                            <ul class="fd_list">
                                <li>Reconhecimento e dissolução de união estável, partilha de bens;</li>
                                <li>Divórcio e partilha de bens;</li>
                                <li>Desconsideração Inversa da Personalidade Jurídica;</li>
                                <li>Separação de corpos;</li>
                                <li>Ação e execução de alimentos;</li>
                                <li>Interdição judicial;</li>
                                <li>Inventário judicial e extrajudicial;</li>
                                <li>Testamento;</li>
                            </ul>
                            <ul class="fd_list">
                                <li>Adoção;</li>
                                <li>Alienação parental;</li>
                                <li>Guarda e regulamentação de visitas;</li>
                                <li>Destituição de poder familiar;</li>
                                <li>Investigação de paternidade;</li>
                                <li>Arrolamento de bens;</li>
                                <li>Arrolamento de bens;</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="coisas-reais">
                    <div class="row">
                        <div class="fd_flex_column">
                            <ul class="fd_list">
                                <li>Ações possessórias (manutenção e reintegração de posse, interdito proibitório), com ênfase em imóveis rurais;</li>
                                <li>Usucapião extrajudicial e judicial;</li>
                                <li>Embargos de terceiros;</li>
                                <li>Nunciação de obra nova;</li>
                                <li>Divisão e demarcação;</li>
                                <li>Desapropriação;</li>
                                <li>Constituição e desconstituição de alienação fiduciária e hipotecas;</li>
                                <li>Condomínio Geral e Edilício;</li>
                                <li>Parcelamento do Solo e Loteamentos;</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="obrigacoes">
                    <div class="row">
                        <div class="fd_flex_column">
                            <ul class="fd_list">
                                <li>Ações relativas às obrigações de dar, de fazer e de não fazer;</li>
                                <li>Recuperação de crédito através de cobranças extrajudiciais e judiciais;</li>
                                <li>Ajuizamento de ação monitória e de execução dos mais diversos títulos obrigacionais;</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="contratos">
                    <div class="row">
                        <div class="fd_flex_column">
                            <p>Elaboração, análise e revisão de contratos, tais como: contratos aleatórios, preliminares, compra e venda, promessa de compra e venda, troca ou permuta,  doação, locação, comodato, mútuo, prestação de serviços, empreitada, incorporação, transporte, ambiental, depósito, distribuição e concessão comercial, corretagem, seguro e planos de saúde, constituição de renda, fiança, transação, compromisso ou arbitragem, parceria e arrendamento rural, sociedade, arrendamento mercantil, bancários, alienação fiduciária, cartões de crédito, franquia, factoring, joint venture, know-how;</p>
                            <p>Modificação de cláusulas contratuais mediante a adequação dos contratos à legislação em vigor ou à condição superveniente;</p>
                            <p>Resolução, Distrato e Resilição Unilateral;</p>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="empresas">
                    <div class="row">
                        <div class="fd_flex_column">
                            <ul class="fd_list">
                                <li>Direitos e deveres da empresa e do empresário;</li>
                                <li>Recuperação de ativos financeiros nas esferas judicial e extrajudicial.</li>
                                <li>Ações executivas, monitória e de cobrança.</li>
                                <li>Defesa e assessoria ambiental.</li>
                                <li>Análise e elaboração de contratos.</li>
                                <li>Defesa do patrimônio dos sócios e da empresa.</li>
                                <li>Sucessão empresarial;</li>
                            </ul>
                            <ul class="fd_list">
                                <li>Registro de marcas e patentes;</li>
                                <li>Conflitos de interesses dos sócios;</li>
                                <li>Dissolução societária;</li>
                                <li>Defesa e ajuizamento de medidas judiciais e extrajudiciais visando proteger o patrimônio material, moral e intelectual da empresa e dos sócios contra atos praticados por terceiros ou por outros sócios;</li>
                                <li>Ação de Prestação de Contas;</li>
                                <li>Exclusão da responsabilidade dos sócios em ações judiciais;</li>
                                <li>Defesa do patrimônio dos sócios no incidente de desconsideração da personalidade jurídica;</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="resp-civil">
                    <div class="row">
                        <div class="fd_flex_column">
                            <ul class="fd_list">
                                <li>Responsabilidade subjetiva e objetiva, contratual e extracontratual;</li>
                                <li>Reparação por danos materiais e morais decorrentes de negócio jurídico ou por ato ilícito;</li>
                                <li>Indenização decorrente de acidente de trânsito;</li>
                                <li>Reparação material e moral por erro médico ou hospitalar, responsabilidade por dano estético;</li>
                                <li>Responsabilidade civil do Estado</li>
                                <li>Indenização decorrente de ato de fabricante, construtor, comerciante e importador de produtos móveis ou imóveis.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- end of /.tab-content -->
        </div><!-- end of /.container -->
    </div><!-- end of /.fd_tabs -->


    <div class="fd_tabs" id="direito-consumidor">
        <div class="container">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist" id="fd_tab_collapse">
                <li role="presentation" class="active">
                    <a href="#familias-e-sucessoes" aria-controls="familias-e-sucessoes" role="tab" data-toggle="tab">Direito do Consumidor</a>
                </li>
            </ul><!-- end of /.nav-tabs -->

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="familias-e-sucessoes">
                    <div class="row">
                        <div class="fd_flex_column">
                            <ul class="fd_list">
                                <li>Contratos de adesão;</li>
                                <li>Cláusulas abusivas;</li>
                                <li>Vantagem e onerosidade excessivas;</li>
                                <li>Responsabilidade do fornecedor e do fabricante;</li>
                                <li>Propaganda enganosa;</li>
                                <li>Produto com vício ou defeito;</li>
                                <li>Responsabilidade do produtor e do vendedor;</li>
                            </ul>
                            <ul class="fd_list">
                                <li>Responsabilidade das concessionárias de serviços públicos;</li>
                                <li>Responsabilidade das empresas aéreas: avarias, extravio ou furto de carga ou bagagem, cancelamento ou atraso de voos, overbooking;</li>
                                <li>Seguro de veículos;</li>
                                <li>Financiamento e empréstimos bancários, cartão de crédito, cheque especial;</li>
                                <li>Inclusão indevida no SPC/SERASA e demais serviços de proteção ao crédito;</li>
                                <li>Restituição de valores pagos indevidamente, repetição de indébito;</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- end of /.tab-content -->
        </div><!-- end of /.container -->
    </div><!-- end of /.fd_tabs -->


    <div class="fd_tabs" id="direito-empresarial">
        <div class="container">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist" id="fd_tab_collapse">
                <li role="presentation" class="active">
                    <a href="#familias-e-sucessoes" aria-controls="familias-e-sucessoes" role="tab" data-toggle="tab">Direito Administrativo</a>
                </li>
            </ul><!-- end of /.nav-tabs -->

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="familias-e-sucessoes">
                    <div class="row">
                        <div class="fd_flex_column">
                            <ul class="fd_list">
                                <li>Defesa judicial e administrativa de servidores públicos municipais, estaduais e federais;</li>
                                <li>Defesas em ações civis públicas e improbidade administrativa;</li>
                                <li>Concessões, permissões e autorizações de serviços públicos;</li>
                                <li>Licitações e contratos administrativos;</li>
                                <li>Concurso público;</li>
                                <li>Defesas perante tribunais de contas e tribunais administrativos;</li>
                                <li>Defesa em intervenção pública na propriedade e domínio econômico;</li>
                                <li>Advocacia de trânsito;</li>
                                <li>Representação e defesa de associações, sindicatos e entidades de classe e de seus associados;</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- end of /.tab-content -->
        </div><!-- end of /.container -->
    </div><!-- end of /.fd_tabs -->


    <div class="fd_tabs" id="direito-imobiliario">
        <div class="container">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist" id="fd_tab_collapse">
                <li role="presentation" class="active">
                    <a href="#familias-e-sucessoes" aria-controls="familias-e-sucessoes" role="tab" data-toggle="tab">Direito Imobiliário</a>
                </li>
            </ul><!-- end of /.nav-tabs -->

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="familias-e-sucessoes">
                    <div class="row">
                        <div class="fd_flex_column">
                            <ul class="fd_list">
                                <li>Indenização por atraso na entrega de imóvel;</li>
                                <li>Resolução, Distrato e Resilição Unilateral de Contrato de Promessa de Compra e Venda de Imóvel;</li>
                                <li>Contratos imobiliários;</li>
                                <li>Assessoria na aquisição de imóveis;</li>
                                <li>Adjudicação compulsória;</li>
                                <li>Despejo;</li>
                                <li>Renovatória de locação;</li>
                                <li>Ações possessórias (reintegração e manutenção de posse e interdito proibitório);</li>
                                <li>Ação de imissão de posse e reivindicatória;</li>
                                <li>Condomínios e Loteamentos;</li>
                                <li>Leilões judiciais e extrajudiciais;</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- end of /.tab-content -->
        </div><!-- end of /.container -->
    </div><!-- end of /.fd_tabs -->



    <div class="fd_tabs" id="direito-eleitoral">
        <div class="container">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist" id="fd_tab_collapse">
                <li role="presentation" class="active">
                    <a href="#familias-e-sucessoes" aria-controls="familias-e-sucessoes" role="tab" data-toggle="tab">Direito Eleitoral</a>
                </li>
            </ul><!-- end of /.nav-tabs -->

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade in active" id="familias-e-sucessoes">
                    <div class="row">
                        <div class="fd_flex_column">
                            <ul class="fd_list">
                                <li>Consultoria e assessoria jurídica a partidos políticos;</li>
                                <li>Defesas administrativas e judiciais em matéria eleitoral;</li>
                                <li>Ações de cassação de mandato e assessoria a Prefeituras e ocupantes de cargos públicos;</li>
                                <li>Assessoria e defesa judicial de candidatos, partidos políticos e coligações partidárias durante o período de campanha eleitoral, especialmente na impugnação ou defesa de registro de candidatura;</li>
                                <li>Ações de defesa de representações relacionadas à propaganda eleitoral;</li>
                                <li>Abuso de poder econômico, abuso de poder de autoridade ou abuso dos meios de comunicação social;</li>
                                <li>Propositura ou defesa de recurso contra a expedição de diploma;</li>
                                <li>Propositura ou defesa de ação constitucional de impugnação de mandato eletivo;</li>
                                <li>Defesa judicial de candidatos e envolvidos em investigações de crimes eleitorais ou infrações à Lei Eleitoral.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!-- end of /.tab-content -->
        </div><!-- end of /.container -->
    </div><!-- end of /.fd_tabs -->


</section>




<!--<noticia-sliders inline-template :noticias='--><?php //echo $noticia_sliders ?><!--' :share="share">-->
    <div class="fd_articles" id="noticias">

    </div>
<!--</noticia-sliders>-->




