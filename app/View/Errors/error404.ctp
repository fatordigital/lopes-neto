<main class="contato interna">
    <section class="header-interna">
        <h1>PÁGINA<br /><small>NÃO ENCONTRADA</small></h1>
    </section>
    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h4>O LINK QUE VOCÊ CLICOU PODE ESTAR INDISPONÍVEL OU
                        A PÁGINA PODE TER SIDO REMOVIDA.</h4>

                    <p class="text-center">VISITE A <a href="" class="link">PÁGINA INICIAL</a> OU ENTRE EM <a href="" class="link">CONTATO</a> CONOSCO SOBRE O PROBLEMA</p>
                </div><!-- end of /.cols -->
            </div><!-- end of /.row -->
        </div><!-- end of /.container -->
    </section>
</main>

