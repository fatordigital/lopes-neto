<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Errors
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<div class="container">
	<br><br><br><br><br><br><br>
	<h2><?php //echo $name; ?>Página não encontrada</h2>
	<!-- <p class="error"> -->
		<?php if (Configure::read('debug') > 0): ?>
			<strong><?php echo __d('cake', 'Error'); ?>: </strong>
			<?php printf(
				__d('cake', 'The requested address %s was not found on this server.'),
				"<strong>'{$url}'</strong>"
			); ?>
		<?php endif; ?>

		<div class="tela_404_content">
			<p>Acreditamos que você achará útil um dos links listados abaixo:</p>
			- <a href="<?php echo $this->Html->Url('/'); ?>" title="Página Inicial">Página Inicial</a>
			<hr />
			<p><strong>Talvez não seja possível exibir a página solicitada devido à um dos seguintes motivos:</strong></p>
			<ol type="a">
				<li><strong>Link de favoritos desatualizado</strong>;</li>
				<li>Um mecanismo de busca que possua uma referência <strong>desatualizada de nosso site</strong>;</li>
				<li>Uma <strong>URL digitada incorretamente</strong>;</li>
			</ol>
		</div>
	<!-- </p> -->
</div>
<?php
if (Configure::read('debug') > 0):
	echo $this->element('exception_stack_trace');
endif;
?>
