<?php $this->start('title') ?><?php echo __('Links Úteis') ?> - <?php $this->end() ?>

<?php $this->start('body') ?>bd-links-uteis<?php $this->end() ?>

<?php $this->start('keywords') ?><?php $this->end() ?>
<?php $this->start('description') ?><?php $this->end() ?>
<?php $this->start('author') ?><?php $this->end() ?>

<?php $this->start('banners') ?>
<section class="topo contato">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1>
                <span><?php echo __('Links') ?></span><br />
                <?php echo __('Úteis') ?></h1>
            <div class="padrao espacamento-linha"></div>
        </div>
    </div>
</section>
<?php $this->end() ?>

<section class="fale-conosco links-uteis">
	<div class="container">
        <?php foreach($areas as $i => $area): ?>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <h3><?php echo $area ?></h3>
            <div class="padrao espacamento-linha"></div>
            <?php foreach($links as $link): ?>
            <?php if($i == $link['Link']['area']): ?>
            <p><?php echo $link['Link']['nome'] ?> » <a target="_blank" href="<?php echo $link['Link']['url'] ?>"><?php echo $link['Link']['url'] ?></a></p>
            <?php endif; ?>
            <?php endforeach; ?>
        </div>
        <?php endforeach; ?>
</section>