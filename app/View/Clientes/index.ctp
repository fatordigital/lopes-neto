<main class="cliente interna">
    <section class="header-interna">
        <h1>
            <?php echo $this->Idioma->palavra('clientes'); ?><br /><small><?php echo $this->Idioma->palavra('uma-orchestra-de-solucoes-integrais'); ?></small>
        </h1>
    </section>

    <section class="page-content">
        <div class="container">
            <div class="row row-content-1">
                <div class="col-xs-12 col-sm-6 col-md-7">
                    <p>
                        <?php echo $this->Idioma->palavra('como-a-orchestra-solucoes-empresariais-disponibiliza'); ?>
                    </p>
                    <p>
                    <ul>
                        <?php echo $this->Bloco->bloco('lista-clientes', 'conteudo'); ?>
                    </ul>
                    </p>
                    <p>
                        <?php echo $this->Idioma->palavra('representamos-o-melhor-suporte-para'); ?>
                    </p>
                </div><!-- end of /.cols -->
                <div class="col-xs-12 col-sm-6 col-md-5">
                    <?php echo $this->Form->create('Sac', array('method' => 'POST')) ?>
                        <p>
                            <small>
                                <?php echo $this->Idioma->palavra('entre-em-contato-conosco'); ?>
                            </small>
                        </p>
                        <div class="row">
                            <div class="fd_key">
                                <?php echo $this->Form->hidden('fd'); ?>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <?php echo $this->Form->text('nome', array(
                                                                            'required' => false,
                                                                            'class' => 'form-control',
                                                                            'placeholder' => $this->Idioma->palavra('nome'),
                                                                            'data-rule-required'    => 'true',
                                                                            'data-msg-required'     => '<i class="fa fa-times"></i>',
                                                                )); ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <?php echo $this->Form->text('telefone', array(
                                                                            'required'              => false,
                                                                            'class'                 => 'form-control telefone',
                                                                            'placeholder'           => $this->Idioma->palavra('telefone'),
                                                                            'data-rule-required'    => 'true',
                                                                            'data-msg-required'     => '<i class="fa fa-times"></i>',
                                                                            'data-rule-minlength'   =>'14',
                                                                            'data-msg-minlength'    => '<i class="fa fa-times"></i>',
                                                                            'data-rule-maxlength'   =>'15',
                                                                            'data-msg-maxlength'    => '<i class="fa fa-times"></i>'
                                                                )); ?>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <?php echo $this->Form->email('email', array(
                                                                            'required'              => false,
                                                                            'class'                 => 'form-control',
                                                                            'placeholder'           => 'E-mail',
                                                                            'data-rule-required'    => 'true',
                                                                            'data-msg-required'     => '<i class="fa fa-times"></i>',
                                                                )); ?>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <?php echo $this->Form->textarea('mensagem', array(
                                                                            'required'              => false,
                                                                            'class'                 => 'form-control',
                                                                            'placeholder'           => $this->Idioma->palavra('deixe-sua-mensagem'),
                                                                            'data-rule-required'    => 'true',
                                                                            'data-msg-required'     => '<i class="fa fa-times"></i>'
                                                                )); ?>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-submit pull-right"><?php echo $this->Idioma->palavra('enviar'); ?></button>
                            </div>
                        </div>
                    <?php echo $this->Form->end() ?>
                </div><!-- end of /.cols -->
            </div><!-- end of /.row.row-content-1 -->
        </div><!-- end of /.container -->
        <div class="container">
            <div class="row row-content-2">
                <div class="col-xs-12 col-sm-6 col-md-4 equal-heights">
                    <figure class="circle">
                        <img src="<?php echo $this->Html->url('/assets/images/icons/acionistas.png') ?>" alt="">
                    </figure>
                    <p class="text-center">
                        <?php echo $this->Idioma->palavra('clientes-acionistas'); ?>
                    </p>
                </div><!-- end of /.cols -->
                <div class="col-xs-12 col-sm-6 col-md-4 equal-heights">
                    <figure class="circle">
                        <img src="<?php echo $this->Html->url('/assets/images/icons/executivos.png') ?>" alt="">
                    </figure>
                    <p class="text-center">
                        <?php echo $this->Idioma->palavra('clientes-executivos'); ?>
                    </p>
                </div><!-- end of /.cols -->
                <div class="col-xs-12 col-sm-6 col-md-4 equal-heights">
                    <figure class="circle">
                        <img src="<?php echo $this->Html->url('/assets/images/icons/credores.png') ?>" alt="">
                    </figure>
                    <p class="text-center">
                        <?php echo $this->Idioma->palavra('clientes-credores'); ?>
                    </p>
                </div><!-- end of /.cols -->
                <div class="col-xs-12 col-sm-6 col-md-4 equal-heights">
                    <figure class="circle">
                        <img src="<?php echo $this->Html->url('/assets/images/icons/instituicoes-financeiras.png') ?>" alt="">
                    </figure>
                    <p class="text-center">
                        <?php echo $this->Idioma->palavra('clientes-instituicoes-financeiras'); ?>
                    </p>
                </div><!-- end of /.cols -->
                <div class="col-xs-12 col-sm-6 col-md-4 equal-heights">
                    <figure class="circle">
                        <img src="<?php echo $this->Html->url('/assets/images/icons/investidores-ou-empresas.png') ?>" alt="">
                    </figure>
                    <p class="text-center">
                        <?php echo $this->Idioma->palavra('clientes-investidores-empresas'); ?>
                    </p>
                </div><!-- end of /.cols -->
                <div class="col-xs-12 col-sm-6 col-md-4 equal-heights">
                    <figure class="circle">
                        <img src="<?php echo $this->Html->url('/assets/images/icons/gestores-de-patrimonios.png') ?>" alt="">
                    </figure>
                    <p class="text-center">
                        <?php echo $this->Idioma->palavra('clientes-gestores-patrimonios'); ?>
                    </p>
                </div><!-- end of /.cols -->
            </div><!-- end of /.row.row-content-2 -->
            <div class="row row-content-2">
                <div class="col-xs-12">
                    <p>
                        <?php echo $this->Idioma->palavra('muitas-solucoes-empresariais-ja-fazem'); ?>
                    </p>
                    <p>
                        <?php echo $this->Idioma->palavra('marque-um-contato-conosco'); ?>
                    </p>
                </div>
            </div>
        </div><!-- end of /.container -->
    </section>
</main>