<?php echo $this->assign('title', ($noticia_tipo['NoticiaTipo']['seo_title'] != "") ? $noticia_tipo['NoticiaTipo']['seo_title'] : $noticia_tipo['NoticiaTipo']['nome']); ?>
<?php 
    if($noticia_tipo['NoticiaTipo']['seo_description'] != ""){
        echo $this->assign('description', $noticia_tipo['NoticiaTipo']['seo_description']);
    }
?>
<?php 
    if($noticia_tipo['NoticiaTipo']['seo_keywords'] != ""){
        echo $this->assign('keywords', $noticia_tipo['NoticiaTipo']['seo_keywords']);
    }
?>

		<?php echo $this->element('site/banner_curso', array('banners' => $banners_noticias)); ?>
	</div>
</div>
<div class="row row-title">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h1><?php echo $noticia_tipo['NoticiaTipo']['nome']; ?></h1>
			<div class="div-campus">
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
			</div>
		</div>
	</div>
</div>
<div class="row row-tabs-gray">
	<div class="container">
		<?php echo $this->element('site/breadcrumb') ?>
	</div>
</div>
<div class="row row-tab-body">
	<div class="container">			
		<div role="tabpanel">
		  <!-- Nav tabs -->
		  <ul class="nav nav-tabs" role="tablist">
		    <li role="presentation" class="active"><a href="#calendario" aria-controls="home" role="tab" data-toggle="tab"><?php echo $noticia_tipo['NoticiaTipo']['nome']; ?> <span class="seta-tab glyphicon glyphicon-triangle-bottom pull-right triangle-red"></span></a></li>		
		  </ul>

		  	<!-- Tab panes -->
		  	<div class="tab-content">
		  		<div role="tabpanel" class="tab-pane active" id="responsabilidade-page">
		  			<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 ">	

		  				<?php if($noticia_tipo['NoticiaTipo']['apresentacao'] != ""): ?>

		  					<?php echo $noticia_tipo['NoticiaTipo']['apresentacao']; ?>

		  				<?php else: ?>

			  				<?php if(isset($noticias) && count($noticias) > 0): ?>  				
			  					
			  					<?php if($this->params['paging']['Noticia']['page'] == 1): ?>
				  					<?php if(isset($noticias[0])): ?>
				  						<?php $noticia = $noticias[0]; ?>
						  				<div class="col-md-12 news news-type-1">
						  					<?php if($noticia['Noticia']['thumb'] != ""): ?>
					                            <?php echo $this->Image->show($noticia['Noticia']['thumb_path'] .'/'. $noticia['Noticia']['thumb_dir'] . '/700x258-' . $noticia['Noticia']['thumb'], array('alt' => $noticia['Noticia']['titulo'], 'class' => 'img-responsive')); ?>
					                        <?php endIf; ?>
											<div class="col-md-12">
												<h3 class="col-md-12 pos-rel"><?php echo $noticia['Noticia']['titulo']; ?></h3>
												<?php if($noticia['Noticia']['descricao_resumida'] != ""): ?>
													<p class="col-md-12"><?php echo $noticia['Noticia']['descricao_resumida']; ?></p>
												<?php endIf; ?>

												<a href="<?php echo $this->Html->Url('/' . $noticia['Noticia']['seo_url']); ?>"  title="<?php echo $noticia['Noticia']['titulo']; ?>" class="pull-right news-read-more">LEIA MAIS</a>
											</div>
										</div>
									<?php endIf; ?>
								<?php endIf; ?>

								<div class="col-md-12 padding-none">
									<div class="col-md-6 padding-left-none highlight-news">

										<?php foreach($noticias as $k => $noticia): ?>

											<?php if($k == 0 && $this->params['paging']['Noticia']['page'] == 1): ?>
												<?php continue; ?>
											<?php endIf; ?>
											
											<div class="col-md-12 news news-type-2">
												<?php if($noticia['Noticia']['thumb'] != ""): ?>
						                            <?php echo $this->Image->show($noticia['Noticia']['thumb_path'] .'/'. $noticia['Noticia']['thumb_dir'] . '/330x150-' . $noticia['Noticia']['thumb'], array('alt' => $noticia['Noticia']['titulo'], 'class' => 'img-responsive')); ?>
						                        <?php endIf; ?>
												<div class="col-md-12">
													<h3 class="pos-rel"><?php echo $noticia['Noticia']['titulo']; ?></h3>
													<?php if($noticia['Noticia']['descricao_resumida'] != ""): ?>
														<p><?php echo $noticia['Noticia']['descricao_resumida']; ?></p>
													<?php endIf; ?>

													<a href="<?php echo $this->Html->Url('/' . $noticia['Noticia']['seo_url']); ?>"  title="<?php echo $noticia['Noticia']['titulo']; ?>" class="pull-right news-read-more">LEIA MAIS</a>
												</div>
											</div>

											<?php if( (($k+1)%2) == 0): ?>
												</div>
												<div class="col-md-6 pull-right padding-right-none highlight-news">
											<?php else: ?>
												</div>
												<div class="col-md-6 padding-left-none highlight-news">
											<?php endIf; ?>

										<?php endForeach; ?>

									</div>
								</div>

								<?php if(isset($this->params['paging']['Noticia']['pageCount']) && $this->params['paging']['Noticia']['pageCount'] > 1): ?>
							    	<div class="paginacao">
								        <ul>
								            <li class="voltar">
								            	<?php echo $this->Paginator->prev(__('« Voltar', true), array(), null, array()); ?>
								            </li>
								            <?php echo $this->Paginator->numbers(array('classs' => 'page', 'separator' => ' ', 'tag' => 'li')); ?>
								            <?php if(($this->Paginator->current()+9) < $this->Paginator->counter(array('format' => '{:pages}'))): ?>
								            	<li>...</li>
								            	<li>
									            	<?php echo $this->Paginator->last($this->Paginator->counter(array('format' => '{:pages}')), array(), null, array()); ?>
									            </li>
								            <?php endIf; ?>
								            <li class="avancar">
								            	<?php echo $this->Paginator->next(__('Avançar »', true), array(), null, array()); ?>
								            </li>
								        </ul>
								    </div>
								<?php endIf; ?>

							<?php else: ?>
								<div class="col-md-12">
					  				<br /><br />
					  				<p>Nenhum projeto encontrado.</p>
					  			</div>
				  			<?php endIf; ?>

				  		<?php endIf; ?>

					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

						<?php if($noticia_tipo['NoticiaTipo']['formulario']): ?>
						      <?php echo $this->element('site/fale_conosco') ?>
						<?php endIf; ?>

	   					<?php echo $this->element('site/redes-sociais-mini') ?>
					</div>	  
			  	</div>
			</div>
		</div>						
	</div>		
</div>