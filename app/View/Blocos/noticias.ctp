<?php echo $this->assign('title', ($bloco['Bloco']['seo_title'] != "") ? $bloco['Bloco']['seo_title'] : $bloco['Bloco']['nome']); ?>
<?php 
    if($bloco['Bloco']['seo_description'] != ""){
        echo $this->assign('description', $bloco['Bloco']['seo_description']);
    }
?>
<?php 
    if($bloco['Bloco']['seo_keywords'] != ""){
        echo $this->assign('keywords', $bloco['Bloco']['seo_keywords']);
    }
?>

		<?php echo $this->element('site/banner_curso', array('banners' => $banners_noticias)); ?>
	</div>
</div>
<div class="row row-title">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h1><?php echo $bloco['Bloco']['nome']; ?></h1>
			<div class="div-campus">
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
			</div>
		</div>
	</div>
</div>
<div class="row row-tabs-gray">
	<div class="container">
		<?php echo $this->element('site/breadcrumb') ?>
	</div>
</div>
<div class="row row-tab-body">
	<div class="container">			
		<div role="tabpanel">
		  <!-- Nav tabs -->
		  <ul class="nav nav-tabs" role="tablist">
		    <li role="presentation" class="active"><a href="#calendario" aria-controls="home" role="tab" data-toggle="tab"><?php echo $bloco['Bloco']['nome']; ?> <span class="seta-tab glyphicon glyphicon-triangle-bottom pull-right triangle-red"></span></a></li>		
		  </ul>

		  <!-- Tab panes -->
		  <div class="tab-content">
		  	<div role="tabpanel" class="tab-pane active" id="calendario">
		  			<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 ">	

	  					<?php if(isset($noticias) && count($noticias) > 0): ?>
		  					<?php foreach($noticias as $k => $noticia): ?>
				  				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 box-news <?php echo ($k%2 == 0) ? 'clear' : ''; ?>">
			  						<div class="box-day">					    				    				
					    				<h5><?php echo $noticia['Noticia']['titulo']; ?></h5>
					    				<p class="text-12"><?php echo $noticia['Noticia']['descricao_resumida']; ?></p>
					    				<a href="<?php echo $this->Html->Url('/' . $noticia['Noticia']['seo_url']); ?>" title="<?php echo $noticia['Noticia']['titulo']; ?>" class="news-read-more upper pull-right">Leia Mais</a>
					    			</div>
				  				</div>
			  				<?php endForeach; ?>

			  				<?php if(isset($this->params['paging']['Noticia']['pageCount']) && $this->params['paging']['Noticia']['pageCount'] > 1): ?>
						    	<?php
									$this->Paginator->options(
						    			array(
										    'url'=> array(
										        'controller' => 'noticias',
										        'action' => 'index'
										    )
										)
									);
								?>
							    <div class="paginacao">
							        <ul>
							            <li class="voltar">
							            	<?php echo $this->Paginator->prev(__('« Voltar', true), array(), null, array()); ?>
							            </li>
							            <?php echo $this->Paginator->numbers(array('classs' => 'page', 'separator' => ' ', 'tag' => 'li')); ?>
							            <?php if(($this->Paginator->current()+9) < $this->Paginator->counter(array('format' => '{:pages}'))): ?>
							            	<li>...</li>
							            	<li>
								            	<?php echo $this->Paginator->last($this->Paginator->counter(array('format' => '{:pages}')), array(), null, array()); ?>
								            </li>
							            <?php endIf; ?>
							            <li class="avancar">
							            	<?php echo $this->Paginator->next(__('Avançar »', true), array(), null, array()); ?>
							            </li>
							        </ul>
							    </div>
							<?php endIf; ?>

			  			<?php else: ?>
			  				<br /><br />
			  				<p>Nenhuma notícia encontrada.</p>
			  			<?php endIf; ?>

		    			<br/><br/><br/><br/>
	    			</div>	
		    		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 sidebar calendario-sidebar">	
		    			<?php echo $this->element('site/calendario') ?>	    	
		    			
		    			<?php echo $this->element('site/redes-sociais-mini') ?>
					</div><!-- end sidebar-->
				</div>
		  	</div>				  			  			  	
		</div>						
	</div>		
</div>