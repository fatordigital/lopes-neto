<main class="diferenciais interna">
    <section class="header-interna">
        <h1>
            <?php echo $this->Idioma->palavra('diferenciais'); ?><br /><small><?php echo $this->Idioma->palavra('uma-orchestra-de-destaque-no-mercado'); ?></small>
        </h1>
    </section>

    <section class="page-content row-content-1">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-7 col-sm-push-5 equal-heights float-right">
                    <p class="big">
                        <?php echo $this->Bloco->bloco('a-visao-holistica-e-interdisciplinar-da-orchestra', 'conteudo'); ?>
                    </p>
                </div>
                <div class="col-xs-12 col-sm-5 col-sm-pull-7 no-padding">
                    <figure class="equal-heights has-after-right">
                    </figure>
                </div>
            </div>
        </div>
    </section>
    <section class="page-content row-content-2">
        <div class="container-fluid">
            <div class="col-xs-12 col-sm-4 col-sm-push-8 col-md-5 col-md-push-7">
                <figure class="equal-heights has-after"></figure>
            </div>
            <div class="col-xs-12 col-sm-8 col-sm-pull-4 col-md-7 col-md-pull-5 equal-heights">
                <div class="text-content">
                    <ul>
                        <?php echo $this->Bloco->bloco('lista-diferenciais', 'conteudo'); ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="page-content row-content-3">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1><span><?php echo $this->Idioma->palavra('beneficios'); ?></span><span><?php echo $this->Idioma->palavra('pensamos-em-nossos-clientes'); ?></span></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-1">
                    <figure class="circle">
                        <?php echo $this->Html->image('/assets/images/icons/amplia-a-confianca.png') ?>
                    </figure>
                    <p>
                        <?php echo $this->Idioma->palavra('amplia-a-confianca-e-evita-a-tradicional-tentativa'); ?>
                    </p>
                </div>
                <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-2">
                    <figure class="circle">
                        <?php echo $this->Html->image('/assets/images/icons/elimina-os-problemas.png') ?>
                    </figure>
                    <p>
                        <?php echo $this->Idioma->palavra('elimina-o-problema-de-ineficiencia-de-solucoes'); ?>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-1">
                    <figure class="circle">
                        <?php echo $this->Html->image('/assets/images/icons/atraves-da-orchestra.png') ?>
                    </figure>
                    <p>
                        <?php echo $this->Idioma->palavra('atraves-da-orchestra-solucoes-empresariais-o-cliente-dispoe'); ?>
                    </p>
                </div>
                <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-2">
                    <figure class="circle">
                        <?php echo $this->Html->image('/assets/images/icons/simplicidade-operacional.png') ?>
                    </figure>
                    <p>
                        <?php echo $this->Idioma->palavra('simplicidade-operacional-significa-custos-menores'); ?>
                    </p>
                </div>
            </div>
        </div>
    </section>
</main>