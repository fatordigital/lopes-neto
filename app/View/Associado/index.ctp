<main class="associados interna">
    <section class="header-interna">
        <h1>
            <?php echo $this->Idioma->palavra('associados'); ?><br /><small><?php echo $this->Idioma->palavra('uma-orchestra-de-virtuoses'); ?></small>
        </h1>
    </section>

    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2><span> <?php echo $this->Idioma->palavra('associados'); ?></span><span><?php echo $this->Idioma->palavra('quem-faz-parte-da-orchestra'); ?></span></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <ul class="list-inline">
                        <?php foreach ($associados as $associado) { ?>
                            <li>
                                <a href="<?php echo $associado['Associados']['href'] ?>" target="_blank">
                                    <img width="150" src="<?php echo $this->Html->url($associado['Associados']['path'] . '/' . $associado['Associados']['dir'] . '/400_' . $associado['Associados']['file']) ?>"
                                         alt="<?php echo $associado['Associados']['nome'] ?>" class="img-responsive center-block">
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</main>