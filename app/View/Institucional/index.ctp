<main class="empresa interna">
    <section class="header-interna">
        <h1>
            <?php echo $this->Idioma->palavra('institucional'); ?><br /><small><?php echo $this->Idioma->palavra('uma-orchestra-de-virtuoses'); ?></small>
        </h1>
    </section>

    <section class="page-content">
        <div class="container-fluid">
            <div class="row hidden-md hidden-lg">
                <div class="col-xs-12">
                    <h2 class="to-mobile hidden-md hidden-lg">
                        <?php echo $this->Idioma->palavra('orchestra'); ?> - <?php echo $this->Idioma->palavra('solucoes-empresariais') ?>
                    </h2>
                </div>
            </div>
            <div class="row row-content">
                <div class="col-xs-12 col-md-5 col-md-push-7 no-padding">
                    <div class="equal-heights has-bg has-after">
                        <!-- <img src="assets/images/bgs/sobre-solucoes.jpg" alt=""> -->
                    </div>
                </div>
                <div class="col-xs-12 col-md-7 col-md-pull-5 equal-heights">
                    <div class="text-content">
                        <h2 class="hidden-xs hidden-sm">
                            <?php echo $this->Idioma->palavra('orchestra'); ?> - <?php echo $this->Idioma->palavra('solucoes-empresariais') ?>
                        </h2>
                        <p class="big">
                            <?php echo strip_tags($this->Bloco->bloco('institucional-solucoes-empresariais', 'conteudo')); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row row-content">
                <div class="col-xs-12">
                    <h2><span><?php echo $this->Idioma->palavra('motivacoes') ?></span><span><?php echo $this->Idioma->palavra('porque-criamos-a-orchestra') ?></span></h2>
                    <?php echo $this->Bloco->bloco('motivacao', 'conteudo') ?>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2><span><?php echo $this->Idioma->palavra('quem-sao') ?></span><span><?php echo $this->Idioma->palavra('os-membros-associados') ?></span></h2>
                </div>
            </div>
        </div>
        <div class="container-fluid associados-interna">
            <div class="row row-content">
                <div class="col-xs-12 col-md-5 no-padding">
                    <div class="equal-heights has-bg has-after-right">

                    </div>
                </div>
                <div class="col-xs-12 col-md-7 equal-heights no-padding">
                    <div class="text-content">
                        <h6><?php echo $this->Idioma->palavra('empresas-prestadoras-de-servicos-que-tenham') ?></h6>
                        <?php echo $this->Bloco->bloco('empresas-prestadoras', 'conteudo') ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container balizadores">
            <div class="row">
                <div class="col-xs-12">
                    <h2>
                        <span><?php echo $this->Idioma->palavra('balizadores'); ?></span>
                        <span><?php echo $this->Idioma->palavra('o-que-rege-a-orchestra'); ?></span>
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3">
                    <h3><?php echo $this->Idioma->palavra('missao'); ?></h3>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                    <p class="light">
                        <?php echo $this->Idioma->palavra('missao-conteudo'); ?>
                    </p>
                </div>
            </div>
            <div class="row row-content">
                <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3">
                    <h3><?php echo $this->Idioma->palavra('visao'); ?></h3>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
                    <p class="light">
                        <?php echo $this->Idioma->palavra('visao-conteudo'); ?>
                    </p>
                </div>
            </div>
        </div>
    </section>
</main>