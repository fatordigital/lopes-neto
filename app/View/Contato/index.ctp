<main class="contato interna">
    <section class="header-interna">
        <h1>
            <?php echo $this->Idioma->palavra('contato'); ?><br /><small><?php echo $this->Idioma->palavra('uma-orchestra-a-sua-disposicao'); ?></small>
        </h1>
    </section>

    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-6">
                    <h4><?php echo $this->Idioma->palavra('fale-com-a-orchestra-preencha-seus-dados'); ?></h4>
                    <?php echo $this->Form->create('Sac', array('method' => 'POST')) ?>
                    <div class="row">
                        <div class="fd_key">
                            <?php echo $this->Form->hidden('fd'); ?>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <?php echo $this->Form->text('nome', array(
                                                                        'required' => false,
                                                                        'class' => 'form-control',
                                                                        'placeholder' => $this->Idioma->palavra('nome'),
                                                                        'data-rule-required'    => 'true',
                                                                        'data-msg-required'     => '<i class="fa fa-times"></i>',
                                                            )); ?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <?php echo $this->Form->text('telefone', array(
                                                                        'required'              => false,
                                                                        'class'                 => 'form-control telefone',
                                                                        'placeholder'           => $this->Idioma->palavra('telefone'),
                                                                        'data-rule-required'    => 'true',
                                                                        'data-msg-required'     => '<i class="fa fa-times"></i>',
                                                                        'data-rule-minlength'   =>'14',
                                                                        'data-msg-minlength'    => '<i class="fa fa-times"></i>',
                                                                        'data-rule-maxlength'   =>'15',
                                                                        'data-msg-maxlength'    => '<i class="fa fa-times"></i>'
                                                            )); ?>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <?php echo $this->Form->email('email', array(
                                                                        'required'              => false,
                                                                        'class'                 => 'form-control',
                                                                        'placeholder'           => 'E-mail',
                                                                        'data-rule-required'    => 'true',
                                                                        'data-msg-required'     => '<i class="fa fa-times"></i>',
                                                            )); ?>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <?php echo $this->Form->textarea('mensagem', array(
                                                                            'required'              => false,
                                                                            'class'                 => 'form-control',
                                                                            'placeholder'           => $this->Idioma->palavra('deixe-sua-mensagem'),
                                                                            'data-rule-required'    => 'true',
                                                                            'data-msg-required'     => '<i class="fa fa-times"></i>',

                                                            )); ?>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-submit pull-right"><?php echo $this->Idioma->palavra('enviar'); ?></button>
                        </div>
                    </div>
                    <?php echo $this->Form->end() ?>
                </div><!-- end of /.cols -->

                <div class="col-xs-12 col-sm-4 col-md-6">
                    <h3 style="font-size: 280%"><?php echo $this->Idioma->palavra('sede'); ?></h3>
                    <h4>São Paulo</h4>
                    <address>
                        R. Augusta, n° 1939 - <?php echo $this->Idioma->palavra('sala'); ?>. 81<br />
                        01.413-000<br />
                        São Paulo/SP, <?php echo $this->Idioma->palavra('brasil'); ?><br />
                        <?php echo $this->Idioma->palavra('telefone'); ?>: <?php echo $this->Idioma->palavra('telefone-1'); ?> / <?php echo $this->Idioma->palavra('telefone-2'); ?>.
                    </address>
                    <br />
                </div><!-- end of /.cols -->
            </div><!-- end of /.row -->
        </div><!-- end of /.container -->
    </section>
</main>