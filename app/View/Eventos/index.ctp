<main class="eventos interna">
    <section class="header-interna">
        <h1>
            <?php echo $this->Idioma->palavra('eventos'); ?><br /><small><?php echo $this->Idioma->palavra('uma-orchestra-de-grandes-espetaculos'); ?></small>
        </h1>
    </section>

    <section class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h2>
                        <span><?php echo $this->Idioma->palavra('eventos') ?></span>
                        <span><?php echo $this->Idioma->palavra('proxima-paletras-e-eventos') ?></span>
                    </h2>
                </div><!-- end of /.cols -->
            </div><!-- end of /.row -->
        </div><!-- end of /.container -->

        <section class="event">
            <div class="container-fluid">

                <?php foreach ($eventos as $e => $evento) { ?>
                    <?php if ($e % 2 == 1) { ?>
                        <div class="row row-event">
                            <div class="col-xs-12 col-sm-4 col-sm-push-8 col-md-5 col-md-push-7 no-padding">
                                <div class="has-bg has-after equal-heights" style="background-image: url(<?php echo $this->Html->url('/assets/images/eventos/evento-1.jpg') ?>)">

                                </div>
                            </div><!-- end of /.cols -->
                            <div class="col-xs-12 col-sm-8 col-sm-pull-4 col-md-7 col-md-pull-5 equal-heights">
                                <div class="event-content flex-end">
                                    <div class="event-info text-right">
                                        <h1><?php echo $this->Idioma->extrair($evento['EventoAtributo'], 'nome') ?></h1>
                                        <p class="text-right" style="margin-right: 30px">
                                            <?php if ($evento['Evento']['pauta'] != '') { ?>
                                                PAUTA: <?php echo $evento['Evento']['pauta'] ?><br />
                                            <?php } ?>
                                            <?php if ($evento['Evento']['patrocinio'] != '') { ?>
                                                PATROCÍNIO: <?php echo $evento['Evento']['patrocinio'] ?><br />
                                            <?php } ?>
                                            <?php if ($evento['Evento']['endereco'] != '') { ?>
                                                LOCAL: <?php echo $evento['Evento']['endereco'] ?><br />
                                            <?php } ?>
<!--                                            --><?php //if ($evento['Evento']['intervalo'] != '') { ?>
<!--                                                DATA: --><?php //echo $evento['Evento']['intervalo'] ?><!--<br />-->
<!--                                            --><?php //} ?>
                                        </p>
                                        <?php if (!in_array($evento['Evento']['id'], $meus_eventos)) { ?>
                                            <a href="#" data-id="<?php echo $evento['Evento']['id'] ?>" class="btn btn-event" data-toggle="modal" data-target=".bs-example-modal-lg">CONFIRMAR PRESENÇA</a>
                                        <?php } else { ?>
                                            <button type="button" class="btn btn-success">
                                                PRESENÇA CONFIRMADA
                                            </button>
                                        <?php } ?>
                                        <p class="compartilhe text-right">
                                            Compartilhe com seus amigos
                                        </p>
                                        <ul class="list-inline">
                                            <li>
                                                <a href="javascript:void(0)" class="sociais">
                                                    <i class="flaticon-google-plus-logo-on-black-background"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)" class="sociais">
                                                    <i class="flaticon-twitter-logo-on-black-background"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)" class="sociais">
                                                    <i class="flaticon-envelope"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)" class="sociais">
                                                    <i class="flaticon-facebook-logo"></i>
                                                </a>
                                            </li>
                                        </ul><!-- end of /ul.list-inline -->
                                    </div><!-- end of /.event-info -->
                                </div><!-- end of /.event-content -->
                            </div><!--  end of /.cols.equal-heights -->
                        </div><!-- end of /.row -->
                    <?php } else { ?>
                        <div class="row row-event">
                            <div class="col-xs-12 col-sm-4 col-md-5 no-padding">
                                <div class="has-bg has-after-right equal-heights" style="background-image: url(<?php echo $this->Html->url('/assets/images/eventos/evento-1.jpg') ?>)">

                                </div>
                            </div><!-- end of /.cols -->
                            <div class="col-xs-12 col-sm-8 col-md-7 equal-heights">
                                <div class="event-content flex-start">
                                    <div class="event-info">
                                        <h1><?php echo $this->Idioma->extrair($evento['EventoAtributo'], 'nome') ?></h1>
                                        <p>
                                            <?php if ($evento['Evento']['pauta'] != '') { ?>
                                                PAUTA: <?php echo $evento['Evento']['pauta'] ?><br />
                                            <?php } ?>
                                            <?php if ($evento['Evento']['patrocinio'] != '') { ?>
                                                PATROCÍNIO: <?php echo $evento['Evento']['patrocinio'] ?><br />
                                            <?php } ?>
                                            <?php if ($evento['Evento']['endereco'] != '') { ?>
                                                LOCAL: <?php echo $evento['Evento']['endereco'] ?><br />
                                            <?php } ?>
<!--                                            --><?php //if ($evento['Evento']['intervalo'] != '') { ?>
<!--                                                DATA: --><?php //echo $evento['Evento']['intervalo'] ?><!--<br />-->
<!--                                            --><?php //} ?>
                                        </p>
                                        <?php if (!in_array($evento['Evento']['id'], $meus_eventos)) { ?>
                                            <a href="#" data-id="<?php echo $evento['Evento']['id'] ?>" class="btn btn-event" data-toggle="modal" data-target=".bs-example-modal-lg">CONFIRMAR PRESENÇA</a>
                                        <?php } else { ?>
                                            <button type="button" class="btn btn-success">
                                                PRESENÇA CONFIRMADA
                                            </button>
                                        <?php } ?>
                                        <p class="compartilhe">
                                            Compartilhe com seus amigos
                                        </p>
                                        <ul class="list-inline">
                                            <li>
                                                <a href="javascript:void(0)" class="sociais">
                                                    <i class="flaticon-google-plus-logo-on-black-background"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)" class="sociais">
                                                    <i class="flaticon-twitter-logo-on-black-background"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)" class="sociais">
                                                    <i class="flaticon-envelope"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0)" class="sociais">
                                                    <i class="flaticon-facebook-logo"></i>
                                                </a>
                                            </li>
                                        </ul><!-- end of /ul.list-inline -->
                                    </div><!-- end of /.event-info -->
                                </div><!-- end of /.event-content -->
                            </div><!--  end of /.cols.equal-heights -->
                        </div><!-- end of /.row -->
                    <?php } ?>
                <?php } ?>
            </div><!-- end of /.container-fluid -->
        </section>
</main>

<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <?php echo $this->Form->create('EventoPresenca') ?>
            <?php echo $this->Form->hidden('id') ?>
            <?php echo $this->Form->hidden('evento_id') ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="nome">
                                <?php echo $this->Idioma->palavra('nome'); ?> <strong class="text-danger">*</strong>
                            </label>
                            <?php echo $this->Form->input('nome', array('required', 'label' => false, 'class' => 'form-control', 'placeholder' => $this->Idioma->palavra('nome'))) ?>
                        </div>
                        <div class="form-group">
                            <label for="telefone">
                                <?php echo $this->Idioma->palavra('telefone'); ?> <strong class="text-danger">*</strong>
                            </label>
                            <?php echo $this->Form->input('telefone', array('required', 'label' => false, 'class' => 'form-control', 'placeholder' => $this->Idioma->palavra('telefone'))) ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="email">
                                E-mail <strong class="text-danger">*</strong>
                            </label>
                            <?php echo $this->Form->input('email', array('required', 'label' => false, 'class' => 'form-control', 'placeholder' => 'E-mail')) ?>
                        </div>
                        <div class="form-group">
                            <label for="">
                                &nbsp;
                            </label>
                            <br>
                            <button class="btn btn-success btn-block" type="submit">
                                <?php echo $this->Idioma->palavra('enviar'); ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <?php echo $this->Form->end() ?>
        </div>
    </div>
</div>