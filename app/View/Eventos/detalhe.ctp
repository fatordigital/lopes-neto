<?php echo $this->assign('title', $evento['Evento']['nome']); ?>

		<?php echo $this->element('site/banner_curso', array()); ?>
		
	</div>
</div>
<div class="row row-title">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h1><?php echo (isset($this->params->params['tipo_calendario']) && $this->params->params['tipo_calendario'] == 'academico') ? 'Calendário Acadêmico' : 'Calendário de Eventos'; ?></h1>
			<div class="div-campus">
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
				<span class="glyphicon glyphicon-star"></span>
			</div>
		</div>
	</div>
</div>
<div class="row row-tabs-gray">
	<div class="container">
		<?php echo $this->element('site/breadcrumb') ?>
	</div>
</div>
<div class="row row-tab-body">
	<div class="container">			
		<div role="tabpanel">
		  <!-- Nav tabs -->
		  <ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active"><a href="#calendario" aria-controls="home" role="tab" data-toggle="tab" >Calendário <span class="seta-tab glyphicon glyphicon-triangle-bottom pull-right triangle-red"></span></a></li>			    
		  </ul>

		  <!-- Tab panes -->
		  <div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="calendario">
					<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 cal-detail">			    			
						<div class="day-detail">
							<h3> <?php echo date('d', strtotime($this->String->dataFormatada("Y-m-d", $evento['Evento']['data_inicio']))); ?> </h3>
							<h4><?php echo $this->String->mes(date('m', strtotime($this->String->dataFormatada("Y-m-d", $evento['Evento']['data_inicio'])))); ?></h4>			    				
						</div>
						<h4 class="title-detail"><?php echo $evento['Evento']['nome']; ?></h4>
						<p><?php echo $evento['Evento']['conteudo']; ?></p>

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 news-detail">

							<?php if(isset($evento['EventoTag']) && count($evento['EventoTag']) > 0): ?>
								<div class="social-share col-md-12">
									<p class="pull-left upper">Tags: </p>
									<?php foreach($evento['EventoTag'] as $k => $tag): ?>
										<a href="<?php echo $this->Html->Url('/' . $tag['seo_url']); ?>" title="<?php echo $tag['nome']; ?>" class="btn-red btn-vest">
											<?php echo $tag['nome']; ?>
										</a> 
									<?php endForeach; ?>						
								</div>
							<?php endIf; ?>

							<div class="div-social pull-right">
								<p class="p-share">Compartilhe:</p>
								<span style="float: left;">
									<!-- Go to www.addthis.com/dashboard to customize your tools --> 
									<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5768a8dbe7999a78"></script> 
									<!-- Go to www.addthis.com/dashboard to customize your tools --> 
									<div class="addthis_inline_share_toolbox_bnzz"></div>
								</span>
								<?php /*<ul class="button-social">
									<li><div class="fb-like" data-href="https://www.facebook.com/UniRitter" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div></li>
									<li><a href="https://twitter.com/share" class="twitter-share-button">Tweet</a></li>
									<li><div class="g-follow" data-annotation="bubble" data-height="20" data-href="//plus.google.com/u/0/116557350517142423038" data-rel="author"></div></li>
								</ul>*/ ?>
							</div>		
							<?php /*<div class="div-social pull-right">
								<p class="p-share">Compartilhe:</p>
								<ul class="button-social">
									<li><div class="fb-like" data-href="https://www.facebook.com/UniRitter" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div></li>
									<li><a href="https://twitter.com/share" class="twitter-share-button">Tweet</a></li>
									<li><div class="g-follow" data-annotation="bubble" data-height="20" data-href="//plus.google.com/u/0/116557350517142423038" data-rel="author"></div></li>
								</ul>
							</div>*/ ?>
						</div>

					</div>	
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 sidebar calendario-sidebar">
						<a href="<?php echo $this->Html->Url(array('controller' => 'eventos', 'action' => 'index')) ?>" class="return-calendar">
							<h5><i class="glyphicon glyphicon-chevron-left"></i><i class="glyphicon glyphicon-chevron-left"></i> Retornar à <span>listagem</span></h5>
						</a>	

						<?php echo $this->element('site/calendario') ?>

		    			<?php if(isset($downloads) && count($downloads) > 0): ?>
			    			<div class="documents">

								<h4 class="title-color title-red">Documentos</h4>
								<p>Faça download dos calendários acadêmicos em formato PDF</p>
								
								<?php foreach($downloads as $k => $download): ?>
									<div class="icon-pdf">
										<img src="assets/images/icons/icon-pdf.png" class="img-responsive">
										<p>
											<a href="<?php echo $this->Html->Url('/' . $download['Download']['path'] . '/' . $download['Download']['dir'] . '/' . $download['Download']['file'], true); ?>" title="<?php echo $download['Download']['nome']; ?>" target="_blank">
												<?php echo $download['Download']['nome']; ?>
											</a>
										</p>
									</div>
								<?php endForeach; ?>
								
							</div>
						<?php endIf; ?>

						<?php echo $this->element('site/redes-sociais-mini') ?>

					</div><!-- end sidebar-->
				</div>
			</div>			  			  	
		</div>						
	</div>
</div>