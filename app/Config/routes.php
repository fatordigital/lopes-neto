<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */

App::import('Model', 'Idioma');
$idiomas = new Idioma();
$only = array();
foreach ($idiomas->cacheData as $idioma) {
    $only[] = $idioma['Idioma']['slug'];
}
$only[] = 'css';

//begin: routes do fatorcms
Router::connect('/fatorcms', array('fatorcms' => true, 'plugin' => 'fd_dashboard', 'controller' => 'fd_dashboard', 'action' => 'index', 'prefix' => 'fatorcms'));
Router::connect('/fatorcms/login', array('fatorcms' => true, 'plugin' => 'fd_dashboard', 'controller' => 'usuarios', 'action' => 'login'));
Router::connect('/fatorcms/logout', array('fatorcms' => true, 'plugin' => 'fd_dashboard', 'controller' => 'usuarios', 'action' => 'logout'));
//end: routes do fatorcms

Router::connect('/share/*', array('controller' => 'share', 'action' => 'index'));

Router::connect('/:idioma', array('controller' => 'home', 'action' => 'index'), array('idioma' => implode('|', $only)));


Router::connect('/', array('fatorcms' => false, 'controller' => 'home', 'action' => 'index'));


/*Adiciona idiomas*/
App::import('Model', 'FdMenus.Link');
App::import('Helper', 'String');
$string = new StringHelper(new View());
$links = new Link();
foreach ($links->cacheData as $link) {
    if (in_array($link['Link']['menu_id'], array(2, 3)) && !empty($link['Link']['rel'])) {
        $controller_index = explode('@', $link['Link']['rel']);
        if (in_array($controller_index[0] . 'Controller', array_keys($string->list_controllers()))) {
            Router::connect('/:idioma/' . $link['Link']['seo_url'] . '/*', array('controller' => $controller_index[0], 'action' => isset($controller_index[1]) ? $controller_index[1] : 'index', 'fatorcms' => false, 'plugin' => false), array('idioma' => implode('|', $only)));
        }
    }
}
/*Adiciona idiomas*/


Router::connect('/sitemap.xml', array('admin' => false, 'controller' => 'sitemaps', 'action' => 'index'));
Router::connect('/sitemap', array('admin' => false, 'controller' => 'sitemaps', 'action' => 'index'));

$request_url = $_SERVER['REQUEST_URI'];
$explode = explode("/", $request_url);
$url = null;

while (count($explode) > 0) {
    if (stripos($_SERVER['SCRIPT_NAME'], $request_url) === false && stripos($_SERVER['SCRIPT_NAME'], $explode[1]) == true) {
        unset($explode[count($explode) - 1]);
        if (count($explode) > 0) {
            $request_url = '';
            foreach ($explode as $ex) {
                if ($ex != "") {
                    $request_url .= '/' . $ex;
                }
            }
        }
    } else {
        $uri = str_replace($request_url . '/', '', $_SERVER['REQUEST_URI']);

        if (strlen($uri) > 1 && substr($uri, 0, 1) == '/') {
            $uri = substr($uri, 1);
        }
        $explode = array();
    }
}

App::import('Helper', 'Idioma');
$idiomaHelper = new IdiomaHelper(new View());

$url = $uri;
if ($_SERVER['QUERY_STRING'] != "") {
    $url = str_replace('?' . $_SERVER['QUERY_STRING'], '', $url);
}

if ($idiomaHelper->slug)
    $url = str_replace($idiomaHelper->slug . '/', '', $url);

//remove parametros
$tem_paramentro = false;
while (strpos(basename($url), ':') != "") {
    $tem_paramentro = true;
    $url = str_replace(basename($url), '', $url);
}

while (substr($url, -1) == '/') {
    $url = substr($url, 0, -1);
}

$url = urldecode($url);

App::import('Model', 'FdRotas.Rota');
$modelRota = new Rota();
$rotas = $modelRota->getRotas(Configure::read('site'));

if (is_array($rotas) && count($rotas) > 0) {

    if (!array_key_exists($url, $rotas)) {
        $denovo = explode("/", $request_url);
        $url = str_replace('/' . $denovo[count($denovo) - 1], '', $url);
    }

    if ($idiomaHelper->slug)
        $url = str_replace($idiomaHelper->slug . '/', '', $url);


    if (array_key_exists($url, $rotas)) {

        $rota = $uri;
        if ($tem_paramentro == true || $_SERVER['QUERY_STRING'] != "") {
            $rota = $url;
        }

        if ($idiomaHelper->slug != '')
            $rota = str_replace($idiomaHelper->slug . '/', '', $rota);

        $params = array();
        if (Configure::read('Site.Idioma.Padrao') == '') {
            if ($rotas[$url]['params_value'] != null) {

                //Begin: customização para o tipo de noticia: Responsabilidade Social
                if ($rotas[$url]['params_id'] == 'controller') {
                    Router::connect("/:idioma/{$rota}", array(
                        'fatorcms' => false,
                        'plugin' => false,
                        'controller' => $rotas[$url]['params_value'],
                        'action' => $rotas[$url]['action'],
                        $rotas[$url]['row_id'],
                        $url), array(
                            'idioma' => implode('|', $only)
                        )
                    );
                    Router::connect("/:idioma/{$rota}/", array('fatorcms' => false, 'plugin' => false, 'controller' => $rotas[$url]['params_value'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id'], $url), array('idioma' => implode('|', $only)));
                    Router::connect("/:idioma/{$rota}/*", array('fatorcms' => false, 'plugin' => false, 'controller' => $rotas[$url]['params_value'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id'], $url), array('idioma' => implode('|', $only)));
                } else {
                    $params = array($rotas[$url]['params_id'] => $rotas[$url]['params_value']);

                    Router::connect("/:idioma/{$rota}", array('fatorcms' => false, 'plugin' => false, 'controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id'], $url, $params), array('idioma' => implode('|', $only)));
                    Router::connect("/:idioma/{$rota}/", array('fatorcms' => false, 'plugin' => false, 'controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id'], $url, $params), array('idioma' => implode('|', $only)));
                    Router::connect("/:idioma/{$rota}/*", array('fatorcms' => false, 'plugin' => false, 'controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id'], $url, $params), array('idioma' => implode('|', $only)));
                }
                //End: customização para o tipo de noticia: Responsabilidade Social

            } else {

                if (isset($denovo)) {
                    Router::connect("/:idioma/{$rota}", array('fatorcms' => false, 'plugin' => false, 'controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id'], $url, $denovo[count($denovo) - 1]), array('idioma' => implode('|', $only)));
                    Router::connect("/:idioma/{$rota}/", array('fatorcms' => false, 'plugin' => false, 'controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id'], $url, $denovo[count($denovo) - 1]), array('idioma' => implode('|', $only)));
                    Router::connect("/:idioma/{$rota}/*", array('fatorcms' => false, 'plugin' => false, 'controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id'], $url, $denovo[count($denovo) - 1]), array('idioma' => implode('|', $only)));
                } else {
                    Router::connect("/:idioma/{$rota}", array('fatorcms' => false, 'plugin' => false, 'controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id'], $url), array('idioma' => implode('|', $only)));
                    Router::connect("/:idioma/{$rota}/", array('fatorcms' => false, 'plugin' => false, 'controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id'], $url), array('idioma' => implode('|', $only)));
                    Router::connect("/:idioma/{$rota}/*", array('fatorcms' => false, 'plugin' => false, 'controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id'], $url), array('idioma' => implode('|', $only)));
                }
            }
        } else {
            Router::connect("/{$rota}/*", array('fatorcms' => false, 'plugin' => false, 'controller' => $rotas[$url]['controller'], 'action' => $rotas[$url]['action'], $rotas[$url]['row_id'], $url));
        }
    }
}

CakePlugin::routes();

require CAKE . 'Config' . DS . 'routes.php';

//
//$routes = Router::$routes;
//
//echo '<pre>';
//die(print_r($routes));
//
