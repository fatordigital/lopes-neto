<?php echo $this->Html->script('fatorcms/jquery.stringToSlug.min'); ?>

<?php echo $this->Html->css('/js/fatorcms/core/bootstrap-fileupload/bootstrap-fileupload') ?>
<?php echo $this->Html->script('/js/fatorcms/core/bootstrap-fileupload/bootstrap-fileupload'); ?>

<?php echo $this->Html->script('/fd_paginas/js/fatorcms/pagina/crud.js'); ?>

<?php $this->Html->addCrumb('Página', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Editar Página') ?>

<h3>
	<?php echo ($this->params->params['abas'] == true) ? 'Editar Aba' : 'Editar Página'; ?>
	<?php echo $this->Html->link('Visualizar versões anteriores', array('plugin' => false, 'controller' => 'svn', 'action' => 'index', 'registro_id' => $this->data['Pagina']['id'], 'model' => 'Pagina'), array('class' => 'btn btn-info pull-right margin-top-5-neg')); ?>
</h3>

<div class="panel">
	<div class="panel-body">

	<?php if($this->data['Pagina']['alerta'] == true): ?>

		<div class="alert alert-warning fade in">
	        <button data-dismiss="alert" class="close close-sm" type="button">
	            <i class="fa fa-times"></i>
	        </button>
	        <strong>Atenção!</strong> Essa aba possui conteúdo com uma formatação frágil. Tome cuidado ao edita-la, e caso necessário, contate a Fator Digital.
	    </div>

    <?php endIf; ?>

    <?php if($this->data['Pagina']['dinamico'] == true): ?>

		<div class="alert alert-warning fade in">
	        <button data-dismiss="alert" class="close close-sm" type="button">
	            <i class="fa fa-times"></i>
	        </button>
	        <strong>Atenção!</strong> Essa aba possui conteúdo dinâmico. Nunca remova a variável {CONTEUDO_DINAMICO} com campo "Conteúdo".
	    </div>

    <?php endIf; ?>

	<?php echo $this->Form->create('Pagina', array('type' => 'file', 'role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
		<?php echo $this->Form->input('id') ?>
		<div class="row">
			<div class="col-md-6 col-lg-6">
				<div class="form-group">
					<label>Status</label>
					<div class="icheck">
						<?php echo $this->Form->input('status', array(
																	'type' => 'radio', 
																	'options' => array(1 => 'Ativo', 0 => 'Inativo', 2 => 'Rascunho'), 
																	'default' => 1, 
																	'legend' => false, 
																	'before' => '<div class="radio">', 
																	'after' => '</div>', 
																	'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
					</div>
				</div>
			</div>
		</div>

		<?php if($this->params->params['abas'] == false): ?>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<label>Tipo</label>
						<div class="icheck">
							<?php echo $this->Form->input('pagina_tipo_id', array(
																		'type' => 'select', 
																		'options' => $pagina_tipo_id, 
																		'class'	=> 'form-control input-sm m-bot15',
																		'label' => false
																	)); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<label>Vincular a um Curso</label>
						<div class="icheck">
							<?php echo $this->Form->input('curso_id', array(
																			'type' => 'select', 
																			'empty' => 'Selecione o curso...',
																			'options' => $curso_id, 
																			'class'	=> 'form-control input-sm m-bot15',
																			'label' => false
																		)); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<label>Site</label>
						<div class="icheck">
							<?php echo $this->Form->input('sites', array(
																		'type' => 'radio', 
																		'options' => $this->String->getSites(), 
																		'legend' => false, 
																		'before' => '<div class="radio">', 
																		'after' => '</div>', 
																		'separator' => '</div><div class="clearfix"></div><div class="radio">'
																	)); ?>
						</div>
					</div>
				</div>
			</div>
		<?php endIf; ?>

		<?php if($this->params->params['abas'] == true): ?>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<label>Página Pai</label>
						<div class="icheck">
							<?php echo $this->Form->input('parent_id', array(
																		'type' => 'select', 
																		'options' => $parent_id, 
																		'class'	=> 'form-control input-sm m-bot15',
																		'label' => false
																	)); ?>
						</div>
					</div>
				</div>
			</div>
		<?php endIf; ?>

		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<?php echo $this->Form->input('nome', array('div' => false, 'class' => 'form-control')) ?>
				</div>
			</div>
		</div>

		<?php if($this->params->params['abas'] == false): ?>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<?php echo $this->Form->input('seo_url', array('label' => 'URL', 'div' => false, 'class' => 'form-control slug')) ?>
					</div>
				</div>
			</div>
		<?php endIf; ?>

		<?php if($this->params->params['abas'] == true): ?>

			<?php if($this->data['Pagina']['conteudo_rascunho'] != ""): ?>

				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<?php echo $this->Form->input('conteudo_rascunho', array('label' => 'Conteúdo', 'div' => false, 'class' => 'form-control editor')) ?>
						</div>
					</div>
				</div>

			<?php else: ?>

				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<?php echo $this->Form->input('conteudo', array('label' => 'Conteúdo', 'div' => false, 'class' => 'form-control editor')) ?>
						</div>
					</div>
				</div>

			<?php endIf; ?>

			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<label>Possui conteúdo dinâmico?</label>
						<div class="icheck">
							<?php echo $this->Form->input('dinamico', array(
																		'type' => 'radio', 
																		'options' => array(1 => 'Sim', 0 => 'Não'), 
																		'default' => 0, 
																		'legend' => false, 
																		'before' => '<div class="radio">', 
																		'after' => '</div>', 
																		'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-7">
					<div class="form-group">
						<?php echo $this->Form->input('dinamico_elemento', array('label' => 'Nome do Elemento', 'type' => 'text', 'div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-7">
					<div class="form-group">
						<?php echo $this->Form->input('aba_ordem', array('label' => 'Ordem', 'type' => 'text', 'div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>

		<?php endIf; ?>
		
		<div class="row">
			<div class="col-lg-7">
				<div class="form-group">
					<?php echo $this->Form->input('sidebar', array('label' => 'Sidebar', 'type' => 'textarea', 'div' => false, 'class' => 'form-control editor')) ?>
				</div>
			</div>
		</div>

		<?php if($this->params->params['abas'] == false): ?>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<?php echo $this->Form->input('seo_title', array('div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<?php echo $this->Form->input('seo_description', array('type' => 'textarea', 'div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<?php echo $this->Form->input('seo_keywords', array('div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<label>Banner</label>
						<div class="fileupload fileupload-new" data-provides="fileupload">

							<?php if(isset($pagina) && $pagina['Pagina']['banner'] != ""): ?>
								<div class="fileupload-new thumbnail" style="width: 200px; height: 119px;">
									<img src="<?php echo $this->Html->Url('/'. $pagina['Pagina']['banner_path'] .'/'. $pagina['Pagina']['banner_dir'] . '/400_' . $pagina['Pagina']['banner'] , true) ?>" width="200" />
								</div>
							<?php else: ?>
			                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
			                    	<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=nenhuma+imagem" alt="" />
			                    </div>
		                    <?php endIf; ?>
		                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>

	                        <?php if(isset($pagina) && $pagina['Pagina']['banner'] != ""): ?>
	                        <div class="icheck minimal">
	                        	<?php echo $this->Form->input('Pagina.banner.remove', array('type' => 'checkbox', 'label' => 'Remover Imagem Atual', 'div' => false)); ?>
	                        </div>
	                        <?php endIf; ?>

		                    <div>
		                       <span class="btn btn-white btn-file">
		                           	<span class="fileupload-new"><i class="fa fa-paper-clip"></i> Selecionar imagem</span>
		                           	<span class="fileupload-exists"><i class="fa fa-undo"></i> Alterar</span>
		                           	<?php echo $this->Form->input('banner', array('type' => 'file', 'class' => 'default', 'label' => false, 'div' => false)); ?>
		                       </span>
		                        <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remover</a>
		                    </div>
		                </div>
		                <!-- <span class="label label-danger">Atenção!</span>
		                <span>
		                 	A visualiação da miniatura da imagem antes do upload é suportado no mais recente Firefox, Chrome, Opera, Safari e Internet Explorer 10 unicamente.
		                </span> -->
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
							<?php echo $this->Form->input('banner_alone', array(
																		'type' => 'checkbox', 
																		'legend' => false,
																		'label' => 'Exibir apenas esse banner' 
																		)); ?>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<label>O banner possui formulário?</label>
						<div class="icheck">
							<?php echo $this->Form->input('banner_tem_formulario', array(
																		'type' => 'radio', 
																		'options' => array(1 => 'Sim', 0 => 'Não'), 
																		'default' => 0, 
																		'legend' => false, 
																		'before' => '<div class="radio">', 
																		'after' => '</div>', 
																		'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<label>Banner Mobile</label>
						<div class="fileupload fileupload-new" data-provides="fileupload">

							<?php if(isset($pagina) && $pagina['Pagina']['banner_mobile'] != ""): ?>
								<div class="fileupload-new thumbnail" style="width: 200px; height: 119px;">
									<img src="<?php echo $this->Html->Url('/'. $pagina['Pagina']['banner_mobile_path'] .'/'. $pagina['Pagina']['banner_mobile_dir'] . '/400_' . $pagina['Pagina']['banner_mobile'] , true) ?>" width="200" />
								</div>
							<?php else: ?>
			                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
			                    	<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=nenhuma+imagem" alt="" />
			                    </div>
		                    <?php endIf; ?>
		                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>

	                        <?php if(isset($pagina) && $pagina['Pagina']['banner_mobile'] != ""): ?>
	                        <div class="icheck minimal">
	                        	<?php echo $this->Form->input('Pagina.banner_mobile.remove', array('type' => 'checkbox', 'label' => 'Remover Imagem Atual', 'div' => false)); ?>
	                        </div>
	                        <?php endIf; ?>

		                    <div>
		                       <span class="btn btn-white btn-file">
		                           	<span class="fileupload-new"><i class="fa fa-paper-clip"></i> Selecionar imagem</span>
		                           	<span class="fileupload-exists"><i class="fa fa-undo"></i> Alterar</span>
		                           	<?php echo $this->Form->input('banner_mobile', array('type' => 'file', 'class' => 'default', 'label' => false, 'div' => false)); ?>
		                       </span>
		                        <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remover</a>
		                    </div>
		                </div>
		                <!-- <span class="label label-danger">Atenção!</span>
		                <span>
		                 	A visualiação da miniatura da imagem antes do upload é suportado no mais recente Firefox, Chrome, Opera, Safari e Internet Explorer 10 unicamente.
		                </span> -->
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<label>Exibir Formulário?</label>
						<div class="icheck">
							<?php echo $this->Form->input('formulario', array(
																		'type' => 'radio', 
																		'options' => array(1 => 'Sim', 0 => 'Não'), 
																		// 'default' => 1, 
																		'legend' => false, 
																		'before' => '<div class="radio">', 
																		'after' => '</div>', 
																		'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-7">
					<div class="form-group">
						<?php echo $this->Form->input('formulario_titulo', array('label' => 'Título do Formulário', 'type' => 'text', 'div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-7">
					<div class="form-group">
						<?php echo $this->Form->input('formulario_email', array('label' => 'Destinatário do Formulário (cópias)', 'type' => 'textarea', 'div' => false, 'class' => 'form-control')) ?>
						<?php if(isset($sac_tipo) && $sac_tipo['SacTipo']['email'] != ""): ?>
							<span>[Destinatários defaults: <?php echo $sac_tipo['SacTipo']['email']; ?>].</span><br />
						<?php endIf; ?>
						<span>[Separe com ponto e vírgula(;) caso deseja que o formulário dispare e-mail para mais de um contato].</span>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<label>Exibir botão de aba?</label>
						<div class="icheck">
							<?php echo $this->Form->input('botao_aba_status', array(
																		'type' => 'radio', 
																		'options' => array(1 => 'Sim', 0 => 'Não'), 
																		'default' => 0, 
																		'legend' => false, 
																		'before' => '<div class="radio">', 
																		'after' => '</div>', 
																		'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-7">
					<div class="form-group">
						<?php echo $this->Form->input('botao_aba_url', array('label' => 'Url do botão', 'type' => 'textarea', 'div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-7">
					<div class="form-group">
						<?php echo $this->Form->input('botao_aba_label', array('label' => 'Label do botão', 'div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<label>Target</label>
						<div class="icheck">
							<?php echo $this->Form->input('botao_aba_target', array(
																		'type' => 'radio',
																		'options' 	=> array(
																			'_self' => '_self',
																			'_blank' => '_blank'
																		),
																		'default' => '_self',
																		'legend' => false,
																		'before' => '<div class="radio">', 
																		'after' => '</div>',
																		'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<label>Exibir segundo botão de aba?</label>
						<div class="icheck">
							<?php echo $this->Form->input('botao_aba_2_status', array(
																		'type' => 'radio', 
																		'options' => array(1 => 'Sim', 0 => 'Não'), 
																		'default' => 0, 
																		'legend' => false, 
																		'before' => '<div class="radio">', 
																		'after' => '</div>', 
																		'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-7">
					<div class="form-group">
						<?php echo $this->Form->input('botao_aba_2_url', array('label' => 'Url do botão 2', 'type' => 'textarea', 'div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-7">
					<div class="form-group">
						<?php echo $this->Form->input('botao_aba_2_label', array('label' => 'Label do botão 2', 'div' => false, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<label>Target 2</label>
						<div class="icheck">
							<?php echo $this->Form->input('botao_aba_2_target', array(
																		'type' => 'radio',
																		'options' 	=> array(
																			'_self' => '_self',
																			'_blank' => '_blank'
																		),
																		'default' => '_self',
																		'legend' => false,
																		'before' => '<div class="radio">', 
																		'after' => '</div>',
																		'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<label>Possui Bloco de Notícias?</label>
						<div class="icheck">
							<?php echo $this->Form->input('bloco_noticia_status', array(
																		'type' => 'radio', 
																		'options' => array(0 => 'Não', 1 => 'Utilizar Existente', 2 => 'Criar Novo'), 
																		'default' => 0, 
																		'legend' => false, 
																		'before' => '<div class="radio">', 
																		'after' => '</div>', 
																		'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<?php echo $this->Form->input('bloco_noticia_id', array('options' => $blocos, 'empty' => 'Selecione a bloco de conteúdo...', 'label' => 'Bloco', 'class' => 'form-control input-sm m-bot15')); ?>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<?php echo $this->Form->input('bloco_noticia_nome', array('label' => 'Nome do Bloco', 'class' => 'form-control input-sm')); ?>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-4">
					<div class="form-group">
						<?php echo $this->Form->input('bloco_noticia_slug', array('label' => 'Chave do Bloco', 'class' => 'form-control input-sm bloco_slug')); ?>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<label>Exibir Abas?</label>
						<div class="icheck">
							<?php echo $this->Form->input('exibir_abas', array(
																		'type' => 'radio', 
																		'options' => array(1 => 'Sim', 0 => 'Não'), 
																		'default' => 1, 
																		'legend' => false, 
																		'before' => '<div class="radio">', 
																		'after' => '</div>', 
																		'separator' => '</div><div class="clearfix"></div><div class="radio">'
																		)); ?>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<label>Template Novo</label>
						<div class="icheck">
							<?php echo $this->Form->input('template_novo', array(
																		'type' => 'radio', 
																		'options' => array(1 => 'Sim', 0 => 'Não'), 
																		'default' => 0, 
																		'legend' => false, 
																		'before' => '<div class="radio">', 
																		'after' => '</div>', 
																		'separator' => '</div><div class="clearfix"></div><div class="radio">'
																		)); ?>
						</div>
					</div>
				</div>
			</div>

		<?php endIf; ?>
		
		<?php if($this->params->params['abas'] == true): ?>
			<?php if($this->data['Pagina']['conteudo_rascunho'] != ""): ?>
				<?php echo $this->Form->submit('Salvar como rascunho', array('name' => 'rascunho', 'class' => 'btn btn-primary', 'div' => false)) ?>
				<?php echo $this->Form->submit('Descartar rascunho', array('name' => 'descartar_rascunho', 'class' => 'btn btn-danger', 'div' => false)) ?>
				<?php echo $this->Form->submit('Salvar e Publicar', array('name' => 'salvar_publicar', 'class' => 'btn btn-success', 'div' => false, 'style' => 'margin-right: 4px;')) ?>
			<?php else: ?>
				<?php echo $this->Form->submit('Salvar', array('name' => 'salvar', 'class' => 'btn btn-success', 'div' => false)) ?>
				<?php echo $this->Form->submit('Salvar como rascunho', array('name' => 'rascunho', 'class' => 'btn btn-primary', 'div' => false, 'style' => 'margin-right: 4px;')) ?>
			<?php endIf; ?>
		<?php else: ?>
			<?php echo $this->Form->submit('Salvar', array('name' => 'salvar', 'class' => 'btn btn-success', 'div' => false)) ?>
		<?php endIf; ?>
		
		<a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">&nbsp;Cancelar</a>
		
	<?php echo $this->Form->end() ?>
	</div>
</div>