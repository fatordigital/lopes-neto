<?php $this->Html->addCrumb('Páginas'); ?>

<div class="row">
     <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
        Páginas 
        <?php echo $this->Html->link('Cadastrar Aba', array('action' => 'add', 'abas' => true), array('class' => 'btn btn-info pull-right')); ?>

        <?php 
            //exibo o botão de adição de página apenas para o usuário ROOT
            if(isset($user['grupo_id']) && ($user['grupo_id'] == 1 || $user['grupo_id'] == 7)){
                echo $this->Html->link('Cadastrar Página', array('action' => 'add', 'abas' => false), array('class' => 'btn btn-info pull-right margin-right-10')); 
            }
        ?>
    </h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <div class="row-fluid">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <?php echo $this->FilterForm->create(null, array('role' => 'form', 'class' => 'form-inline')) ?>
                            <div class="form-group">
                                <?php echo $this->FilterForm->input('filtro_nome', array('placeholder' => 'Filtrar por nome', 'class' => 'form-control')) ?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->FilterForm->input('filtro_conteudo', array('placeholder' => 'Filtrar por conteúdo', 'class' => 'form-control')) ?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->FilterForm->input('filtro_pagina_tipo_id', array('placeholder' => 'Filtrar por tipo', 'class' => 'form-control')) ?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->FilterForm->input('filtro_sites', array('placeholder' => 'Filtrar por site', 'class' => 'form-control')) ?>
                            </div>
                            <?php echo $this->FilterForm->submit('Filtrar', array('class' => 'btn btn-success', 'div' => false)) ?>
                            <?php echo $this->Html->link('Limpar Filtro', array('action' => 'index'), array('class' => 'btn btn-warning')) ?>
                        <?php echo $this->FilterForm->end() ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-hover general-table">
                    <thead>
                        <tr>
                            <th style="width:60px"><?php echo $this->Paginator->sort('id', '#') ?></th>
                            <th><?php echo $this->Paginator->sort('sites', 'Site') ?></th>
                            <th><?php echo $this->Paginator->sort('nome') ?></th>
                            <th><?php echo $this->Paginator->sort('seo_url', 'URL') ?></th>
							<th><?php echo $this->Paginator->sort('status') ?></th>
                            <th><?php echo $this->Paginator->sort('modified', 'Modificação') ?></th>
                            <th style="width:180px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($paginas) > 0): ?>
                            <?php foreach($paginas AS $y => $pagina): ?>
                            <tr>
                                <td style="font-weight: bold;"><?php echo $y+1; ?></td>
                                <td>   
                                    <?php echo $this->String->getBtnBySite($pagina['Pagina']['sites']); ?>
                                </td>
                                <td style="font-size: 15px; font-weight: bold;"><?php echo $pagina['Pagina']['nome'] ?></td>
                                <td style="font-weight: bold;"><a href="<?php echo $this->Html->Url('/'.$pagina['Pagina']['seo_url'], true); ?>" title="<?php echo $this->Html->Url('/'.$pagina['Pagina']['seo_url'], true); ?>" target="_blank"><?php echo $pagina['Pagina']['seo_url']; ?></a></td>
								<td style="font-weight: bold;">
                                    <input type="checkbox" class="atualiza-status" value="<?php echo $pagina['Pagina']['status'] == 1 ? 0 : 1 ?>" data-id="<?php echo $pagina['Pagina']['id'] ?>" data-url="<?php echo $this->Html->url(array('action' => 'status')) ?>" data-on-text="Sim" data-on-color="success" data-off-text="Não" data-off-color="danger"<?php echo $pagina['Pagina']['status'] == 1 ? ' checked="checked"' : '' ?>>
                                </td>
                                <td><?php echo $this->Time->format($pagina['Pagina']['modified'], '%d/%m/%Y %H:%M:%S') ?></td>
                                <td>
                                    <?php echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('action' => 'edit', $pagina['Pagina']['id'], 'abas' => false), array('class' => 'btn btn-primary btn-sm', 'escape' => false)) ?>
                                    <?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('action' => 'delete', $pagina['Pagina']['id'], 'abas' => false), array('class' => 'btn btn-danger btn-sm', 'escape' => false), __('Deseja mesmo remover o registro # %s?', $pagina['Pagina']['nome'])) ?>
                                </td>
                            </tr>

                            <?php if(isset($pagina['Aba']) && count($pagina['Aba']) > 0): ?>
                                <?php foreach($pagina['Aba'] AS $k => $aba): ?>
                                    <tr>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $y+1; ?>.<?php echo $k+1; ?></td>
                                        <td>&nbsp;</td>
                                        <td><?php echo $aba['nome'] ?></td>
                                        <td><a href="<?php echo $this->Html->Url('/'.$pagina['Pagina']['seo_url'].'#'.$aba['seo_url'], true); ?>" title="<?php echo $this->Html->Url('/'.$pagina['Pagina']['seo_url'].'#'.$aba['seo_url'], true); ?>" target="_blank"><?php echo $pagina['Pagina']['seo_url'].'#'.$aba['seo_url']; ?></a#</td>
                                        <td>
                                            <?php if($aba['status'] == 2): ?>
                                                <label class="btn btn-warning">Rascunho</label>
                                            <?php elseif($aba['status'] == 1): ?>
                                                <label class="btn btn-success">Publicado</label>
                                            <?php else: ?>
                                                <label class="btn btn-danger">&nbsp;&nbsp;Inativo&nbsp;&nbsp;&nbsp;</label>
                                            <?php endIf; ?>
                                        </td>
                                        <td><?php echo $this->Time->format($aba['modified'], '%d/%m/%Y %H:%M:%S') ?></td>
                                        <td>
                                            <?php if($aba['conteudo_rascunho'] != ""): ?>
                                                <?php echo $this->Html->link('<i class="fa fa-eye"></i>', '/pagina/preview/' . $pagina['Pagina']['seo_url'], array('class' => 'btn btn-primary btn-xs tooltips', 'escape' => false, 'data-placement' => 'top', 'data-toggle' => 'tooltip', 'data-original-title' => 'Pré-Visualizar Conteúdo da Aba')) ?>

                                                <?php echo $this->Html->link('Editar', array('action' => 'edit', $aba['id'], 'abas' => true), array('class' => 'btn btn-primary btn-xs', 'escape' => false)) ?> 
                                                <?php echo $this->Html->link('Remover', array('action' => 'delete', $aba['id'], 'abas' => false), array('class' => 'btn btn-danger btn-xs', 'escape' => false), __('Deseja mesmo remover o registro # %s?', $aba['nome'])) ?>
                                            <?php else: ?>
                                                <?php echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('action' => 'edit', $aba['id'], 'abas' => true), array('class' => 'btn btn-primary btn-xs', 'escape' => false)) ?> 
                                                <?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('action' => 'delete', $aba['id'], 'abas' => false), array('class' => 'btn btn-danger btn-xs', 'escape' => false), __('Deseja mesmo remover o registro # %s?', $aba['nome'])) ?>
                                            <?php endIf; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>                                
                            <?php endIf; ?>

                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="5" class="text-center">Nenhum registro encontrado.</td>
                            </tr>
                        <?php endIf; ?>
                    </tbody>
                </table>

                <?php echo $this->Element('FdDashboard.paginator'); ?>
            </div>
        </section>
    </div>
</div>