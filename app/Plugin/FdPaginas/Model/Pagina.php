<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');

class Pagina extends FdPaginasAppModel {

	public $actsAs = array(
		'Containable',
        'Encoder',
        'Tree',
        'Upload.Upload' => array(			
            'file' => array(
				'fields' => array(
                    'dir' => 'dir',
					'type' => 'type',
					'size' => 'size',
                ),
				'thumbnailMethod' => 'php',
                'thumbnailSizes' => array(
                    'thumb' => '100x100',
                    '400' => '400w'
                )
            ),
            'banner' => array(
                'fields' => array(
                    'dir' => 'banner_dir',
                    'type' => 'banner_type',
                    'size' => 'banner_size',
                ),
                'thumbnailMethod' => 'php',
                'thumbnailSizes' => array(
                    'thumb' => '100x100',
                    '400' => '400w'
                ),
            ),
            'banner_mobile' => array(
                'fields' => array(
                    'dir' => 'banner_mobile_dir',
                    'type' => 'banner_mobile_type',
                    'size' => 'banner_mobile_size',
                ),
                'thumbnailMethod' => 'php',
                'thumbnailSizes' => array(
                    'thumb' => '100x100',
                    '400' => '400w'
                )
            )
        )
    );

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'pagina_tipo_id' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
		'parent_id' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
		'nome' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
		'seo_url' => array(
			'url_unica' => array(
	             'rule' => array('check_url_unica'),
	             'message' => 'Url já existente. Tenta outra.',
	        ),
			'notempty' => array(
				'rule' => array('valida_aba'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
		'conteudo' => array(
			'notempty' => array(
				'rule' => array('valida_aba'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
	);
	
/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'PaginaTipo' => array(
			'className' => 'PaginaTipo',
			'foreignKey' => 'pagina_tipo_id'
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Aba' => array(
				'className' => 'Pagina',
				'order' => 'aba_ordem',
				'foreignKey' => 'parent_id',
				'dependent' => false,
		),
	);


/**
 * valida_aba
 */
	public function valida_aba() {
        if(isset($this->data[$this->alias]['parent_id']) && $this->data[$this->alias]['parent_id'] > 0){
            return true;
        }

        if(isset($this->data[$this->alias]['seo_url']) && empty($this->data[$this->alias]['seo_url'])){
            return false;
        }

        return true;
    }

	public function beforeSave($options = array()) {
		App::import("helper", "String");
        $this->String = new StringHelper(new View(null));

        $user = AuthComponent::user();
		$this->data[$this->alias]['usuario_id'] = $user['id'];

		//trata url
		if (isset($this->data[$this->alias]['nome']) && (isset($this->data[$this->alias]['seo_url']) && $this->data[$this->alias]['seo_url'] == "")) {
			$this->data[$this->alias]['seo_url'] =  strtolower(Inflector::slug($this->data[$this->alias]['nome'], '-'));
        }

        if (isset($this->data[$this->alias]['parent_id']) && $this->data[$this->alias]['parent_id'] > 0) {
			$this->data[$this->alias]['seo_url'] =  strtolower(Inflector::slug($this->data[$this->alias]['nome'], '-'));
        }

        //trata seo_title
        if(isset($this->data[$this->alias]['seo_title']) && $this->data[$this->alias]['seo_title'] == ""){
        	$this->data[$this->alias]['seo_title'] = $this->data[$this->alias]['nome'];
        }

        if(isset($this->data[$this->alias]['conteudo'])){
        	$conteudo = utf8_encode(html_entity_decode(strip_tags($this->data[$this->alias]['conteudo'])));
	        //trata seo_description
	        if(isset($this->data[$this->alias]['seo_description']) && $this->data[$this->alias]['seo_description'] == ""){
	        	$this->data[$this->alias]['seo_description'] = $this->String->truncate_str($conteudo, 150);
	        	if(strlen($conteudo) > 150){
	        		$this->data[$this->alias]['seo_description'] .= "...";
	        	}
	        }

	        if(isset($this->data[$this->alias]['conteudo']) && $this->data[$this->alias]['conteudo'] != ""){
	        	$this->data[$this->alias]['conteudo'] = str_replace('src="../../app/', 'src="/app/', $this->data[$this->alias]['conteudo']);
	        	$this->data[$this->alias]['conteudo'] = str_replace('src="../../../app/', 'src="/app/', $this->data[$this->alias]['conteudo']);
	        }
        }
        
        parent::beforeSave($options = array());
    }


/**
 * afterSave
 *
 * sobrecarga do metodo executado depois de salvar o registro
 */
	public function afterSave($created, $options = array()) {

		parent::afterSave($created, $options = array());

		//begin salva o registro da URL nas rotas
		$controller 	= 'Paginas';
		$model 			= 'Pagina';
		$action			= 'detalhe';
		$params_id		= null;
		$params_value	= null;

		//params
		$params = Router::getParams();
		
		//if(isset($this->data[$this->alias]['parent_id']) && $this->data[$this->alias]['parent_id'] == 0){
			if(isset($this->data[$this->alias]['seo_url']) || $params['abas'] == true){
				App::import('Model', 'FdRotas.Rota');
				$this->Rota = new Rota();

				if($params['abas'] == true){
					$page = $this->find('first', array('recursive' => -1, 'conditions' => array('id' => $this->data[$this->alias]['parent_id'])));
					if(!empty($page)){
						$this->data[$this->alias]['id'] 		= $this->data[$this->alias]['parent_id'];
						$this->data[$this->alias]['seo_url'] 	= $page['Pagina']['seo_url'].'#'.$this->data[$this->alias]['seo_url'];

						if(isset($this->data[$this->alias]['nome'])){
							$this->data[$this->alias]['nome'] 		= $page['Pagina']['nome'] . ': ' . $this->data[$this->alias]['nome'];
						}

						$this->data[$this->alias]['seo_keywords'] 		= $page['Pagina']['seo_keywords'];
						$this->data[$this->alias]['seo_description'] 	= $page['Pagina']['seo_description'];
						$this->data[$this->alias]['sites'] 				= $page['Pagina']['sites'];
					}

					$conditions = array(
										'AND' => array(
												'row_id' => $this->data[$this->alias]['id'],
												'model'  => $model,
												'seo_url' => $this->data[$this->alias]['seo_url']
											)
									);
				}else{
					$conditions = array(
										'AND' => array(
												'row_id' => $this->data[$this->alias]['id'],
												'model'  => $model
											)
									);
				}

				$rt = $this->Rota->find('first', array('conditions' => $conditions));

				if($rt){
					$rota['id'] 		= $rt['Rota']['id'];
				}else{
					$rota['id'] 		= null;
				}

				$rota['controller'] 	= $controller;
				$rota['model'] 			= $model;
				$rota['action'] 		= $action;
				$rota['params_id'] 		= $params_id;
				$rota['params_value'] 	= $params_value;
				$rota['row_id'] 		= $this->data[$this->alias]['id'];
				$rota['seo_url'] 		= $this->data[$this->alias]['seo_url'];
				$rota['buscavel'] 		= true;
				$rota['buscavel_ordem'] = 4;

				if(isset($this->data[$this->alias]['nome'])){
					$rota['nome'] 		= $this->data[$this->alias]['nome'];
				}
				if(isset($this->data[$this->alias]['conteudo'])){
					$rota['conteudo'] 		= $this->data[$this->alias]['conteudo'];

					$rota['conteudo'] = strip_tags($rota['conteudo']);
				}
				if(isset($this->data[$this->alias]['seo_keywords'])){
					$rota['seo_keywords'] 		= $this->data[$this->alias]['seo_keywords'];
				}
				if(isset($this->data[$this->alias]['seo_description'])){
					$rota['seo_description'] 	= $this->data[$this->alias]['seo_description'];
				}
				if(isset($this->data[$this->alias]['sites']) && $this->data[$this->alias]['sites'] != ""){
				  	$rota['sites'] = $this->data[$this->alias]['sites'];
				}

				$this->Rota->save($rota);
			}
		// }
		//end salva o registro da URL nas rotas

		//get dados atualizados da pagina
    	$pagina = $this->find('first', array('recursive' => -1, 'conditions' => array('id' => $this->id), 'fields' => array('banner', 'banner_dir', 'banner_mobile', 'banner_mobile_dir')));

		//begin rename files
    	if(isset($this->data[$this->alias]['banner']) && $this->data[$this->alias]['banner'] != ""){
    		if(isset($this->data[$this->alias]['nome'])){
	    		$pre_new_name = strtolower(Inflector::slug($this->data[$this->alias]['nome'], '-'));
				$pre_new_name = str_replace("_", "-", $pre_new_name);
				$path = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'files' . DS . 'pagina' . DS . 'banner';
	    	}
    		$pre_old_name = $this->data[$this->alias]['banner'];

    		$new_name = $pre_new_name;
			$old_name = $pre_old_name;

			if(file_exists($path . DS . $pagina[$this->alias]['banner_dir'] . DS . $old_name)){
				$file = $path . DS . $pagina[$this->alias]['banner_dir'] . DS . $old_name;
				$infos = pathinfo($file);
				$new_file = $path . DS . $pagina[$this->alias]['banner_dir'] . DS . $new_name . '.' . $infos['extension'];

				if(rename($file, $new_file)){

					//load lib
        			include_once(ROOT . DS . APP_DIR . DS . 'Lib' . DS . 'Wideimage/WideImage.php');

					$this->query('UPDATE paginas SET banner = "' . $new_name . '.' . $infos['extension'] . '" WHERE id = ' . $this->id);

					$caminho_novo 	= $path . DS . $pagina[$this->alias]['banner_dir'];
					$outFile    	= $caminho_novo.DS.$pre_new_name . '.' . $infos['extension'];
					if(strtolower($infos['extension']) == 'jpg'){
                        WideImage::load($new_file)->resize(1500)->crop('center', 'top', 1500, 370)->saveToFile($outFile, 80); 
                    }else{
                        WideImage::load($new_file)->resize(1500)->crop('center', 'top', 1500, 370)->saveToFile($outFile);
                    }

					//outros renames
					$file = $path . DS . $pagina[$this->alias]['banner_dir'] . DS . '400_' . $old_name;
					$infos = pathinfo($file);
					$new_file = $path . DS . $pagina[$this->alias]['banner_dir'] . DS . '400_' . $new_name . '.' . $infos['extension'];
					rename($file, $new_file);

					$file = $path . DS . $pagina[$this->alias]['banner_dir'] . DS . 'thumb_' . $old_name;
					$infos = pathinfo($file);
					$new_file = $path . DS . $pagina[$this->alias]['banner_dir'] . DS . 'thumb_' . $new_name . '.' . $infos['extension'];
					rename($file, $new_file);
				}
			}
		}


		//begin rename banner_mobile
    	if(isset($this->data[$this->alias]['banner_mobile']) && $this->data[$this->alias]['banner_mobile'] != ""){
    		if(isset($this->data[$this->alias]['nome'])){
	    		$pre_new_name = strtolower(Inflector::slug($this->data[$this->alias]['nome'], '-'));
				$pre_new_name = str_replace("_", "-", $pre_new_name);
				$path = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'files' . DS . 'pagina' . DS . 'banner_mobile';
	    	}
    		$pre_old_name = $this->data[$this->alias]['banner_mobile'];

    		$new_name = $pre_new_name;
			$old_name = $pre_old_name;

			if(file_exists($path . DS . $pagina[$this->alias]['banner_mobile_dir'] . DS . $old_name)){
				$file = $path . DS . $pagina[$this->alias]['banner_mobile_dir'] . DS . $old_name;
				$infos = pathinfo($file);
				$new_file = $path . DS . $pagina[$this->alias]['banner_mobile_dir'] . DS . $new_name . '.' . $infos['extension'];
				$new_file_orginal = $path . DS . $pagina[$this->alias]['banner_mobile_dir'] . DS . 'original-' . $new_name . '.' . $infos['extension'];

				if(rename($file, $new_file)){

					//load lib
        			include_once(ROOT . DS . APP_DIR . DS . 'Lib' . DS . 'Wideimage/WideImage.php');

					$this->query('UPDATE paginas SET banner_mobile = "' . $new_name . '.' . $infos['extension'] . '" WHERE id = ' . $this->id);

					copy($new_file, $new_file_orginal);

					$caminho_novo 	= $path . DS . $pagina[$this->alias]['banner_mobile_dir'];
					$outFile    	= $caminho_novo.DS.$pre_new_name . '.' . $infos['extension'];
					if(strtolower($infos['extension']) == 'jpg'){
                        WideImage::load($new_file)->resize(500)->crop('center', 'top', 500, 500)->saveToFile($outFile, 80); 
                    }else{
                        WideImage::load($new_file)->resize(500)->crop('center', 'top', 500, 500)->saveToFile($outFile);
                    }

					//outros renames
					$file = $path . DS . $pagina[$this->alias]['banner_mobile_dir'] . DS . '400_' . $old_name;
					$infos = pathinfo($file);
					$new_file = $path . DS . $pagina[$this->alias]['banner_mobile_dir'] . DS . '400_' . $new_name . '.' . $infos['extension'];
					rename($file, $new_file);

					$file = $path . DS . $pagina[$this->alias]['banner_mobile_dir'] . DS . 'thumb_' . $old_name;
					$infos = pathinfo($file);
					$new_file = $path . DS . $pagina[$this->alias]['banner_mobile_dir'] . DS . 'thumb_' . $new_name . '.' . $infos['extension'];
					rename($file, $new_file);
				}
			}
		}
	}

/**
 * afterDelete
 *
 * sobrecarga do metodo executado depois de deletar o registro
 */
	public function afterDelete() {
		parent::afterDelete();
		
	    App::import('Model', 'FdRotas.Rota');
	    $this->Rota = new Rota();

	    $rota = $this->Rota->find('first', array('conditions' => array('Rota.model' => 'Pagina', 'Rota.row_id' => $this->id)));

	    if(!empty($rota)){
	      $this->Rota->delete($rota['Rota']['id'], false);
	    }
	}
}