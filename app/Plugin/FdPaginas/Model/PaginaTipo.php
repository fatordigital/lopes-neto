<?php
App::uses('AppModel', 'Model');
/**
 * PaginaTipo Model
 *
 * @property PaginaTipo $PaginaTipo
 */
class PaginaTipo extends FdPaginasAppModel {

	public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'nome' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Pagina' => array(
			'className' => 'Pagina',
			'foreignKey' => 'pagina_tipo_id',
			'dependent' => false,
		)
	);

	public function beforeValidate($options = array()){
		//trata seo_url
		if (isset($this->data[$this->alias]['nome']) && (isset($this->data[$this->alias]['seo_url']) && $this->data[$this->alias]['seo_url'] == "")) {
			$this->data[$this->alias]['seo_url'] = 	strtolower(Inflector::slug($this->data[$this->alias]['nome'], '-'));
		}
    }

	public function beforeSave($options = array()) {
		//trata seo_title
        if(isset($this->data[$this->alias]['seo_title']) && $this->data[$this->alias]['seo_title'] == ""){
        	$this->data[$this->alias]['seo_title'] = $this->data[$this->alias]['nome'];
        }

		parent::beforeSave($options = array());
    }

	public function afterSave($created, $options = array()) {
		parent::afterSave($created, $options = array());

		//begin salva o registro da URL nas rotas
		$controller 	= 'PaginaTipos';
		$model 			= 'PaginaTipo';
		$action			= 'index';
		$params_id		= null;
		$params_value	= null;

		//salvo a rota da categoria
		if(isset($this->data[$this->alias]['seo_url'])){
			App::import('Model', 'Rota');
			$this->Rota = new Rota();

			$rt = $this->Rota->find('first', array('conditions' => array(
																		'AND' => array(
																				'row_id' => $this->data[$this->alias]['id'],
																				'model'  => $model
																			)
																	)
													)
									);

			if($rt){
				$rota['id'] 		= $rt['Rota']['id'];
			}else{
				$rota['id'] 		= null;
			}

			$rota['controller'] 	= $controller;
			$rota['model'] 			= $model;
			$rota['action'] 		= $action;
			$rota['params_id'] 		= $params_id;
			$rota['params_value'] 	= $params_value;
			$rota['row_id'] 		= $this->data[$this->alias]['id'];
			$rota['seo_url'] 		= $this->data[$this->alias]['seo_url'];
			$rota['sites'] 			= '["portal"]';
			$this->Rota->save($rota);
		}
		//end salva o registro da URL nas rotas
	}

	public function afterDelete() {
		parent::afterDelete();
		
		App::import('Model', 'Rota');
		$this->Rota = new Rota();

		$rota = $this->Rota->find('first', array('conditions' => array('Rota.model' => 'PaginaTipo', 'Rota.row_id' => $this->id)));

		if(!empty($rota)){
			$this->Rota->delete($rota['Rota']['id'], false);
		}

		//removo vinculo na mão, para evitar recursividade
	    $this->Rota->query('UPDATE paginas SET pagina_tipo_id = null WHERE pagina_tipo_id = '. $this->id);
	}

}
