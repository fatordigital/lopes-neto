<?php

class FdPaginaTiposController extends FdPaginasAppController {

	public $uses = array('FdPaginas.PaginaTipo');

/**
 * admin_index method
 *
 * @return void
 */
	public function fatorcms_index($page = 1) {

		// Add filter
		$this->FilterResults->addFilters(
			array(
				'filter' => array(
					'OR' => array(
						'PaginaTipo.nome'    => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
					)
				),
				'filtro_nome' => array(
					'PaginaTipo.nome'     	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				),
			)
		);
		// Define conditions
		$this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

		//exportar? 
		if(isset($this->params->params['named']['acao']) && $this->params->params['named']['acao'] == "exportar"){
			$this->Reports->xls($this->PaginaTipo->find('all', array('conditions' => $this->FilterResults->getConditions(), 'callbacks' => false)), 'Notícias');
		}

		// Paginate
		$options['conditions'] 	= $this->FilterResults->getConditions();
		$options['order'] 		= array('PaginaTipo.id' => 'ASC');
		$this->paginate = $options;

		$this->PaginaTipo->recursive = 0;
		$this->set('pagina_tipos', $this->paginate());
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function fatorcms_add() {
		if ($this->request->is('post')) {
			$this->PaginaTipo->create();
			if ($this->PaginaTipo->save($this->request->data)) {
				//set caches
				$this->_resetCaches();
				$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_edit($id = null) {		
		if (!$this->PaginaTipo->exists($id)) {
			throw new NotFoundException(__('Registro Inválido.'));
		}

		// set pagina_tipo
		$options = array('conditions' => array('PaginaTipo.' . $this->PaginaTipo->primaryKey => $id));
		$pagina_tipo = $this->PaginaTipo->find('first', $options);

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->PaginaTipo->save($this->request->data)) {
				//set caches
				$this->_resetCaches();
				$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');

				//$this->redirect(array('action' => 'index'));
				$this->_redirectFilter($this->Session->read('referer'));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}
		} else {
			$options = array('conditions' => array('PaginaTipo.' . $this->PaginaTipo->primaryKey => $id));
			$this->request->data = $this->PaginaTipo->find('first', $options);

			$this->Session->write('referer', $this->referer());
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_delete($id = null) {
		$this->PaginaTipo->id = $id;
		if (!$this->PaginaTipo->exists()) {
            throw new NotFoundException('Registro inválido.');
		}
		$this->request->is('get');
		if ($this->PaginaTipo->delete()) {
			$this->_resetCaches();
			$this->Session->setFlash(__('Registro deletado.'),'FdDashboard.alerts/fatorcms_success');
			// $this->redirect(array('action' => 'index'));
			$this->_redirectFilter($this->referer());
		}
		$this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
		// $this->redirect(array('action' => 'index'));
		$this->_redirectFilter($this->referer());
	}

/**
 * fatorcms_status method
 *
 * @return void
 */
    public function fatorcms_status(){
        if (!$this->request->is('post')){
            throw new NotFoundException('Registro inválido.');
        }
        echo $this->_saveStatus('PaginaTipo', $this->request->data['id'], $this->request->data['value']);
        die;
    }
}