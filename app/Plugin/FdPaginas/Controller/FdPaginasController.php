<?php

class FdPaginasController extends FdPaginasAppController {

	public $uses = array('FdPaginas.Pagina');

	public $pagina_tipos;

/**
 * beforeFilter method
 *
 * @return void
 */
	public function beforeFilter(){
		parent::beforeFilter();

		if (!$this->request->is('ajax')) {
			if ($this->_isAdminMode()) {
				//busco as paginas pais, para a listagem dos fields parent_id				
				// $parent_id = $this->Pagina->find('list', array('fields' => array('Pagina.id', 'Pagina.nome'), 'conditions' => array('Pagina.parent_id' => 0, 'Pagina.status' => true)));
				
				$parent_id = array();
				App::import('Helper', 'String');
				$this->String = new StringHelper(new View(null));
				
				$sites = $this->String->getSites();
				
				$paginas = $this->Pagina->find('all', array('fields' => array('Pagina.id', 'Pagina.nome', 'Pagina.sites'), 'conditions' => array('Pagina.parent_id' => 0, 'Pagina.status' => true), 'order' => 'Pagina.sites ASC, Pagina.nome ASC'));
				$parent_id = array();
				if(!empty($paginas)){
					foreach ($paginas as $key => $pag) {
						$parent_id[ $sites[$pag['Pagina']['sites']] ] [$pag['Pagina']['id']] = $pag['Pagina']['nome'];
					}
				}
				
				$this->set(compact('parent_id'));

				//busco os tipos de paginas
				$this->loadModel('FdPaginas.PaginaTipo');
				$this->pagina_tipos = $this->PaginaTipo->find('list', array('fields' => array('PaginaTipo.id', 'PaginaTipo.nome'), 'conditions' => array('PaginaTipo.status' => true)));
				$this->set('pagina_tipo_id', $this->pagina_tipos);

				//busco os cursos
//				$this->loadModel('FdCursos.Curso');
//				$this->curso_id = $this->Curso->find('list', array('fields' => array('Curso.id', 'Curso.nome'), 'conditions' => array('Curso.status' => true), 'order' => array('Curso.nome ASC')));
//				$this->set('curso_id', $this->curso_id);
            }
        }
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function fatorcms_index($page = 1) {

		App::import('Helper', 'String');
		$this->String = new StringHelper(new View(null));

		//load do model afins de pegar o hasMany
		App::import('Model', 'FdPaginas.Pagina');
		$this->Pagina = new Pagina();

		// Add filter
		$this->FilterResults->addFilters(
			array(
				'filter' => array(
					'OR' => array(
						'Pagina.nome'    	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
						'Pagina.conteudo' 	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
					)
				),
				'filtro_nome' => array(
					'Pagina.nome'     	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				),
				'filtro_conteudo' => array(
					'Pagina.conteudo'    => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				),
				'filtro_pagina_tipo_id' => array(
					'Pagina.pagina_tipo_id'    =>  array('select' => $this->FilterResults->select('Tipo de Página...', $this->pagina_tipos))
				),
				'filtro_sites' => array(
					'Pagina.sites'    =>  array(
												'select' => $this->FilterResults->select('Site...', $this->String->getSites() ), 
												'operator' => 'LIKE', 
												'value' => array( 'before' => '%', 'after'  => '%')
											)
				)
			)
		);

		$this->FilterResults->setPaginate('page', $page);
		// Define conditions
		$this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

		$nome_ou_conteudo = false;
		if(count($this->FilterResults->getConditions())){
			foreach($this->FilterResults->getConditions() as $itens){
				if(count($itens)){
					foreach ($itens as $key => $item) {
						if($key == 'Pagina.nome LIKE' || $key == 'Pagina.conteudo LIKE'){
							$nome_ou_conteudo = true;
						}
					}
				}
			}
		}
		
		if($nome_ou_conteudo == false){
			$condi = array('Pagina.parent_id' => 0, $this->FilterResults->getConditions());
		}else{
			$condi = array($this->FilterResults->getConditions());
		}

		// Paginate
		$options['conditions'] 	= $condi;
		$options['group'] 	= array('Pagina.id');
		$options['recursive'] 	= 1;
		$this->paginate = $options;
		$paginas = $this->paginate();

		//begin: validação caso retorne aba e pagina ao mesmo tempo (comum com paginas e abas com mesmo nome)
			$paginas_pai = array();
			$paginas_filha = array();
			foreach ($paginas as $key => $pagina) {
				if($pagina['Pagina']['parent_id'] > 0){
					$paginas_filha[$key] = $pagina['Pagina']['parent_id'];
				}else{
					$paginas_pai[$key] = $pagina['Pagina']['id'];
				}
			}
			foreach ($paginas_filha as $key => $pagina) {
				if(in_array($pagina, $paginas_pai)){
					unset($paginas[$key]);
				}
			}
		//end: validação caso retorne aba e pagina ao mesmo tempo (comum com paginas e abas com mesmo nome)

		$this->set('paginas', $paginas);

		// Get user
        $user = $this->Auth->user();
        $this->set(compact('user'));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function fatorcms_add() {
		if ($this->request->is('post')) {
			//não é aba? então é pagina pai.
			if($this->params->params['abas'] == false && !isset($this->request->data['Pagina']['parent_id'])){
				$this->request->data['Pagina']['parent_id'] = 0;
			}

			//é preview ou rascunho?
			if(isset($this->request->data['preview']) || isset($this->request->data['rascunho'])){
				$this->request->data['Pagina']['status'] = 2;
				$this->request->data['Pagina']['conteudo_rascunho'] = $this->request->data['Pagina']['conteudo'];
			}

			//validacao burra (mas necessária)
			$erro_bloco = false;
			if(isset($this->request->data['Curso']['bloco_noticia_nome']) && $this->request->data['Curso']['bloco_noticia_nome'] != ""){
				if(isset($this->request->data['Curso']['bloco_noticia_slug']) && $this->request->data['Curso']['bloco_noticia_slug'] == ""){
					$this->request->data['Curso']['bloco_noticia_slug'] = strtolower(Inflector::slug($this->request->data['Curso']['bloco_noticia_nome'], '-'));
				}
				$bloco = $this->Bloco->find('first', array('conditions' => array('Bloco.slug' => $this->request->data['Curso']['bloco_noticia_slug'])));
				if(!empty($bloco)){
					$this->Curso->invalidate('bloco_noticia_slug', 'Esta chave já consta em nosso banco de dados');
					$erro_bloco = true;
				}
			}

			if($erro_bloco == false){
				$this->Pagina->create();
				if ($this->Pagina->saveAll($this->request->data)) {

					//verifica se está cadastrando um novo bloco
					if($this->request->data['Pagina']['bloco_noticia_status'] == 2){
						if(isset($this->request->data['Pagina']['bloco_noticia_nome']) && $this->request->data['Pagina']['bloco_noticia_nome'] != ""){
							App::import('Model', 'FdBlocos.Bloco');
							$this->Bloco = new Bloco();
							$bloco_tipo = $this->Bloco->BlocoTipo->find('first', array('conditions' => array('BlocoTipo.slug' => 'noticias')));

							$bloco = array();
							$bloco['Bloco']['id'] 				= null;
							$bloco['Bloco']['nome'] 			= $this->request->data['Pagina']['bloco_noticia_nome'];
							$bloco['Bloco']['slug'] 			= $this->request->data['Pagina']['bloco_noticia_slug'];
							$bloco['Bloco']['bloco_tipo_id'] 	= (isset($bloco_tipo['BlocoTipo']['id'])) ? $bloco_tipo['BlocoTipo']['id'] : 1;
							$bloco['Bloco']['status'] 			= true;

							if($this->Bloco->save($bloco)){
								$this->Pagina->save(array('bloco_noticia_id' => $this->Bloco->id, 'bloco_noticia_status' => 1), false);
							}
						}
					}

					$this->_resetCaches();
					$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');

					//mando pra index só se clicar em salvar
					if(isset($this->request->data['salvar'])){
						$this->redirect(array('action' => 'index'));
					}else{
						$this->redirect(array('action' => 'edit', $this->Pagina->id));
					}

				} else {
					$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
				}
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}

		}

		// Lista os blocos do tipo noticia
		App::import('Model', 'FdBlocos.Bloco');
        $this->Bloco = new Bloco();;
        $blocos = $this->Bloco->find('list', array('contain' => array('BlocoTipo'), 'fields' => array('Bloco.id', 'Bloco.nome'), 'conditions' => array('Bloco.status' => true, 'BlocoTipo.slug' => 'noticias')));
        $this->set(compact('blocos'));

        // Lista as galerias do tipo interna
		App::import('Model', 'FdSac.SacTipo');
        $this->SacTipo = new SacTipo();;
        $sac_tipo = $this->SacTipo->find('first', array('conditions' => array('SacTipo.slug' => 'fale_conosco_interna')));
		$this->set(compact('sac_tipo'));

		// Lista de Cursos 
//		$this->loadModel('FdCursos.Curso');
//		$cursos = $this->Curso->find('list', array('fields' => array('Curso.id', 'Curso.nome'), 'conditions' => array('Curso.status' => true), 'order' => array('Curso.nome ASC')));
//		$this->set(compact('cursos'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_edit($id = null) {		
		if (!$this->Pagina->exists($id)) {
            throw new NotFoundException('Registro inválido.');
		}

		// set pagina
		$options = array('conditions' => array('Pagina.' . $this->Pagina->primaryKey => $id));
		$pagina = $this->Pagina->find('first', $options);
		$this->set(compact('pagina'));
		
		// redirect caso o cliente caia no edit da aba, com a url de paginas 
		if($pagina['Pagina']['parent_id'] > 0 && $this->params->params['abas'] == false){
			$this->redirect(array('action' => 'edit', 'abas' => true, $id));
		}

		if ($this->request->is('post') || $this->request->is('put')) {

			//é preview ou rascunho?
			if(isset($this->request->data['preview']) || isset($this->request->data['rascunho'])){
				$this->request->data['Pagina']['status'] = 2;
				if(isset($this->request->data['Pagina']['conteudo'])){
					$this->request->data['Pagina']['conteudo_rascunho'] = $this->request->data['Pagina']['conteudo'];
				}
			}

			// se for descartar o rascunho...
			if(isset($this->request->data['descartar_rascunho'])){
				if($this->request->data['Pagina']['status'] == 2){
					$this->request->data['Pagina']['status'] = 1;
				}
				$this->request->data['Pagina']['conteudo_rascunho'] = null;
			}

			// se for publicar rascunho...
			if(isset($this->request->data['salvar_publicar'])){
				if($this->request->data['Pagina']['status'] == 2){
					$this->request->data['Pagina']['status'] = 1;
				}
				$this->request->data['Pagina']['conteudo'] = $this->request->data['Pagina']['conteudo_rascunho'];
				$this->request->data['Pagina']['conteudo_rascunho'] = null;
			}

			// não eh pagina de cursos? zero o campo 'curso_id'
			if(isset($this->request->data['pagina_tipo_id']) && $this->request->data['pagina_tipo_id'] != 4){
				$this->request->data['curso_id'] = null;		
			}

			//Begin: bloco: validacao burra (mas necessária)
			$erro_bloco = false;
			if(isset($this->request->data['Curso']['bloco_noticia_nome']) && $this->request->data['Curso']['bloco_noticia_nome'] != ""){
				App::import('Model', 'FdBlocos.Bloco');
				$this->Bloco = new Bloco();
				if(isset($this->request->data['Curso']['bloco_noticia_slug']) && $this->request->data['Curso']['bloco_noticia_slug'] == ""){
					$this->request->data['Curso']['bloco_noticia_slug'] = strtolower(Inflector::slug($this->request->data['Curso']['bloco_noticia_nome'], '-'));
				}
				$bloco = $this->Bloco->find('first', array('conditions' => array('Bloco.slug' => $this->request->data['Curso']['bloco_noticia_slug'])));
				if(!empty($bloco)){
					$this->Curso->invalidate('bloco_noticia_slug', 'Esta chave já consta em nosso banco de dados');
					$erro_bloco = true;
				}
			}
			//End: bloco: validacao burra (mas necessária)

			if($erro_bloco == false){
				if ($this->Pagina->saveAll($this->request->data)) {

					//verifica se está cadastrando um novo bloco
					if(isset($this->request->data['Pagina']['bloco_noticia_status']) && $this->request->data['Pagina']['bloco_noticia_status'] == 2){
						if(isset($this->request->data['Pagina']['bloco_noticia_nome']) && $this->request->data['Pagina']['bloco_noticia_nome'] != ""){
							App::import('Model', 'FdBlocos.Bloco');
							$this->Bloco = new Bloco();
							$bloco_tipo = $this->Bloco->BlocoTipo->find('first', array('conditions' => array('BlocoTipo.slug' => 'noticias')));

							$bloco = array();
							$bloco['Bloco']['id'] 				= null;
							$bloco['Bloco']['nome'] 			= $this->request->data['Pagina']['bloco_noticia_nome'];
							$bloco['Bloco']['slug'] 			= $this->request->data['Pagina']['bloco_noticia_slug'];
							$bloco['Bloco']['bloco_tipo_id'] 	= (isset($bloco_tipo['BlocoTipo']['id'])) ? $bloco_tipo['BlocoTipo']['id'] : 1;
							$bloco['Bloco']['status'] 			= true;

							if($this->Bloco->save($bloco)){
								$this->Pagina->save(array('bloco_noticia_id' => $this->Bloco->id, 'bloco_noticia_status' => 1), false);
							}
						}
					}

					//begin save SVN
					if(isset($this->request->data['Pagina']['conteudo'])){
						$condicao = $pagina['Pagina']['conteudo'] != $this->request->data['Pagina']['conteudo'];
					}elseif(isset($this->request->data['Pagina']['conteudo_rascunho'])){
						$condicao = $pagina['Pagina']['conteudo_rascunho'] != $this->request->data['Pagina']['conteudo_rascunho'];
					}
					
					if(isset($condicao) && $condicao == true){
						App::import('Model', 'FdSvn.Svn');
						$this->Svn = new Svn();

						$svn = array();
						$svn['Svn']['id'] 					= null;
						$svn['Svn']['usuario_id'] 			= $this->Auth->User('id');
						$svn['Svn']['registro_id'] 			= $id;
						$svn['Svn']['nome'] 				= $pagina['Pagina']['nome'];
						$svn['Svn']['conteudo'] 			= $pagina['Pagina']['conteudo'];
						$svn['Svn']['conteudo_rascunho'] 	= $pagina['Pagina']['conteudo_rascunho'];
						$svn['Svn']['model']				= 'Pagina';

						$this->Svn->save($svn);
					}
					//end save SVN

					$this->_resetCaches();
					$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');

					//$this->redirect(array('action' => 'index'));
					$this->_redirectFilter($this->Session->read('referer'));
				} else {
					$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
				}
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}
		} else {
			$options = array('conditions' => array('Pagina.' . $this->Pagina->primaryKey => $id));
			$this->request->data = $this->Pagina->find('first', $options);

			$this->Session->write('referer', $this->referer());
		}

		// Lista os blocos do tipo noticia
		App::import('Model', 'FdBlocos.Bloco');
        $this->Bloco = new Bloco();;
        $blocos = $this->Bloco->find('list', array('contain' => array('BlocoTipo'), 'fields' => array('Bloco.id', 'Bloco.nome'), 'conditions' => array('Bloco.status' => true, 'BlocoTipo.slug' => 'noticias')));
        $this->set(compact('blocos'));

        // Lista as galerias do tipo interna
		App::import('Model', 'FdSac.SacTipo');
        $this->SacTipo = new SacTipo();;
        $sac_tipo = $this->SacTipo->find('first', array('conditions' => array('SacTipo.slug' => 'fale_conosco_interna')));
		$this->set(compact('sac_tipo'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_delete($id = null) {
		$this->Pagina->id = $id;
		if (!$this->Pagina->exists()) {
            throw new NotFoundException('Registro inválido.');
		}
		$this->request->is('get');
		if ($this->Pagina->delete()) {
			$this->_resetCaches();
			$this->Session->setFlash(__('Registro deletado.'),'FdDashboard.alerts/fatorcms_success');
			// $this->redirect(array('action' => 'index'));
			$this->_redirectFilter($this->referer());
		}
		$this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
		// $this->redirect(array('action' => 'index'));
		$this->_redirectFilter($this->referer());
	}

/**
 * fatorcms_status method
 *
 * @return void
 */
    public function fatorcms_status(){
        if (!$this->request->is('post')){
            throw new NotFoundException('Registro inválido.');
        }
        echo $this->_saveStatus('Pagina', $this->request->data['id'], $this->request->data['value']);
        die;
    }
}