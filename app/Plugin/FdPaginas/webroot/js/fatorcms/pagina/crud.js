$(document).ready(function(){
	$("[name='data[Pagina][nome]']").stringToSlug({
		getPut: '.slug'
	});

	gerencia_campos_dinamico($("[name='data[Pagina][dinamico]']:checked").val());
	$("[name='data[Pagina][dinamico]']").on('ifChecked', function(event){
	 	gerencia_campos_dinamico($(this).val());
	});

	gerencia_campos_formulario($("[name='data[Pagina][formulario]']:checked").val());
	$("[name='data[Pagina][formulario]']").on('ifChecked', function(event){
	 	gerencia_campos_formulario($(this).val());
	});

	gerencia_campos_botao($("[name='data[Pagina][botao_aba_status]']:checked").val());
	$("[name='data[Pagina][botao_aba_status]']").on('ifChecked', function(event){
	 	gerencia_campos_botao($(this).val());
	});

    gerencia_campos_botao_2($("[name='data[Pagina][botao_aba_2_status]']:checked").val());
    $("[name='data[Pagina][botao_aba_2_status]']").on('ifChecked', function(event){
        gerencia_campos_botao_2($(this).val());
    });

    gerencia_campos_bloco($("[name='data[Pagina][bloco_noticia_status]']:checked").val());
    $("[name='data[Pagina][bloco_noticia_status]']").on('ifChecked', function(event){
        gerencia_campos_bloco($(this).val());
    });

    gerencia_campos_curso($("[name='data[Pagina][pagina_tipo_id]']").val());
    $("[name='data[Pagina][pagina_tipo_id]']").on('change', function(event){
        gerencia_campos_curso($(this).val());
    });

    $("[name='data[Pagina][bloco_noticia_nome]']").stringToSlug({
        getPut: '.bloco_slug'
    });
});

function gerencia_campos_curso(value){
    $("[name='data[Pagina][curso_id]']").attr("disabled", "disabled").closest('.row').hide();
    if (value == '4') {
        $("[name='data[Pagina][curso_id]']").removeAttr("disabled").closest('.row').show();
    } else {
        $("[name='data[Pagina][curso_id]']").attr("disabled", "disabled").closest('.row').hide();
    }
}

function gerencia_campos_dinamico(value){
	if (value == '1') {
        $("[name='data[Pagina][dinamico_elemento]']").removeAttr("disabled").closest('.row').show();
    } else if(value == '0'){
        $("[name='data[Pagina][dinamico_elemento]']").attr("disabled", "disabled").closest('.row').hide();
    }
}

function gerencia_campos_formulario(value){
	if (value == '1') {
        $("[name='data[Pagina][formulario_email]']").removeAttr("disabled").closest('.row').show();
        $("[name='data[Pagina][formulario_titulo]']").removeAttr("disabled").closest('.row').show();
    } else if(value == '0'){
        $("[name='data[Pagina][formulario_email]']").attr("disabled", "disabled").closest('.row').hide();
        $("[name='data[Pagina][formulario_titulo]']").attr("disabled", "disabled").closest('.row').hide();
    }
}

function gerencia_campos_botao(value){
	if (value == '1') {
        $("[name='data[Pagina][botao_aba_url]']").removeAttr("disabled").closest('.row').show();
        $("[name='data[Pagina][botao_aba_label]']").removeAttr("disabled").closest('.row').show();
        // $("[name='data[Pagina][botao_aba_target]']").removeAttr("disabled").closest('.row').show();
        $("[name='data[Pagina][botao_aba_target]']").closest('.row').show();
    } else if(value == '0'){
    	$("[name='data[Pagina][botao_aba_url]']").attr("disabled", "disabled").closest('.row').hide();
        $("[name='data[Pagina][botao_aba_label]']").attr("disabled", "disabled").closest('.row').hide();
        // $("[name='data[Pagina][botao_aba_target]']").attr("disabled", "disabled").closest('.row').hide();
        $("[name='data[Pagina][botao_aba_target]']").closest('.row').hide();
    }
}

function gerencia_campos_botao_2(value){
    if (value == '1') {
        $("[name='data[Pagina][botao_aba_2_url]']").removeAttr("disabled").closest('.row').show();
        $("[name='data[Pagina][botao_aba_2_label]']").removeAttr("disabled").closest('.row').show();
        // $("[name='data[Pagina][botao_aba_2_target]']").removeAttr("disabled").closest('.row').show();
        $("[name='data[Pagina][botao_aba_2_target]']").closest('.row').show();
    } else if(value == '0'){
        $("[name='data[Pagina][botao_aba_2_url]']").attr("disabled", "disabled").closest('.row').hide();
        $("[name='data[Pagina][botao_aba_2_label]']").attr("disabled", "disabled").closest('.row').hide();
        // $("[name='data[Pagina][botao_aba_2_target]']").attr("disabled", "disabled").closest('.row').hide();
        $("[name='data[Pagina][botao_aba_2_target]']").closest('.row').hide();
    }
}

function gerencia_campos_bloco(value){
    $("[name='data[Pagina][bloco_noticia_id]']").attr("disabled", "disabled").closest('.row').hide();
    $("[name='data[Pagina][bloco_noticia_nome]']").attr("disabled", "disabled").closest('.row').hide();
    $("[name='data[Pagina][bloco_noticia_slug]']").attr("disabled", "disabled").closest('.row').hide();

    if (value == '1' ) {
        $("[name='data[Pagina][bloco_noticia_id]']").removeAttr("disabled").closest('.row').show();
    } else if(value == '2'){
        $("[name='data[Pagina][bloco_noticia_nome]']").removeAttr("disabled").closest('.row').show();
        $("[name='data[Pagina][bloco_noticia_slug]']").removeAttr("disabled").closest('.row').show();
    }else{
        $("[name='data[Pagina][bloco_noticia_id]']").attr("disabled", "disabled").closest('.row').hide();
        $("[name='data[Pagina][bloco_noticia_nome]']").attr("disabled", "disabled").closest('.row').hide();
        $("[name='data[Pagina][bloco_noticia_slug]']").attr("disabled", "disabled").closest('.row').hide();      
    }
}