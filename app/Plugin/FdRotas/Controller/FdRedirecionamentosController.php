<?php

class FdRedirecionamentosController extends FdRotasAppController {

	public $uses = array('FdRotas.Redirecionamento');

/**
 * admin_index method
 *
 * @return void
 */
	public function fatorcms_index($page = 1) {

		// Add filter
		$this->FilterResults->addFilters(
			array(
				'filter' => array(
					'OR' => array(
						'Redirecionamento.url_antiga'    => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
						'Redirecionamento.url_nova' 	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
					)
				),
				'filtro_url_antiga' => array(
					'Redirecionamento.url_antiga'     	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				),
				'filtro_url_nova' => array(
					'Redirecionamento.url_nova'    => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				)
			)
		);

		// Define page
		$this->FilterResults->setPaginate('page', $page);

		// Define conditions
		$this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

		// Exportar?
        if(isset($this->params->params['named']['acao']) && $this->params->params['named']['acao'] == "exportar"){
            $this->Reports->xls($this->Redirecionamento->find('all', array('conditions' => $this->FilterResults->getConditions(), 'recursive' => -1, 'callbacks' => false)), 'Redirecionamento');
        }

		// Paginate
		$this->Redirecionamento->recursive = 0;
		$redirecionamentos = $this->paginate();

		$this->set('redirecionamentos', $redirecionamentos);
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function fatorcms_add() {
		// eh post?
		if ($this->request->is('post')) {
			$this->Redirecionamento->create();
			if ($this->Redirecionamento->save($this->request->data)) {
				$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_edit($id = null) {
		if (!$this->Redirecionamento->exists($id)) {
            throw new NotFoundException('Registro inválido.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Redirecionamento->save($this->request->data)) {
				$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');

				//$this->redirect(array('action' => 'index'));
				$this->_redirectFilter($this->Session->read('referer'));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}
		} else {
			$options = array('conditions' => array('Redirecionamento.' . $this->Redirecionamento->primaryKey => $id));
			$this->request->data = $this->Redirecionamento->find('first', $options);

			$this->Session->write('referer', $this->referer());
		}

		// set redirecionamento
		$options = array('conditions' => array('Redirecionamento.' . $this->Redirecionamento->primaryKey => $id));
		$redirecionamento = $this->Redirecionamento->find('first', $options);
		$this->set(compact('redirecionamento'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_delete($id = null) {
		$this->Redirecionamento->id = $id;
		if (!$this->Redirecionamento->exists()) {
            throw new NotFoundException('Registro inválido.');
		}
		$this->request->is('get');

		if ($this->Redirecionamento->delete()) {
			$this->Session->setFlash(__('Registro deletado com sucesso.'),'FdDashboard.alerts/fatorcms_success');
			// $this->redirect(array('action' => 'index'));
			$this->_redirectFilter($this->referer());
		}
		$this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
		// $this->redirect(array('action' => 'index'));
		$this->_redirectFilter($this->referer());
	}
}