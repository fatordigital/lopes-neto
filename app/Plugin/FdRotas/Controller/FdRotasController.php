<?php

class FdRotasController extends FdRotasAppController {

	public $uses = array('FdRotas.Rota');

/**
 * admin_index method
 *
 * @return void
 */
	public function fatorcms_index($page = 1) {

		// Add filter
		$this->FilterResults->addFilters(
			array(
				'filter' => array(
					'OR' => array(
						'Rota.seo_url'    => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
						'Rota.controller' 	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
					)
				),
				'filtro_seo_url' => array(
					'Rota.seo_url'     	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				),
				'filtro_controller' => array(
					'Rota.controller'    => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				)
			)
		);

		$this->FilterResults->setPaginate('page', $page);
		// Define conditions
		$this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

		// Paginate
		$this->Rota->recursive = 0;
		$rotas = $this->paginate();

		if(count($rotas) > 0){
			foreach($rotas as $l => $rota){
				$this->loadModel($rota['Rota']['model']);
				$row = $this->{$rota['Rota']['model']}->find('first', array('recursive' => -1, 'conditions' => array('id' => $rota['Rota']['row_id'])));
				if($row){
					$rotas[$l][$rota['Rota']['model']] = $row[$rota['Rota']['model']];
				}
			}
		}

		$this->set('rotas', $rotas);
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function fatorcms_add() {
		if ($this->request->is('post')) {
			$this->Rota->create();
			if ($this->Rota->saveAll($this->request->data)) {
				$this->_resetCaches();
				$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_edit($id = null) {
		if (!$this->Rota->exists($id)) {
            throw new NotFoundException('Registro inválido.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Rota->saveAll($this->request->data)) {
				if(isset($this->request->data[$this->request->data['Rota']['model']])){
					$this->loadModel($this->request->data['Rota']['model']);
					$this->request->data[$this->request->data['Rota']['model']]['seo_url'] = $this->request->data['Rota']['seo_url'];
					$this->{$this->request->data['Rota']['model']}->save($this->request->data[$this->request->data['Rota']['model']]);
				}

				$this->_resetCaches();
				$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');

				//$this->redirect(array('action' => 'index'));
				$this->_redirectFilter($this->Session->read('referer'));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}
		} else {
			$options = array('conditions' => array('Rota.' . $this->Rota->primaryKey => $id));
			$this->request->data = $this->Rota->find('first', $options);

			$this->Session->write('referer', $this->referer());
		}

		$this->loadModel($this->request->data['Rota']['model']);
		$row = $this->{$this->request->data['Rota']['model']}->find('first', array('recursive' => -1, 'conditions' => array('id' => (int) $this->request->data['Rota']['row_id'])));
		$this->request->data[$this->request->data['Rota']['model']] = $row[$this->request->data['Rota']['model']];

		// set rota
		$options = array('conditions' => array('Rota.' . $this->Rota->primaryKey => $id));
		$rota = $this->Rota->find('first', $options);
		$this->set(compact('rota'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_delete($id = null) {
		$this->Rota->id = $id;
		if (!$this->Rota->exists()) {
            throw new NotFoundException('Registro inválido.');
		}
		$this->request->is('get');
		if ($this->Rota->delete()) {
			$this->_resetCaches();
			$this->Session->setFlash(__('Registro deletado.'),'FdDashboard.alerts/fatorcms_success');
			// $this->redirect(array('action' => 'index'));
			$this->_redirectFilter($this->referer());
		}
		$this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
		// $this->redirect(array('action' => 'index'));
		$this->_redirectFilter($this->referer());
	}
}