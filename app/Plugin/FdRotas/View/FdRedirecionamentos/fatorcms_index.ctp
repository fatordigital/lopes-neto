<?php $this->Html->addCrumb('Redirecionamentos'); ?>

<div class="row">
    <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
        Redirecionamentos 
        <?php echo $this->Html->link('Cadastrar Redirecionamento', array('action' => 'add'), array('class' => 'btn btn-info pull-right')); ?>
    </h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <div class="row-fluid">
                    <div class="col-sm-12 col-md-10 col-lg-11">
                        <?php echo $this->FilterForm->create(null, array('role' => 'form', 'class' => 'form-inline')) ?>
                            <div class="form-group">
                                <?php echo $this->FilterForm->input('filtro_url_antiga', array('placeholder' => 'Filtrar pela url antiga', 'class' => 'form-control')) ?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->FilterForm->input('filtro_url_nova', array('placeholder' => 'Filtrar pela url nova', 'class' => 'form-control')) ?>
                            </div>
                            <?php echo $this->FilterForm->submit('Filtrar', array('class' => 'btn btn-success', 'div' => false)) ?>
                            <?php echo $this->Html->link('Limpar Filtro', array('action' => 'index'), array('class' => 'btn btn-warning')) ?>
                            <?php echo $this->Html->link('<i class="fa fa-share-square-o"></i>&nbsp;Exportar', $this->params->params['named']+array('acao' => 'exportar') , array('class' => 'btn btn-info btn-small btn-export', 'escape' => false)); ?>
                        <?php echo $this->FilterForm->end() ?>
                    </div>
                    <div class="col-lg-1 visible-md visible-lg">
                       <?php echo $this->Element('FdDashboard.limit'); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-hover general-table">
                    <thead>
                        <tr>
                            <th style="width:60px"><?php echo $this->Paginator->sort('id', '#') ?></th>
                            <th><?php echo $this->Paginator->sort('url_antiga') ?></th>
                            <th><?php echo $this->Paginator->sort('url_nova') ?></th>
                            <th><?php echo $this->Paginator->sort('tipo') ?></th>
                            <th style="width:180px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($redirecionamentos) > 0): ?>
                            <?php foreach($redirecionamentos AS $redirecionamento): ?>
                            <tr>
                                <td><?php echo $redirecionamento['Redirecionamento']['id'] ?></td>
                                <td><?php echo $redirecionamento['Redirecionamento']['url_antiga'] ?></td>
                                <td><?php echo $redirecionamento['Redirecionamento']['url_nova'] ?></td>
                                <td><?php echo $redirecionamento['Redirecionamento']['tipo'] ?></td>
                                <td>
                                    <?php echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('action' => 'edit', $redirecionamento['Redirecionamento']['id']), array('class' => 'btn btn-primary btn-sm', 'escape' => false)) ?> 
                                    <?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('action' => 'delete', $redirecionamento['Redirecionamento']['id']), array('class' => 'btn btn-danger btn-sm', 'escape' => false), __('Deseja mesmo remover o registro # %s?', $redirecionamento['Redirecionamento']['url_antiga'])) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="5" class="text-center">Nenhum registro encontrado.</td>
                            </tr>
                        <?php endIf; ?>
                    </tbody>
                </table>

                <?php echo $this->Element('FdDashboard.paginator'); ?>
            </div>
        </section>
    </div>
</div>
