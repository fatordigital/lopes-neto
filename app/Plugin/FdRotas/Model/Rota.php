<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');

class Rota extends FdRotasAppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'seo_url' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
		'controller' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
		'model' => array(
            'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
        ),
		'action' => array(
            'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
        ),
		'row_id' => array(
            'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
        ),
	);

/**
* getRotas
*
* Método que retornas as rotas cadastradas no banco
* @return array com rotas
*/
	public function getRotas($site = 'portal'){
		if (Cache::read('rotas') === false) {
			// $rotas = $this->find('all', array('fields' => array('seo_url', 'controller', 'model', 'action', 'row_id', 'params_id', 'params_value')));
			$rotas = $this->find('all', array('fields' => array('seo_url', 'controller', 'action', 'row_id', 'params_id', 'params_value', 'model'), 'conditions' => array('status' => true, 'sites LIKE' => "%" . $site . "%")));
			$retorno  = array();
			if($rotas){
				foreach($rotas as $rota){
					if(substr($rota['Rota']['seo_url'],0,1) == '/'){
						$rota['Rota']['seo_url'] = substr($rota['Rota']['seo_url'], 1, strlen($rota['Rota']['seo_url']));
					}
					$retorno[$rota['Rota']['seo_url']] = $rota['Rota'];
				}
			}
			Cache::write('rotas', $retorno);
		}else{
			$retorno = Cache::read('rotas');
		}

		return $retorno;
	}

}