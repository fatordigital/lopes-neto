<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');

class Redirecionamento extends FdRotasAppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'url_antiga' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
		'url_nova' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
	);
}