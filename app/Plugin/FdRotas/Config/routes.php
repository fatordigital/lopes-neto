<?php
	Router::connect("/fatorcms/rotas", array('plugin' => 'fd_rotas', 'controller' => 'fd_rotas', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
    Router::connect("/fatorcms/rotas/:action/*", array('plugin' => 'fd_rotas', 'controller' => 'fd_rotas', 'prefix' => 'fatorcms', 'fatorcms' => true));

	Router::connect("/fatorcms/redirecionamentos", array('plugin' => 'fd_rotas', 'controller' => 'fd_redirecionamentos', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
    Router::connect("/fatorcms/redirecionamentos/:action/*", array('plugin' => 'fd_rotas', 'controller' => 'fd_redirecionamentos', 'prefix' => 'fatorcms', 'fatorcms' => true));