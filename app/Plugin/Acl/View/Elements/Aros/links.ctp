<div id="aros_link" class="acl_links">
<?php
$selected = isset($selected) ? $selected : $this->params['action'];

$links = array();
$links[] = $this->Html->link(__d('acl', 'Atualizar lista de métodos'), '/fatorcms/acl/aros/check', array('class' => ($selected == 'fatorcms_check' )? 'selected' : null));
//$links[] = $this->Html->link(__d('acl', 'Users roles'), '/fatorcms/acl/aros/users', array('class' => ($selected == 'fatorcms_users' )? 'selected' : null));

if(Configure :: read('acl.gui.roles_permissions.ajax') === true)
{
    $links[] = $this->Html->link(__d('acl', 'Gerenciar Permissões'), '/fatorcms/acl/aros/ajax_role_permissions', array('class' => ($selected == 'fatorcms_role_permissions' || $selected == 'fatorcms_ajax_role_permissions' )? 'selected' : null));
}
else
{
    $links[] = $this->Html->link(__d('acl', 'Gerenciar Permissões'), '/fatorcms/acl/aros/role_permissions', array('class' => ($selected == 'fatorcms_role_permissions' || $selected == 'fatorcms_ajax_role_permissions' )? 'selected' : null));
}
//$links[] = $this->Html->link(__d('acl', 'Users permissions'), '/fatorcms/acl/aros/user_permissions', array('class' => ($selected == 'fatorcms_user_permissions' )? 'selected' : null));

echo $this->Html->nestedList($links, array('class' => 'acl_links'));
?>
</div>