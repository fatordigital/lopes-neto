<?php echo $this->Html->css('FdDashboard.core/bootstrap-fileupload/bootstrap-fileupload') ?>
<?php echo $this->Html->script('FdDashboard.core/bootstrap-fileupload/bootstrap-fileupload'); ?>

<?php echo $this->Html->script('FdDashboard.core/bootstrap-daterangepicker/moment.min'); ?>

<?php $this->Html->addCrumb('Banners', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Cadastrar Banner') ?>

    <h3>Cadastrar Banner</h3>

<?php echo $this->Form->create('Banner', array('type' => 'file', 'role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
<?php echo $this->Form->input('id') ?>

<?php echo $this->Element('FdBanners.banner_form') ?>

<?php echo $this->Form->end() ?>