<?php echo $this->element('FdDashboard.idiomas') ?>

<?php if (isset($idiomas)) { ?>
    <?php
    $model = 'BannerAtributo';
    ?>
    <div class="panel">
        <div class="panel-body">
            <div class="tab-content">
                <?php foreach ($idiomas as $i => $idioma) { ?>

                    <?php echo $this->Form->hidden("{$model}.{$i}.idioma_id", array('value' => $idioma['Idioma']['id'])) ?>
                    <?php echo $this->Form->hidden("{$model}.{$i}.id") ?>

                    <div class="tab-pane fade in <?php echo $idioma['Idioma']['padrao'] == 1 ? 'active' : '' ?>"
                         id="<?php echo $idioma['Idioma']['slug'] ?>">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">
                                        Título
                                        [<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]
                                    </label>
                                    <?php echo $this->Form->text("BannerAtributo.{$i}.titulo", array('div' => false, 'class' => 'form-control')) ?>
                                    <span class="help-block">Máx: 60 caracteres</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">
                                        Descricao
                                        [<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]
                                    </label>
                                    <?php echo $this->Form->text("BannerAtributo.{$i}.descricao", array('div' => false, 'class' => 'form-control')) ?>
                                    <span class="help-block">Máx: 60 caracteres</span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>

<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="form-group">
                    <label>Status</label>
                    <div class="icheck">
                        <?php echo $this->Form->input('status', array(
                            'type' => 'radio',
                            'options' => array(1 => 'Ativo', 0 => 'Inativo'),
                            'default' => 1,
                            'legend' => false,
                            'before' => '<div class="radio">',
                            'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row row-arquivo">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Arquivo</label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">

                        <?php if (isset($banner) && $banner['Banner']['file'] != "") { ?>
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 119px;">
                                <img
                                    src="<?php echo $this->Html->Url('/' . $banner['Banner']['path'] . '/' . $banner['Banner']['dir'] . '/thumb_' . $banner['Banner']['file'], true) ?>"
                                    width="200"/>
                            </div>
                        <?php } else { ?>
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=nenhuma+imagem"
                                     alt=""/>
                            </div>
                        <?php } ?>

                        <div class="fileupload-preview fileupload-exists thumbnail"
                             style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>

                        <?php if (isset($banner) && $banner['Banner']['file'] != ""): ?>
                            <div class="icheck minimal">
                                <?php echo $this->Form->input('Banner.file.remove', array('type' => 'checkbox', 'label' => 'Remover Imagem Atual', 'div' => false)); ?>
                            </div>
                        <?php endIf; ?>

                        <div>
                           <span class="btn btn-white btn-file">
                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Selecionar imagem</span>
                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Alterar</span>
                               <?php echo $this->Form->input('file', array('type' => 'file', 'class' => 'default', 'label' => false, 'div' => false)); ?>
                               <?php echo $this->Form->hidden('dir') ?>
                           </span>
                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i
                                    class="fa fa-trash"></i> Remover</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label>Mobile</label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">


                        <?php if (isset($banner) && $banner['Banner']['file_mobile'] != ""): ?>
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 119px;">
                                <img
                                    src="<?php echo $this->Html->Url('/' . $banner['Banner']['path_mobile'] . '/' . $banner['Banner']['dir'] . '/thumb_' . $banner['Banner']['file_mobile'], true) ?>"
                                    width="200"/>
                            </div>
                        <?php else: ?>
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=nenhuma+imagem"
                                     alt=""/>
                            </div>
                        <?php endIf; ?>


                        <div class="fileupload-preview fileupload-exists thumbnail"
                             style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>


                        <?php if (isset($banner) && $banner['Banner']['file_mobile'] != ""): ?>
                            <div class="icheck minimal">
                                <?php echo $this->Form->input('Banner.file_mobile.remove', array('type' => 'checkbox', 'label' => 'Remover Imagem Atual', 'div' => false)); ?>
                            </div>
                        <?php endIf; ?>
                        <div>
                           <span class="btn btn-white btn-file">
                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Selecionar imagem</span>
                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Alterar</span>
                               <?php echo $this->Form->input('file_mobile', array('type' => 'file', 'class' => 'default', 'label' => false, 'div' => false)); ?>
                               <?php echo $this->Form->hidden('dir_mobile') ?>
                           </span>
                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i
                                    class="fa fa-trash"></i> Remover</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row row-ordem">
            <div class="col-lg-3">
                <div class="form-group">
                    <?php echo $this->Form->input('ordem', array('div' => false, 'class' => 'form-control')) ?>
                </div>
            </div>
        </div>
        <?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>
    </div>
</div>