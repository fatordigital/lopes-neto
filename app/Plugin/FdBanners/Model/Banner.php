<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');

class Banner extends FdBannersAppModel
{

    public $cacheData;

    public $actsAs = array(
        'Upload.Upload' => array(
            'file' => array(
                'fields' => array(
                    'dir' => 'dir',
                    'type' => 'type',
                    'size' => 'size',
                ),
                'thumbnailMethod' => 'php',
                'thumbnailSizes' => array(
                    'thumb' => '100x100',
                    '960' => '960w',
                    '400' => '400w'
                )
            ),
            'file_mobile' => array(
                'fields' => array(
                    'dir' => 'dir_mobile',
                    'type' => 'type_mobile',
                    'size' => 'size_mobile',
                ),
                'thumbnailMethod' => 'php',
                'thumbnailSizes' => array(
                    'thumb' => '100x100',
                    '400' => '400w'
                )
            )
        ),
        'Idioma'
    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'nome' => array(
            'notempty' => array(
                'rule' => array('notBlank'),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
        ),
        'file' => array(
            array(
                'rule' => 'isFileUpload',
                'message' => 'Campo de preenchimento obrigatório.',
                'on' => 'create'
            ),
            array(
                'rule' => array('isValidExtension', array('jpg', 'png', 'bmp', 'gif'), false),
                'message' => 'Extensão inválida. Utilize JPG, PNG, BMP e GIF.',
                'on' => 'create'
            )
        )
    );

    public $belongsTo = array(
        'BannerTipo' => array(
            'className' => 'BannerTipo',
            'foreignKey' => 'banner_tipo_id'
        ),
    );

    public $hasMany = array(
        'BannerAtributo' => array(
            'className' => 'BannerAtributo',
            'foreignKey' => 'banner_id'
        )
    );

    public function __construct($id = null, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        $this->cache();
    }

    private function cache()
    {
        $banners = array();
        if (Cache::read('banners') === false) {
            $banners = $this->find('all', array(
                    'recursive' => 1,
                    'conditions' => array(
                        'Banner.status' => true
                    ),
                    'order' => 'coalesce(Banner.ordem,10000) DESC'
                )
            );
            Cache::write('banners', $banners);
        } else {
            $banners = Cache::read('banners');
        }
        $this->cacheData = $banners;
    }


    public function extraiBanners($array_banners, $tipo_banner, $banners_ids = null)
    {
        $return = array();
        foreach ($array_banners as $key => $v) {
            if ($banners_ids == null) {
                if ($v['BannerTipo']['codigo'] == $tipo_banner) {
                    $return[] = $v;
                }
            } elseif (is_array($banners_ids) && count($banners_ids) > 0) {
                foreach ($banners_ids as $id) {
                    if ($v['Banner']['id'] == $id) {
                        $return[] = $v;
                    }
                }
            }
        }
        return $return;
    }


    /**
     * beforeSave
     *
     * sobrecarga do metodo executado antes de salvar o registro
     */
    public function beforeSave($options = array())
    {

        App::import("helper", "String");
        $this->String = new StringHelper(new View(null));

        if (isset($this->data[$this->alias]['sites']) && count($this->data[$this->alias]['sites']) > 0) {
            $this->data[$this->alias]['sites'] = json_encode($this->data[$this->alias]['sites']);
        }

        parent::beforeSave($options = array());
    }

    /**
     * afterSave
     *
     * sobrecarga do metodo executado depois de salvar o registro
     */
    public function afterSave($created, $options = array())
    {

        parent::afterSave($created, $options = array());

        //get dados atualizados do banner
        $dados = $this->find('first', array('recursive' => -1, 'conditions' => array('id' => $this->id), 'fields' => array('file', 'dir', 'dir_mobile')));

        $file_name = $this->data['Banner']['file_name_idioma'];

        //begin rename files
        if (isset($this->data[$this->alias]['file']) && $this->data[$this->alias]['file'] != "") {

            $pre_new_name = strtolower(Inflector::slug($file_name, '-'));
            $pre_new_name = str_replace("_", "-", $pre_new_name);
            $path = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'files' . DS . 'banner' . DS . 'file';

            $pre_old_name = $this->data[$this->alias]['file'];

            $new_name = $pre_new_name;
            $old_name = $pre_old_name;

            if (file_exists($path . DS . $dados[$this->alias]['dir'] . DS . $old_name)) {
                $file = $path . DS . $dados[$this->alias]['dir'] . DS . $old_name;
                $infos = pathinfo($file);
                $new_file = $path . DS . $dados[$this->alias]['dir'] . DS . $new_name . '.' . $infos['extension'];
                $new_file_orginal = $path . DS . $dados[$this->alias]['dir'] . DS . 'original-' . $new_name . '.' . $infos['extension'];

                if (rename($file, $new_file)) {

                    //load lib
                    include_once(ROOT . DS . APP_DIR . DS . 'Lib' . DS . 'Wideimage/WideImage.php');

                    $this->query('UPDATE banners SET file = "' . $new_name . '.' . $infos['extension'] . '" WHERE id = ' . $this->id);

                    copy($new_file, $new_file_orginal);

                    $caminho_novo = $path . DS . $dados[$this->alias]['dir'];
                    $outFile = $caminho_novo . DS . $pre_new_name . '.' . $infos['extension'];
                    if (strtolower($infos['extension']) == 'jpg') {
                        WideImage::load($new_file)->resize(1500)->crop('center', 'center', 1500, 370)->saveToFile($outFile, 80);
                    } else {
                        WideImage::load($new_file)->resize(1500)->crop('center', 'center', 1500, 370)->saveToFile($outFile);
                    }

                    //outros renames
                    $file = $path . DS . $dados[$this->alias]['dir'] . DS . '960_' . $old_name;
                    $infos = pathinfo($file);
                    $new_file = $path . DS . $dados[$this->alias]['dir'] . DS . '960_' . $new_name . '.' . $infos['extension'];
                    rename($file, $new_file);

                    $file = $path . DS . $dados[$this->alias]['dir'] . DS . '400_' . $old_name;
                    $infos = pathinfo($file);
                    $new_file = $path . DS . $dados[$this->alias]['dir'] . DS . '400_' . $new_name . '.' . $infos['extension'];
                    rename($file, $new_file);

                    $file = $path . DS . $dados[$this->alias]['dir'] . DS . 'thumb_' . $old_name;
                    $infos = pathinfo($file);
                    $new_file = $path . DS . $dados[$this->alias]['dir'] . DS . 'thumb_' . $new_name . '.' . $infos['extension'];
                    rename($file, $new_file);
                }
            }
        }

        //begin rename files
        if (isset($this->data[$this->alias]['file_mobile']) && $this->data[$this->alias]['file_mobile'] != "") {

            $pre_new_name = strtolower(Inflector::slug($file_name, '-'));
            $pre_new_name = str_replace("_", "-", $pre_new_name);
            $path = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'files' . DS . 'banner' . DS . 'file_mobile';

            $pre_old_name = $this->data[$this->alias]['file_mobile'];

            $new_name = $pre_new_name;
            $old_name = $pre_old_name;

            if (file_exists($path . DS . $dados[$this->alias]['dir_mobile'] . DS . $old_name)) {
                $file = $path . DS . $dados[$this->alias]['dir_mobile'] . DS . $old_name;
                $infos = pathinfo($file);
                $new_file = $path . DS . $dados[$this->alias]['dir_mobile'] . DS . $new_name . '.' . $infos['extension'];
                $new_file_orginal = $path . DS . $dados[$this->alias]['dir_mobile'] . DS . 'original-' . $new_name . '.' . $infos['extension'];

                if (rename($file, $new_file)) {

                    //load lib
                    include_once(ROOT . DS . APP_DIR . DS . 'Lib' . DS . 'Wideimage/WideImage.php');

                    $this->query('UPDATE banners SET file_mobile = "' . $new_name . '.' . $infos['extension'] . '" WHERE id = ' . $this->id);

                    copy($new_file, $new_file_orginal);

                    /*$caminho_novo   = $path . DS . $dados[$this->alias]['dir_mobile'];
                    $outFile        = $caminho_novo.DS.$pre_new_name . '.' . $infos['extension'];
                    if(strtolower($infos['extension']) == 'jpg'){
                        WideImage::load($new_file)->resize(1500)->crop('center', 'center', 1500, 370)->saveToFile($outFile, 80);
                    }else{
                        WideImage::load($new_file)->resize(1500)->crop('center', 'center', 1500, 370)->saveToFile($outFile);
                    }*/

                    //outros renames
                    $file = $path . DS . $dados[$this->alias]['dir_mobile'] . DS . '400_' . $old_name;
                    $infos = pathinfo($file);
                    $new_file = $path . DS . $dados[$this->alias]['dir_mobile'] . DS . '400_' . $new_name . '.' . $infos['extension'];
                    rename($file, $new_file);

                    $file = $path . DS . $dados[$this->alias]['dir_mobile'] . DS . 'thumb_' . $old_name;
                    $infos = pathinfo($file);
                    $new_file = $path . DS . $dados[$this->alias]['dir_mobile'] . DS . 'thumb_' . $new_name . '.' . $infos['extension'];
                    rename($file, $new_file);
                }
            }
        }
    }

    /**
     * afterFind
     *
     * sobrecarga do metodo executado depois de buscar o registro
     */
    public function afterFind($results, $primary = false)
    {
        App::import("helper", "String");
        $this->String = new StringHelper(new View(null));

        $params = Router::getParams();

        if (!empty($results)) {
            foreach ($results as $k => &$r) {
                if (isset($r[$this->alias])) {
                    //tem sites? dou decode neles...
                    if (isset($r[$this->alias]['sites']) && $r[$this->alias]['sites'] != "") {
                        @$r[$this->alias]['sites'] = json_decode($r[$this->alias]['sites'], true);
                    }
                }
            }
        }

        return $results;
    }
}