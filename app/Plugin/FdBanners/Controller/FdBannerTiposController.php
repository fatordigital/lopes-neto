<?php

class FdBannerTiposController extends FdBannersAppController
{

    public $uses = array('FdBanners.BannerTipo');

    /**
     * admin_index method
     *
     * @return void
     */
    public function fatorcms_index($page = 1)
    {

        // Add filter
        $this->FilterResults->addFilters(
            array(
                'filter' => array(
                    'OR' => array(
                        'BannerTipo.nome' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%')),
                    )
                ),
                'filtro_nome' => array(
                    'BannerTipo.nome' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%'))
                ),
            )
        );

        $this->FilterResults->setPaginate('page', $page);
        // Define conditions
        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

        // BannerTipo
        $this->BannerTipo->recursive = 0;
        $banner_tipos = $this->paginate();

        $this->set(compact('banner_tipos'));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function fatorcms_add()
    {
        if ($this->request->is('post')) {
            $this->BannerTipo->create();
            if ($this->BannerTipo->saveAll($this->request->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
            }
        }
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function fatorcms_edit($id = null)
    {
        if (!$this->BannerTipo->exists($id)) {
            throw new NotFoundException('Registro inválido.');
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->BannerTipo->saveAll($this->request->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');

                //$this->redirect(array('action' => 'index'));
                $this->_redirectFilter($this->Session->read('referer'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
            }
        } else {
            $options = array('conditions' => array('BannerTipo.' . $this->BannerTipo->primaryKey => $id));
            $this->request->data = $this->BannerTipo->find('first', $options);

            $this->Session->write('referer', $this->referer());
        }

        // set BannerTipo
        $options = array('conditions' => array('BannerTipo.' . $this->BannerTipo->primaryKey => $id));
        $banner_tipo = $this->BannerTipo->find('first', $options);
        $this->set(compact('banner_tipo'));
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function fatorcms_delete($id = null)
    {
        $this->BannerTipo->id = $id;
        if (!$this->BannerTipo->exists()) {
            throw new NotFoundException('Registro inválido.');
        }
        $this->request->is('get');
        if ($this->BannerTipo->delete()) {
            $this->Session->setFlash(__('Registro deletado.'),'FdDashboard.alerts/fatorcms_success');
            // $this->redirect(array('action' => 'index'));
            $this->_redirectFilter($this->referer());
        }
        $this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
        // $this->redirect(array('action' => 'index'));
        $this->_redirectFilter($this->referer());
    }

    /**
     * fatorcms_status method
     *
     * @return void
     */
    public function fatorcms_status()
    {
        if (!$this->request->is('post')) {
            throw new NotFoundException('Registro inválido.');
        }
        echo $this->_saveStatus('BannerTipo', $this->request->data['id'], $this->request->data['value']);
        die;
    }
}