<?php

class FdBannersController extends FdBannersAppController
{

    public $uses = array('FdBanners.Banner');

    /**
     * admin_index method
     *
     * @return void
     */
    public function fatorcms_index($page = 1)
    {
        App::import('Helper', 'String');
        $this->String = new StringHelper(new View(null));

        $this->loadModel('FdBanners.BannerTipo');
        $banner_tipos = $this->BannerTipo->find('list', array('fields' => array('BannerTipo.id', 'BannerTipo.nome'), 'conditions' => array('BannerTipo.status' => true)));
        $this->set('banner_tipos', $banner_tipos);

        $this->FilterResults->addFilters(
            array(
                'filter' => array(
                    'OR' => array(
                        'BannerAtributo.titulo' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%')),
                    )
                ),
                'filtro_nome' => array(
                    'BannerAtributo.titulo' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%'))
                ),
                'filtro_banner_tipo_id' => array(
                    'Banner.banner_tipo_id' => array('select' => $this->FilterResults->select('Tipo de Banner...', $banner_tipos))
                ),
                'filtro_sites' => array(
                    'Banner.sites' => array(
                        'select' => $this->FilterResults->select('Site...', $this->String->getSites()),
                        'operator' => 'LIKE',
                        'value' => array('before' => '%', 'after' => '%')
                    )
                ),
            )
        );
        $this->FilterResults->setPaginate('page', $page);
        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());


        $this->Banner->order = array('created' => 'desc');
        $banners = $this->paginate();

        $this->set('banners', $banners);
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function fatorcms_add()
    {
        if ($this->request->is('post')) {

            $this->request->data['Banner']['banner_tipo_id'] = 1;
            $this->Banner->create();
            if ($this->Banner->saveAll($this->request->data)) {
                $this->_resetCaches();
                $this->Session->setFlash(__('Registro salvo com sucesso.'), 'FdDashboard.alerts/fatorcms_success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'FdDashboard.alerts/fatorcms_danger');
            }

        }

        // set tipos de banners
        $bannerTipos = $this->Banner->BannerTipo->find('list', array('fields' => array('BannerTipo.id', 'BannerTipo.nome'), 'conditions' => array('BannerTipo.status' => true), 'order' => array('BannerTipo.nome ASC')));
        $this->set(compact('bannerTipos'));

        // set as paginas
        App::import('Helper', 'String');
        $this->String = new StringHelper(new View(null));
        $sites = $this->String->getSites();

        App::import('Model', 'FdPaginas.Pagina');
        $this->Pagina = new Pagina();
        $paginas = $this->Pagina->find('all', array('fields' => array('Pagina.id', 'Pagina.nome', 'Pagina.sites', 'PaginaTipo.nome'), 'conditions' => array('Pagina.parent_id' => 0, 'Pagina.status' => true), 'order' => 'Pagina.sites ASC, Pagina.nome ASC'));

        $pagina_id = array();
        if (!empty($paginas)) {
            foreach ($paginas as $key => $pag) {
                if (isset($sites[$pag['Pagina']['sites']])) {
                    $pagina_id[$sites[$pag['Pagina']['sites']]] [$pag['Pagina']['id']] = $pag['Pagina']['nome'];
                }
            }
        }
        $this->set(compact('pagina_id'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function fatorcms_edit($id = null)
    {
        if (!$this->Banner->exists($id)) {
            throw new NotFoundException('Registro inválido.');
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Banner->saveAll($this->request->data)) {
                $this->_resetCaches();
                $this->Session->setFlash(__('Registro salvo com sucesso.'), 'FdDashboard.alerts/fatorcms_success');

                //$this->redirect(array('action' => 'index'));
                $this->_redirectFilter($this->Session->read('referer'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'FdDashboard.alerts/fatorcms_danger');
            }
        } else {
            $options = array('conditions' => array('Banner.' . $this->Banner->primaryKey => $id));
            $this->request->data = $this->Banner->find('first', $options);

            $this->Session->write('referer', $this->referer());
        }

        // set banner
        $options = array('conditions' => array('Banner.' . $this->Banner->primaryKey => $id));
        $banner = $this->Banner->find('first', $options);
        $this->set(compact('banner'));

        // set tipos de banners
        $bannerTipos = $this->Banner->BannerTipo->find('list', array('fields' => array('BannerTipo.id', 'BannerTipo.nome'), 'conditions' => array('BannerTipo.status' => true), 'order' => array('BannerTipo.nome ASC')));
        $this->set(compact('bannerTipos'));

        // set as paginas
        App::import('Helper', 'String');
        $this->String = new StringHelper(new View(null));
        $sites = $this->String->getSites();

        App::import('Model', 'FdPaginas.Pagina');
        $this->Pagina = new Pagina();
        $paginas = $this->Pagina->find('all', array('fields' => array('Pagina.id', 'Pagina.nome', 'Pagina.sites', 'PaginaTipo.nome'), 'conditions' => array('Pagina.parent_id' => 0, 'Pagina.status' => true), 'order' => 'Pagina.sites ASC, Pagina.nome ASC'));

        $pagina_id = array();
        if (!empty($paginas)) {
            foreach ($paginas as $key => $pag) {
                if (isset($sites[$pag['Pagina']['sites']])) {
                    $pagina_id[$sites[$pag['Pagina']['sites']]] [$pag['Pagina']['id']] = $pag['Pagina']['nome'];
                }
            }
        }
        $this->set(compact('pagina_id'));
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function fatorcms_delete($id = null)
    {
        $this->Banner->id = $id;
        if (!$this->Banner->exists()) {
            throw new NotFoundException('Registro inválido.');
        }
        $this->request->is('get');
        if ($this->Banner->delete()) {
            $this->_resetCaches();
            $this->Session->setFlash(__('Registro deletado.'), 'FdDashboard.alerts/fatorcms_success');
            // $this->redirect(array('action' => 'index'));
            $this->_redirectFilter($this->referer());
        }
        $this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
        // $this->redirect(array('action' => 'index'));
        $this->_redirectFilter($this->referer());
    }

    /**
     * fatorcms_status method
     *
     * @return void
     */
    public function fatorcms_status()
    {
        if (!$this->request->is('post')) {
            throw new NotFoundException('Registro inválido.');
        }
        echo $this->_saveStatus('Banner', $this->request->data['id'], $this->request->data['value']);
        $this->_resetCaches();
        die;
    }
}