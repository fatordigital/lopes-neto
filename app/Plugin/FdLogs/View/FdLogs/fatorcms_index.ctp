<?php $this->Html->addCrumb('Logs'); ?>

<div class="row">
     <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
        Logs 
    </h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <div class="row-fluid">
                    <div class="col-sm-12 col-md-10 col-lg-11">
                        <?php echo $this->FilterForm->create(null, array('role' => 'form', 'class' => 'form-inline')) ?>
                            <div class="form-group">
                                <?php echo $this->FilterForm->input('filtro_nome', array('placeholder' => 'Filtrar por nome', 'class' => 'form-control')) ?>
                            </div>
							<div class="form-group">
                                <?php echo $this->FilterForm->input('filtro_email', array('placeholder' => 'Filtrar por e-mail', 'class' => 'form-control')) ?>
                            </div>
							<div class="form-group">
                                <?php echo $this->FilterForm->input('filtro_info', array('placeholder' => 'Filtrar por info', 'class' => 'form-control')) ?>
                            </div>
                            <?php echo $this->FilterForm->submit('Filtrar', array('class' => 'btn btn-success', 'div' => false)) ?>
                            <?php echo $this->Html->link('Limpar Filtro', array('action' => 'index'), array('class' => 'btn btn-warning')) ?>
                        <?php echo $this->FilterForm->end() ?>
                    </div>
                    <div class="col-lg-1 visible-md visible-lg">
                       <?php echo $this->Element('FdDashboard.limit'); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-hover general-table">
                    <thead>
                        <tr>
                            <th style="width:60px"><?php echo $this->Paginator->sort('id', '#') ?></th>
                            <th><?php echo $this->Paginator->sort('usuario_id') ?></th>
                            <th><?php echo $this->Paginator->sort('acao') ?></th>
							<th><?php echo $this->Paginator->sort('info') ?></th>
                            <th><?php echo $this->Paginator->sort('created', 'Criado') ?></th>
                            <th style="width:180px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($logs) > 0): ?>
                            <?php foreach($logs AS $log): ?>
                            <tr>
                                <td><?php echo $log['Log']['id'] ?></td>
                                <td><a href="<?php echo $this->Html->Url(array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'edit', $log['Usuario']['id'])); ?>"><?php echo $log['Usuario']['nome']; ?></a></td>
                                <td><?php echo $log['Log']['acao'] ?></td>
								<td><?php echo $log['Log']['info'] ?></td>
                                <td><?php echo $this->Time->format($log['Log']['created'], '%d/%m/%Y %H:%M:%S') ?></td>
                                <td>
                                    <?php echo $this->Html->link('<i class="fa fa-edit"></i> Detalhe', array('action' => 'view', $log['Log']['id']), array('class' => 'btn btn-primary btn-sm', 'escape' => false)) ?> 
                                    <?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('action' => 'delete', $log['Log']['id']), array('class' => 'btn btn-danger btn-sm', 'escape' => false), __('Deseja mesmo remover o registro # %s?', $log['Log']['id'])) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="5" class="text-center">Nenhum registro encontrado.</td>
                            </tr>
                        <?php endIf; ?>
                    </tbody>
                </table>

                <?php echo $this->Element('FdDashboard.paginator'); ?>
            </div>
        </section>
    </div>
</div>