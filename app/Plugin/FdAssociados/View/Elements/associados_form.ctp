<div class="panel">
    <div class="panel-body">

        <div class="row">
            <div class="col-lg-6">
                <?php echo $this->Form->input('nome', array('class' => 'form-control')) ?>
            </div>
        </div>

        <br />
        <br />

        <div class="row row-arquivo">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Arquivo</label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">

                        <?php if (isset($banner) && $banner['Associados']['file'] != "") { ?>
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 119px;">
                                <img
                                    src="<?php echo $this->Html->Url($banner['Associados']['path'] . '/' . $banner['Associados']['dir'] . '/thumb_' . $banner['Associados']['file'], true) ?>"
                                    width="200"/>
                            </div>
                        <?php } else { ?>
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=nenhuma+imagem"
                                     alt=""/>
                            </div>
                        <?php } ?>

                        <div class="fileupload-preview fileupload-exists thumbnail"
                             style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>

                        <?php if (isset($banner) && $banner['Associados']['file'] != ""): ?>
                            <div class="icheck minimal">
                                <?php echo $this->Form->input('Associados.file.remove', array('type' => 'checkbox', 'label' => 'Remover Imagem Atual', 'div' => false)); ?>
                            </div>
                        <?php endIf; ?>

                        <div>
                           <span class="btn btn-white btn-file">
                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Selecionar imagem</span>
                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Alterar</span>
                               <?php echo $this->Form->input('file', array('type' => 'file', 'class' => 'default', 'label' => false, 'div' => false)); ?>
                               <?php echo $this->Form->hidden('dir') ?>
                           </span>
                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i
                                    class="fa fa-trash"></i> Remover</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php echo $this->Form->submit('Salvar Associado', array('class' => 'btn btn-success', 'div' => false)) ?>
    </div>
</div>