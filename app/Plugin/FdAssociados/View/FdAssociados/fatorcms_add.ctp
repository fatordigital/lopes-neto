<?php echo $this->Html->css('FdDashboard.core/bootstrap-fileupload/bootstrap-fileupload') ?>
<?php echo $this->Html->script('FdDashboard.core/bootstrap-fileupload/bootstrap-fileupload'); ?>

<?php echo $this->Html->script('FdDashboard.core/bootstrap-daterangepicker/moment.min'); ?>

<?php $this->Html->addCrumb('Associados', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Cadastrar Associados') ?>

    <h3>Cadastrar Associados</h3>

<?php echo $this->Form->create('Associados', array('type' => 'file', 'role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
<?php echo $this->Form->hidden('id') ?>

<?php echo $this->Element('FdAssociados.associados_form') ?>

<?php echo $this->Form->end() ?>