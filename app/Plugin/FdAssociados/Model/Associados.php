<?php

class Associados extends FdAssociadosAppModel
{

    public $cacheData;

    public $actsAs = array(
        'Upload.Upload' => array(
            'file' => array(
                'fields' => array(
                    'dir' => 'dir',
                    'type' => 'type',
                    'size' => 'size',
                ),
                'thumbnailMethod' => 'php',
                'thumbnailSizes' => array(
                    'thumb' => '100x100',
                    '960' => '960w',
                    '400' => '400w'
                )
            )
        )
    );

    public $validate = array(
        'nome' => array(
            'notempty' => array(
                'rule' => array('notBlank'),
                'message' => 'Campo de preenchimento obrigatório.',
            )
        )
    );

    public function __construct($id = null, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        $this->cache();
    }

    private function cache()
    {
        $associados = array();
        if (Cache::read('banners') === false) {
            $associados = $this->find('all', array(
                    'order' => array('Associados.order' => 'ASC')
                )
            );
            Cache::write('associados', $associados);
        } else {
            $associados = Cache::read('associados');
        }
        $this->cacheData = $associados;
    }

    public function beforeSave($options = array())
    {
        $this->data[$this->alias]['slug'] = Inflector::slug(strtolower($this->data[$this->alias]['nome']));
        parent::beforeSave($options);
    }


    public function afterSave($created, $options = array())
    {

        parent::afterSave($created, $options = array());

        $dados = $this->find('first', array('recursive' => -1, 'conditions' => array('id' => $this->id), 'fields' => array('file', 'dir')));

        $file_name = $this->data['Associados']['file_name'];
        $to = '/files/associados/file';

        //begin rename files
        if (isset($this->data[$this->alias]['file']) && $this->data[$this->alias]['file'] != "") {

            $pre_new_name = strtolower(Inflector::slug($this->data[$this->alias]['nome'], '-'));;
            $pre_new_name = str_replace("_", "-", $pre_new_name);
            $path = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'files' . DS . 'associados' . DS . 'file';

            $pre_old_name = $this->data[$this->alias]['file'];

            $new_name = $pre_new_name;
            $old_name = $pre_old_name;

            if (file_exists($path . DS . $dados[$this->alias]['dir'] . DS . $old_name)) {
                $file = $path . DS . $dados[$this->alias]['dir'] . DS . $old_name;
                $infos = pathinfo($file);
                $new_file = $path . DS . $dados[$this->alias]['dir'] . DS . $new_name . '.' . $infos['extension'];
                $new_file_orginal = $path . DS . $dados[$this->alias]['dir'] . DS . 'original-' . $new_name . '.' . $infos['extension'];

                if (rename($file, $new_file)) {

                    //load lib
                    include_once(ROOT . DS . APP_DIR . DS . 'Lib' . DS . 'Wideimage/WideImage.php');

                    $this->query('UPDATE `associados` SET path = "' . $to . '", file = "' . $new_name . '.' . $infos['extension'] . '" WHERE id = ' . $this->id);

                    copy($new_file, $new_file_orginal);

                    $caminho_novo = $path . DS . $dados[$this->alias]['dir'];
                    $outFile = $caminho_novo . DS . $pre_new_name . '.' . $infos['extension'];
                    if (strtolower($infos['extension']) == 'jpg') {
                        WideImage::load($new_file)->resize(1500)->crop('center', 'center', 1500, 370)->saveToFile($outFile, 80);
                    } else {
                        WideImage::load($new_file)->resize(1500)->crop('center', 'center', 1500, 370)->saveToFile($outFile);
                    }

                    //outros renames
                    $file = $path . DS . $dados[$this->alias]['dir'] . DS . '960_' . $old_name;
                    $infos = pathinfo($file);
                    $new_file = $path . DS . $dados[$this->alias]['dir'] . DS . '960_' . $new_name . '.' . $infos['extension'];
                    rename($file, $new_file);

                    $file = $path . DS . $dados[$this->alias]['dir'] . DS . '400_' . $old_name;
                    $infos = pathinfo($file);
                    $new_file = $path . DS . $dados[$this->alias]['dir'] . DS . '400_' . $new_name . '.' . $infos['extension'];
                    rename($file, $new_file);

                    $file = $path . DS . $dados[$this->alias]['dir'] . DS . 'thumb_' . $old_name;
                    $infos = pathinfo($file);
                    $new_file = $path . DS . $dados[$this->alias]['dir'] . DS . 'thumb_' . $new_name . '.' . $infos['extension'];
                    rename($file, $new_file);
                }
            }
        }
    }

}