<?php

class FdAssociadosController extends FdAssociadosAppController
{

    public $uses = array('FdAssociados.Associados');

    public function fatorcms_index($page = 1)
    {
        App::import('Helper', 'String');
        $this->String = new StringHelper(new View(null));

        $this->FilterResults->addFilters(
            array(
                'filter' => array(
                    'OR' => array(
                        'Associados.nome' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%')),
                    )
                ),
                'filtro_nome' => array(
                    'Associados.nome' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%'))
                )
            )
        );
        $this->FilterResults->setPaginate('page', $page);
        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

        $this->Associados->order = array('created' => 'desc');
        $associados = $this->paginate();

        $this->set(compact('associados'));
    }

    public function fatorcms_add()
    {
        if ($this->request->is('post')) {
            if ($this->Associados->save($this->request->data)) {
                $this->Session->setFlash(__('Registro salvo com sucesso.'), 'FdDashboard.alerts/fatorcms_success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'FdDashboard.alerts/fatorcms_danger');
            }
        }
    }

    public function fatorcms_edit($id = null)
    {
        if (!$this->Associados->exists($id)) {
            throw new NotFoundException('Registro inválido.');
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Associados->saveAll($this->request->data)) {
                $this->_resetCaches();
                $this->Session->setFlash(__('Registro salvo com sucesso.'), 'FdDashboard.alerts/fatorcms_success');
                $this->_redirectFilter($this->Session->read('referer'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'FdDashboard.alerts/fatorcms_danger');
            }
        } else {
            $options = array('conditions' => array('Associados.' . $this->Associados->primaryKey => $id));
            $this->request->data = $this->Associados->find('first', $options);
            $this->Session->write('referer', $this->referer());
        }

        // set banner
        $options = array('conditions' => array('Associados.' . $this->Associados->primaryKey => $id));
        $banner = $this->Associados->find('first', $options);
        $this->set(compact('banner'));
    }

    public function fatorcms_delete($id = null)
    {
        $this->Associados->id = $id;
        if (!$this->Associados->exists()) {
            throw new NotFoundException('Registro inválido.');
        }
        $this->request->is('get');
        if ($this->Associados->delete()) {
            $this->_resetCaches();
            $this->Session->setFlash(__('Registro deletado.'), 'FdDashboard.alerts/fatorcms_success');
            // $this->redirect(array('action' => 'index'));
            $this->_redirectFilter($this->referer());
        }
        $this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
        // $this->redirect(array('action' => 'index'));
        $this->_redirectFilter($this->referer());
    }

}