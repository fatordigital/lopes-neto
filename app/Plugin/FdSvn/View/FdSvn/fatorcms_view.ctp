<?php $this->Html->addCrumb('Controle de Versão', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Detalhe da versão') ?>

<h3>Detalhe Versão</h3>

<div class="panel">
	<div class="panel-body">
	<?php echo $this->Form->create('Svn', array('type' => 'file', 'role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
		<div class="row">
			<div class="col-lg-6">
				<strong>Nome: </strong> <?php echo $svn['Svn']['nome']; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<strong>Usuário: </strong> <a href="<?php echo $this->Html->Url(array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'edit', $svn['Usuario']['id'])); ?>"><?php echo $svn['Usuario']['nome']; ?></a>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<?php echo $this->Form->input('conteudo', array('label' => 'Conteúdo', 'div' => false, 'class' => 'form-control editor', 'disabled' => true)) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<strong>Editado: </strong> <?php echo $svn['Svn']['created']; ?>
			</div>
		</div>
		<div class="row">
			<br />
		</div>

		<?php echo $this->Form->submit('Voltar para essa versão', array('class' => 'btn btn-success', 'div' => false)) ?>
		<a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">Cancelar</a>
	<?php echo $this->Form->end() ?>
	</div>
</div>