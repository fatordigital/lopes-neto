<?php $this->Html->addCrumb('Controle de Versão'); ?>

<div class="row">
     <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
        Controle de Versão 
    </h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <div class="row-fluid">
                    <div class="col-sm-12 col-md-10 col-lg-11">
                        <?php echo $this->FilterForm->create(null, array('role' => 'form', 'class' => 'form-inline')) ?>
                           <div class="form-group">
                                <?php echo $this->FilterForm->input('filtro_conteudo', array('placeholder' => 'Filtrar por info', 'class' => 'form-control')) ?>
                            </div>
                            <?php echo $this->FilterForm->submit('Filtrar', array('class' => 'btn btn-success', 'div' => false)) ?>
                            <?php echo $this->Html->link('Limpar Filtro', array('action' => 'index'), array('class' => 'btn btn-warning')) ?>
                        <?php echo $this->FilterForm->end() ?>
                    </div>
                    <div class="col-lg-1 visible-md visible-lg">
                       <?php echo $this->Element('FdDashboard.limit'); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-hover general-table">
                    <thead>
                        <tr>
                            <th style="width:60px"><?php echo $this->Paginator->sort('id', '#') ?></th>
                            <th><?php echo $this->Paginator->sort('usuario_id') ?></th>
                            <th><?php echo $this->Paginator->sort('model') ?></th>
							<th><?php echo $this->Paginator->sort('nome') ?></th>
                            <th><?php echo $this->Paginator->sort('obs') ?></th>
                            <th><?php echo $this->Paginator->sort('created', 'Criado') ?></th>
                            <th style="width:180px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($svns) > 0): ?>
                            <?php foreach($svns AS $svn): ?>
                            <tr>
                                <td><?php echo $svn['Svn']['id'] ?></td>
                                <td><a href="<?php echo $this->Html->Url(array('plugin' => 'fd_usuarios', 'controller' => 'fd_usuarios', 'action' => 'edit', $svn['Usuario']['id'])); ?>"><?php echo $svn['Usuario']['nome']; ?></a></td>
                                <td><?php echo $svn['Svn']['model'] ?></td>
								<td><?php echo $svn['Svn']['nome'] ?></td>
                                <td><?php echo $svn['Svn']['obs'] ?></td>
                                <td><?php echo $this->Time->format($svn['Svn']['created'], '%d/%m/%Y %H:%M:%S') ?></td>
                                <td>
                                    <?php echo $this->Html->link('<i class="fa fa-edit"></i> Detalhe', array('action' => 'view', $svn['Svn']['id']), array('class' => 'btn btn-primary btn-sm', 'escape' => false)) ?> 
                                    <?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('action' => 'delete', $svn['Svn']['id']), array('class' => 'btn btn-danger btn-sm', 'escape' => false), __('Deseja mesmo remover o registro # %s?', $svn['Svn']['id'])) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="5" class="text-center">Nenhum registro encontrado.</td>
                            </tr>
                        <?php endIf; ?>
                    </tbody>
                </table>

                <?php echo $this->Element('FdDashboard.paginator'); ?>
            </div>
        </section>
    </div>
</div>