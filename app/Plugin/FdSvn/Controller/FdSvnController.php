<?php

class FdSvnController extends FdSvnAppController {

	public $uses = array('FdSvn.Svn');

/**
 * admin_index method
 *
 * @return void
 */
	public function fatorcms_index($page = 1) {

		// Add filter
		$this->FilterResults->addFilters(
			array(
				'filter' => array(
					'OR' => array(
						'Svn.conteudo'   => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
					)
				),
				'filtro_conteudo' => array(
					'Svn.conteudo'     	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				),
			)
		);

		$this->FilterResults->setPaginate('page', $page);
		// Define conditions
		$this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

		// Svn
		$this->Svn->recursive = 0;
		
		// Paginate
		$options['conditions'] 	= $this->FilterResults->getConditions();
		if(isset($this->params->params['named']['registro_id']) && isset($this->params->params['named']['model'])){
			if(is_array($options['conditions'])){
				$options['conditions'] = array_merge($options['conditions'], array('Svn.registro_id' => $this->params->params['named']['registro_id'], 'Svn.model' => $this->params->params['named']['model']));
			}else{
				$options['conditions'] = array('Svn.registro_id' => $this->params->params['named']['registro_id'], 'Svn.model' => $this->params->params['named']['model']);
			}
		}
		$options['order'] = 'Svn.id DESC';

		$this->paginate = $options;
		$svn = $this->paginate();

		$this->set('svns', $svn);
	}
	
/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_view($id = null) {
		if (!$this->Svn->exists($id)) {
            throw new NotFoundException('Registro inválido.');
		}

		// set Svn
		$options = array('conditions' => array('Svn.' . $this->Svn->primaryKey => $id));
		$svn = $this->Svn->find('first', $options);
		$this->set(compact('svn'));

		if ($this->request->is('post') || $this->request->is('put')) {

			//find o registro atual
			$this->loadModel("Fd".$svn['Svn']['model']."s.".$svn['Svn']['model']."");
			$model = $this->{$svn['Svn']['model']}->find('first', array('recursive' => -1, 'conditions' => array('id' => $svn['Svn']['registro_id'])));
			if(!empty($model)){

				$new = array();
				$new['Svn']['id'] 					= null;
				$new['Svn']['usuario_id'] 			= $this->Auth->User('id');
				$new['Svn']['registro_id'] 			= $model[$svn['Svn']['model']]['id'];
				$new['Svn']['nome'] 				= $model[$svn['Svn']['model']]['nome'];
				$new['Svn']['conteudo'] 			= $model[$svn['Svn']['model']]['conteudo'];
				$new['Svn']['conteudo_rascunho'] 	= $model[$svn['Svn']['model']]['conteudo_rascunho'];
				$new['Svn']['model']				= $svn['Svn']['model'];
				$new['Svn']['obs']					= 'Restore para o registro ' . $id;

				if($this->Svn->save($new)){
					if($this->{$svn['Svn']['model']}->save(array('id' => $svn['Svn']['registro_id'], 'conteudo' => $svn['Svn']['conteudo'], 'conteudo_rascunho' => $svn['Svn']['conteudo_rascunho']))){
						$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');
						$this->redirect(array('plugin' => false, 'controller' => strtolower($svn['Svn']['model']).'s', 'action' => 'edit', $model[$svn['Svn']['model']]['id']));	
					}else{
						$this->Session->setFlash(__('O registro não pode ser salvo. Tente novamente ou contate o administrador do sistema'),'FdDashboard.alerts/fatorcms_danger');
					}
				}else{
					$this->Session->setFlash(__('O registro não pode ser salvo. Tente novamente ou contate o administrador do sistema'),'FdDashboard.alerts/fatorcms_danger');
				}
			}else{
				$this->Session->setFlash(__('O registro não pode ser salvo. Tente novamente ou contate o administrador do sistema'),'FdDashboard.alerts/fatorcms_danger');
			}
		}
		
		// set data
		$this->request->data = $svn;
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_delete($id = null) {
		$this->Svn->id = $id;
		if (!$this->Svn->exists()) {
            throw new NotFoundException('Registro inválido.');
		}
		$this->request->is('get');
		if ($this->Svn->delete()) {
			$this->_resetCaches();
			$this->Session->setFlash(__('Registro deletado.'),'FdDashboard.alerts/fatorcms_success');
			// $this->redirect(array('action' => 'index'));
			$this->_redirectFilter($this->referer());
		}
		$this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
		// $this->redirect(array('action' => 'index'));
		$this->_redirectFilter($this->referer());
	}

/**
 * fatorcms_status method
 *
 * @return void
 */
    public function fatorcms_status(){
        if (!$this->request->is('post')){
            throw new NotFoundException('Registro inválido.');
        }
        echo $this->_saveStatus('Svn', $this->request->data['id'], $this->request->data['value']);
        die;
    }
}