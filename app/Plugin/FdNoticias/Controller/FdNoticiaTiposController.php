<?php

class FdNoticiaTiposController extends FdNoticiasAppController {

	public $uses = array('FdNoticias.NoticiaTipo');

/**
 * admin_index method
 *
 * @return void
 */
	public function fatorcms_index($page = 1) {

		// Add filter
		$this->FilterResults->addFilters(
			array(
				'filter' => array(
					'OR' => array(
						'NoticiaTipo.nome'    => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
					)
				),
				'filtro_nome' => array(
					'NoticiaTipo.nome'     	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				),
			)
		);
		// Define conditions
		$this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

		//exportar? 
		if(isset($this->params->params['named']['acao']) && $this->params->params['named']['acao'] == "exportar"){
			$this->Reports->xls($this->NoticiaTipo->find('all', array('conditions' => $this->FilterResults->getConditions(), 'callbacks' => false)), 'Notícias');
		}

		// Paginate
		$options['conditions'] 	= $this->FilterResults->getConditions();
		$options['order'] 		= array('NoticiaTipo.id' => 'ASC');
		$this->paginate = $options;

		$this->NoticiaTipo->recursive = 0;
		$this->set('noticia_tipos', $this->paginate());
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function fatorcms_add() {
		if ($this->request->is('post')) {
			$this->NoticiaTipo->create();
			if ($this->NoticiaTipo->save($this->request->data)) {
				//set caches
				$this->_resetCaches();
				$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_edit($id = null) {		
		if (!$this->NoticiaTipo->exists($id)) {
			throw new NotFoundException(__('Registro Inválido.'));
		}

		// set noticia_tipo
		$options = array('conditions' => array('NoticiaTipo.' . $this->NoticiaTipo->primaryKey => $id));
		$noticia_tipo = $this->NoticiaTipo->find('first', $options);
		$this->set(compact('noticia_tipo'));

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->NoticiaTipo->save($this->request->data)) {
				//set caches
				$this->_resetCaches();
				$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');

				//$this->redirect(array('action' => 'index'));
				$this->_redirectFilter($this->Session->read('referer'));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}
		} else {
			$options = array('conditions' => array('NoticiaTipo.' . $this->NoticiaTipo->primaryKey => $id));
			$this->request->data = $this->NoticiaTipo->find('first', $options);

			$this->Session->write('referer', $this->referer());
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_delete($id = null) {
		$this->NoticiaTipo->id = $id;
		if (!$this->NoticiaTipo->exists()) {
            throw new NotFoundException('Registro inválido.');
		}
		$this->request->is('get');
		if ($this->NoticiaTipo->delete()) {
			$this->_resetCaches();
			$this->Session->setFlash(__('Registro deletado.'),'FdDashboard.alerts/fatorcms_success');
			// $this->redirect(array('action' => 'index'));
			$this->_redirectFilter($this->referer());
		}
		$this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
		// $this->redirect(array('action' => 'index'));
		$this->_redirectFilter($this->referer());
	}

/**
 * fatorcms_status method
 *
 * @return void
 */
    public function fatorcms_status(){
        if (!$this->request->is('post')){
            throw new NotFoundException('Registro inválido.');
        }
        echo $this->_saveStatus('NoticiaTipo', $this->request->data['id'], $this->request->data['value']);
        die;
    }
}