<?php

class FdNoticiasAppController extends AppController {

	public $noticia_tipo_id;

	public function beforeFilter(){

		parent::beforeFilter();

		if(isset($this->params->params['tipo']) && $this->params->params['tipo'] != ''){
			$this->noticia_tipo_id = $this->params->params['tipo'];
		}

		if($this->noticia_tipo_id != ""){
			if ($this->request->is('post')) {

				if(isset($this->request->data['Noticia'])){
					$this->request->data['Noticia']['noticia_tipo_id'] = $this->noticia_tipo_id;			
				}
				
				if(isset($this->request->data['NoticiaCategoria'])){
					$this->request->data['NoticiaCategoria']['noticia_tipo_id'] = $this->noticia_tipo_id;
				}

				if(isset($this->request->data['filter'])){
					$this->request->data['filter']['filtro_noticia_tipo_id'] = $this->noticia_tipo_id;
				}
			}

			$this->loadModel('FdNoticias.Noticia');
			$noticia_tipo = $this->Noticia->NoticiaTipo->find('first', array('recursive' => -1, 'fields' => array('NoticiaTipo.id', 'NoticiaTipo.nome'), 'conditions' => array('NoticiaTipo.id' => $this->noticia_tipo_id, 'NoticiaTipo.status' => true)));
			$this->set(compact('noticia_tipo'));
		}		
	}

}