<?php

class FdNoticiaCategoriasController extends FdNoticiasAppController
{

    public $uses = array('FdNoticias.NoticiaCategoria');

    public function beforeFilter()
    {
        parent::beforeFilter();

        App::import('Model', 'FdNoticias.NoticiaTipo');
        $this->NoticiaTipo = new NoticiaTipo();
        App::import('Model', 'FdNoticias.NoticiaCategoria');
        $this->NoticiaCategoria = new NoticiaCategoria();
        $noticia_tipos_ids = $this->NoticiaTipo->find('list',
            array(
                'recursive' => -1,
                'fields' => array('NoticiaTipo.id', 'NoticiaTipo.nome')
            )
        );
        $this->set(compact('noticia_tipos_ids'));
    }

    /**
     * admin_index method
     *
     * @return void
     */
    public function fatorcms_index($page = 1)
    {

        
        
        // Add filter
        $this->FilterResults->addFilters(
            array(
                'filter' => array(
                    'OR' => array(
                        'NoticiaCategoriaAtributo.titulo' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%')),
                    )
                ),
                'filtro_nome' => array(
                    'NoticiaCategoriaAtributo.titulo' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%'))
                ),
            )
        );
        // Define conditions
        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

        //exportar?
        if (isset($this->params->params['named']['acao']) && $this->params->params['named']['acao'] == "exportar") {
            $this->Reports->xls($this->NoticiaCategoria->find('all', array('conditions' => $this->FilterResults->getConditions(), 'callbacks' => false)), 'Categorias de Notícias');
        }

        // Paginate
        if (count($this->FilterResults->getConditions()) > 0) {
            $options['conditions'] = array($this->FilterResults->getConditions(), 'NoticiaCategoria.noticia_tipo_id' => $this->noticia_tipo_id);
        } else {
            $options['conditions'] = array('NoticiaCategoria.noticia_tipo_id' => $this->noticia_tipo_id);
        }
        $options['order'] = array('NoticiaCategoria.id' => 'ASC');
        $this->paginate = $options;

        $this->set('noticia_categorias', $this->paginate());
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function fatorcms_add()
    {
        if ($this->request->is('post')) {
            $this->NoticiaCategoria->create();
            if ($this->NoticiaCategoria->saveAll($this->request->data)) {
                //set caches
                $this->_resetCaches();
                $this->Session->setFlash(__('Registro salvo com sucesso.'), 'FdDashboard.alerts/fatorcms_success');
                $this->redirect(array('action' => 'index', 'tipo' => $this->params->params['tipo']));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'FdDashboard.alerts/fatorcms_danger');
            }
        }
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function fatorcms_edit($id = null)
    {
        if (!$this->NoticiaCategoria->exists($id)) {
            throw new NotFoundException(__('Registro Inválido.'));
        }

        // set noticia_categoria
        $options = array('conditions' => array('NoticiaCategoria.' . $this->NoticiaCategoria->primaryKey => $id));
        $noticia_categoria = $this->NoticiaCategoria->find('first', $options);

        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->NoticiaCategoria->saveAll($this->request->data)) {
                //set caches
                $this->_resetCaches();
                $this->Session->setFlash(__('Registro salvo com sucesso.'), 'FdDashboard.alerts/fatorcms_success');

                //$this->redirect(array('action' => 'index'));
                $this->_redirectFilter($this->Session->read('referer'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'FdDashboard.alerts/fatorcms_danger');
            }
        } else {
            $options = array('conditions' => array('NoticiaCategoria.' . $this->NoticiaCategoria->primaryKey => $id));
            $this->request->data = $this->NoticiaCategoria->find('first', $options);

            $this->Session->write('referer', $this->referer());
        }
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function fatorcms_delete($id = null)
    {
        $this->NoticiaCategoria->id = $id;
        if (!$this->NoticiaCategoria->exists()) {
            throw new NotFoundException('Registro inválido.');
        }
        $this->request->is('get');
        if ($this->NoticiaCategoria->delete()) {
            $this->_resetCaches();
            $this->Session->setFlash(__('Registro deletado.'), 'FdDashboard.alerts/fatorcms_success');
            // $this->redirect(array('action' => 'index'));
            $this->_redirectFilter($this->referer());
        }
        $this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
        // $this->redirect(array('action' => 'index'));
        $this->_redirectFilter($this->referer());
    }

    /**
     * fatorcms_status method
     *
     * @return void
     */
    public function fatorcms_status()
    {
        if (!$this->request->is('post')) {
            throw new NotFoundException('Registro inválido.');
        }
        echo $this->_saveStatus('NoticiaCategoria', $this->request->data['id'], $this->request->data['value']);
        die;
    }
}