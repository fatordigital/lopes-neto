<?php

class FdNoticiasController extends FdNoticiasAppController
{

    public $uses = array('FdNoticias.Noticia');

    public $noticia_tipos;

    /**
     * beforeFilter method
     *
     * @return void
     */
    public function beforeFilter()
    {
        parent::beforeFilter();

        // App::import('Model', 'FdNoticias.Noticia');
        // $this->Noticia = new Noticia();
        // $this->Noticia = ClassRegistry::init('Noticia');
        // $this->loadModel('FdNoticias.Noticia');

        App::import('Model', 'FdNoticias.NoticiaTipo');
        $this->NoticiaTipo = new NoticiaTipo();
        $noticia_tipos_ids = $this->NoticiaTipo->find('list',
            array(
                'recursive' => -1,
                'fields' => array('NoticiaTipo.id', 'NoticiaTipo.nome')
            )
        );
        $this->set(compact('noticia_tipos_ids'));

        if (!$this->request->is('ajax')) {
            if ($this->_isAdminMode()) {
                //busco os tipos de noticias
                $this->loadModel('FdNoticias.NoticiaTipo');
                $this->noticia_tipos = $this->NoticiaTipo->find('list', array('fields' => array('NoticiaTipo.id', 'NoticiaTipo.nome'), 'conditions' => array('NoticiaTipo.status' => true)));
                $noticia_tipo_id = $this->noticia_tipos;
                $this->set(compact('noticia_tipo_id'));

                //busco as categorias de noticias
                $this->loadModel('FdNoticias.NoticiaCategoriaAtributo');
                $noticia_categoria_id = $this->NoticiaCategoriaAtributo->find('list',
                    array(
                        'fields' => array('NoticiaCategoriaAtributo.noticia_categoria_id', 'NoticiaCategoriaAtributo.titulo'),
                        'recursive' => -1,
                        'conditions' => array(
                            'NoticiaCategoriaAtributo.idioma_id' => 1
                        )
                    )
                );
//                $this->loadModel('FdNoticias.NoticiaCategoria->find('list', array('fields' => array('NoticiaCategoria.id', 'NoticiaCategoriaAtributo.nome'), 'conditions' => array('NoticiaCategoria.noticia_tipo_id' => $this->noticia_tipo_id, 'NoticiaCategoria.status' => true)));
                $this->set(compact('noticia_categoria_id'));
            }
        }
    }

    /**
     * admin_index method
     *
     * @return void
     */
    public function fatorcms_index($page = 1)
    {

        // Add filter
        $this->FilterResults->addFilters(
            array(
                'filter' => array(
                    'OR' => array(
                        'NoticiaAtributo.titulo' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%')),
                        'NoticiaAtributo.conteudo' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%')),
                    )
                ),
                'filtro_titulo' => array(
                    'NoticiaAtributo.titulo' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%'))
                ),
                'filtro_conteudo' => array(
                    'NoticiaAtributo.conteudo' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%'))
                ),
                'filtro_noticia_tipo_id' => array(
                    'Noticia.noticia_tipo_id' => array('select' => $this->FilterResults->select('Tipo de Notícia...', $this->noticia_tipos))
                ),
            )
        );
        // Define conditions
        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

        //exportar?
        if (isset($this->params->params['named']['acao']) && $this->params->params['named']['acao'] == "exportar") {
            $this->Reports->xls($this->Noticia->find('all', array('conditions' => $this->FilterResults->getConditions(), 'callbacks' => false)), 'Notícias');
        }

        // Paginate
        if (count($this->FilterResults->getConditions()) > 0) {
            $options['conditions'] = array($this->FilterResults->getConditions(), 'Noticia.noticia_tipo_id' => $this->noticia_tipo_id);
        } else {
//            $options['conditions'] = array('Noticia.noticia_tipo_id' => $this->noticia_tipo_id);
        }

        $options['order'] = array('Noticia.id' => 'DESC');
        $this->paginate = $options;


        $this->set('noticias', $this->paginate());
    }

    public function fatorcms_add()
    {
        if ($this->request->is('post')) {

            $data = $this->request->data;
            if (isset($data['preview']) || isset($data['rascunho'])) {
                $data['Noticia']['status'] = 2;
                $data['Noticia']['conteudo_rascunho'] = $data['Noticia']['conteudo'];
            }

            if ($this->Noticia->saveAll($data)) {
                if (isset($data['Noticia']['tags']) && count($data['Noticia']['tags'])) {

                    App::import('Model', 'FdNoticias.NoticiaTag');
                    $this->NoticiaTag = new NoticiaTag();

                    $this->loadModel('FdNoticias.NoticiaNoticiaTag');

                    foreach ($data['Noticia']['tags'] as $key => $tag) {

                        $tag_id = 0;
                        if (stripos($key, '-')) {
                            $explode = explode('-', $key);
                            $tag_id = $explode[0];
                        } else {
                            $new_tag['id'] = null;
                            $new_tag['nome'] = $tag;
                            $new_tag['seo_url'] = "";
                            $new_tag['seo_title'] = $new_tag['nome'];
                            $new_tag['status'] = true;
                            if ($this->NoticiaTag->save($new_tag)) {
                                $tag_id = $this->NoticiaTag->id;
                            }
                        }

                        if ($tag_id > 0) {
                            if (!$this->NoticiaNoticiaTag->find('count', array('recursive' => -1, 'conditions' => array('AND' => array('tag_id' => $tag_id, 'noticia_id' => $this->Noticia->id))))) {
                                $this->NoticiaNoticiaTag->save(array('id' => null, 'tag_id' => $tag_id, 'noticia_id' => $this->Noticia->id));
                            }
                        }
                    }
                }

                //begin: salva o evento (se tiver)
                if (isset($data['Noticia']['evento']) && $data['Noticia']['evento'] == true) {
                    $noticia = $this->Noticia->find('first', array('conditions' => array('Noticia.id' => $this->Noticia->id)));

                    if (!empty($noticia)) {
                        App::import('Model', 'FdEventos.Evento');
                        $this->Evento = new Evento();

                        $new_event = array();
                        $new_event['Evento']['id'] = null;
                        $new_event['Evento']['evento_tipo'] = 'evento';
                        $new_event['Evento']['noticia_id'] = $this->Noticia->id;
                        $new_event['Evento']['nome'] = $data['Noticia']['evento_nome'];
                        $new_event['Evento']['intervalo'] = $data['Noticia']['evento_intervalo'];
                        $new_event['Evento']['status'] = true;
                        if ($data['Noticia']['evento_link_noticia'] == true) {
                            $new_event['Evento']['noticia_seo_url'] = $noticia['Noticia']['seo_url'];
                        }

                        if ($this->Evento->save($new_event)) {
                            $this->Noticia->save(array('id' => $this->Noticia->id, 'evento_id' => $this->Evento->id), false);
                        }
                    }
                }

                $this->_resetCaches();
                $this->Session->setFlash(__('Registro salvo com sucesso.'), 'FdDashboard.alerts/fatorcms_success');
                $this->redirect(array('action' => 'index', 'tipo' => $this->params->params['tipo']));
            } else {

                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'FdDashboard.alerts/fatorcms_danger');
            }
        }

        // Set NoticiaTipo
        App::import('Model', 'FdNoticias.NoticiaTipo');
        $this->NoticiaTipo = new NoticiaTipo();
        $this->set('noticiaTipos', $this->NoticiaTipo->find('list', array('recursive' => -1, 'fields' => array('NoticiaTipo.id', 'NoticiaTipo.nome'), 'conditions' => array('NoticiaTipo.status' => true))));

        // Seto a data atual, casa não seja definida
        if (!isset($data['Noticia']['data_publicacao']) || $data['Noticia']['data_publicacao'] == "") {
            App::import("helper", "String");
            $this->String = new StringHelper(new View(null));
            $data['Noticia']['data_publicacao'] = $this->String->dataFormatada("d/m/Y", date('Y-m-d'));
        }

        // set tags de noticias
        $this->loadModel('FdNoticias.NoticiaTag');
        $noticiaTags = $this->NoticiaTag->find('all', array('fields' => array('NoticiaTag.id', 'NoticiaTag.nome'), 'conditions' => array('NoticiaTag.status' => true), 'order' => array('NoticiaTag.nome ASC')));
        $tags = array();
        if (count($noticiaTags) > 0) {
            foreach ($noticiaTags as $key => $tag) {
                $tags[] = array('id' => $tag['NoticiaTag']['id'], 'label' => $tag['NoticiaTag']['nome'], 'value' => $tag['NoticiaTag']['nome']);
            }
        }
        $tags = json_encode($tags);
        $this->set(compact('tags'));

        // Lista as galerias do tipo noticias
        App::import('Model', 'FdGalerias.Galeria');
        $this->Galeria = new Galeria();
        $galeria_id = $this->Galeria->find('list', array('fields' => array('Galeria.id', 'Galeria.nome'), 'conditions' => array('Galeria.status' => true, 'Galeria.galeria_tipo_id' => $this->Galeria->noticias)));
        $this->set(compact('galeria_id'));

        // Lista as galerias do tipo interna
        App::import('Model', 'FdSac.SacTipo');
        $this->SacTipo = new SacTipo();;
        $sac_tipo = $this->SacTipo->find('first', array('conditions' => array('SacTipo.slug' => 'fale_conosco_interna')));
        $this->set(compact('sac_tipo'));

        // Lista os blocos do tipo noticia
        App::import('Model', 'FdBlocos.Bloco');
        $this->Bloco = new Bloco();;
        $blocos = $this->Bloco->find('list', array('contain' => array('BlocoTipo'), 'fields' => array('Bloco.id', 'Bloco.nome'), 'conditions' => array('Bloco.status' => true, 'BlocoTipo.slug' => 'noticias')));
        $this->set(compact('blocos'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function fatorcms_edit($id = null)
    {
        if (!$this->Noticia->exists($id)) {
            throw new NotFoundException(__('Registro Inválido.'));
        }

        App::import('Helper', 'Idioma');
        $this->IdiomaHelper = new IdiomaHelper(new View());

        // set noticia
        $options = array('conditions' => array('Noticia.' . $this->Noticia->primaryKey => $id));
        $noticia = $this->Noticia->find('first', $options);

        if ($this->request->is('post') || $this->request->is('put')) {

            //é preview ou rascunho?
            if (isset($this->request->data['preview']) || isset($this->request->data['rascunho'])) {
                $this->request->data['Noticia']['status'] = 2;
                if (isset($this->request->data['Noticia']['conteudo'])) {
                    $this->request->data['Noticia']['conteudo_rascunho'] = $this->request->data['Noticia']['conteudo'];
                }
            }

            // se for descartar o rascunho...
            if (isset($this->request->data['descartar_rascunho'])) {
                if ($this->request->data['Noticia']['status'] == 2) {
                    $this->request->data['Noticia']['status'] = 1;
                }
                $this->request->data['Noticia']['conteudo_rascunho'] = null;
            }

            // se for publicar rascunho...
            if (isset($this->request->data['salvar_publicar'])) {
                if ($this->request->data['Noticia']['status'] == 2) {
                    $this->request->data['Noticia']['status'] = 1;
                }
                $this->request->data['Noticia']['conteudo'] = $this->request->data['Noticia']['conteudo_rascunho'];
                $this->request->data['Noticia']['conteudo_rascunho'] = null;
            }

            // $this->request->data['Noticia']['noticia_tipo_id'] = $this->noticia_tipo_id;
            if ($this->Noticia->saveAll($this->request->data)) {

                //begin save SVN
                $condicao = false;
                if (isset($this->request->data['Noticia']['conteudo'])) {
                    $condicao = $noticia['Noticia']['conteudo'] != $this->request->data['Noticia']['conteudo'];
                } elseif (isset($this->request->data['Noticia']['conteudo_rascunho'])) {
                    $condicao = $noticia['Noticia']['conteudo_rascunho'] != $this->request->data['Noticia']['conteudo_rascunho'];
                }

                if ($condicao) {
                    App::import('Model', 'FdSvn.Svn');
                    $this->Svn = new Svn();

                    $svn = array();
                    $svn['Svn']['id'] = null;
                    $svn['Svn']['usuario_id'] = $this->Auth->User('id');
                    $svn['Svn']['registro_id'] = $id;
                    $svn['Svn']['nome'] = $noticia['Noticia']['titulo'];
                    $svn['Svn']['conteudo'] = $noticia['Noticia']['conteudo'];
                    $svn['Svn']['conteudo_rascunho'] = $noticia['Noticia']['conteudo_rascunho'];
                    $svn['Svn']['model'] = 'Noticia';

                    $this->Svn->save($svn);
                }
                //end save SVN

                //begin: salva as tags das noticias
                if (isset($this->request->data['Noticia']['tags']) && count($this->request->data['Noticia']['tags'])) {
                    // $this->loadModel('FdNoticias.NoticiaTag');
                    App::import('Model', 'FdNoticias.NoticiaTag');
                    $this->NoticiaTag = new NoticiaTag();
                    $this->loadModel('FdNoticias.NoticiaNoticiaTag');

                    // $this->NoticiaNoticiaTag->query('DELETE FROM noticia_noticia_tags WHERE noticia_id = ' . $this->Noticia->id);

                    //quem já tava vinculado?
                    $ja_vinculados = $this->NoticiaNoticiaTag->find('all', array('recursive' => -1, 'conditions' => array('NoticiaNoticiaTag.noticia_id' => $this->Noticia->id)));

                    //removo os registros que estavam vinculados e não vieram selecionados. (pq? logo, o cara 'desmarcou' esse cara)
                    if (!empty($ja_vinculados)) {
                        foreach ($this->request->data['Noticia']['tags'] as $key => $tag) {
                            if (stripos($key, '-')) {
                                $explode = explode('-', $key);
                                $tags[] = $explode[0];
                            }
                        }
                        foreach ($ja_vinculados as $key => $vinculo) {
                            if (!in_array($vinculo['NoticiaNoticiaTag']['tag_id'], $tags)) {
                                $this->Noticia->query("DELETE FROM noticia_noticia_tags WHERE tag_id = " . $vinculo['NoticiaNoticiaTag']['tag_id'] . " AND noticia_id = " . $this->Noticia->id);
                            }
                        }
                    }

                    foreach ($this->request->data['Noticia']['tags'] as $key => $tag) {

                        $tag_id = 0;
                        if (stripos($key, '-')) {
                            $explode = explode('-', $key);
                            $tag_id = $explode[0];
                        } else {
                            $new_tag['id'] = null;
                            $new_tag['nome'] = $tag;
                            $new_tag['seo_url'] = "";
                            $new_tag['seo_title'] = $new_tag['nome'];
                            $new_tag['status'] = true;
                            if ($this->NoticiaTag->save($new_tag)) {
                                $tag_id = $this->NoticiaTag->id;
                            }
                        }

                        if ($tag_id > 0) {
                            if (!$this->NoticiaNoticiaTag->find('count', array('recursive' => -1, 'conditions' => array('AND' => array('tag_id' => $tag_id, 'noticia_id' => $this->Noticia->id))))) {
                                $this->NoticiaNoticiaTag->save(array('id' => null, 'tag_id' => $tag_id, 'noticia_id' => $this->Noticia->id));
                            }
                            // $this->NoticiaNoticiaTag->query('INSERT INTO  noticia_noticia_tags (noticia_id, tag_id) VALUES (' . $this->Noticia->id . ', ' . $tag_id . ')');
                        }
                    }
                }
                //end: salva as tags das noticias

                //begin: salva a relação dos blocos
                if (isset($this->request->data['Noticia']['NoticiaBloco'])) {
                    App::import('Model', 'FdNoticias.NoticiaBloco');
                    $this->NoticiaBloco = new NoticiaBloco();

                    //disable
                    $this->NoticiaBloco->query('UPDATE noticia_blocos SET status_old = status, status = 0 WHERE noticia_id = ' . $this->Noticia->id);

                    $error = false;

                    if (is_array($this->request->data['Noticia']['NoticiaBloco']) && count($this->request->data['Noticia']['NoticiaBloco']) > 0) {
                        foreach ($this->request->data['Noticia']['NoticiaBloco'] as $key => $value) {
                            $bloco = array();
                            $bloco['NoticiaBloco']['id'] = null;
                            $bloco['NoticiaBloco']['noticia_id'] = $this->Noticia->id;
                            $bloco['NoticiaBloco']['bloco_id'] = $value;
                            $bloco['NoticiaBloco']['status'] = true;
                            if (!$this->NoticiaBloco->save($bloco)) {
                                $error = true;
                            }
                        }
                    } elseif (!is_array($this->request->data['Noticia']['NoticiaBloco']) && $this->request->data['Noticia']['NoticiaBloco'] != "") {
                        $bloco = array();
                        $bloco['NoticiaBloco']['id'] = null;
                        $bloco['NoticiaBloco']['noticia_id'] = $this->Noticia->id;
                        $bloco['NoticiaBloco']['bloco_id'] = $this->request->data['Noticia']['NoticiaBloco'];
                        $bloco['NoticiaBloco']['status'] = true;
                        if (!$this->NoticiaBloco->save($bloco)) {
                            $error = true;
                        }
                    }

                    if ($error == false) {
                        $this->NoticiaBloco->query('DELETE FROM  noticia_blocos WHERE status = 0 AND noticia_id = ' . $this->Noticia->id);
                    }
                }
                //end: salva a relação dos blocos

                //begin: salva o evento (se tiver)
                if (isset($this->request->data['Noticia']['evento']) && $this->request->data['Noticia']['evento'] == true) {
                    $noticia = $this->Noticia->find('first', array('conditions' => array('Noticia.id' => $this->Noticia->id)));

                    if (!empty($noticia)) {
                        App::import('Model', 'FdEventos.Evento');
                        $this->Evento = new Evento();

                        $new_event = array();
                        $new_event['Evento']['id'] = $this->request->data['Noticia']['evento_id'];
                        $new_event['Evento']['evento_tipo'] = 'evento';
                        $new_event['Evento']['noticia_id'] = $this->Noticia->id;
                        $new_event['Evento']['nome'] = $this->request->data['Noticia']['evento_nome'];
                        $new_event['Evento']['intervalo'] = $this->request->data['Noticia']['evento_intervalo'];
                        $new_event['Evento']['status'] = true;
                        if ($this->request->data['Noticia']['evento_link_noticia'] == true) {
                            $new_event['Evento']['noticia_seo_url'] = $noticia['Noticia']['seo_url'];
                        }

                        if ($this->Evento->save($new_event, false)) {
                            $this->Noticia->save(array('id' => $this->Noticia->id, 'evento_id' => $this->Evento->id), false);
                        }
                    }
                }
                //end: salva o evento (se tiver)

                //begin removo as outras variacoes de thumb, caso ainda tenham
                if (isset($this->request->data['Noticia']['thumb']['remove']) && $this->request->data['Noticia']['thumb']['remove'] == 1) {
                    $path = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'files' . DS . 'noticia' . DS . 'thumb';

                    $thumb = '250x250-' . $noticia['Noticia']['thumb'];
                    if (file_exists($path . DS . $noticia['Noticia']['thumb_dir'] . DS . $thumb)) {
                        unlink($path . DS . $noticia['Noticia']['thumb_dir'] . DS . $thumb);
                    }

                    $thumb = '330x150-' . $noticia['Noticia']['thumb'];
                    if (file_exists($path . DS . $noticia['Noticia']['thumb_dir'] . DS . $thumb)) {
                        unlink($path . DS . $noticia['Noticia']['thumb_dir'] . DS . $thumb);
                    }

                    $thumb = '700x258-' . $noticia['Noticia']['thumb'];
                    if (file_exists($path . DS . $noticia['Noticia']['thumb_dir'] . DS . $thumb)) {
                        unlink($path . DS . $noticia['Noticia']['thumb_dir'] . DS . $thumb);
                    }

                    $thumb = '700x400-' . $noticia['Noticia']['thumb'];
                    if (file_exists($path . DS . $noticia['Noticia']['thumb_dir'] . DS . $thumb)) {
                        unlink($path . DS . $noticia['Noticia']['thumb_dir'] . DS . $thumb);
                    }
                }
                //end removo as outras variacoes de thumb, caso ainda tenham

                //set caches
                $this->_resetCaches();
                $this->Session->setFlash(__('Registro salvo com sucesso.'), 'FdDashboard.alerts/fatorcms_success');

                //$this->redirect(array('action' => 'index'));
                $this->_redirectFilter($this->Session->read('referer'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'FdDashboard.alerts/fatorcms_danger');
            }
        } else {
            $options = array('conditions' => array('Noticia.' . $this->Noticia->primaryKey => $id));
            $this->request->data = $this->Noticia->find('first', $options);

            $this->Session->write('referer', $this->referer());

            // Seto a data atual, caso não seja definida
            if (!isset($this->request->data['Noticia']['data_publicacao']) || $this->request->data['Noticia']['data_publicacao'] == "") {
                App::import("helper", "String");
                $this->String = new StringHelper(new View(null));
                $this->request->data['Noticia']['data_publicacao'] = $this->String->dataFormatada("d/m/Y", date('Y-m-d'));
            }

            //formato as tags, para ficar no padrão do plugin de tags
            if (isset($this->request->data['NoticiaTag']) && count($this->request->data['NoticiaTag']) > 0) {
                foreach ($this->request->data['NoticiaTag'] as $key => $tag) {
                    $this->request->data['Noticia']['tags'][$tag['id'] . '-a'] = $tag['nome'];
                }
            }


            if (isset($this->request->data['NoticiaBloco']) && count($this->request->data['NoticiaBloco']) > 0) {
                foreach ($this->request->data['NoticiaBloco'] as $key => $valeu) {
                    $this->request->data['Noticia']['blocos_selecionados'][$valeu['bloco_id']] = $valeu['bloco_id'];
                }
            }
        }

        //tratamento para exibir paneas a url
        if ($this->IdiomaHelper->extrair($this->request->data['NoticiaAtributo'], 'titulo')) {
            foreach ($this->request->data['NoticiaAtributo'] as $index => $item) {
                $explode = array_reverse(explode('/', $item['seo_url']));
                $this->request->data['NoticiaAtributo'][$index]['seo_url'] = $explode[0];
            }
        }

        // set noticia
        $options = array('conditions' => array('Noticia.' . $this->Noticia->primaryKey => $id));
        $noticia = $this->Noticia->find('first', $options);
        $this->set(compact('noticia'));

        // set tipos de noticias
        $noticiaTipos = $this->Noticia->NoticiaTipo->find('list', array('fields' => array('NoticiaTipo.id', 'NoticiaTipo.nome'), 'conditions' => array('NoticiaTipo.status' => true), 'order' => array('NoticiaTipo.nome ASC')));
        $this->set(compact('noticiaTipos'));

        // set tags de noticias
        $this->loadModel('FdNoticias.NoticiaTag');
        $noticiaTags = $this->NoticiaTag->find('all', array('fields' => array('NoticiaTag.id', 'NoticiaTag.nome'), 'conditions' => array('NoticiaTag.status' => true), 'order' => array('NoticiaTag.nome ASC')));
        $tags = array();
        if (count($noticiaTags) > 0) {
            foreach ($noticiaTags as $key => $tag) {
                $tags[] = array('id' => $tag['NoticiaTag']['id'], 'label' => $tag['NoticiaTag']['nome'], 'value' => $tag['NoticiaTag']['nome']);
            }
        }
        $tags = json_encode($tags);
        $this->set(compact('tags'));

        // Lista as galerias do tipo noticias
        App::import('Model', 'FdGalerias.Galeria');
        $this->Galeria = new Galeria();
        $galeria_id = $this->Galeria->find('list', array('fields' => array('Galeria.id', 'Galeria.nome'), 'conditions' => array('Galeria.status' => true, 'Galeria.galeria_tipo_id' => $this->Galeria->noticias)));
        $this->set(compact('galeria_id'));

        // Lista as galerias do tipo interna
        App::import('Model', 'FdSac.SacTipo');
        $this->SacTipo = new SacTipo();;
        $sac_tipo = $this->SacTipo->find('first', array('conditions' => array('SacTipo.slug' => 'fale_conosco_interna')));
        $this->set(compact('sac_tipo'));

        // Lista os blocos do tipo noticia
        App::import('Model', 'FdBlocos.Bloco');
        $this->Bloco = new Bloco();;
        $blocos = $this->Bloco->find('list', array('contain' => array('BlocoTipo'), 'fields' => array('Bloco.id', 'Bloco.nome'), 'conditions' => array('Bloco.status' => true, 'BlocoTipo.slug' => 'noticias')));
        $this->set(compact('blocos'));
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function fatorcms_delete($id = null)
    {
        $this->Noticia->id = $id;
        if (!$this->Noticia->exists()) {
            throw new NotFoundException('Registro inválido.');
        }
        $this->request->is('get');
        if ($this->Noticia->delete()) {
            $this->_resetCaches();
            $this->Session->setFlash(__('Registro deletado.'), 'FdDashboard.alerts/fatorcms_success');
            // $this->redirect(array('action' => 'index'));
            $this->_redirectFilter($this->referer());
        }
        $this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
        // $this->redirect(array('action' => 'index'));
        $this->_redirectFilter($this->referer());
    }

    /**
     * fatorcms_status method
     *
     * @return void
     */
    public function fatorcms_status()
    {
        if (!$this->request->is('post')) {
            throw new NotFoundException('Registro inválido.');
        }
        echo $this->_saveStatus('Noticia', $this->request->data['id'], $this->request->data['value']);

        App::import('Model', 'FdRotas.Rota');
        $this->Rota = new Rota();
        $rota = $this->Rota->find('first', array('conditions' => array('Rota.model' => 'Noticia', 'Rota.row_id' => $this->request->data['id'])));
        if (!empty($rota)) {
            $this->Rota->save(array('id' => $rota['Rota']['id'], 'status' => $this->request->data['value']), false);
        }
        $this->_resetCaches();
        die;
    }
}