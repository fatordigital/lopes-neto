<?php
    //noticias
	Router::connect("/fatorcms/noticias", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true, 'tipo' => 1));
    Router::connect("/fatorcms/noticias/:action/*", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'prefix' => 'fatorcms', 'fatorcms' => true, 'tipo' => 1));

	Router::connect("/fatorcms/noticia_categorias", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticia_categorias', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true, 'tipo' => 1));
    Router::connect("/fatorcms/noticia_categorias/:action/*", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticia_categorias', 'prefix' => 'fatorcms', 'fatorcms' => true, 'tipo' => 1));
    
    //inquietos
    Router::connect("/fatorcms/inquietos", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true, 'tipo' => 2));
    Router::connect("/fatorcms/inquietos/:action/*", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'prefix' => 'fatorcms', 'fatorcms' => true, 'tipo' => 2));

    Router::connect("/fatorcms/inquieto_categorias", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticia_categorias', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true, 'tipo' => 2));
    Router::connect("/fatorcms/inquieto_categorias/:action/*", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticia_categorias', 'prefix' => 'fatorcms', 'fatorcms' => true, 'tipo' => 2));


    //responsabilidade_social
    Router::connect("/fatorcms/responsabilidade_social", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true, 'tipo' => 3));
    Router::connect("/fatorcms/responsabilidade_social/:action/*", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'prefix' => 'fatorcms', 'fatorcms' => true, 'tipo' => 3));
    Router::connect("/fatorcms/responsabilidade_social/*", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true, 'tipo' => 3));

    Router::connect("/fatorcms/responsabilidade_social_categorias", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticia_categorias', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true, 'tipo' => 3));
    Router::connect("/fatorcms/responsabilidade_social_categorias/:action/*", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticia_categorias', 'prefix' => 'fatorcms', 'fatorcms' => true, 'tipo' => 3));

    //uniritteremrede
    Router::connect("/fatorcms/uniritteremrede", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true, 'tipo' => 4));
    Router::connect("/fatorcms/uniritteremrede/:action/*", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'prefix' => 'fatorcms', 'fatorcms' => true, 'tipo' => 4));
    Router::connect("/fatorcms/uniritteremrede/*", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticias', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true, 'tipo' => 4));

    Router::connect("/fatorcms/uniritteremrede_categorias", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticia_categorias', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true, 'tipo' => 4));
    Router::connect("/fatorcms/uniritteremrede_categorias/:action/*", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticia_categorias', 'prefix' => 'fatorcms', 'fatorcms' => true, 'tipo' => 4));

    //others
	Router::connect("/fatorcms/noticia_tags", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticia_tags', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
    Router::connect("/fatorcms/noticia_tags/:action/*", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticia_tags', 'prefix' => 'fatorcms', 'fatorcms' => true));

    Router::connect("/fatorcms/noticia_tipos", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticia_tipos', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
    Router::connect("/fatorcms/noticia_tipos/:action/*", array('plugin' => 'fd_noticias', 'controller' => 'fd_noticia_tipos', 'prefix' => 'fatorcms', 'fatorcms' => true));
