$(document).ready(function(){
	$("[data-slug]").on('keyup', function(){
        $('[name="data[NoticiaAtributo]['+$(this).data('slug')+'][seo_url]"]').val(slugify($(this).val()));
    });

	$(".count_me").characterCounter({
		limit: '160',
		renderTotal: true,
		counterFormat: '[Recomenda-se utilizar no máximo 160 caracteres no resumo do conteúdo]. %1'
	});

	gerencia_campos_formulario($("[name='data[Noticia][formulario]']:checked").val());
	$("[name='data[Noticia][formulario]']").on('ifChecked', function(event){
	 	gerencia_campos_formulario($(this).val());
	});

	gerencia_campos_destaque($("[name='data[Noticia][destaque]']:checked").val());
	$("[name='data[Noticia][destaque]']").on('ifChecked', function(event){
	 	gerencia_campos_destaque($(this).val());
	});

	gerencia_campos_evento($("[name='data[Noticia][evento]']:checked").val());
	$("[name='data[Noticia][evento]']").on('ifChecked', function(event){
	 	gerencia_campos_evento($(this).val());
	});

	$("#NoticiaGaleriaId").select2({
        minimumInputLength: 2
    });

    $("#NoticiaNoticiaBloco").select2();

    $('.date-range-picker').daterangepicker({
		locale: {
			applyLabel: 'Aplicar',
			clearLabel: 'Limpar',
			fromLabel: 'De',
			toLabel: 'Até'
		},
		format: "DD/MM/YYYY"
	});
});

function gerencia_campos_formulario(value){
	if (value == '1') {
        $("[name='data[Noticia][formulario_email]']").removeAttr("disabled").closest('.row').show();
        $("[name='data[Noticia][formulario_titulo]']").removeAttr("disabled").closest('.row').show();
    } else if(value == '0'){
        $("[name='data[Noticia][formulario_email]']").attr("disabled", "disabled").closest('.row').hide();
        $("[name='data[Noticia][formulario_titulo]']").attr("disabled", "disabled").closest('.row').hide();
    }
}

function gerencia_campos_destaque(value){
	$("[name='data[Noticia][destaque_noticia_posicao]']").closest('.row').hide();
	$("[name='data[Noticia][destaque_inquietos_posicao]']").closest('.row').hide();
	
	if($("[name='data[Noticia][tipo]']").val() == '1'){
		if (value == '1') {
	        $("[name='data[Noticia][destaque_noticia_posicao]']").closest('.row').show();
	    } else if(value == '0'){
	        $("[name='data[Noticia][destaque_noticia_posicao]']").closest('.row').hide();
	    }
	}

	if($("[name='data[Noticia][tipo]']").val() == '2'){
		if (value == '1') {
	        $("[name='data[Noticia][destaque_inquietos_posicao]']").closest('.row').show();
	    } else if(value == '0'){
	        $("[name='data[Noticia][destaque_inquietos_posicao]']").closest('.row').hide();
	    }
	}
}

function gerencia_campos_evento(value){
	if (value == '1') {
        $("[name='data[Noticia][evento_nome]']").removeAttr("disabled").closest('.row').show();
        $("[name='data[Noticia][evento_intervalo]']").removeAttr("disabled").closest('.row').show();
        $("[name='data[Noticia][evento_link_noticia]']").closest('.row').show();
    } else if(value == '0'){
        $("[name='data[Noticia][evento_nome]']").attr("disabled", "disabled").closest('.row').hide();
        $("[name='data[Noticia][evento_intervalo]']").attr("disabled", "disabled").closest('.row').hide();
        $("[name='data[Noticia][evento_link_noticia]']").closest('.row').hide();
    }
}