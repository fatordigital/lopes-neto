$(document).ready(function(){
	$("[name='data[NoticiaTipo][nome]']").stringToSlug({
		getPut: '.slug'
	});

	gerencia_campos_formulario($("[name='data[NoticiaTipo][formulario]']").val());
	$("[name='data[NoticiaTipo][formulario]']").on('ifChecked', function(event){
	 	gerencia_campos_formulario($(this).val());
	});
});

function gerencia_campos_formulario(value){
	if (value == '1') {
        $("[name='data[NoticiaTipo][formulario_email]']").removeAttr("disabled").closest('.control-group').show();
        $("[name='data[NoticiaTipo][formulario_titulo]']").removeAttr("disabled").closest('.control-group').show();
    } else if(value == '0'){
        $("[name='data[NoticiaTipo][formulario_email]']").attr("disabled", "disabled").closest('.control-group').hide();
        $("[name='data[NoticiaTipo][formulario_titulo]']").attr("disabled", "disabled").closest('.control-group').hide();
    }
}