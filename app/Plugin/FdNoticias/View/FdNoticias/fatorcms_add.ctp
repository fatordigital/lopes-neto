<?php
echo $this->Html->script('FdDashboard.jquery.stringToSlug.min');

echo $this->Html->css('FdDashboard.core/tagedit/css/ui-lightness/jquery-ui-1.9.2.custom.min');
echo $this->Html->css('FdDashboard.core/tagedit/css/jquery.tagedit');
echo $this->Html->script('FdDashboard.core/tagedit/js/jquery.autoGrowInput');
echo $this->Html->script('FdDashboard.core/tagedit/js/jquery.tagedit');

echo $this->Html->css('FdDashboard.core/select2/select2');
echo $this->Html->script('FdDashboard.core/select2/select2');

echo $this->Html->css('FdDashboard.core/bootstrap-fileupload/bootstrap-fileupload');
echo $this->Html->script('FdDashboard.core/bootstrap-fileupload/bootstrap-fileupload');

echo $this->Html->script('FdDashboard.core/jquery-character-counter/jquery.charactercounter');

echo $this->Html->css('FdDashboard.date-time/datepicker');

echo $this->Html->css('FdDashboard.date-time/daterangepicker');

echo $this->Html->script('FdDashboard.date-time/bootstrap-datepicker.min');
echo $this->Html->script('FdDashboard.date-time/bootstrap-timepicker.min');

echo $this->Html->script('FdDashboard.date-time/moment.min');

echo $this->Html->script('FdDashboard.date-time/daterangepicker.min');
?>

<?php echo $this->Html->script('FdNoticias.fatorcms/noticia/crud.js'); ?>

<?php $this->Html->addCrumb('Notícias', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Cadastrar Notícia'); ?>

    <h3>Cadastrar Notícia</h3>

<?php if (isset($tags) && $tags != ""): ?>
    <script type="text/javascript">
        $(document).ready(function () {
            // Local Source
            var localJSON = <?php echo $tags; ?>;
            $('input.tag').tagedit({
                autocompleteOptions: {
                    source: localJSON
                }
            });
        });
    </script>
<?php endIf; ?>

<?php echo $this->Form->create('Noticia', array('type' => 'file', 'role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
<?php echo $this->element('FdNoticias.noticia_form') ?>
<?php echo $this->Form->end() ?>