<?php if (isset($noticia_tipo['NoticiaTipo'])) { ?>
    <?php $this->Html->addCrumb($noticia_tipo['NoticiaTipo']['nome']); ?>

    <div class="row">
         <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
            <?php echo $noticia_tipo['NoticiaTipo']['nome'] ?>
            <?php echo $this->Html->link('Cadastrar ' . $noticia_tipo['NoticiaTipo']['nome'], array('action' => 'add', 'tipo' => $this->params->params['tipo']), array('class' => 'btn btn-info pull-right')); ?>
        </h3>
    </div>

<?php } else { ?>

    <?php $this->Html->addCrumb('Notícias'); ?>

    <div class="row">
        <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
            <?php echo $this->Html->link('Cadastrar Notícia', array('action' => 'add', 'tipo' => $this->params->params['tipo']), array('class' => 'btn btn-info pull-right')); ?>
        </h3>
    </div>

<?php } ?>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <div class="row-fluid">
                    <div class="col-sm-12 col-md-10 col-lg-11">
                        <?php echo $this->FilterForm->create(null, array('role' => 'form', 'class' => 'form-inline')) ?>
                            <div class="form-group">
                                <?php echo $this->FilterForm->input('filtro_titulo', array('placeholder' => 'Filtrar por título', 'class' => 'form-control')) ?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->FilterForm->input('filtro_conteudo', array('placeholder' => 'Filtrar por conteúdo', 'class' => 'form-control')) ?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->FilterForm->input('filtro_noticia_tipo_id', array('placeholder' => 'Filtrar por tipo', 'class' => 'form-control')) ?>
                            </div>
                            <?php echo $this->FilterForm->submit('Filtrar', array('class' => 'btn btn-success', 'div' => false)) ?>
                            <?php echo $this->Html->link('Limpar Filtro', array('action' => 'index', 'tipo' => $this->params->params['tipo']), array('class' => 'btn btn-warning')) ?>
                        <?php echo $this->FilterForm->end() ?>
                    </div>
                    <div class="col-lg-1 visible-md visible-lg">
                       <?php echo $this->Element('FdDashboard.limit'); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-hover general-table">
                    <thead>
                        <tr>
                            <th style="width:60px"><?php echo $this->Paginator->sort('id', '#') ?></th>
                            <th><?php echo $this->Paginator->sort('titulo') ?></th>
                            <th><?php echo $this->Paginator->sort('seo_url', 'URL') ?></th>
                            <th><?php echo $this->Paginator->sort('modified', 'Modificação') ?></th>
                            <th style="width:180px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($noticias) > 0): ?>
                            <?php foreach($noticias as $y => $noticia):
                            ?>
                            <tr>
                                <td><?php echo $noticia['Noticia']['id'] ?></td>
                                <td><?php echo $this->Idioma->extrair($noticia['NoticiaAtributo'], 'titulo') ?></td>
                                <td><?php echo $this->Idioma->extrair($noticia['NoticiaAtributo'], 'seo_url') ?></td>
                                <td><?php echo $this->Time->format($noticia['Noticia']['modified'], '%d/%m/%Y %H:%M:%S') ?></td>
                                <td>
                                    <?php echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('action' => 'edit', 'tipo' => $this->params->params['tipo'], $noticia['Noticia']['id'], 'abas' => false), array('class' => 'btn btn-primary btn-sm', 'escape' => false)) ?>
                                    <?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('action' => 'delete', 'tipo' => $this->params->params['tipo'], $noticia['Noticia']['id'], 'abas' => false), array('class' => 'btn btn-danger btn-sm', 'escape' => false), __('Deseja mesmo remover o registro # %s?', $this->Idioma->extrair($noticia['NoticiaAtributo'], 'titulo'))) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="5" class="text-center">Nenhum registro encontrado.</td>
                            </tr>
                        <?php endIf; ?>
                    </tbody>
                </table>
				
				<?php
					$this->Paginator->options(
						array(
							'url'=> array(
								'controller' => 'fd_noticias',
								'action' => 'index',
								'fatorcms' => true, 
								'tipo' => $this->params->params['tipo']
							)
						)
					);
				?>

                <?php echo $this->Element('FdDashboard.paginator'); ?>
            </div>
        </section>
    </div>
</div>