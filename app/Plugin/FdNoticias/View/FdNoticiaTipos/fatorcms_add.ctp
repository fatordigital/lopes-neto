<?php echo $this->Html->script('FdDashboard.jquery.stringToSlug.min'); ?>
<?php echo $this->Html->script('FdNoticias.fatorcms/noticia_tipo/crud.js'); ?>

<?php echo $this->Html->css('FdDashboard.core/bootstrap-fileupload/bootstrap-fileupload') ?>
<?php echo $this->Html->script('FdDashboard.core/bootstrap-fileupload/bootstrap-fileupload'); ?>

<?php $this->Html->addCrumb('Notícias', array('controller' => 'noticias', 'action' => 'index')) ?>
<?php $this->Html->addCrumb('Tipos de Notícias', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Cadastrar Tipo') ?>

<h3>Cadastrar Tipo</h3>

<?php echo $this->Form->create('NoticiaTipo', array('type' => 'file', 'role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
<?php echo $this->Element('FdNoticias.noticia_tipo_form') ?>
<?php echo $this->Form->end() ?>