<?php $this->Html->addCrumb('Notícias', array('controller' => 'noticias', 'action' => 'index')) ?>
<?php $this->Html->addCrumb('Tipos de Notícias'); ?>

<div class="row">
     <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
        Tipos de Notícias 
        <?php echo $this->Html->link('Cadastrar Tipo', array('action' => 'add'), array('class' => 'btn btn-info pull-right')); ?>
    </h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <div class="row-fluid">
                    <div class="col-sm-12 col-md-10 col-lg-11">
                        <?php echo $this->FilterForm->create(null, array('role' => 'form', 'class' => 'form-inline')) ?>
                            <div class="form-group">
                                <?php echo $this->FilterForm->input('filtro_nome', array('placeholder' => 'Filtrar por nome', 'class' => 'form-control')) ?>
                            </div>
                            <?php echo $this->FilterForm->submit('Filtrar', array('class' => 'btn btn-success', 'div' => false)) ?>
                            <?php echo $this->Html->link('Limpar Filtro', array('action' => 'index'), array('class' => 'btn btn-warning')) ?>
                        <?php echo $this->FilterForm->end() ?>
                    </div>
                    <div class="col-lg-1 visible-md visible-lg">
                       <?php echo $this->Element('FdDashboard.limit'); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-hover general-table">
                    <thead>
                        <tr>
                            <th style="width:60px"><?php echo $this->Paginator->sort('id', '#') ?></th>
                            <th><?php echo $this->Paginator->sort('nome') ?></th>
                            <th><?php echo $this->Paginator->sort('seo_url', 'URL') ?></th>
							<th><?php echo $this->Paginator->sort('status') ?></th>
                            <th><?php echo $this->Paginator->sort('modified', 'Modificação') ?></th>
                            <th style="width:180px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($noticia_tipos) > 0): ?>
                            <?php foreach($noticia_tipos AS $y => $noticia_tipo): ?>
                            <tr>
                                <td><?php echo $noticia_tipo['NoticiaTipo']['id'] ?></td>
                                <td><?php echo $noticia_tipo['NoticiaTipo']['nome'] ?></td>
                                <td><?php echo $noticia_tipo['NoticiaTipo']['seo_url'] ?></td>
								<td>
                                    <input type="checkbox" class="atualiza-status" value="<?php echo $noticia_tipo['NoticiaTipo']['status'] == 1 ? 0 : 1 ?>" data-id="<?php echo $noticia_tipo['NoticiaTipo']['id'] ?>" data-url="<?php echo $this->Html->url(array('action' => 'status')) ?>" data-on-text="Sim" data-on-color="success" data-off-text="Não" data-off-color="danger"<?php echo $noticia_tipo['NoticiaTipo']['status'] == 1 ? ' checked="checked"' : '' ?>>
                                </td>
                                <td><?php echo $this->Time->format($noticia_tipo['NoticiaTipo']['modified'], '%d/%m/%Y %H:%M:%S') ?></td>
                                <td>
                                    <?php echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('action' => 'edit', $noticia_tipo['NoticiaTipo']['id'], 'abas' => false), array('class' => 'btn btn-primary btn-sm', 'escape' => false)) ?>
                                    <?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('action' => 'delete', $noticia_tipo['NoticiaTipo']['id'], 'abas' => false), array('class' => 'btn btn-danger btn-sm', 'escape' => false), __('Deseja mesmo remover o registro # %s?', $noticia_tipo['NoticiaTipo']['nome'])) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="5" class="text-center">Nenhum registro encontrado.</td>
                            </tr>
                        <?php endIf; ?>
                    </tbody>
                </table>

                <?php echo $this->Element('FdDashboard.paginator'); ?>
            </div>
        </section>
    </div>
</div>