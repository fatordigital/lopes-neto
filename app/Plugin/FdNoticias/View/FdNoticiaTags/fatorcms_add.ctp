<?php echo $this->Html->script('FdDashboard.jquery.stringToSlug.min'); ?>
<?php echo $this->Html->script('FdNoticias.fatorcms/noticia_tag/crud.js'); ?>

<?php $this->Html->addCrumb('Notícias', array('controller' => 'noticias', 'action' => 'index')) ?>
<?php $this->Html->addCrumb('Tags de Notícias', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Cadastrar Tag') ?>

    <h3>Cadastrar Tag</h3>

<?php echo $this->Form->create('NoticiaTag', array('type' => 'file', 'role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
<?php echo $this->Element('FdNoticias.tag_form') ?>
<?php echo $this->Form->end() ?>