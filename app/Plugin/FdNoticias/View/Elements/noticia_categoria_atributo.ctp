

<?php echo $this->element('FdDashboard.idiomas') ?>
<?php if (isset($idiomas)) { ?>
    <?php
    $model = 'NoticiaCategoriaAtributo';
    ?>
    <div class="panel">
        <div class="panel-body">
            <div class="tab-content">
                <?php foreach ($idiomas as $i => $idioma) { ?>

                    <?php echo $this->Form->hidden("{$model}.{$i}.idioma_id", array('value' => $idioma['Idioma']['id'])) ?>
                    <?php echo $this->Form->hidden("{$model}.{$i}.id") ?>

                    <div class="tab-pane fade in <?php echo $idioma['Idioma']['padrao'] == 1 ? 'active' : '' ?>"
                         id="<?php echo $idioma['Idioma']['slug'] ?>">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">
                                        Título
                                        [<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]
                                    </label>
                                    <?php echo $this->Form->text("{$model}.{$i}.titulo", array('div' => false, 'class' => 'form-control', 'data-slug' => $i)) ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">
                                        Descrição resumida
                                        [<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]
                                    </label>
                                    <?php echo $this->Form->textarea("{$model}.{$i}.resumo", array('maxlength' => '255', 'div' => false, 'class' => 'form-control')) ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">
                                        Seo Title
                                        [<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]
                                    </label>
                                    <?php echo $this->Form->input("{$model}.{$i}.seo_title", array('label' => false, 'div' => false, 'class' => 'form-control')) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">
                                        Seo Description
                                        [<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]
                                    </label>
                                    <?php echo $this->Form->input("{$model}.{$i}.seo_description", array('label' => false, 'type' => 'textarea', 'div' => false, 'class' => 'form-control count_me')) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">
                                        Seo Keywords
                                        [<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]
                                    </label>
                                    <?php echo $this->Form->input("{$model}.{$i}.seo_keywords", array('label' => false, 'div' => false, 'class' => 'form-control')) ?>
                                    <span class="help-block">Utilize ponto e vírgual (;) como separador das palavras-chave.</span>
                                </div>
                            </div>
                        </div>

                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>


<div class="panel">
    <div class="panel-body">
        <?php echo $this->Form->create('NoticiaCategoria', array('type' => 'file', 'role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
        <?php echo $this->Form->input('id') ?>
        <div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="form-group">
                    <label>Status</label>
                    <div class="icheck">
                        <?php echo $this->Form->input('status', array(
                            'type' => 'radio',
                            'options' => array(1 => 'Ativo', 0 => 'Inativo'),
                            'default' => 1,
                            'legend' => false,
                            'before' => '<div class="radio">',
                            'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">
																')); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="form-group">
                    <label>Tipo</label>
                    <div class="icheck">
                        <?php echo $this->Form->input('noticia_tipo_id', array(
                            'type' => 'select',
                            'options' => $noticia_tipos_ids,
                            'class'	=> 'form-control input-sm m-bot15',
                            'label' => false
                        )); ?>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('seo_url', array('label' => 'URL', 'div' => false, 'class' => 'form-control slug')) ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('ordem', array('label' => 'Ordem', 'div' => false, 'class' => 'form-control')) ?>
                </div>
            </div>
        </div>

        <?php echo $this->Form->submit('Salvar', array('name' => 'salvar', 'class' => 'btn btn-success', 'div' => false)) ?>
        <a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">Cancelar</a>

        <?php echo $this->Form->end() ?>
    </div>
</div>