<?php echo $this->Form->input('id') ?>
<?php echo $this->Form->hidden('tipo', array('value' => $this->params->params['tipo'])) ?>

<?php echo $this->element('FdDashboard.idiomas') ?>
<?php if (isset($idiomas)) { ?>
    <?php
    $model = 'NoticiaAtributo';
    ?>
    <div class="panel">
        <div class="panel-body">
            <div class="tab-content">
                <?php foreach ($idiomas as $i => $idioma) { ?>

                    <?php echo $this->Form->hidden("{$model}.{$i}.idioma_id", array('value' => $idioma['Idioma']['id'])) ?>
                    <?php echo $this->Form->hidden("{$model}.{$i}.id") ?>

                    <div class="tab-pane fade in <?php echo $idioma['Idioma']['padrao'] == 1 ? 'active' : '' ?>"
                         id="<?php echo $idioma['Idioma']['slug'] ?>">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">
                                        Título
                                        [<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]
                                    </label>
                                    <?php echo $this->Form->text("{$model}.{$i}.titulo", array('div' => false, 'class' => 'form-control', 'data-slug' => $i)) ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">
                                        Descrição resumida
                                        [<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]
                                    </label>
                                    <?php echo $this->Form->textarea("{$model}.{$i}.resumo", array('maxlength' => '255', 'div' => false, 'class' => 'form-control')) ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">
                                        Conteúdo[<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]
                                    </label>
                                    <?php echo $this->Form->textarea("{$model}.{$i}.conteudo", array('div' => false, 'class' => 'form-control editor')) ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">
                                        Link (URl) [<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]
                                    </label>
                                    <?php echo $this->Form->text("{$model}.{$i}.seo_url", array('label' => false, 'div' => false, 'class' => 'form-control')) ?>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">
                                        Seo Title
                                        [<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]
                                    </label>
                                    <?php echo $this->Form->input("{$model}.{$i}.seo_title", array('label' => false, 'div' => false, 'class' => 'form-control')) ?>
                                    <span class="help-block">Esse campo será alimentado com o nome da notícia, caso não seja preenchido.</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">
                                        Seo Description
                                        [<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]
                                    </label>
                                    <?php echo $this->Form->input("{$model}.{$i}.seo_description", array('label' => false, 'type' => 'textarea', 'div' => false, 'class' => 'form-control count_me')) ?>
                                    <span class="help-block">Esse campo será alimentado o resumo da notícia, caso não seja preenchido.</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">
                                        Seo Keywords
                                        [<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]
                                    </label>
                                    <?php echo $this->Form->input("{$model}.{$i}.seo_keywords", array('label' => false, 'div' => false, 'class' => 'form-control')) ?>
                                    <span class="help-block">Utilize ponto e vírgual (;) como separador das palavras-chave.</span>
                                </div>
                            </div>
                        </div>

                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>

<div class="panel">
    <div class="panel-body">
        <?php
        if ($auth['Grupo']['nome'] == 'Redator') {
            $status = array(0 => 'Inativo', 2 => 'Rascunho');
            $default = 2;
        } else {
            $status = array(1 => 'Ativo', 0 => 'Inativo', 2 => 'Rascunho');
            $default = 1;
        }
        ?>
        <div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="form-group">
                    <label>Status</label>
                    <div class="icheck">
                        <?php echo $this->Form->input('status', array(
                            'type' => 'radio',
                            'options' => $status, 'default' => $default, 'legend' => false, 'before' => '<div class="radio">', 'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="form-group">
                    <label>Destaque</label>
                    <div class="icheck">
                        <?php echo $this->Form->input('destaque', array(
                            'type' => 'radio',
                            'options' => array('0' => 'Inatvio', '1' => 'Ativo'), 'default' => $default, 'legend' => false, 'before' => '<div class="radio">', 'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="form-group">
                    <label>Categoria</label>
                    <div class="icheck">
                        <?php echo $this->Form->input('noticia_categoria_id', array(
                            'type' => 'select',
                            'options' => $noticia_categoria_id,
                            'class'	=> 'form-control input-sm m-bot15',
                            'label' => false
                        )); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="form-group">
                    <label>Tipo</label>
                    <div class="icheck">
                        <?php echo $this->Form->input('noticia_tipo_id', array(
                            'type' => 'select',
                            'options' => $noticia_tipos_ids,
                            'class'	=> 'form-control input-sm m-bot15',
                            'label' => false
                        )); ?>
                    </div>
                </div>
            </div>
        </div>

<!--        <div class="row">-->
<!--            <div class="col-md-6 col-lg-6">-->
<!--                <div class="form-group">-->
<!--                    <label>Posição de Destaque</label>-->
<!--                    <div class="icheck">-->
<!--                        --><?php //echo $this->Form->input('destaque_noticia_posicao', array(
//                            'type' => 'radio',
//                            'options' => array('TOPO' => 'Topo', 'ESQUERDA' => 'Esquerda', 'DIREITA' => 'Direita'), 'default' => 'TOPO', 'legend' => false, 'before' => '<div class="radio">', 'after' => '</div>',
//                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!---->
<!--        <div class="row">-->
<!--            <div class="col-md-6 col-lg-6">-->
<!--                <div class="form-group">-->
<!--                    <label>Posição de Destaque</label>-->
<!--                    <div class="icheck">-->
<!--                        --><?php //echo $this->Form->input('destaque_inquietos_posicao', array(
//                            'type' => 'radio',
//                            'options' => array('ESQUERDA' => 'Esquerda', 'DIREITA' => 'Direita'), 'default' => 'ESQUERDA', 'legend' => false, 'before' => '<div class="radio">', 'after' => '</div>',
//                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->

<!--        <div class="row">-->
<!--            <div class="col-md-6 col-lg-6">-->
<!--                <div class="form-group">-->
<!--                    <label>Visível</label>-->
<!--                    <div class="icheck">-->
<!--                        --><?php //echo $this->Form->input('visivel', array(
//                            'type' => 'radio',
//                            'options' => array('GERAL' => 'Geral', 'ALUNO' => 'Aluno', 'PROSPECT' => 'Prospect'),
//                            'default' => 'GERAL', 'legend' => false, 'before' => '<div class="radio">', 'after' => '</div>',
//                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->

        <div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="form-group">
                    <label>Criar Evento?</label>
                    <div class="icheck">
                        <?php echo $this->Form->input('evento', array(
                            'type' => 'radio', 'options' => array(1 => 'Sim', 0 => 'Não'), 'default' => 0, 'legend' => false, 'before' => '<div class="radio">', 'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <?php if (isset($this->request->data['Noticia']['tags']) && count($this->request->data['Noticia']['tags']) > 0): ?>
                        <div class="input text required">
                            <label>Tags</label>
                            <?php foreach ($this->request->data['Noticia']['tags'] as $key => $value): ?>
                                <?php echo $this->Form->input("Noticia.tags.{$key}", array('label' => false, 'value' => $value, 'class' => 'tag')); ?>
                            <?php endForeach; ?>
                        </div>
                    <?php else: ?>
                        <?php echo $this->Form->input("tags.", array('label' => 'Tags', 'multile' => true, 'class' => 'tag')); ?>
                    <?php endIf; ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('data_publicacao', array('type' => 'text', 'label' => 'Data de Publicação', 'div' => false, 'class' => 'form-control data')) ?>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label>Thumb</label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">

                        <?php if(isset($noticia) && $noticia['Noticia']['thumb'] != ""): ?>
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 119px;">
                                <img src="<?php echo $this->Html->Url('/'. $noticia['Noticia']['thumb_path'] .'/'. $noticia['Noticia']['thumb_dir'] . '/250x250-' . $noticia['Noticia']['thumb'] , true) ?>" width="200" />
                            </div>
                        <?php else: ?>
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=nenhuma+imagem" alt="" />
                            </div>
                        <?php endIf; ?>
                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>

                        <?php if(isset($noticia) && $noticia['Noticia']['thumb'] != ""): ?>
                            <div class="icheck minimal">
                                <?php echo $this->Form->input('Noticia.thumb.remove', array('type' => 'checkbox', 'label' => 'Remover Imagem Atual', 'div' => false)); ?>
                            </div>
                        <?php endIf; ?>

                        <div>
	                       <span class="btn btn-white btn-file">
	                           	<span class="fileupload-new"><i class="fa fa-paper-clip"></i> Selecionar imagem</span>
	                           	<span class="fileupload-exists"><i class="fa fa-undo"></i> Alterar</span>
                               <?php echo $this->Form->input('thumb', array('type' => 'file', 'class' => 'default', 'label' => false, 'div' => false)); ?>
	                       </span>
                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remover</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label>Banner</label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">

                        <?php if(isset($noticia) && $noticia['Noticia']['banner'] != ""): ?>
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 119px;">
                                <img src="<?php echo $this->Html->Url('/'. $noticia['Noticia']['banner_path'] .'/'. $noticia['Noticia']['banner_dir'] . '/400_' . $noticia['Noticia']['banner'] , true) ?>" width="200" />
                            </div>
                        <?php else: ?>
                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=nenhuma+imagem" alt="" />
                            </div>
                        <?php endIf; ?>
                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>

                        <?php if(isset($noticia) && $noticia['Noticia']['banner'] != ""): ?>
                            <div class="icheck minimal">
                                <?php echo $this->Form->input('Noticia.banner.remove', array('type' => 'checkbox', 'label' => 'Remover Imagem Atual', 'div' => false)); ?>
                            </div>
                        <?php endIf; ?>

                        <div>
	                       <span class="btn btn-white btn-file">
	                           	<span class="fileupload-new"><i class="fa fa-paper-clip"></i> Selecionar imagem</span>
	                           	<span class="fileupload-exists"><i class="fa fa-undo"></i> Alterar</span>
                               <?php echo $this->Form->input('banner', array('type' => 'file', 'class' => 'default', 'label' => false, 'div' => false)); ?>
	                       </span>
                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remover</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="form-group">
                    <?php echo $this->Form->input('banner_alone', array(
                        'type' => 'checkbox',
                        'legend' => false,
                        'label' => 'Exibir apenas esse banner'
                    )); ?>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('evento_nome', array('div' => false, 'class' => 'form-control', 'required' => 'required')) ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('evento_intervalo', array('type' => 'text', 'label' => 'Data', 'div' => false, 'class' => 'form-control date-range-picker', 'required' => 'required')) ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="form-group">
                    <label>Exibir link da notícia?</label>
                    <div class="icheck">
                        <?php echo $this->Form->input('evento_link_noticia', array(
                            'type' => 'radio',
                            'options' => array(1 => 'Sim', 0 => 'Não'),
                            'default' => 0,
                            'legend' => false,
                            'before' => '<div class="radio">',
                            'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">
																')); ?>
                    </div>
                </div>
            </div>
        </div>

        <?php if ($auth['Grupo']['nome'] != 'Redator'): ?>
            <?php echo $this->Form->submit('Salvar', array('name' => 'salvar', 'class' => 'btn btn-success', 'div' => false)) ?>
        <?php endIf; ?>
        <?php echo $this->Form->submit('Salvar como rascunho', array('name' => 'rascunho', 'class' => 'btn btn-primary', 'div' => false)) ?>
        <a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">Cancelar</a>
    </div>
</div>