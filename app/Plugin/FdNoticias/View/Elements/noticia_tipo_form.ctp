<div class="panel">
    <div class="panel-body">

        <?php echo $this->Form->input('id') ?>
        <div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="form-group">
                    <label>Status</label>
                    <div class="icheck">
                        <?php echo $this->Form->input('status', array(
                            'type' => 'radio',
                            'options' => array(1 => 'Ativo', 0 => 'Inativo'),
                            'default' => 1,
                            'legend' => false,
                            'before' => '<div class="radio">',
                            'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">
																')); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('nome', array('div' => false, 'class' => 'form-control')) ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('seo_url', array('label' => 'URL', 'div' => false, 'class' => 'form-control slug')) ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('seo_title', array('div' => false, 'class' => 'form-control')) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('seo_description', array('type' => 'textarea', 'div' => false,  'class' => 'form-control')) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('seo_keywords', array('div' => false, 'class' => 'form-control')) ?>
                </div>
            </div>
        </div>

        <br />

        <div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="form-group">
                    <label>Exibir Formulário?</label>
                    <div class="icheck">
                        <?php echo $this->Form->input('formulario', array(
                            'type' => 'radio',
                            'options' => array(1 => 'Sim', 0 => 'Não'),
                            'default' => 0,
                            'legend' => false,
                            'before' => '<div class="radio">',
                            'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-7">
                <div class="form-group">
                    <?php echo $this->Form->input('formulario_titulo', array('label' => 'Título do Formulário', 'type' => 'text', 'div' => false, 'class' => 'form-control')) ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-7">
                <div class="form-group">
                    <?php echo $this->Form->input('formulario_email', array('label' => 'Destinatário do Formulário (cópias)', 'type' => 'textarea', 'div' => false, 'class' => 'form-control')) ?>
                    <?php if(isset($sac_tipo) && $sac_tipo['SacTipo']['email'] != ""): ?>
                        <span>[Destinatários defaults: <?php echo $sac_tipo['SacTipo']['email']; ?>].</span><br />
                    <?php endIf; ?>
                    <span>[Separe com ponto e vírgula(;) caso deseja que o formulário dispare e-mail para mais de um contato].</span>
                </div>
            </div>
        </div>

        <?php /* não será usado no add
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group">
					<label>Conceitual</label>
					<div class="fileupload fileupload-new" data-provides="fileupload">
	                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
	                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=nenhuma+imagem" alt="" />
	                    </div>
	                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
	                    <div>
	                       <span class="btn btn-white btn-file">
	                           	<span class="fileupload-new"><i class="fa fa-paper-clip"></i> Selecionar imagem</span>
	                           	<span class="fileupload-exists"><i class="fa fa-undo"></i> Alterar</span>
	                           	<?php echo $this->Form->input('conceitual', array('type' => 'file', 'class' => 'default', 'label' => false, 'div' => false)); ?>
	                           	<?php echo $this->Form->hidden('conceitual_dir') ?>
	                       </span>
	                        <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remover</a>
	                    </div>
	                </div>
	                <span class="label label-danger">Atenção!</span>
	                <span>
	                 	A visualiação da miniatura da imagem antes do upload é suportado no mais recente Firefox, Chrome, Opera, Safari e Internet Explorer 10 unicamente.
	                </span>
				</div>
			</div>
		</div> */ ?>

        <?php echo $this->Form->submit('Salvar', array('name' => 'salvar', 'class' => 'btn btn-success', 'div' => false)) ?>
        <a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">Cancelar</a>
    </div>
</div>