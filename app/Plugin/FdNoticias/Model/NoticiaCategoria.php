<?php
App::uses('AppModel', 'Model');

/**
 * NoticiaCategoria Model
 *
 * @property NoticiaCategoria $NoticiaCategoria
 */
class NoticiaCategoria extends FdNoticiasAppModel
{

    public $actsAs = array('Containable');

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Noticia' => array(
            'className' => 'Noticia',
            'foreignKey' => 'noticia_categoria_id',
            'dependent' => false,
        ),
        'NoticiaCategoriaAtributo' => array(
            'className' => 'NoticiaCategoriaAtributo',
            'foreignKey' => 'noticia_categoria_id'
        )
    );


    public $belongsTo = array(
        'NoticiaTipo' => array(
            'className' => 'NoticiaTipo',
            'foreignKey' => 'noticia_tipo_id'
        )
    );

    public function beforeValidate($options = array())
    {
        //trata seo_url
        if (isset($this->data[$this->alias]['nome']) && (isset($this->data[$this->alias]['seo_url']) && $this->data[$this->alias]['seo_url'] == "")) {
            $this->data[$this->alias]['seo_url'] = strtolower(Inflector::slug($this->data[$this->alias]['nome'], '-'));
        }

        //add a categoria na url
        $params = Router::getParams();
        if (isset($params['tipo']) && $params['tipo'] != "") {
            App::import('Model', 'FdNoticias.NoticiaTipo');
            $this->NoticiaTipo = new NoticiaTipo();
            $noticia_tipo = $this->NoticiaTipo->find('first', array('recursive' => -1, 'conditions' => array('NoticiaTipo.id' => $params['tipo']), 'fields' => array('NoticiaTipo.seo_url')));
            if (!empty($noticia_tipo)) {
                if ($this->data[$this->alias]['id'] == null) {
                    $this->data[$this->alias]['seo_url'] = $noticia_tipo['NoticiaTipo']['seo_url'] . '/' . $this->data[$this->alias]['seo_url'];
                } else {
                    if (stripos($this->data[$this->alias]['seo_url'], '/')) {
                        $explode = array_reverse(explode('/', $this->data[$this->alias]['seo_url']));
                        $this->data[$this->alias]['seo_url'] = $explode[0];
                    }
                    $this->data[$this->alias]['seo_url'] = $noticia_tipo['NoticiaTipo']['seo_url'] . '/' . $this->data[$this->alias]['seo_url'];
                }
            }
        }
    }

    public function beforeSave($options = array())
    {
        //trata seo_title
        if (isset($this->data[$this->alias]['seo_title']) && $this->data[$this->alias]['seo_title'] == "") {
            $this->data[$this->alias]['seo_title'] = $this->data[$this->alias]['nome'];
        }

        parent::beforeSave($options = array());
    }

    public function afterSave($created, $options = array())
    {

        parent::afterSave($created, $options = array());

        //begin salva o registro da URL nas rotas
        $controller = 'NoticiaCategorias';


        $model = 'NoticiaCategoria';
        $action = 'index';
        $params_id = null;
        $params_value = null;

        //salvo a rota da categoria
        if (isset($this->data[$this->alias]['seo_url'])) {

            //Begin: customização para o tipo de noticia: Responsabilidade Social
            $cat = $this->find('first', array('conditions' => array('NoticiaCategoria.id' => $this->data[$this->alias]['id'])));
            if (!empty($cat) && $cat[$this->alias]['noticia_tipo_id'] == 3) {
                $controller = 'ResponsabilidadeSocial';
                $action = 'categoria';
            } elseif (!empty($cat) && $cat[$this->alias]['noticia_tipo_id'] == 4) {
                $controller = 'UniritterEmRede';
                $action = 'categoria';
            }

            //End: customização para o tipo de noticia: Responsabilidade Social

            App::import('Model', 'Rota');
            $this->Rota = new Rota();

            $rt = $this->Rota->find('first', array('conditions' => array(
                    'AND' => array(
                        'row_id' => $this->data[$this->alias]['id'],
                        'model' => $model
                    )
                )
                )
            );

            if ($rt) {
                $rota['id'] = $rt['Rota']['id'];
            } else {
                $rota['id'] = null;
            }

            $rota['controller'] = $controller;
            $rota['model'] = $model;
            $rota['action'] = $action;
            $rota['params_id'] = $params_id;
            $rota['params_value'] = $params_value;
            $rota['row_id'] = $this->data[$this->alias]['id'];
            $rota['seo_url'] = $this->data[$this->alias]['seo_url'];
            $rota['sites'] = '["portal"]';
            $this->Rota->save($rota);
        }
        //end salva o registro da URL nas rotas
    }

    public function afterDelete()
    {
        parent::afterDelete();

        App::import('Model', 'Rota');
        $this->Rota = new Rota();

        $rota = $this->Rota->find('first', array('conditions' => array('Rota.model' => 'NoticiaCategoria', 'Rota.row_id' => $this->id)));

        if (!empty($rota)) {
            $this->Rota->delete($rota['Rota']['id'], false);
        }

        //removo vinculo na mão, para evitar recursividade
        $this->Rota->query('UPDATE noticias SET noticia_categoria_id = null WHERE noticia_categoria_id = ' . $this->id);
    }

}
