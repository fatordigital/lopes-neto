<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');
App::uses('AuthComponent', 'Controller/Component');
App::uses('String', 'Helper');

class Noticia extends FdNoticiasAppModel
{

    public $cacheData;

    public $actsAs = array(
        'Containable',
        'Upload.Upload' => array(
            'thumb' => array(
                'fields' => array(
                    'dir' => 'thumb_dir',
                    'type' => 'thumb_type',
                    'size' => 'thumb_size',
                )
            ),
            'banner' => array(
                'fields' => array(
                    'dir' => 'banner_dir',
                    'type' => 'banner_type',
                    'size' => 'banner_size',
                ),
                'thumbnailMethod' => 'php',
                'thumbnailSizes' => array(
                    'thumb' => '100x100',
                    '400' => '400w',
                    '220' => '220x268'
                ),
            )
        )
    );

    public $validate = array(
        'noticia_tipo_id' => array(
            'notempty' => array(
                'rule' => array('notBlank'),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
        ),
        'noticia_categoria_id' => array(
            'notempty' => array(
                'rule' => array('notBlank'),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
        ),
        'titulo' => array(
            'notempty' => array(
                'rule' => array('notBlank'),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
        ),
        'conteudo' => array(
            'notempty' => array(
                'rule' => array('notBlank'),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
        ),
        'seo_url' => array(
            'url_unica' => array(
                'rule' => array('check_url_unica'),
                'message' => 'Url já existente. Tenta outra.',
            ),
        ),
        'evento_nome' => array(
            'notempty' => array(
                'rule' => array('notBlank'),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
        ),
        'evento_intervalo' => array(
            'notempty' => array(
                'rule' => array('notBlank'),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
        )
    );

    public $hasMany = array(
        'NoticiaBloco' => array(
            'className' => 'NoticiaBloco',
            'foreignKey' => 'noticia_id'
        ),
        'NoticiaAtributo' => array(
            'className' => 'NoticiaAtributo',
            'foreignKey' => 'noticia_id'
        )
    );

    public $belongsTo = array(
        'NoticiaTipo' => array(
            'className' => 'NoticiaTipo',
            'foreignKey' => 'noticia_tipo_id'
        ),
        'NoticiaCategoria' => array(
            'className' => 'NoticiaCategoria',
            'foreignKey' => 'noticia_categoria_id'
        ),
        'Evento' => array(
            'className' => 'Evento',
            'foreignKey' => 'evento_id'
        )
    );

    public $hasOne = array(
        'Rota' => array(
            'className' => 'Rota',
            'foreignKey' => 'row_id',
            'conditions' => array(
                'Rota.model' => 'Noticia'
            )
        )
    );

    public $hasAndBelongsToMany = array(
        'NoticiaTag' => array(
            'className' => 'NoticiaTag',
            'joinTable' => 'noticia_noticia_tags',
            'foreignKey' => 'noticia_id',
            'associationForeignKey' => 'tag_id'
        )
    );

    public function __construct($id = null, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        $this->cache();
    }
    
    public function cache()
    {
        $noticias = array();
        if (Cache::read('eventos') === false) {
            $noticias = $this->find('all', array(
                    'conditions' => array(
                        'Noticia.status' => 1
                    )
                )
            );
            Configure::write('noticias', $noticias);
        } else {
            $noticias = Cache::read('noticias');
        }
        $this->cacheData = $noticias;
    }

    public function beforeValidate($options = array())
    {

        //trata seo_url
        if (isset($this->data[$this->alias]['titulo']) && $this->data[$this->alias]['seo_url'] == "") {
            $this->data[$this->alias]['seo_url'] = strtolower(Inflector::slug($this->data[$this->alias]['titulo'], '-'));
        }

        //add a categoria na url
        $params = Router::getParams();


        if (isset($this->data[$this->alias]['noticia_categoria_id']) && $this->data[$this->alias]['noticia_categoria_id'] != "") {
            App::import('Model', 'FdNoticias.NoticiaCategoria');
            $this->NoticiaCategoria = new NoticiaCategoria();
            $noticia_categoria = $this->NoticiaCategoria->find('first', array('recursive' => -1, 'conditions' => array('NoticiaCategoria.id' => $this->data[$this->alias]['noticia_categoria_id']), 'fields' => array('NoticiaCategoria.seo_url')));
            if (!empty($noticia_categoria)) {
                if ($this->data[$this->alias]['id'] == null) {
                    foreach ($this->data['NoticiaAtributo'] as $key => $noticia_atributo) {
                        if (stripos($noticia_atributo['seo_url'], '/')) {
                            $explode = array_reverse(explode('/', $noticia_atributo['seo_url']));
                            $this->data['NoticiaAtributo'][$key]['seo_url'] = $explode[0];
                            $this->data['NoticiaAtributo'][$key]['seo_url'] = $noticia_categoria['NoticiaCategoria']['seo_url'] . '/' . $noticia_atributo['seo_url'];
                        }
                    }
                } else {
                    foreach ($this->data['NoticiaAtributo'] as $key => $noticia_atributo) {
                        if (stripos($noticia_atributo['seo_url'], '/')) {
                            $explode = array_reverse(explode('/', $noticia_atributo['seo_url']));
                            $this->data['NoticiaAtributo'][$key]['seo_url'] = $explode[0];
                            $this->data['NoticiaAtributo'][$key]['seo_url'] = $noticia_categoria['NoticiaCategoria']['seo_url'] . '/' . $noticia_atributo['seo_url'];
                        }
                    }
                }
            }
        } elseif (isset($params['tipo']) && $params['tipo'] != "") {
            App::import('Model', 'FdNoticias.NoticiaTipo');
            $this->NoticiaTipo = new NoticiaTipo();
            $noticia_tipo = $this->NoticiaTipo->find('first', array('recursive' => -1, 'conditions' => array('NoticiaTipo.id' => $params['tipo']), 'fields' => array('NoticiaTipo.seo_url')));
            if (!empty($noticia_tipo)) {
                if ($this->data[$this->alias]['id'] == null) {
                    foreach ($this->data['NoticiaAtributo'] as $key => $noticia_atributo) {
                        if (stripos($noticia_atributo['seo_url'], '/')) {
                            $explode = array_reverse(explode('/', $noticia_atributo['seo_url']));
                            $this->data['NoticiaAtributo'][$key]['seo_url'] = $explode[0];
                            $this->data['NoticiaAtributo'][$key]['seo_url'] = $noticia_tipo['NoticiaTipo']['seo_url'] . '/' . $noticia_atributo['seo_url'];
                        }
                    }
                } else {
                    foreach ($this->data['NoticiaAtributo'] as $key => $noticia_atributo) {
                        if (stripos($noticia_atributo['seo_url'], '/')) {
                            $explode = array_reverse(explode('/', $noticia_atributo['seo_url']));
                            $this->data['NoticiaAtributo'][$key]['seo_url'] = $explode[0];
                            $this->data['NoticiaAtributo'][$key]['seo_url'] = $noticia_tipo['NoticiaTipo']['seo_url'] . '/' . $noticia_atributo['seo_url'];
                        }
                    }
                }
            }
        }


        App::import('Model', 'FdRotas.Rota');
        $this->Rota = new Rota();

        foreach ($this->data['NoticiaAtributo'] as $key => $noticia_atributo) {
            if ($noticia_atributo['seo_url']) {
                $rota = $this->Rota->find('all', array('conditions' => array('seo_url' => $noticia_atributo['seo_url'])));
                if ($rota) {
                    $this->data['NoticiaAtributo'][$key]['seo_url'] = $noticia_atributo['seo_url'] . '-' . (count($rota) + 1);
                }
            }
        }

    }


    public function beforeSave($options = array())
    {
        App::import("helper", "String");
        $this->String = new StringHelper(new View(null));

        $user = AuthComponent::user();
        $this->data[$this->alias]['usuario_id'] = $user['id'];

        $data = $this->data[$this->alias];

        if (isset($data['data_publicacao']) && !empty($data['data_publicacao']) && $this->String->valid_date($data['data_publicacao'])) {
            $this->data[$this->alias]['data_publicacao'] = $this->String->dataFormatada("Y-m-d", $this->data[$this->alias]['data_publicacao']);
        } else {
            $this->data[$this->alias]['data_publicacao'] = date('Y-m-d');
        }
        

        App::import('Model', 'FdNoticias.NoticiaCategoria');
        $this->NoticiaCategoria = new NoticiaCategoria();
        $noticia_categoria = $this->NoticiaCategoria->find('first', array(
                'recursive' => -1,
                'conditions' => array(
                    'NoticiaCategoria.id' => $this->data[$this->alias]['noticia_categoria_id']
                ),
                'fields' => array('NoticiaCategoria.seo_url')
            )
        );


        if (isset($this->data['NoticiaAtributo'])) {
            $no_a = $this->data['NoticiaAtributo'];
            foreach ($no_a as $key => $noticia_atributo) {
                if (isset($noticia_atributo['NoticiaAtributo']['seo_url']) && $noticia_atributo['NoticiaAtributo']['seo_url'] != '') {
                    $this->data['NoticiaAtributo'][$key]['NoticiaAtributo']['seo_url'] = $noticia_categoria['NoticiaCategoria']['seo_url'] . '/' . strtolower(Inflector::slug($noticia_atributo['NoticiaAtributo']['seo_url']));
                } else {
                    $this->data['NoticiaAtributo'][$key]['NoticiaAtributo']['seo_url'] = $noticia_categoria['NoticiaCategoria']['seo_url'] . '/' . strtolower(Inflector::slug($noticia_atributo['NoticiaAtributo']['titulo']));
                }
            }
        }

        if (isset($this->data[$this->alias]['titulo']) && (isset($this->data[$this->alias]['seo_url']) && $this->data[$this->alias]['seo_url'] == "")) {
            $this->data[$this->alias]['seo_url'] = strtolower(Inflector::slug($this->data[$this->alias]['titulo'], '-'));
        }

        if (isset($this->data[$this->alias]['seo_title']) && $this->data[$this->alias]['seo_title'] == "") {
            $this->data[$this->alias]['seo_title'] = $this->data[$this->alias]['titulo'];
        }

        if (isset($this->data[$this->alias]['conteudo'])) {
            $conteudo = utf8_encode(html_entity_decode(strip_tags($this->data[$this->alias]['conteudo'])));
            if (isset($this->data[$this->alias]['descricao_resumida']) && $this->data[$this->alias]['descricao_resumida'] == "") {
                $this->data[$this->alias]['descricao_resumida'] = $this->String->truncate_str($conteudo, 150);
                if (strlen($conteudo) > 150) {
                    $this->data[$this->alias]['descricao_resumida'] .= "...";
                }
            }

            if (isset($this->data[$this->alias]['seo_description']) && $this->data[$this->alias]['seo_description'] == "") {
                $this->data[$this->alias]['seo_description'] = $this->data[$this->alias]['descricao_resumida'];
            }

            if (isset($this->data[$this->alias]['seo_description']) && $this->data[$this->alias]['seo_description'] == "") {
                $this->data[$this->alias]['seo_description'] = $this->String->truncate_str(strip_tags($this->data[$this->alias]['conteudo']), 150);
            }

            if (isset($this->data[$this->alias]['conteudo']) && $this->data[$this->alias]['conteudo'] != "") {
                $this->data[$this->alias]['conteudo'] = str_replace('src="../../app/', 'src="/app/', $this->data[$this->alias]['conteudo']);
                $this->data[$this->alias]['conteudo'] = str_replace('src="../../../app/', 'src="/app/', $this->data[$this->alias]['conteudo']);
            }
        }

        if ($user['grupo_id'] == 8) {
            $this->data[$this->alias]['status'] = 2;
        }

        parent::beforeSave($options = array());
    }

    public function afterSave($created, $options = array())
    {

        parent::afterSave($created, $options = array());

        $noticia = $this->find('first', array('recursive' => -1, 'conditions' => array('Noticia.id' => $this->id), 'fields' => array('thumb', 'thumb_dir', 'destaque', 'noticia_tipo_id')));

        //begin salva o registro da URL nas rotas
        $controller = 'Noticias';
        $model = 'Noticia';
        $action = 'detalhe';
        $params_id = null;
        $params_value = null;

        App::import('Model', 'Rota');
        $this->Rota = new Rota();

        if (isset($this->data['NoticiaAtributo'])) {

            foreach ($this->data['NoticiaAtributo'] as $key => $item) {
                if (!empty($item['NoticiaAtributo']['seo_url'])) {
                    $rt = $this->Rota->find('first', array('conditions' => array(
                            'AND' => array(
                                'row_id' => $this->data[$this->alias]['id'],
                                'model' => $model
                            )
                        )
                        )
                    );

                    if ($rt) {
                        $rota['id'] = $rt['Rota']['id'];
                    } else {
                        $rota['id'] = null;
                    }

                    $rota['controller'] = $controller;
                    $rota['model'] = $model;
                    $rota['action'] = $action;
                    $rota['params_id'] = $params_id;
                    $rota['params_value'] = $params_value;
                    $rota['row_id'] = $this->data[$this->alias]['id'];
                    $rota['seo_url'] = $item['NoticiaAtributo']['seo_url'];
                    $rota['buscavel'] = true;
                    $rota['buscavel_ordem'] = 2;
                    $rota['sites'] = '["portal"]';
                    $rota['status'] = $this->data[$this->alias]['status'];

                    if (isset($this->data[$this->alias]['titulo'])) {
                        $rota['nome'] = $item['NoticiaAtributo']['titulo'];
                    }
                    if (isset($this->data[$this->alias]['conteudo'])) {
                        $rota['conteudo'] = $item['NoticiaAtributo']['conteudo'];

                        $rota['conteudo'] = strip_tags($rota['conteudo']);
                    }
                    if (isset($this->data[$this->alias]['seo_keywords'])) {
                        $rota['seo_keywords'] = $item['NoticiaAtributo']['seo_keywords'];
                    }
                    if (isset($this->data[$this->alias]['seo_description'])) {
                        $rota['seo_description'] = $item['NoticiaAtributo']['seo_description'];
                    }
                    if (isset($this->data[$this->alias]['data_publicacao'])) {
                        $rota['data'] = $this->data[$this->alias]['data_publicacao'];
                    }

                    $this->Rota->save($rota);
                }
            }
        }


        //begin rename files
        $path = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'files' . DS . 'noticia' . DS . 'thumb';
        if (isset($this->data[$this->alias]['thumb']) && $this->data[$this->alias]['thumb'] != "") {

            $pre_new_name = strtolower(Inflector::slug($this->data['Noticia']['thumb'], '-'));
            $pre_new_name = str_replace("_", "-", $pre_new_name);
            $pre_old_name = $this->data[$this->alias]['thumb'];

            $new_name = $pre_new_name;
            $old_name = $pre_old_name;

            if (file_exists($path . DS . $this->id . DS . $old_name)) {
                $file = $path . DS . $this->id . DS . $old_name;
                $infos = pathinfo($file);
                $new_file = $path . DS . $this->id . DS . $new_name . '.' . $infos['extension'];

                if (rename($file, $new_file)) {
                    $this->save(array('thumb' => $new_name . '.' . $infos['extension'], 'thumb_dir' => $this->id), array('callbacks' => false, 'validate' => false));
                    // $this->query('UPDATE noticias SET thumb = "' . $new_name . '.' . $infos['extension'] . '" WHERE id = ' . $this->id);

                    //load lib
                    include_once(ROOT . DS . APP_DIR . DS . 'Lib' . DS . 'Wideimage/WideImage.php');

                    if (isset($this->data[$this->alias]['thumb_w']) && $this->data[$this->alias]['thumb_w'] != "") {
                        WideImage::load($new_file)->resize(610)->saveToFile($new_file);
                        WideImage::load($new_file)->crop($this->data[$this->alias]['thumb_x'], $this->data[$this->alias]['thumb_y'], $this->data[$this->alias]['thumb_w'], $this->data[$this->alias]['thumb_h'])->saveToFile($new_file);
                    }

                    $caminho_novo = $path . DS . $this->id;

                    $outFile = $caminho_novo . DS . '700x400-' . $pre_new_name . '.' . $infos['extension'];
                    WideImage::load($new_file)->resize(700)->crop('center', 'center', 700, 400)->saveToFile($outFile);

                    $outFile = $caminho_novo . DS . '700x258-' . $pre_new_name . '.' . $infos['extension'];
                    WideImage::load($new_file)->resize(700)->crop('center', 'center', 700, 258)->saveToFile($outFile);

                    $outFile = $caminho_novo . DS . '330x150-' . $pre_new_name . '.' . $infos['extension'];
                    WideImage::load($new_file)->resize(330)->crop('center', 'center', 330, 150)->saveToFile($outFile);

                    $outFile = $caminho_novo . DS . '250x250-' . $pre_new_name . '.' . $infos['extension'];
                    WideImage::load($new_file)->resize(250)->crop('center', 'center', 250, 250)->saveToFile($outFile);

                    $outFile = $caminho_novo . DS . '220x268-' . $pre_new_name . '.' . $infos['extension'];
                    WideImage::load($new_file)->resize(220)->crop('center', 'center', 220, 268)->saveToFile($outFile);

                    $outFile = $caminho_novo . DS . '300x300-' . $pre_new_name . '.' . $infos['extension'];
                    WideImage::load($new_file)->resize(600, 600)->crop('center', 'center', 300, 300)->saveToFile($outFile);
                }
            }
        } elseif (isset($this->data[$this->alias]['thumb_w']) && $this->data[$this->alias]['thumb_w'] != "") {
            $new_file = $path . DS . $noticia[$this->alias]['thumb_dir'] . DS . $noticia[$this->alias]['thumb'];

            //load lib
            include_once(ROOT . DS . APP_DIR . DS . 'Lib' . DS . 'Wideimage/WideImage.php');

            if (isset($this->data[$this->alias]['thumb_w']) && $this->data[$this->alias]['thumb_w'] != "") {
                if (strtolower($infos['extension']) == 'jpg') {
                    WideImage::load($new_file)->resize(610)->saveToFile($new_file, 80);
                    WideImage::load($new_file)->crop($this->data[$this->alias]['thumb_x'], $this->data[$this->alias]['thumb_y'], $this->data[$this->alias]['thumb_w'], $this->data[$this->alias]['thumb_h'])->saveToFile($new_file, 80);
                } else {
                    WideImage::load($new_file)->resize(610)->saveToFile($new_file);
                    WideImage::load($new_file)->crop($this->data[$this->alias]['thumb_x'], $this->data[$this->alias]['thumb_y'], $this->data[$this->alias]['thumb_w'], $this->data[$this->alias]['thumb_h'])->saveToFile($new_file);
                }
            }

            $caminho_novo = $path . DS . $this->id;

            // $outFile    = $caminho_novo.DS.'700x400-'.$pre_new_name . '.' . $infos['extension'];
            // WideImage::load($new_file)->resize(700)->crop('center', 'center', 700, 400)->saveToFile($outFile);

            $outFile = $caminho_novo . DS . '700x258-' . $pre_new_name . '.' . $infos['extension'];
            if (strtolower($infos['extension']) == 'jpg') {
                WideImage::load($new_file)->resize(700)->crop('center', 'center', 700, 258)->saveToFile($outFile, 80);
            } else {
                WideImage::load($new_file)->resize(700)->crop('center', 'center', 700, 258)->saveToFile($outFile);
            }

            $outFile = $caminho_novo . DS . '330x150-' . $pre_new_name . '.' . $infos['extension'];
            if (strtolower($infos['extension']) == 'jpg') {
                WideImage::load($new_file)->resize(330)->crop('center', 'center', 330, 150)->saveToFile($outFile, 80);
            } else {
                WideImage::load($new_file)->resize(330)->crop('center', 'center', 330, 150)->saveToFile($outFile);
            }

            $outFile = $caminho_novo . DS . '250x250-' . $pre_new_name . '.' . $infos['extension'];
            if (strtolower($infos['extension']) == 'jpg') {
                WideImage::load($new_file)->resize(250)->crop('center', 'center', 250, 250)->saveToFile($outFile, 80);
            } else {
                WideImage::load($new_file)->resize(250)->crop('center', 'center', 250, 250)->saveToFile($outFile);
            }
        }
        //end rename files


        //begin rename files
        if (isset($this->data[$this->alias]['banner']) && $this->data[$this->alias]['banner'] != "") {


            if (isset($this->data['NoticiaAtributo'][0]['NoticiaAtributo']['titulo'])) {
                $pre_new_name = strtolower(Inflector::slug($this->data['Noticia']['banner'], '-'));
                $pre_new_name = str_replace("_", "-", $pre_new_name);
                $path = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'files' . DS . 'banner' . DS . 'banner';
            }
            $pre_old_name = $this->data[$this->alias]['banner'];

            $new_name = $pre_new_name;
            $old_name = $pre_old_name;

            if (file_exists($path . DS . $noticia[$this->alias]['banner_dir'] . DS . $old_name)) {
                $file = $path . DS . $noticia[$this->alias]['banner_dir'] . DS . $old_name;
                $infos = pathinfo($file);
                $new_file = $path . DS . $noticia[$this->alias]['banner_dir'] . DS . $new_name . '.' . $infos['extension'];

                if (rename($file, $new_file)) {

                    //load lib
                    include_once(ROOT . DS . APP_DIR . DS . 'Lib' . DS . 'Wideimage/WideImage.php');

                    $this->query('UPDATE noticias SET banner = "' . $new_name . '.' . $infos['extension'] . '" WHERE id = ' . $this->id);

                    $caminho_novo = $path . DS . $noticia[$this->alias]['banner_dir'];
                    $outFile = $caminho_novo . DS . $pre_new_name . '.' . $infos['extension'];
                    if (strtolower($infos['extension']) == 'jpg') {
                        WideImage::load($new_file)->resize(1500)->crop('center', 'top', 1500, 370)->saveToFile($outFile, 80);
                    } else {
                        WideImage::load($new_file)->resize(1500)->crop('center', 'top', 1500, 370)->saveToFile($outFile);
                    }

                    //outros renames
                    $file = $path . DS . $noticia[$this->alias]['banner_dir'] . DS . '400_' . $old_name;
                    $infos = pathinfo($file);
                    $new_file = $path . DS . $noticia[$this->alias]['banner_dir'] . DS . '400_' . $new_name . '.' . $infos['extension'];
                    rename($file, $new_file);

                    $file = $path . DS . $noticia[$this->alias]['banner_dir'] . DS . 'thumb_' . $old_name;
                    $infos = pathinfo($file);
                    $new_file = $path . DS . $noticia[$this->alias]['banner_dir'] . DS . 'thumb_' . $new_name . '.' . $infos['extension'];
                    rename($file, $new_file);
                }
            }
        }
    }

    public function beforeFind($queryData)
    {

        parent::beforeFind($queryData);

        $params = Router::getParams();

        if (!isset($params['fatorcms']) || $params['fatorcms'] == false) {
            App::import("helper", "String");
            $this->String = new StringHelper(new View(null));
            if (is_array($queryData['conditions'])) {
                $queryData['conditions'] = array_merge($queryData['conditions'], array('Noticia.visivel' => array('GERAL', Configure::read('NAVEGACAO_TIPO'))));
            }
        }

        return $queryData;

    }

    public function afterFind($results, $primary = false)
    {
        App::import("helper", "String");
        $this->String = new StringHelper(new View(null));

        if (!empty($results)) {
            foreach ($results as $k => &$r) {
                if (isset($r[$this->alias])) {
                    if (isset($r[$this->alias]['data_publicacao']) && $r[$this->alias]['data_publicacao'] != "") {
                        @$r[$this->alias]['data_publicacao'] = $this->String->dataFormatada("d/m/Y", $r[$this->alias]['data_publicacao']);
                    }
                }
            }
        }

        return $results;
    }

    public function afterDelete()
    {
        parent::afterDelete();

        App::import('Model', 'Rota');
        $this->Rota = new Rota();

        $rota = $this->Rota->find('first', array('conditions' => array('Rota.model' => 'Noticia', 'Rota.row_id' => $this->id)));

        if (!empty($rota)) {
            $this->Rota->delete($rota['Rota']['id'], false);
        }

        //removo vinculo na mão, para evitar recursividade
        $this->Rota->query('DELETE FROM noticia_noticia_tags WHERE noticia_id = ' . $this->id);
    }
}