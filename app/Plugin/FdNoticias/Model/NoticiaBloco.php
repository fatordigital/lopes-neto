<?php
App::uses('AppModel', 'Model');
/**
 * NoticiaBloco Model
 *
 * @property NoticiaBloco $NoticiaBloco
 */
class NoticiaBloco extends FdNoticiasAppModel {

	public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'noticia_id' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
		'bloco_id' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */

	public $belongsTo = array(
		'Noticia' => array(
			'className' => 'Noticia',
			'foreignKey' => 'noticia_id'
		),
		'FdBlocos.Bloco' => array(
			'className' => 'Bloco',
			'foreignKey' => 'bloco_id'
		)
	);
}
