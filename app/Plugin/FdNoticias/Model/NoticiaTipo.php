<?php
App::uses('AppModel', 'Model');

/**
 * NoticiaTipo Model
 *
 * @property NoticiaTipo $NoticiaTipo
 */
class NoticiaTipo extends FdNoticiasAppModel
{

    //ids dos tipos
    public $noticias = 1;
    public $inquietos = 2;
    public $responsabilidade_social = 3;
    public $unirriteremrede = 4;

    public $actsAs = array(
        'Containable',
        'Upload.Upload' => array(
            'conceitual' => array(
                'fields' => array(
                    'dir' => 'conceitual_dir',
                    'type' => 'conceitual_type',
                    'size' => 'conceitual_size',
                ),
                'thumbnailMethod' => 'php',
                'thumbnailSizes' => array(
                    'thumb' => '100x100',
                    '400' => '400w'
                )
            )
        )
    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'nome' => array(
            'notempty' => array(
                'rule' => array('notBlank'),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Noticia' => array(
            'className' => 'Noticia',
            'foreignKey' => 'noticia_tipo_id'
        )
    );

    public $cacheData;


    public function beforeValidate($options = array())
    {
        //trata seo_url
        if (isset($this->data[$this->alias]['nome']) && (isset($this->data[$this->alias]['seo_url']) && $this->data[$this->alias]['seo_url'] == "")) {
            // $this->data[$this->alias]['seo_url'] = 	'noticia/'.strtolower(Inflector::slug($this->data[$this->alias]['nome'], '-'));
            $this->data[$this->alias]['seo_url'] = strtolower(Inflector::slug($this->data[$this->alias]['nome'], '-'));
        }

        // if(stripos($this->data[$this->alias]['seo_url'], 'noticia/') === false){
        // 	$this->data[$this->alias]['seo_url'] = 'noticia/' . $this->data[$this->alias]['seo_url'];
        // }
    }

    public function beforeSave($options = array())
    {
        //trata seo_title
        if (isset($this->data[$this->alias]['seo_title']) && $this->data[$this->alias]['seo_title'] == "") {
            $this->data[$this->alias]['seo_title'] = $this->data[$this->alias]['nome'];
        }

        parent::beforeSave($options = array());
    }

    public function afterSave($created, $options = array())
    {
        parent::afterSave($created, $options = array());

        //begin salva o registro da URL nas rotas
        $controller = 'NoticiaTipos';
        $model = 'NoticiaTipo';
        $action = 'index';
        $params_id = null;
        $params_value = null;

        //salvo a rota da categoria
        if (isset($this->data[$this->alias]['seo_url'])) {
            App::import('Model', 'Rota');
            $this->Rota = new Rota();

            $rt = $this->Rota->find('first', array('conditions' => array(
                    'AND' => array(
                        'row_id' => $this->data[$this->alias]['id'],
                        'model' => $model
                    )
                )
                )
            );

            if ($rt) {
                $rota['id'] = $rt['Rota']['id'];
            } else {
                $rota['id'] = null;
            }

            $rota['controller'] = $controller;
            $rota['model'] = $model;
            $rota['action'] = $action;
            // $rota['params_id'] 		= $params_id;
            // $rota['params_value'] 	= $params_value;
            $rota['row_id'] = $this->data[$this->alias]['id'];
            $rota['seo_url'] = $this->data[$this->alias]['seo_url'];
            $rota['sites'] = '["portal"]';
            $this->Rota->save($rota);
        }
        //end salva o registro da URL nas rotas

        //get dados atualizados do banner
        $dados = $this->find('first', array('recursive' => -1, 'conditions' => array('id' => $this->id), 'fields' => array('conceitual', 'conceitual_dir')));

        //begin rename files
        if (isset($this->data[$this->alias]['conceitual']) && $this->data[$this->alias]['conceitual'] != "") {
            if (isset($this->data[$this->alias]['nome'])) {
                $pre_new_name = strtolower(Inflector::slug($this->data[$this->alias]['nome'], '-'));
                $pre_new_name = str_replace("_", "-", $pre_new_name);
                $path = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'files' . DS . 'noticia_tipo' . DS . 'conceitual';
            }
            $pre_old_name = $this->data[$this->alias]['conceitual'];

            $new_name = $pre_new_name;
            $old_name = $pre_old_name;

            if (file_exists($path . DS . $dados[$this->alias]['conceitual_dir'] . DS . $old_name)) {
                $file = $path . DS . $dados[$this->alias]['conceitual_dir'] . DS . $old_name;
                $infos = pathinfo($file);
                $new_file = $path . DS . $dados[$this->alias]['conceitual_dir'] . DS . $new_name . '.' . $infos['extension'];
                $new_file_orginal = $path . DS . $dados[$this->alias]['conceitual_dir'] . DS . 'original-' . $new_name . '.' . $infos['extension'];

                if (rename($file, $new_file)) {

                    //load lib
                    include_once(ROOT . DS . APP_DIR . DS . 'Lib' . DS . 'Wideimage/WideImage.php');

                    $this->query('UPDATE noticia_tipos SET conceitual = "' . $new_name . '.' . $infos['extension'] . '" WHERE id = ' . $this->id);

                    copy($new_file, $new_file_orginal);

                    $caminho_novo = $path . DS . $dados[$this->alias]['conceitual_dir'];
                    $outFile = $caminho_novo . DS . $pre_new_name . '.' . $infos['extension'];
                    if (strtolower($infos['extension']) == 'jpg') {
                        WideImage::load($new_file)->resize(1920)->saveToFile($outFile, 80);
                    } else {
                        WideImage::load($new_file)->resize(1920)->saveToFile($outFile);
                    }

                    //outros renames
                    $file = $path . DS . $dados[$this->alias]['conceitual_dir'] . DS . '400_' . $old_name;
                    $infos = pathinfo($file);
                    $new_file = $path . DS . $dados[$this->alias]['conceitual_dir'] . DS . '400_' . $new_name . '.' . $infos['extension'];
                    rename($file, $new_file);

                    $file = $path . DS . $dados[$this->alias]['conceitual_dir'] . DS . 'thumb_' . $old_name;
                    $infos = pathinfo($file);
                    $new_file = $path . DS . $dados[$this->alias]['conceitual_dir'] . DS . 'thumb_' . $new_name . '.' . $infos['extension'];
                    rename($file, $new_file);
                }
            }
        }
    }

    public function afterDelete()
    {
        parent::afterDelete();

        App::import('Model', 'Rota');
        $this->Rota = new Rota();

        $rota = $this->Rota->find('first', array('conditions' => array('Rota.model' => 'NoticiaTipo', 'Rota.row_id' => $this->id)));

        if (!empty($rota)) {
            $this->Rota->delete($rota['Rota']['id'], false);
        }

        //removo vinculo na mão, para evitar recursividade
        $this->Rota->query('UPDATE noticias SET noticia_tipo_id = null WHERE noticia_tipo_id = ' . $this->id);
    }

}
