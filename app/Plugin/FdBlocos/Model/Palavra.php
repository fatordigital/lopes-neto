<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');

class Palavra extends FdBlocosAppModel
{

    public $cacheData;

    public $validate = array(
        'slug' => array(
            'rule' => 'notBlank',
            'message' => 'Informe uma palavra chave',
        )
    );

    public $hasMany = array(
        'PalavraAtributo' => array(
            'className' => 'PalavraAtributo',
            'foreignKey' => 'palavra_id'
        )
    );

    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        $this->cache();
    }

    private function cache()
    {
        $cache = array();
        if (Cache::read('palavras') === false) {
            $cache = $this->find('all');
            foreach ($cache as $key => $bloc) {
                $cache[$bloc['Palavra']['slug']] = $bloc['PalavraAtributo'];
            }
            Cache::write('palavras', $cache);
        } else {
            Cache::read('palavras', $cache);
        }
        $this->cacheData = $cache;
    }

    public function beforeSave($options = array())
    {

        $data = $this->data[$this->alias];
        if (isset($data['slug']) && !empty($data['slug'])) {
            $this->data[$this->alias]['slug'] = Inflector::slug(strtolower($data['slug']), '-');
        }

        parent::beforeSave($options = array());
    }

}