<?php echo $this->Html->script('FdDashboard.jquery.stringToSlug.min'); ?>
<?php echo $this->Html->script('FdDashboard.core/jquery-character-counter/jquery.charactercounter'); ?>
<?php echo $this->Html->script('FdBlocos.fatorcms/bloco/crud.js'); ?>

<?php $this->Html->addCrumb('Blocos', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Editar Bloco') ?>

    <h3>Editar Bloco</h3>

<?php echo $this->Form->create('Bloco', array('type' => 'file', 'role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
<?php echo $this->Form->input('id') ?>
<?php echo $this->Element('FdBlocos.bloco_form') ?>
<?php echo $this->Form->end() ?>