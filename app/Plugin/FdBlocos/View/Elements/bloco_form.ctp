<?php echo $this->element('FdDashboard.idiomas') ?>

<?php if (isset($idiomas)) { ?>
    <?php
    $model = 'BlocoAtributo';
    ?>
    <div class="panel">
        <div class="panel-body">
            <div class="tab-content">
                <?php foreach ($idiomas as $i => $idioma) { ?>

                    <?php echo $this->Form->hidden("{$model}.{$i}.idioma_id", array('value' => $idioma['Idioma']['id'])) ?>
                    <?php echo $this->Form->hidden("{$model}.{$i}.id") ?>

                    <div class="tab-pane fade in <?php echo $idioma['Idioma']['padrao'] == 1 ? 'active' : '' ?>" id="<?php echo $idioma['Idioma']['slug'] ?>">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">
                                        Conteúdo [<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]
                                    </label>
                                    <?php echo $this->Form->textarea("{$model}.{$i}.conteudo", array('div' => false, 'class' => 'form-control editor')) ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">
                                        Link (URl) [<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]
                                    </label>
                                    <?php echo $this->Form->text("{$model}.{$i}.seo_url", array('label' => false, 'div' => false, 'class' => 'form-control')) ?>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">
                                        Seo Title
                                        [<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]
                                    </label>
                                    <?php echo $this->Form->input("{$model}.{$i}.seo_title", array('label' => false, 'div' => false, 'class' => 'form-control')) ?>
                                    <span class="help-block">Esse campo será alimentado com o nome da notícia, caso não seja preenchido.</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">
                                        Seo Description
                                        [<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]
                                    </label>
                                    <?php echo $this->Form->input("{$model}.{$i}.seo_description", array('label' => false, 'type' => 'textarea', 'div' => false, 'class' => 'form-control count_me')) ?>
                                    <span class="help-block">Esse campo será alimentado o resumo da notícia, caso não seja preenchido.</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">
                                        Seo Keywords
                                        [<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]
                                    </label>
                                    <?php echo $this->Form->input("{$model}.{$i}.seo_keywords", array('label' => false, 'div' => false, 'class' => 'form-control')) ?>
                                    <span class="help-block">Utilize ponto e vírgual (;) como separador das palavras-chave.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>

<div class="panel">
    <div class="panel-body">

        <div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="form-group">
                    <label>Status</label>
                    <div class="icheck">
                        <?php echo $this->Form->input('status', array(
                            'type' => 'radio',
                            'options' => array(1 => 'Ativo', 0 => 'Inativo'),
                            'default' => 1,
                            'legend' => false,
                            'before' => '<div class="radio">',
                            'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <?php echo $this->Form->input('bloco_tipo_id', array('options' => $blocoTipos, 'class' => 'form-control input-sm m-bot15')); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <?php echo $this->Form->input('nome', array('label' => 'Nome', 'div' => false, 'class' => 'form-control')) ?>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <?php echo $this->Form->input('slug', array('label' => 'Chave', 'div' => false, 'class' => 'form-control slug')) ?>
                </div>
            </div>
        </div>

        <?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>
        <a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">Cancelar</a>

    </div>
</div>