<?php echo $this->element('FdDashboard.idiomas') ?>

<?php if (isset($idiomas)) { ?>
    <?php
    $model = 'PalavraAtributo';
    ?>
    <div class="panel">
        <div class="panel-body">
            <div class="tab-content">
                <?php foreach ($idiomas as $i => $idioma) { ?>

                    <?php echo $this->Form->hidden("{$model}.{$i}.idioma_id", array('value' => $idioma['Idioma']['id'])) ?>
                    <?php echo $this->Form->hidden("{$model}.{$i}.id") ?>

                    <div class="tab-pane fade in <?php echo $idioma['Idioma']['padrao'] == 1 ? 'active' : '' ?>" id="<?php echo $idioma['Idioma']['slug'] ?>">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">
                                        Conteúdo [<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]
                                    </label>
                                    <?php echo $this->Form->input("{$model}.{$i}.titulo", array('div' => false, 'class' => 'form-control editor')) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>

<div class="panel">
    <div class="panel-body">

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <?php echo $this->Form->input('Palavra.slug', array('label' => 'Palavra chave', 'div' => false, 'class' => 'form-control')) ?>
                </div>
            </div>
        </div>

        <?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>
        <a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">Cancelar</a>

    </div>
</div>