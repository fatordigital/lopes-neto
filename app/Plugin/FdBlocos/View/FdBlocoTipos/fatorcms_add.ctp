<?php echo $this->Html->script('fatorcms/jquery.stringToSlug.min'); ?>
<?php echo $this->Html->script('/fd_blocos/js/fatorcms/bloco_tipo/crud.js'); ?>

<?php $this->Html->addCrumb('Blocos', array('controller' => 'blocos', 'action' => 'index')) ?>
<?php $this->Html->addCrumb('Tipos de Blocos', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Cadastrar Tipo') ?>

<h3>Cadastrar Tipo de Bloco</h3>

<div class="panel">
	<div class="panel-body">
	<?php echo $this->Form->create('BlocoTipo', array('type' => 'file', 'role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
		<?php echo $this->Form->input('id') ?>
		<div class="row">
			<div class="col-md-6 col-lg-6">
				<div class="form-group">
					<label>Status</label>
					<div class="icheck">
						<?php echo $this->Form->input('status', array(
																	'type' => 'radio', 
																	'options' => array(1 => 'Ativo', 0 => 'Inativo'), 
																	'default' => 1, 
																	'legend' => false, 
																	'before' => '<div class="radio">', 
																	'after' => '</div>', 
																	'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<?php echo $this->Form->input('nome', array('label' => 'Nome', 'div' => false, 'class' => 'form-control')) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<?php echo $this->Form->input('slug', array('label' => 'Chave', 'div' => false, 'class' => 'form-control slug')) ?>
				</div>
			</div>
		</div>
		<?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>
		<a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">Cancelar</a>
	<?php echo $this->Form->end() ?>
	</div>
</div>