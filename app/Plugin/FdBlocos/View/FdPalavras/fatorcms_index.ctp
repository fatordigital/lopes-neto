<?php $this->Html->addCrumb('Palavra'); ?>

<div class="row">
    <h3 class="col-lg-12 pull-left">
        Palavra
        <?php echo $this->Html->link('Cadastrar Palavra', array('action' => 'add'), array('class' => 'btn btn-info pull-right')); ?>
    </h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <div class="row-fluid">
                    <div class="col-sm-12 col-md-10 col-lg-11">
                        <?php echo $this->FilterForm->create(null, array('role' => 'form', 'class' => 'form-inline')) ?>
                        <div class="form-group">
                            <?php echo $this->FilterForm->input('filtro_nome', array('placeholder' => 'Filtrar por título', 'class' => 'form-control')) ?>
                        </div>
                        <?php echo $this->FilterForm->submit('Filtrar', array('class' => 'btn btn-success', 'div' => false)) ?>
                        <?php echo $this->Html->link('Limpar Filtro', array('action' => 'index'), array('class' => 'btn btn-warning')) ?>
                        <?php echo $this->FilterForm->end() ?>
                    </div>
                    <div class="col-lg-1 visible-md visible-lg">
                        <?php echo $this->Element('FdDashboard.limit'); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-hover general-table">
                    <thead>
                    <tr>
                        <th style="width:60px"><?php echo $this->Paginator->sort('id', '#') ?></th>
                        <th>Slug</th>
                        <th><?php echo $this->Paginator->sort('titulo') ?></th>
                        <th><?php echo $this->Paginator->sort('modified', 'Modificação') ?></th>
                        <th style="width:180px">Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(isset($palavras)): ?>
                        <?php foreach($palavras AS $bloco): ?>
                            <tr>
                                <td><?php echo $bloco['Palavra']['id'] ?></td>
                                <td><?php echo $bloco['Palavra']['slug'] ?></td>
                                <td><?php echo $this->Idioma->extrair($bloco['PalavraAtributo'], 'titulo'); ?></td>
                                <td><?php echo $this->Time->format($bloco['Palavra']['modified'], '%d/%m/%Y %H:%M:%S') ?></td>
                                <td>
                                    <?php echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('action' => 'edit', $bloco['Palavra']['id']), array('class' => 'btn btn-primary btn-sm', 'escape' => false)) ?>
                                    <?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('action' => 'delete', $bloco['Palavra']['id']), array('class' => 'btn btn-danger btn-sm', 'escape' => false), __('Deseja mesmo remover o registro # %s?', $this->Idioma->extrair($bloco['PalavraAtributo'], 'titulo'))) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="5" class="text-center">Nenhum registro encontrado.</td>
                        </tr>
                    <?php endIf; ?>
                    </tbody>
                </table>

                <?php echo $this->Element('FdDashboard.paginator'); ?>
            </div>
        </section>
    </div>
</div>