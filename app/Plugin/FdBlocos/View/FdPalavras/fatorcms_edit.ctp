<?php $this->Html->addCrumb('Palavras', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Editar Palavra') ?>

    <h3>Editar Palavra</h3>

<?php echo $this->Form->create('Palavra', array('type' => 'file', 'role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
<?php echo $this->Form->input('Palavra.id') ?>
<?php echo $this->Element('FdBlocos.palavra_form') ?>
<?php echo $this->Form->end() ?>