<?php
Router::connect("/fatorcms/blocos", array('plugin' => 'fd_blocos', 'controller' => 'fd_blocos', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
Router::connect("/fatorcms/blocos/:action/*", array('plugin' => 'fd_blocos', 'controller' => 'fd_blocos', 'prefix' => 'fatorcms', 'fatorcms' => true));

Router::connect("/fatorcms/bloco_tipos", array('plugin' => 'fd_blocos', 'controller' => 'fd_bloco_tipos', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
Router::connect("/fatorcms/bloco_tipos/:action/*", array('plugin' => 'fd_blocos', 'controller' => 'fd_bloco_tipos', 'prefix' => 'fatorcms', 'fatorcms' => true));


Router::connect("/fatorcms/palavras", array('plugin' => 'fd_blocos', 'controller' => 'fd_palavras', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
Router::connect("/fatorcms/palavras/:action/*", array('plugin' => 'fd_blocos', 'controller' => 'fd_palavras', 'prefix' => 'fatorcms', 'fatorcms' => true));