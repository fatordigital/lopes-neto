<?php

class FdBlocosController extends FdBlocosAppController
{

    public $uses = array('FdBlocos.Bloco');

    /**
     * admin_index method
     *
     * @return void
     */
    public function fatorcms_index($page = 1)
    {

        App::import('Helper', 'String');
        $this->String = new StringHelper(new View(null));

        // Add filter
        $this->FilterResults->addFilters(
            array(
                'filter' => array(
                    'OR' => array(
                        'Bloco.titulo' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%')),
                    )
                ),
                'filtro_nome' => array(
                    'Bloco.titulo' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%'))
                ),
                'filtro_sites' => array(
                    'Bloco.sites' => array(
                        'select' => $this->FilterResults->select('Site...', $this->String->getSites()),
                        'operator' => 'LIKE',
                        'value' => array('before' => '%', 'after' => '%')
                    )
                )
            )
        );

        $this->FilterResults->setPaginate('page', $page);
        // Define conditions
        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

        // Paginate
        $blocos = $this->paginate();

        $this->set('blocos', $blocos);
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function fatorcms_add()
    {
        if ($this->request->is('post')) {
            $this->Bloco->create();
            if ($this->Bloco->saveAll($this->request->data)) {
                $this->_resetCaches();
                $this->Session->setFlash(__('Registro salvo com sucesso.'), 'FdDashboard.alerts/fatorcms_success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'FdDashboard.alerts/fatorcms_danger');
            }
        }

        // set tipos de blocos
        $blocoTipos = $this->Bloco->BlocoTipo->find('list', array('fields' => array('BlocoTipo.id', 'BlocoTipo.nome'), 'conditions' => array('BlocoTipo.status' => true), 'order' => array('BlocoTipo.nome ASC')));
        $this->set(compact('blocoTipos'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function fatorcms_edit($id = null)
    {
        if (!$this->Bloco->exists($id)) {
            throw new NotFoundException('Registro inválido.');
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Bloco->saveAll($this->request->data)) {
                $this->_resetCaches();
                $this->Session->setFlash(__('Registro salvo com sucesso.'), 'FdDashboard.alerts/fatorcms_success');

                //$this->redirect(array('action' => 'index'));
                $this->_redirectFilter($this->Session->read('referer'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'FdDashboard.alerts/fatorcms_danger');
            }
        } else {
            $options = array('conditions' => array('Bloco.' . $this->Bloco->primaryKey => $id));
            $this->request->data = $this->Bloco->find('first', $options);

            $this->Session->write('referer', $this->referer());
        }

        // set bloco
        $options = array('conditions' => array('Bloco.' . $this->Bloco->primaryKey => $id));
        $bloco = $this->Bloco->find('first', $options);
        $this->set(compact('bloco'));

        // set tipos de blocos
        $blocoTipos = $this->Bloco->BlocoTipo->find('list', array('fields' => array('BlocoTipo.id', 'BlocoTipo.nome'), 'conditions' => array('BlocoTipo.status' => true), 'order' => array('BlocoTipo.nome ASC')));
        $this->set(compact('blocoTipos'));
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function fatorcms_delete($id = null)
    {
        $this->Bloco->id = $id;
        if (!$this->Bloco->exists()) {
            throw new NotFoundException('Registro inválido.');
        }
        $this->request->is('get');
        if ($this->Bloco->delete()) {
            $this->_resetCaches();
            $this->Session->setFlash(__('Registro deletado.'), 'FdDashboard.alerts/fatorcms_success');
            // $this->redirect(array('action' => 'index'));
            $this->_redirectFilter($this->referer());
        }
        $this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
        // $this->redirect(array('action' => 'index'));
        $this->_redirectFilter($this->referer());
    }


    /**
     * fatorcms_status method
     *
     * @return void
     */
    public function fatorcms_status()
    {
        if (!$this->request->is('post')) {
            throw new NotFoundException('Registro inválido.');
        }
        echo $this->_saveStatus('Bloco', $this->request->data['id'], $this->request->data['value']);
        $this->_resetCaches();
        die;
    }
}