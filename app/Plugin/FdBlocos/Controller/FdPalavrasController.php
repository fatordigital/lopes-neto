<?php

class FdPalavrasController extends FdBlocosAppController
{

    public $uses = array('FdBlocos.Palavra');


    public function fatorcms_index($page = 1)
    {
        $this->FilterResults->addFilters(
            array(
                'filter' => array(
                    'OR' => array(
                        'PalavraAtributo.titulo' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%')),
                    )
                ),
                'filtro_nome' => array(
                    'PalavraAtributo.titulo' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%'))
                )
            )
        );


        $this->FilterResults->setPaginate('page', $page);
        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());


        $paginate = $this->paginate();

        $this->set('palavras', $paginate);
    }

    public function fatorcms_add()
    {
        if ($this->request->is('post')) {
            if ($this->Palavra->saveAll($this->request->data)) {
                $this->Session->setFlash('Palavra adicionada com sucesso, <a href="/fatorcms/palavras/edit/'.$this->Palavra->id.'">Clique aqui para editar</a>', 'FdDashboard.alerts/fatorcms_success');
                $this->redirect(array('action' => 'add'));
            }
        }
    }

    public function fatorcms_edit($id)
    {
        $data = $this->Palavra->findById($id);
        if (!$data) {
            $this->Session->setFlash('Palavra não encontrada', 'FdDashboard.alerts/fatorcms_danger');
            $this->redirect(array('action' => 'index'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Palavra->saveAll($this->request->data)) {
                $this->Session->setFlash('Palavra editada com sucesso', 'FdDashboard.alerts/fatorcms_success');
                $this->redirect(array('action' => 'edit', $id));
            }
            $data = $this->Palavra->findById($data);
        }

        $this->request->data = $data;
    }

    public function fatorcms_delete($id)
    {
        $data = $this->Palavra->findById($id);
        if (!$data) {
            $this->Session->setFlash('Palavra não encontrada', 'FdDashboard.alerts/fatorcms_danger');
            $this->redirect(array('action' => 'index'));
        }

        if ($this->Palavra->delete($id)) {

            $this->loadModel('FdBlocos.PalavraAtributo');
            $this->PalavraAtributo->deleteAll(array('PalavraAtributo.palavra_id' => $id));

            $this->Session->setFlash('Palavra deletada com sucesso', 'FdDashboard.alerts/fatorcms_success');
            $this->redirect(array('action' => 'index'));
        }

        $this->Session->setFlash('Erro ao tentar remove a palavra, tente novamente.', 'FdDashboard.alerts/fatorcms_danger');
        $this->redirect(array('action' => 'index'));
    }

}