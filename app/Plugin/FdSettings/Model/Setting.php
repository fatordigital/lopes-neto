<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');

class Setting extends FdSettingsAppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'key' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
		'value' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
	);

	public function loadSettings(){
		if (Cache::read('settings') === false) {
            $settings = $this->find('all', array(
						             	   'fields' => array(
						                	    'Setting.key',
						                	    'Setting.label',
						                   	 	'Setting.value',
						                	),
						                	'conditions' => array('status' => true)
            							)
									);
            Cache::write('settings', $settings);
        } else {
            $settings = Cache::read('settings');
        }

		if(is_array($settings) && count($settings) > 0){
	        foreach ($settings AS $setting) {
	            Configure::write($setting['Setting']['key'], $setting['Setting']['value']);
	            Configure::write($setting['Setting']['key'].'-Label', $setting['Setting']['label']);
	        }
		}
	}
}