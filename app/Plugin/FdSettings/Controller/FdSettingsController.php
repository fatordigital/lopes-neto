<?php

class FdSettingsController extends FdSettingsAppController {

	public $uses = array('FdSettings.Setting');

/**
 * admin_index method
 *
 * @return void
 */
	public function fatorcms_index($page = 1) {

		// Add filter
		$this->FilterResults->addFilters(
			array(
				'filter' => array(
					'OR' => array(
						'Setting.key'    => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
						'Setting.value' 	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
					)
				),
				'filtro_key' => array(
					'Setting.key'     	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				),
				'filtro_value' => array(
					'Setting.value'    => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				)
			)
		);

		$this->FilterResults->setPaginate('page', $page);
		// Define conditions
		$this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

		// Paginate
		$this->Setting->recursive = 0;
		$settings = $this->paginate();

		$this->set('settings', $settings);
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function fatorcms_add() {
		if ($this->request->is('post')) {
			$this->Setting->create();
			if ($this->Setting->saveAll($this->request->data)) {
				$this->_resetCaches();
				$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_edit($id = null) {
		if (!$this->Setting->exists($id)) {
            throw new NotFoundException('Registro inválido.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Setting->saveAll($this->request->data)) {
				$this->_resetCaches();
				$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');

				//$this->redirect(array('action' => 'index'));
				$this->_redirectFilter($this->Session->read('referer'));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}
		} else {
			$options = array('conditions' => array('Setting.' . $this->Setting->primaryKey => $id));
			$this->request->data = $this->Setting->find('first', $options);

			$this->Session->write('referer', $this->referer());
		}

		// set setting
		$options = array('conditions' => array('Setting.' . $this->Setting->primaryKey => $id));
		$setting = $this->Setting->find('first', $options);
		$this->set(compact('setting'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_delete($id = null) {
		$this->Setting->id = $id;
		if (!$this->Setting->exists()) {
            throw new NotFoundException('Registro inválido.');
		}
		$this->request->is('get');
		if ($this->Setting->delete()) {
			$this->_resetCaches();
			$this->Session->setFlash(__('Registro deletado.'),'FdDashboard.alerts/fatorcms_success');
			// $this->redirect(array('action' => 'index'));
			$this->_redirectFilter($this->referer());
		}
		$this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
		// $this->redirect(array('action' => 'index'));
		$this->_redirectFilter($this->referer());
	}

/**
 * fatorcms_prefix method
 *
 * @throws NotFoundException
 * @param string $prefix
 * @return void
 */
	public function fatorcms_prefix($prefix = null, $sufix = null) {
		if ($this->request->is('post') || $this->request->is('put')) {
			//debug($this->request->data);
			if ($this->Setting->saveAll($this->request->data['Setting'])) {
				$this->resetCaches();
				$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');
				$this->redirect(array('action' => 'prefix', $prefix));
			} else {
				foreach($this->request->data['Setting'] as $y => $set){
					$settings[$y]['Setting']['id'] = $set['id'];
					$settings[$y]['Setting']['key'] = $set['key'];
					$settings[$y]['Setting']['value'] = $set['value'];
				}
				//debug($this->Setting->invalidFields());
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}
		} else {
			if($sufix == null){
				$options = array('conditions' => array('Setting.visivel' => true, 'Setting.key LIKE ' => $prefix . '.%'));	
			}elseif($sufix != null){
				$options = array('conditions' => array('Setting.visivel' => true, 'Setting.key LIKE ' => $prefix . '.'. $sufix));
			}

			$settings = $this->Setting->find('all', $options);
		}

		$this->set(compact('settings', 'prefix'));

		// addBreadcrumb for view
		$this->_addBreadcrumb('Configurações', array('admin' => true, 'controller' => 'settings', 'action' => 'index'));
		$this->_addBreadcrumb('Editar ' . $prefix);
	}

/**
 * fatorcms_reset method
 *
 * @throws NotFoundException
 * @return void
 */
	public function fatorcms_reset() {
		$this->_resetCaches();
		$this->Session->setFlash(__('Dados atualizados com sucesso.'),'FdDashboard.alerts/fatorcms_success');
		$this->redirect($this->referer());
		die;
	}
}