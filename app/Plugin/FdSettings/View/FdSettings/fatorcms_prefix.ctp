<?php $this->Html->addCrumb('Configurações', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Editar ' . $prefix); ?>

<div class="row">
    <h3 class="col-lg-12 pull-left">Configurações - <?php echo $prefix ?></h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-hover general-table">
                    <thead>
                        <tr>
                            <th style="width:60px">#</th>
                            <th>Chave</th>
                            <th>Valor</th>
                            <th>Status</th>
                            <th>Modificação</th>
                            <th style="width:180px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($settings) > 0): ?>
                            <?php foreach($settings AS $setting): ?>
                            <tr>
                                <td><?php echo $setting['Setting']['id'] ?></td>
                                <td><?php echo $setting['Setting']['key'] ?></td>
                                <td><?php echo $setting['Setting']['value'] ?></td>
                                <td><?php echo $this->String->getStatus($setting['Setting']['status']); ?></td>
                                <td><?php echo $this->Time->format($setting['Setting']['modified'], '%d/%m/%Y %H:%M:%S') ?></td>
                                <td>
                                    <?php echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('action' => 'edit', $setting['Setting']['id']), array('class' => 'btn btn-primary btn-sm', 'escape' => false)) ?> 
                                    <?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('action' => 'delete', $setting['Setting']['id']), array('class' => 'btn btn-danger btn-sm', 'escape' => false), __('Deseja mesmo remover o registro # %s?', $setting['Setting']['key'])) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="5" class="text-center">Nenhum registro encontrado.</td>
                            </tr>
                        <?php endIf; ?>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>