<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');

class Template extends FdEmailsAppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'nome' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
		'slug' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
		'conteudo' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
	);
}