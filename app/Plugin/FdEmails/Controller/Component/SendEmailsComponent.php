<?php

App::uses('CakeEmail', 'Network/Email');

class SendEmailsComponent extends Component
{

    public $controller = null;
    public $email = '';
    public $templates = array();

    public function initialize(Controller $controller)
    {
        $this->controller = $controller;

        $this->templates = $this->get_templates();
    }

    private function get_templates()
    {
        if (Cache::read('templates') === false) {
            App::import('Model', 'FdEmails.Template');
            $this->Template = new Template();
            $templates = $this->Template->find('list', array(
                    'fields' => array(
                        'Template.slug',
                        'Template.conteudo',
                    ),
                    'conditions' => array('status' => true)
                )
            );
            Cache::write('templates', $templates);
        } else {
            $templates = Cache::read('templates');
        }

        return $templates;
    }

    private function set_log($to, $subject, $content, $status)
    {
        //BEGIN: salva log de e-mails
        App::import('Model', 'FdEmails.Email');
        $this->Email = new Email();

        $log['id'] = null;
        $log['to'] = $to;
        $log['subject'] = $subject;
        $log['content'] = $content;
        $log['status'] = $status;
        $this->Email->save($log);
    }

    public function send($to, $subject = null, $content, $from_default = true)
    {

        if (is_null($subject)) {
            $subject = 'Contato realizado pelo site';
        }

        $this->email = new CakeEmail(json_decode(Configure::read('Smtp'), true));
        $this->email->emailFormat('html');
        if (!$from_default) {
            $this->email->from(array('andrew.rodrigues@fatordigital.com.br' => 'Fator'));
        }

        $ccs = explode(';', Configure::read('Site.Email.CC'));
        $bcc = array();
        foreach ($ccs as $cc) {
            if ($cc != "") {
                // if(Validation::email($cc, true)){
                $bcc[] = trim($cc);
                // }
            }
        }
        $this->email->bcc($bcc);


        $this->email->to($to);
        $this->email->subject($subject);

        $status = null;

        if ($this->email->send($content)) {
            $status = true;
        } else {
            $status = false;
        }

        // set log
        $this->set_log(json_encode($to), $subject, $content, $status);

        return $status;
    }

    public function presenca(array $data = array())
    {
        $content_email = str_replace(
            array(
                '{SITE_NOME}',
                '{SITE_URL}',
                '{DATA}',
                '{NOME}',
                '{EMAIL}',
                '{TELEFONE}'
            ),
            array(
                Configure::read('Site.Nome'),
                Router::url(null, true),
                date('d/m/Y H:i:s'),
                $data['nome'],
                $data['email'],
                $data['telefone']
            ), $this->templates['evento_presenca']
        );
        $this->send(Configure::read('Site.Email.CC'), 'Confirmação de presença', $content_email);
    }


    public function _enviaSac($data, array $attributes = array())
    {
        App::import("helper", "String");
        $this->String = new StringHelper(new View(null));

        $content_email = str_replace(
            array(
                '{SITE_NOME}',
                '{SITE_URL}',
                '{DATA}',
                '{QUEM_EH}',
                '{NOME}',
                '{EMAIL}',
                '{TELEFONE}',
                '{MENSAGEM}'
            ),
            array(
                Configure::read('Site.Nome'),
                Router::url(null, true),
                date('d/m/Y H:i:s'),
                $data['lead_tipo'],
                $data['nome'],
                $data['email'],
                $data['telefone'],
                $data['mensagem']
            ), $this->templates['fale_conosco']
        );


        App::import('Model', 'FdSac.SacTipo');
        $this->SacTipo = new SacTipo();

        $sac_tipo = $this->SacTipo->find('first', array('conditions' => array('SacTipo.id' => $data['sac_tipo_id'])));

        if (!empty($sac_tipo) && $sac_tipo['SacTipo']['email'] != "") {
            if (count(explode(';', $sac_tipo['SacTipo']['email'])) > 1) {
                foreach (explode(';', $sac_tipo['SacTipo']['email']) as $key => $value) {
                    $to[] = trim($value);
                }
                if (isset($to))
                    return $this->send($to, isset($attributes['subject']) ? $attributes['subject'] : null, $content_email);
                else {
                    die("E-mail não registrado ao " . $sac_tipo['SacTipo']['nome']);
                }
            } else {
                return $this->send($sac_tipo['SacTipo']['email'], null, $content_email);
            }
        }

    }

}
