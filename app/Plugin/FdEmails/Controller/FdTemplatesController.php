<?php

class FdTemplatesController extends FdEmailsAppController {

	public $uses = array('FdEmails.Template');

/**
 * admin_index method
 *
 * @return void
 */
	public function fatorcms_index($page = 1) {

		// Add filter
		$this->FilterResults->addFilters(
			array(
				'filter' => array(
					'OR' => array(
						'Template.nome'    	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
						'Template.conteudo' 	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
					)
				),
				'filtro_nome' => array(
					'Template.nome'     	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				),
				'filtro_conteudo' => array(
					'Template.conteudo'    => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				)
			)
		);

		$this->FilterResults->setPaginate('page', $page);
		// Define conditions
		$this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

		// Paginate
		$this->Template->recursive = 0;
		$templates = $this->paginate();

		$this->set('templates', $templates);
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function fatorcms_add() {
		if ($this->request->is('post')) {
			$this->Template->create();
			if ($this->Template->saveAll($this->request->data)) {
				$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_edit($id = null) {
		if (!$this->Template->exists($id)) {
            throw new NotFoundException('Registro inválido.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Template->saveAll($this->request->data)) {
				$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');

				//$this->redirect(array('action' => 'index'));
				$this->_redirectFilter($this->Session->read('referer'));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}
		} else {
			$options = array('conditions' => array('Template.' . $this->Template->primaryKey => $id));
			$this->request->data = $this->Template->find('first', $options);

			$this->Session->write('referer', $this->referer());
		}

		// set template
		$options = array('conditions' => array('Template.' . $this->Template->primaryKey => $id));
		$template = $this->Template->find('first', $options);
		$this->set(compact('template'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_delete($id = null) {
		$this->Template->id = $id;
		if (!$this->Template->exists()) {
            throw new NotFoundException('Registro inválido.');
		}
		$this->request->is('get');
		if ($this->Template->delete()) {
			$this->Session->setFlash(__('Registro deletado.'),'FdDashboard.alerts/fatorcms_success');
			// $this->redirect(array('action' => 'index'));
			$this->_redirectFilter($this->referer());
		}
		$this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
		// $this->redirect(array('action' => 'index'));
		$this->_redirectFilter($this->referer());
	}
}