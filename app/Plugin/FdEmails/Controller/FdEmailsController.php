<?php

class FdEmailsController extends FdEmailsAppController {

	public $uses = array('FdEmails.Email');

/**
 * admin_index method
 *
 * @return void
 */
	public function fatorcms_index($page = 1) {

		// Add filter
		$this->FilterResults->addFilters(
			array(
				'filter' => array(
					'OR' => array(
						'Email.to'    	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
						'Email.subject' 	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
					)
				),
				'filtro_to' => array(
					'Email.to'     	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				),
				'filtro_subject' => array(
					'Email.subject'    => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				)
			)
		);

		$this->FilterResults->setPaginate('page', $page);
		// Define conditions
		$this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

		// Paginate
		$this->Email->recursive = 0;
		$emails = $this->paginate();

		$this->set('emails', $emails);
	}
	
/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_view($id = null) {
		if (!$this->Email->exists($id)) {
            throw new NotFoundException('Registro inválido.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Email->saveAll($this->request->data)) {
				$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}
		}

		// set email
		$options = array('conditions' => array('Email.' . $this->Email->primaryKey => $id));
		$email = $this->Email->find('first', $options);
		$this->set(compact('email'));
	}
}