<?php
Router::connect("/fatorcms/templates", array('plugin' => 'fd_emails', 'controller' => 'fd_templates', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
Router::connect("/fatorcms/templates/:action/*", array('plugin' => 'fd_emails', 'controller' => 'fd_templates', 'prefix' => 'fatorcms', 'fatorcms' => true));

Router::connect("/fatorcms/emails", array('plugin' => 'fd_emails', 'controller' => 'fd_emails', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
Router::connect("/fatorcms/emails/:action/*", array('plugin' => 'fd_emails', 'controller' => 'fd_emails', 'prefix' => 'fatorcms', 'fatorcms' => true));