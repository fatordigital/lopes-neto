<?php

/**
 * Link Model
 *
 */
class Link extends FdMenusAppModel
{

    public $cacheData;

    public $actsAs = array(
        'Containable',
        'Encoder',
        'Tree',
        'Cached' => array(
            'prefix' => array(
                'link_',
                'menu_',
            ),
        ),
        'Upload.Upload' => array(
            'thumb_file' => array(
                'fields' => array(
                    'dir' => 'thumb_dir',
                    'type' => 'thumb_type',
                    'size' => 'thumb_size',
                ),
                'thumbnailMethod' => 'php',
                'thumbnailSizes' => array(
                    'thumb' => '100x100'
                )
            )
        )
    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'seo_url' => array(
            'rule' => array('validaLink'),
            'message' => 'Campo de preenchimento obrigatório.',
        ),
    );

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Menu' => array('counterCache' => true)
    );

    public $hasMany = array(
        'LinkAtributo' => array(
            'className' => 'LinkAtributo',
            'foreignKey' => 'link_id'
        )
    );


    public function __construct($id = null, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        $this->cache();
    }

    public function cache()
    {
        $cache = array();
        if (Cache::read('links') == false) {
            $this->recursive = -1;
            $cache = $this->find('all');
            Cache::write('links', $cache);
        } else {
            $cache = Cache::read('links');
        }
        $this->cacheData = $cache;
    }


    public function validaLink($data)
    {
        if (isset($this->data[$this->alias]['pagina_id']) || isset($this->data[$this->alias]['categoria_id'])) {
            return true;
        } else if (strlen(reset($data)) > 0) {
            return true;
        }
        return false;
    }

    public function beforeSave($options = array())
    {
        App::import("helper", "Idioma");
        $this->Idioma = new IdiomaHelper(new View(null));

        // //obtem e seta a url da pagina
        // if (isset($this->data[$this->alias]['pagina_id']) && $this->data[$this->alias]['pagina_id'] != "" && $this->data[$this->alias]['tipo'] == "PAGINA") {
        // 	$model = ClassRegistry::init('Pagina');
        // 	$pagina = $model->find('first', array('fields' => array('Pagina.seo_url'), 'conditions' => array('Pagina.id' => $this->data[$this->alias]['pagina_id'])));

        // 	$this->data[$this->alias]['link'] = '/'.$pagina['Pagina']['seo_url'];
        //       }

        $this->data[$this->alias]['title'] = $this->Idioma->extrair($this->data['LinkAtributo'], 'title', 'LinkAtributo');
        parent::beforeSave($options = array());
    }

}

?>