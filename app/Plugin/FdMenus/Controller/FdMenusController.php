<?php

/**
 * FdMenus Controller
 *
 * @property Menu $Menu
 */
class FdMenusController extends FdMenusAppController {

	public $uses = array('FdMenus.Menu');

/**
 * admin_index method
 *
 * @return void
 */
	public function fatorcms_index($page = 1) {

		// Add filter
		$this->FilterResults->addFilters(
			array(
				'filter' => array(
					'OR' => array(
						'Menu.nome'    => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
					)
				),
				'filtro_nome' => array(
					'Menu.nome'     	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				),
			)
		);

		$this->FilterResults->setPaginate('page', $page);

		// Get user
        $user = $this->Auth->user();

		// Define conditions
		// $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());
		$options['conditions'] = $this->FilterResults->getConditions();

		 if($user['grupo_id'] != 1 && $user['grupo_id'] != 7){
            if(is_array($options['conditions'])){
                $options['conditions'] = array_merge($options['conditions'], array('Menu.id <>' => 1));
            }else{
                $options['conditions'] = array('Menu.id <>' => 1);
            }
        }
        $this->paginate = $options;

		// Exportar? 
        if(isset($this->params->params['named']['acao']) && $this->params->params['named']['acao'] == "exportar"){
            $this->Reports->xls($this->Menu->find('all', array('conditions' => $this->FilterResults->getConditions(), 'recursive' => -1, 'callbacks' => false)), 'Menus');
        }

		// Paginate
		$this->Menu->recursive = 0;
		$menus = $this->paginate();
		$this->set('menus', $menus);
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function fatorcms_add() {
		// eh post?
		if ($this->request->is('post')) {
			$this->Menu->create();
			if ($this->Menu->save($this->request->data)) {
				$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_edit($id = null) {
		if (!$this->Menu->exists($id)) {
            throw new NotFoundException('Registro inválido.');
		}

		// Get user logado
        $user = $this->Auth->user();

        // Se o menu a ser exibido é o admin, e o usuario logado não for, não deixo entrar
        if( ($user['grupo_id'] != 7) && $id == 1 ){
            $this->Session->setFlash(__('Você não tem permissão para alterar esse registro'),'FdDashboard.alerts/fatorcms_danger');
            $this->_redirectFilter();
        }

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Menu->save($this->request->data)) {
				$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');

				//$this->redirect(array('action' => 'index'));
				$this->_redirectFilter($this->Session->read('referer'));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}
		} else {
			$options = array('conditions' => array('Menu.' . $this->Menu->primaryKey => $id));
			$this->request->data = $this->Menu->find('first', $options);

			$this->Session->write('referer', $this->referer());
		}

		// Set menu
		$options = array('conditions' => array('Menu.' . $this->Menu->primaryKey => $id));
		$menu = $this->Menu->find('first', $options);
		$this->set(compact('menu'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_delete($id = null) {
		$this->Menu->id = $id;
		if (!$this->Menu->exists()) {
            throw new NotFoundException('Registro inválido.');
		}
		$this->request->is('get');
		if ($this->Menu->delete()) {
			$this->_resetCaches();
			$this->Session->setFlash(__('Registro deletado.'),'FdDashboard.alerts/fatorcms_success');
			// $this->redirect(array('action' => 'index'));
			$this->_redirectFilter($this->referer());
		}
		$this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
		// $this->redirect(array('action' => 'index'));
		$this->_redirectFilter($this->referer());
	}
}