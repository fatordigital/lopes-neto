<?php echo $this->element('FdDashboard.idiomas') ?>

<?php if (isset($idiomas)) { ?>
    <?php
    $model = 'LinkAtributo';
    ?>
    <div class="panel">
        <div class="panel-body">
            <div class="tab-content">
                <?php foreach ($idiomas as $i => $idioma) { ?>

                    <?php echo $this->Form->hidden("{$model}.{$i}.idioma_id", array('value' => $idioma['Idioma']['id'])) ?>
                    <?php echo $this->Form->hidden("{$model}.{$i}.id") ?>

                    <div class="tab-pane fade in <?php echo $idioma['Idioma']['padrao'] == 1 ? 'active' : '' ?>" id="<?php echo $idioma['Idioma']['slug'] ?>">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="">
                                        Título [<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]
                                    </label>
                                    <?php echo $this->Form->text("{$model}.{$i}.title", array('div' => false, 'class' => 'form-control', 'maxlength' => 255)) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>

<div class="panel">
    <div class="panel-body">

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label>Status</label>
                    <div class="icheck">
                        <?php echo $this->Form->input('status', array(
                            'type' => 'radio',
                            'options' => array(1 => 'Ativo', 0 => 'Inativo'),
                            'legend' => false,
                            'before' => '<div class="radio">',
                            'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <label>Modo de redirecionamento</label>
                    <div class="icheck">
                        <?php echo $this->Form->input('target', array(
                            'type' => 'radio',
                            'options' 	=> array(
                                '_self' => '_self',
                                '_blank' => '_blank'
                            ),
                            'legend' => false,
                            'before' => '<div class="radio">',
                            'after' => '</div>',
                            'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo $this->Form->input('seo_url', array('label' => 'URL', 'class' => 'form-control')) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <?php echo $this->Form->input('menu_id', array('options' => $menus, 'value' => $menuId, 'class' => 'form-control input-sm m-bot15')); ?>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <?php echo $this->Form->input('parent_id', array('options' => $parentIds, 'empty' => true, 'class' => 'form-control input-sm m-bot15')); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <?php echo $this->Form->input('visibility_grupos', array('label' => 'Visível', 'options' => $grupos, 'multiple' => true, 'class' => 'form-control')); ?>
                </div>
            </div>
        </div>

        <?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>
        <a href="<?php echo $this->Html->Url(array('action' => 'index', 'menu' => $this->params->named['menu'])); ?>" class="btn btn-default">&nbsp;Cancelar</a>

    </div>
</div>