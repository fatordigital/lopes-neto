<?php $this->Html->addCrumb('Menus', array('controller' => 'fd_menus', 'action' => 'index')) ?>
<?php $this->Html->addCrumb('Links', array('action' => 'index', 'menu' => $menu['Menu']['id'])) ?>
<?php $this->Html->addCrumb($menu['Menu']['nome'], array('action' => 'index', 'menu' => $menuId)) ?>
<?php $this->Html->addCrumb('Cadastrar Link') ?>

    <h3>Cadastrar Link no Menu: <?php echo $menu['Menu']['nome']; ?></h3>


<?php echo $this->Form->create('Link', array('type' => 'file', 'role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
<?php echo $this->Form->input('id') ?>

<?php echo $this->Element('FdMenus.link_form') ?>

<?php echo $this->Form->end() ?>