$(document).ready(function () {

    if ($.summernote) {
        $('.editor').summernote({
            height: 200
        });
    } else {
        if ($('.editor').length) {
            console.error('Existe campo de texto utilizando a classe ".editor", mas, não está encontrando o plugin/summernote.');
        }
    }


    if ($('.escolha-idioma').length > 0) {
        $(".escolha-idioma").tabs();
    }

    if ($('.data').length > 0) {
        $('.data').datepicker({
            dateFormat: 'dd/mm/yy'
        });
    }

    $('#limit').change(function () {
        if ($(this).val() != "") {
            window.location = $(this).val();
        } else {
            window.location = $('#limit').attr('rel').replace(/\/limit:(.*)/gi, '');
        }
    });

    setMasks();
});

function setMasks() {
    $('input.date').mask('11/11/1111');
    $('input.time').mask('00:00:00');
    $('input.date_time').mask('99/99/9999 00:00:00');
    $('input.cep').mask('99999-999');
    $('input.cpf').mask('999.999.999-99');
    $('input.cnpj').mask('99.999.999.9999/99');
    $('input.phone').mask('9999-99999');
    $('input.phone_with_ddd').mask('(99) 9999-9999');
    $('input.card_credit').mask('9999999999999999');
    $('input.code_validate').mask('999');
    $('input.number').mask('99999999999999999999999999#');
    $('input.money1').mask('000.000.000.000.000,00', {reverse: true});
}

function removerAcentos(newStringComAcento) {
    var string = newStringComAcento;
    var mapaAcentosHex = {
        a: /[\xE0-\xE6]/g,
        A: /[\xC0-\xC6]/g,
        e: /[\xE8-\xEB]/g,
        E: /[\xC8-\xCB]/g,
        i: /[\xEC-\xEF]/g,
        I: /[\xCC-\xCF]/g,
        o: /[\xF2-\xF6]/g,
        O: /[\xD2-\xD6]/g,
        u: /[\xF9-\xFC]/g,
        U: /[\xD9-\xDC]/g,
        c: /\xE7/g,
        C: /\xC7/g,
        n: /\xF1/g,
        N: /\xD1/g
    };

    for (var letra in mapaAcentosHex) {
        var expressaoRegular = mapaAcentosHex[letra];
        string = string.replace(expressaoRegular, letra);
    }

    return string;
}

function slugify(text, replaceTo) {
    replaceTo = replaceTo ? replaceTo : '-';
    var text = removerAcentos(text).toString().toLowerCase()
        .replace(/\s+/g, replaceTo)           // Replace spaces with -
        .replace(/[^\w\-]+/g, replaceTo)       // Remove all non-word chars
        .replace(/\-\-+/g, replaceTo)         // Replace multiple - with single -
        .replace(/^-+/, replaceTo)             // Trim - from start of text
        .replace(/-+$/, replaceTo);            // Trim - from end of text
    return text;
}