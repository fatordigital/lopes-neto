<?php if (isset($idiomas)) {?>
    <header class="panel-heading tab-bg-dark-navy-blue ">
        <ul class="nav nav-tabs">
            <?php foreach ($idiomas as $i => $idioma) {   ?>
                <?php if ($idioma['Idioma']['status'] == 1){ ?>
                    <li<?php echo $idioma['Idioma']['padrao'] == 1 ? ' class="active"' : '' ?>>
                        <a data-toggle="tab" href="#<?php echo $idioma['Idioma']['slug'] ?>">
                            <?php echo $idioma['Idioma']['titulo'] ?> [<?php echo $this->Html->image('flags/16/' . $idioma['Idioma']['bandeira']) ?>]
                        </a>
                    </li>
                <?php } ?>
            <?php } ?>
        </ul>
    </header>
<?php } ?>