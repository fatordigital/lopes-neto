<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $this->Html->url('/assets/images/favicon/apple-icon-57x57.png') ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $this->Html->url('/assets/images/favicon/apple-icon-60x60.png') ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $this->Html->url('/assets/images/favicon/apple-icon-72x72.png') ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $this->Html->url('/assets/images/favicon/apple-icon-76x76.png') ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $this->Html->url('/assets/images/favicon/apple-icon-114x114.png') ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $this->Html->url('/assets/images/favicon/apple-icon-120x120.png') ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $this->Html->url('/assets/images/favicon/apple-icon-144x144.png') ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $this->Html->url('/assets/images/favicon/apple-icon-152x152.png') ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $this->Html->url('/assets/images/favicon/apple-icon-180x180.png') ?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $this->Html->url('/assets/images/favicon/android-icon-192x192.png') ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $this->Html->url('/assets/images/favicon/favicon-32x32.png') ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $this->Html->url('/assets/images/favicon/favicon-96x96.png') ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $this->Html->url('/assets/images/favicon/favicon-16x16.png') ?>">
    <link rel="manifest" href="<?php echo $this->Html->url('/assets/images/favicon/manifest.json') ?>">

    <title><?php echo Configure::read('Site.Nome') ?></title>

    <!--Core CSS -->
    <?php echo $this->Html->css('FdDashboard.core/bootstrap3/bootstrap.min') ?>
    <?php echo $this->Html->css('FdDashboard.core/bootstrap-datepicker/datepicker'); ?>
    <?php echo $this->Html->css('FdDashboard.core/bootstrap-switch'); ?>
    <?php echo $this->Html->css('FdDashboard.core/jquery-ui/jquery-ui-1.10.1.custom.min') ?>
    <?php echo $this->Html->css('FdDashboard.core/bootstrap-reset') ?>
    <?php echo $this->Html->css('FdDashboard.core/font-awesom/css/font-awesome') ?>
    <?php echo $this->Html->css('FdDashboard.core/clndr') ?>

    <!--clock css-->
    <?php echo $this->Html->css('FdDashboard.core/css3clock/style') ?>


    <!-- Custom styles for this template -->
    <?php echo $this->Html->css('FdDashboard.style') ?>
    <?php echo $this->Html->css('FdDashboard.style-responsive') ?>
    <?php echo $this->Html->css('FdDashboard.core/iCheck/minimal') ?>
    <!-- css adicionais específicos -->
    <?php echo $this->Html->css('FdDashboard.custom') ?>

    <?php echo $this->Html->css('FdDashboard./plugins/summernote/summernote') ?>

    <?php echo $this->fetch('css') ?>

    <!--[if lt IE 9]>
    <?php echo $this->Html->script('FdDashboard.ie8-responsive-file-warning/ie8-responsive-file-warning') ?>
    <![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <?php echo $this->Html->script('FdDashboard.core/jquery') ?>
</head>
<body>
<section id="container">

    <?php echo $this->element('FdDashboard.header') ?>

    <?php echo $this->element('FdDashboard.sidebar') ?>

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <?php echo $this->Session->flash('auth'); ?>
            <?php echo $this->Session->flash(); ?>

            <div class="row">
                <div class="col-md-12">
                    <?php echo $this->Html->getCrumbList(
                        array('class' => 'breadcrumb', 'firstClass' => false, 'lastClass' => 'active'),
                        array(
                            'text' => 'Início',
                            'url' => array(
                                'plugin' => 'fd_dashboard',
                                'controller' => 'fd_dashboard',
                                'action' => 'index',
                                'prefix' => 'fatorcms'
                            )
                        )
                    ); ?>
                </div>
            </div>

            <?php echo $this->fetch('content'); ?>
            <div class="row">
                <div class="col-md-12"><?php echo $this->element('sql_dump'); ?></div>
            </div>
        </section>
    </section>
    <!--main content end-->

</section>

<!-- Placed js at the end of the document so the pages load faster -->
<!--Core js-->
<?php echo $this->Html->script('FdDashboard.core/jquery-ui/jquery-ui-1.10.1.custom.min') ?>
<?php echo $this->Html->script('FdDashboard.core/bootstrap3/bootstrap.min') ?>
<?php echo $this->Html->script('FdDashboard.core/bootstrap-datepicker/bootstrap-datepicker') ?>
<?php echo $this->Html->script('FdDashboard.core/bootstrap-switch') ?>
<?php echo $this->Html->script('FdDashboard.core/jquery.dcjqaccordion.2.7') ?>

<?php echo $this->Html->script('FdDashboard./plugins/summernote/summernote.js') ?>

<?php echo $this->Html->script('FdDashboard.core/jquery.scrollTo.min') ?>
<?php echo $this->Html->script('FdDashboard.core/jQuery-slimScroll-1.3.0/jquery.slimscroll') ?>
<?php echo $this->Html->script('FdDashboard.core/jquery.nicescroll'); ?>
<!--[if lte IE 8]><?php echo $this->Html->script('FdDashboard.core/flot-chart/excanvas.min') ?><![endif]-->
<?php echo $this->Html->script('FdDashboard.core/skycons/skycons') ?>
<?php echo $this->Html->script('FdDashboard.core/jquery.scrollTo/jquery.scrollTo') ?>

<?php echo $this->Html->script('FdDashboard.core/gauge/gauge') ?>
<!--clock init-->
<?php echo $this->Html->script('FdDashboard.core/css3clock/css3clock') ?>

<?php echo $this->Html->script('FdDashboard.core/jquery.customSelect.min') ?>
<!--common script init for all pages-->
<?php echo $this->Html->script('FdDashboard.core/iCheck/jquery.icheck'); ?>
<?php echo $this->Html->script('FdDashboard.core/scripts') ?>
<!--mask js-->

<?php echo $this->Html->script('jquery.mask.min') ?>
<?php echo $this->Html->script('FdDashboard.main') ?>
<!--script for this page-->
<!-- scripts adicionais específicos -->
<?php echo $this->fetch('script') ?>
</body>
</html>