<!DOCTYPE html>
<html lang="en">
<head>
    <?php echo $this->Html->charset() ?>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo $this->Html->Url('/assets/images/favicon/favicon.png', true) ?>" />

    <title>Efetue seu login - FatorCMS</title>

    <!--Core CSS -->
    <?php echo $this->Html->css('FdDashboard.core/bootstrap3/bootstrap.min') ?>
    <?php echo $this->Html->css('FdDashboard.core/bootstrap-reset') ?>
    <?php echo $this->Html->css('FdDashboard.core/font-awesom/css/font-awesome') ?>

    <!-- Custom styles for this template -->
    <?php echo $this->Html->css('FdDashboard.style') ?>
    <?php echo $this->Html->css('FdDashboard.style-responsive') ?>

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <?php echo $this->Html->script('FdDashboard.ie8-responsive-file-warning/ie8-responsive-file-warning') ?>
    <![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

  <body class="login-body">

    <div class="container">

      <?php echo $this->fetch('content') ?>

    </div>

    <!-- Placed js at the end of the document so the pages load faster -->

    <!--Core js-->
    <?php echo $this->Html->script('FdDashboard.jquery.min') ?>
    <?php echo $this->Html->script('FdDashboard.fatorcms/core/bootstrap3/bootstrap.min') ?>

  </body>
</html>
