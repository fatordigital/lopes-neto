<?php

class FdPosController extends FdSacAppController {

	public $uses = array('FdSac.LpPos');

/**
 * admin_index method
 *
 * @return void
 */
	public function fatorcms_index($page = 1) {

		// Add filter
		$this->FilterResults->addFilters(
			array(
				'filter' => array(
					'OR' => array(
						'LpPos.nome'   => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
					)
				),
				'filtro_nome' => array(
					'LpPos.nome'     	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				),
			)
		);

		$this->FilterResults->setPaginate('page', $page);
		// Define conditions
		$this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

		// Exportar?
        if(isset($this->params->params['named']['acao']) && $this->params->params['named']['acao'] == "exportar"){

        	$dados = $this->LpPos->find('all', array('conditions' =>$this->FilterResults->getConditions(), 'recursive' => -1, 'callbacks' => false));
        	$data = array();
        	foreach ($dados as $key => $value) {
        		$data[$key]['LpPos'] = $value['LpPos'];
        		
        		$data[$key]['LpPos']['criado'] = $data[$key]['LpPos']['created'];
        		unset($data[$key]['LpPos']['modified']);
        		unset($data[$key]['LpPos']['created']);
        	}

            $this->Reports->xls($data, 'Pos', false);
        }

		// Paginate
		$this->LpPos->recursive = 0;
		$leads = $this->paginate();

		$this->set('leads', $leads);
	}
}