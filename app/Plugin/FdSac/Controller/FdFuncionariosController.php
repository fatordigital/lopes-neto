<?php

class FdFuncionariosController extends FdSacAppController {

	public $uses = array('FdSac.LpFuncionario');

/**
 * admin_index method
 *
 * @return void
 */
	public function fatorcms_index($page = 1) {

		// Add filter
		$this->FilterResults->addFilters(
			array(
				'filter' => array(
					'OR' => array(
						'LpFuncionario.nome_funcionario'   => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
					)
				),
				'filtro_nome' => array(
					'LpFuncionario.nome_funcionario'     	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				),
			)
		);

		$this->FilterResults->setPaginate('page', $page);
		// Define conditions
		$this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

		// Exportar?
        if(isset($this->params->params['named']['acao']) && $this->params->params['named']['acao'] == "exportar"){

        	$dados = $this->LpFuncionario->find('all', array('conditions' =>$this->FilterResults->getConditions(), 'recursive' => -1, 'callbacks' => false));
        	$data = array();
        	foreach ($dados as $key => $value) {
        		$data[$key]['LpFuncionario'] = $value['LpFuncionario'];
        		
        		$data[$key]['LpFuncionario']['criado'] = $data[$key]['LpFuncionario']['created'];
        		unset($data[$key]['LpFuncionario']['modified']);
        		unset($data[$key]['LpFuncionario']['created']);
        		unset($data[$key]['LpFuncionario']['nome_segundo']);
        		unset($data[$key]['LpFuncionario']['telefone_segundo']);
        		unset($data[$key]['LpFuncionario']['curso_ies_segundo']);
        		unset($data[$key]['LpFuncionario']['instituicao_segundo']);
        		unset($data[$key]['LpFuncionario']['email_segundo']);
        		unset($data[$key]['LpFuncionario']['nome_terceiro']);
        		unset($data[$key]['LpFuncionario']['telefone_terceiro']);
        		unset($data[$key]['LpFuncionario']['curso_ies_terceiro']);
        		unset($data[$key]['LpFuncionario']['instituicao_terceiro']);
        		unset($data[$key]['LpFuncionario']['email_terceiro']);
        	}

            $this->Reports->xls($data, 'Funcionarios', false);
        }

		// Paginate
		$this->LpFuncionario->recursive = 0;
		$funcionarios = $this->paginate();

		$this->set('funcionarios', $funcionarios);
	}
}