<?php

class FdSacController extends FdSacAppController
{

    public $uses = array('FdSac.Sac');

    /**
     * admin_index method
     *
     * @return void
     */
    public function fatorcms_index($page = 1)
    {

        // Lista os tipos de sac
        App::import('Model', 'FdSac.SacTipo');
        $this->SacTipo = new SacTipo();
        $lead_tipos = $this->Sac->find('list', array('fields' => array('Sac.lead_tipo', 'Sac.lead_tipo'), 'order' => array('Sac.lead_tipo ASC')));

        // Add filter
        $this->FilterResults->addFilters(
            array(
                'filter' => array(
                    'OR' => array(
                        'Sac.nome' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%')),
                        'Sac.email' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%')),
                    )
                ),
                'filtro_nome' => array(
                    'Sac.nome' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%'))
                ),
                'filtro_email' => array(
                    'Sac.email' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%'))
                ),
                'filtro_lead_tipo' => array(
                    'Sac.lead_tipo' => array('select' => $this->FilterResults->select('Tipo de Sac...', $lead_tipos))
                ),
                'filtro_voce' => array(
                    'Sac.formulario' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%'))
                )
            )
        );

        $this->FilterResults->setPaginate('page', $page);
        // Define conditions
        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());


        // Exportar?
        if (isset($this->params->params['named']['acao']) && $this->params->params['named']['acao'] == "exportar") {

            $dados = $this->Sac->find('all', array(
                'fields' => array('Sac.id', 'Sac.nome', 'Sac.lead_tipo', 'Sac.email', 'Sac.telefone', 'Sac.mensagem', 'Sac.created', 'Sac.modified'),
                'conditions' => $this->FilterResults->getConditions(),
                'recursive' => -1,
                'callbacks' => false));
            $data = array();
            foreach ($dados as $key => $value) {
                $data[$key]['Sac'] = $value['Sac'];

                $data[$key]['Sac']['criado'] = $data[$key]['Sac']['created'];
                unset($data[$key]['Sac']['modified']);
                unset($data[$key]['Sac']['created']);
            }

            $this->Reports->xls($data, 'Sac', false);
        }

        // Paginate
        $this->Sac->recursive = 0;
        $sacs = $this->paginate();

        $this->set('sacs', $sacs);
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function fatorcms_view($id = null)
    {
        if (!$this->Sac->exists($id)) {
            throw new NotFoundException('Registro inválido.');
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Sac->save($this->request->data, false)) {
                $this->Session->setFlash(__('Registro salvo com sucesso.'), 'FdDashboard.alerts/fatorcms_success');
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'FdDashboard.alerts/fatorcms_danger');
            }
        }

        // set sac
        $options = array('conditions' => array('Sac.' . $this->Sac->primaryKey => $id));
        $sac = $this->Sac->find('first', $options);
        $this->request->data = $sac;
        $this->set(compact('sac'));
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function fatorcms_delete($id = null)
    {
        $this->Sac->id = $id;
        if (!$this->Sac->exists()) {
            throw new NotFoundException('Registro inválido.');
        }
        $this->request->is('get');
        if ($this->Sac->delete()) {
            $this->Session->setFlash(__('Registro deletado.'), 'FdDashboard.alerts/fatorcms_success');
            // $this->redirect(array('action' => 'index'));
            $this->_redirectFilter($this->referer());
        }
        $this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
        // $this->redirect(array('action' => 'index'));
        $this->_redirectFilter($this->referer());
    }
}