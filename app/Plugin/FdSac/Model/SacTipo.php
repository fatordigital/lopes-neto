<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');

class SacTipo extends FdSacAppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'nome' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
	);
}