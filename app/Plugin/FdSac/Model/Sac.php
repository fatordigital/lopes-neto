<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');

class Sac extends FdSacAppModel
{

    public $useTable = "sac";

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'nome' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
        ),
        'email' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'O e-mail obrigatório',
                'required' => true,
            ),
            'email' => array(
                'rule' => array('email', true),
                'message' => 'Informe um e-mail válido',
            ),
        ),
    );

//The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'SacTipo' => array(
            'className' => 'SacTipo',
            'foreignKey' => 'sac_tipo_id'
        ),
    );

    /**
     * beforeSave
     *
     * sobrecarga do metodo executado antes de salvar o registro
     */
    public function beforeSave($options = array())
    {

        if (isset($this->data[$this->alias]['destinos']) && count($this->data[$this->alias]['destinos']) > 0) {
            $this->data[$this->alias]['destinos'] = json_encode($this->data[$this->alias]['destinos']);
        }

        parent::beforeSave($options = array());
    }

    /**
     * afterFind
     *
     * sobrecarga do metodo executado depois de buscar o registro
     */
    public function afterFind($results, $primary = false)
    {
        if (!empty($results)) {
            foreach ($results as $k => &$r) {
                if (isset($r[$this->alias])) {
                    //tem destinos? dou decode neles...
                    if (isset($r[$this->alias]['destinos']) && $r[$this->alias]['destinos'] != "") {
                        @$r[$this->alias]['destinos'] = json_decode($r[$this->alias]['destinos'], true);
                    }
                }
            }
        }
        return $results;
    }
}