<?php $this->Html->addCrumb('Funcionários'); ?>

<div class="row">
     <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
        Funcionários 
    </h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <div class="row-fluid">
                    <div class="col-sm-12 col-md-10 col-lg-11">
                        <?php echo $this->FilterForm->create(null, array('role' => 'form', 'class' => 'form-inline')) ?>
                            <div class="form-group">
                                <?php echo $this->FilterForm->input('filtro_nome', array('placeholder' => 'Filtrar por nome', 'class' => 'form-control')) ?>
                            </div>
							<?php echo $this->FilterForm->submit('Filtrar', array('class' => 'btn btn-success', 'div' => false)) ?>
                            <?php echo $this->Html->link('Limpar Filtro', array('action' => 'index'), array('class' => 'btn btn-warning')) ?>
							<?php echo $this->Html->link('<i class="fa fa-share-square-o"></i>&nbsp;Exportar', $this->params->params['named']+array('acao' => 'exportar') , array('class' => 'btn btn-info btn-small btn-export', 'escape' => false)); ?>
                        <?php echo $this->FilterForm->end() ?>
                    </div>
                    <div class="col-lg-1 visible-md visible-lg">
                       <?php echo $this->Element('FdDashboard.limit'); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-hover general-table">
                    <thead>
                        <tr>
                            <th style="width:60px"><?php echo $this->Paginator->sort('id', '#') ?></th>
                            <th><?php echo $this->Paginator->sort('nome_funcionario') ?></th>
							<th><?php echo $this->Paginator->sort('nome_primeiro') ?></th>
							<th><?php echo $this->Paginator->sort('telefone_primeiro') ?></th>
							<th><?php echo $this->Paginator->sort('curso_ies_primeiro') ?></th>
                            <th><?php echo $this->Paginator->sort('instituicao_primeiro') ?></th>
							<th><?php echo $this->Paginator->sort('email_primeiro') ?></th>
							<th><?php echo $this->Paginator->sort('created', 'Criado') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($funcionarios) > 0): ?>
                            <?php foreach($funcionarios AS $funcionario): ?>
                            <tr>
                                <td><?php echo $funcionario['LpFuncionario']['id'] ?></td>
                                <td><?php echo $funcionario['LpFuncionario']['nome_funcionario'] ?></td>
                                <td><?php echo $funcionario['LpFuncionario']['nome_primeiro'] ?></td>
								<td><?php echo $funcionario['LpFuncionario']['telefone_primeiro'] ?></td>
								<td><?php echo $funcionario['LpFuncionario']['curso_ies_primeiro'] ?></td>
                                <td><?php echo $funcionario['LpFuncionario']['instituicao_primeiro'] ?></td>
								<td><?php echo $funcionario['LpFuncionario']['email_primeiro'] ?></td>
								<td><?php echo $this->Time->format($funcionario['LpFuncionario']['created'], '%d/%m/%Y %H:%M:%S') ?></td>
                            </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="7" class="text-center">Nenhum registro encontrado.</td>
                            </tr>
                        <?php endIf; ?>
                    </tbody>
                </table>

                <?php echo $this->Element('FdDashboard.paginator'); ?>
            </div>
        </section>
    </div>
</div>