<?php $this->Html->addCrumb('Sac', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Detalhe Sac') ?>

<h3>Detalhe Sac</h3>

<div class="panel">
	<div class="panel-body">
	<?php echo $this->Form->create('Sac', array('type' => 'file', 'role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
		<?php echo $this->Form->input('id') ?>

		<?php if($sac['Sac']['lead_tipo'] != ""): ?>
			<div class="row">
				<div class="col-lg-6">
					<strong>Quem é: </strong> <?php echo $this->String->getSacTipoLead($sac['Sac']['lead_tipo']); ?>
				</div>
			</div>		
		<?php endIf; ?>

		<?php if($sac['Sac']['url'] != ""): ?>
			<div class="row">
				<div class="col-lg-6">
					<strong>Url do Contato: </strong> <?php echo $sac['Sac']['url']; ?>
				</div>
			</div>		
		<?php endIf; ?>

		<div class="row">
			<div class="col-lg-6">
				<strong>Nome: </strong> <?php echo $sac['Sac']['nome']; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<strong>E-mail: </strong> <?php echo $sac['Sac']['email']; ?>
			</div>
		</div>
		
		<?php if(isset($sac['Sac']['telefone']) && $sac['Sac']['telefone'] != ""): ?>
			<div class="row">
				<div class="col-lg-6">
					<strong>Telefone: </strong> <?php echo $sac['Sac']['telefone']; ?>
				</div>
			</div>
		<?php endIf; ?>
		
		<?php if(isset($sac['Sac']['curso']) && $sac['Sac']['curso'] != ""): ?>
			<div class="row">
				<div class="col-lg-6">
					<strong>Curso: </strong> <?php echo $sac['Sac']['curso']; ?>
				</div>
			</div>
		<?php endIf; ?>
		
		<?php if(isset($sac['Sac']['semestre']) && $sac['Sac']['semestre'] != ""): ?>
			<div class="row">
				<div class="col-lg-6">
					<strong>Semestre: </strong> <?php echo $sac['Sac']['semestre']; ?>
				</div>
			</div>
		<?php endIf; ?>
		
		<?php if(isset($sac['Sac']['data_nascimento']) && $sac['Sac']['data_nascimento'] != ""): ?>
			<div class="row">
				<div class="col-lg-6">
					<strong>Data de Nascimento: </strong> <?php echo $sac['Sac']['data_nascimento']; ?>
				</div>
			</div>
		<?php endIf; ?>
		
		<?php if(isset($sac['Sac']['celular']) && $sac['Sac']['celular'] != ""): ?>
			<div class="row">
				<div class="col-lg-6">
					<strong>Celular: </strong> <?php echo $sac['Sac']['celular']; ?>
				</div>
			</div>
		<?php endIf; ?>
		
		<?php if(isset($sac['Sac']['intercambio']) && $sac['Sac']['intercambio'] != ""): ?>
			<div class="row">
				<div class="col-lg-6">
					<strong>Intercâmbio: </strong> <?php echo ($sac['Sac']['intercambio']) ? 'Sim' : 'Não'; ?>
				</div>
			</div>
		<?php endIf; ?>
		
		<?php if(isset($sac['Sac']['intercambio_local']) && $sac['Sac']['intercambio_local'] != ""): ?>
			<div class="row">
				<div class="col-lg-6">
					<strong>Local do Intercâmbio: </strong> <?php echo $sac['Sac']['intercambio_local']; ?>
				</div>
			</div>
		<?php endIf; ?>
		
		<?php if(isset($sac['Sac']['destinos']) && $sac['Sac']['destinos'] != ""): ?>
			<div class="row">
				<div class="col-lg-6">
					<strong>Destinos: </strong> <?php echo $sac['Sac']['destinos']; ?>
				</div>
			</div>
		<?php endIf; ?>
		
		<?php if(isset($sac['Sac']['destino_outro']) && $sac['Sac']['destino_outro'] != ""): ?>
			<div class="row">
				<div class="col-lg-6">
					<strong>Outros Destinos: </strong> <?php echo $sac['Sac']['destino_outro']; ?>
				</div>
			</div>
		<?php endIf; ?>
		
		<?php if(isset($sac['Sac']['mensagem']) && $sac['Sac']['mensagem'] != ""): ?>
			<div class="row">
				<div class="col-lg-6">
					<strong>Mensagem: </strong> <?php echo $sac['Sac']['mensagem']; ?>
				</div>
			</div>
		<?php endIf; ?>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<?php echo $this->Form->input('observacao', array('label' => 'Observação', 'type' => 'textarea', 'div' => false, 'class' => 'form-control')) ?>
				</div>
			</div>
		</div>
		<?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>
		<a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">Voltar</a>
	<?php echo $this->Form->end() ?>
	</div>
</div>