<?php $this->Html->addCrumb('Sac', array('controller' => 'sac', 'action' => 'index')) ?>
<?php $this->Html->addCrumb('Tipos de Sac', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Editar Tipo de Sac') ?>

<h3>Editar Tipo de Sac</h3>

<div class="panel">
	<div class="panel-body">
	<?php echo $this->Form->create('SacTipo', array('type' => 'file', 'role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
		<?php echo $this->Form->input('id') ?>
		<div class="row">
			<div class="col-md-6 col-lg-6">
				<div class="form-group">
					<label>Status</label>
					<div class="icheck">
						<?php echo $this->Form->input('status', array(
																	'type' => 'radio', 
																	'options' => array(1 => 'Ativo', 0 => 'Inativo'), 
																	'default' => 1, 
																	'legend' => false, 
																	'before' => '<div class="radio">', 
																	'after' => '</div>', 
																	'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<?php echo $this->Form->input('nome', array('div' => false, 'class' => 'form-control')) ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<?php echo $this->Form->input('slug', array('div' => false, 'class' => 'form-control')) ?>
				</div>
			</div>
		</div>
		<?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>
	<?php echo $this->Form->end() ?>
	</div>
</div>