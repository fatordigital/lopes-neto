<?php
	Router::connect("/fatorcms/eventos", array('plugin' => 'fd_eventos', 'controller' => 'fd_eventos', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true, 'tipo' => 'evento'));
    Router::connect("/fatorcms/eventos/:action/*", array('plugin' => 'fd_eventos', 'controller' => 'fd_eventos', 'prefix' => 'fatorcms', 'fatorcms' => true, 'tipo' => 'evento'));

    Router::connect("/fatorcms/calendario/academicos", array('plugin' => 'fd_eventos', 'controller' => 'fd_eventos', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true, 'tipo' => 'academico'));
    Router::connect("/fatorcms/calendario/academicos/:action/*", array('plugin' => 'fd_eventos', 'controller' => 'fd_eventos', 'prefix' => 'fatorcms', 'fatorcms' => true, 'tipo' => 'academico'));
	
	Router::connect("/fatorcms/evento_tags", array('plugin' => 'fd_eventos', 'controller' => 'fd_evento_tags', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
    Router::connect("/fatorcms/evento_tags/:action/*", array('plugin' => 'fd_eventos', 'controller' => 'fd_evento_tags', 'prefix' => 'fatorcms', 'fatorcms' => true));