<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');

/**
 * Evento Model
 *
 * @property Evento $Evento
 */
class EventoPresenca extends FdEventosAppModel
{

    public $actsAs = array('Containable');

    public $validate = array(
        'nome' => array(
            'notempty' => array(
                'rule' => array('notBlank'),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
        ),
        'email' => array(
            'notempty' => array(
                'rule' => array('notBlank'),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
        ),
        'telefone' => array(
            'notempty' => array(
                'rule' => array('notBlank'),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
        )
    );

    public function custom_save(array $data = array())
    {
        $meus_eventos = CakeSession::read('meus_eventos') ? CakeSession::read('meus_eventos') : array();
        $exist = $this->find('first',
            array(
                'conditions' => array(
                    'EventoPresenca.email' => $data['EventoPresenca']['email'],
                    'EventoPresenca.evento_id' => $data['EventoPresenca']['evento_id']
                )
            )
        );
        if (!$exist) {
            $meus_eventos[] = $data['EventoPresenca']['evento_id'];
            CakeSession::write('meus_eventos', $meus_eventos);
            return $this->save($data);
        } else {
            $meus_eventos[] = $exist['EventoPresenca']['evento_id'];
            CakeSession::write('meus_eventos', $meus_eventos);
        }
        return null;
    }

}