<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');
App::uses('AuthComponent', 'Controller/Component');

/**
 * Evento Model
 *
 * @property Evento $Evento
 */
class Evento extends FdEventosAppModel
{

    public $cacheData;

    public function __construct($id = null, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        $this->cache();
    }

    public $actsAs = array(
        'Containable',
    );

    public $validate = array(
        'nome' => array(
            'notempty' => array(
                'rule' => array('notBlank'),
                'message' => 'Campo de preenchimento obrigatório.',
            ),
        ),
        'seo_url' => array(
            'url_unica' => array(
                'rule' => array('check_url_unica'),
                'message' => 'Url já existente. Tenta outra.',
            ),
        ),
    );

    public $hasAndBelongsToMany = array(
        'EventoTag' => array(
            'className' => 'EventoTag',
            'joinTable' => 'evento_evento_tags',
            'foreignKey' => 'evento_id',
            'associationForeignKey' => 'tag_id'
        )
    );

    public $hasMany = array(
        'EventoAtributo' => array(
            'className' => 'EventoAtributo',
            'foreignKey' => 'evento_id'
        )
    );


    public function cache()
    {
        $eventos = array();
        if (Cache::read('eventos') === false) {
            $eventos = $this->find('all', array(
                    'conditions' => array(
                        'Evento.status' => 1,
                        '(date(Evento.data_inicio) <= ? AND date(Evento.data_fim) >= ?) || (Evento.data_inicio = "" || Evento.data_inicio IS NULL)' => array(date('Y-m-d'), date('Y-m-d'))
                    )
                )
            );
            Configure::write('eventos', $eventos);
        } else {
            $eventos = Cache::read('eventos');
        }
        $this->cacheData = $eventos;
    }

    public function beforeFind($queryData)
    {

        parent::beforeFind($queryData);

        $params = Router::getParams();

        if (isset($params['fatorcms']) && $params['fatorcms'] == true) {
            App::import("helper", "String");
            $this->String = new StringHelper(new View(null));
            if (is_array($queryData['conditions'])) {
                foreach ($queryData['conditions'] as $key => $condition) {
                    if (is_array($condition)) {
                        foreach ($condition as $y => $con) {
                            if (!is_array($con)) {
                                if ($y == 'Evento.data LIKE' && stripos($con, '/') !== false) {
                                    $queryData['conditions'][$key][$y] = $this->String->dataFormatada("Y-m-d", $con);
                                }
                                if ($y == 'Evento.data_inicio LIKE' && stripos($con, '/') !== false) {
                                    $queryData['conditions'][$key][$y] = $this->String->dataFormatada("Y-m-d", $con);
                                }
                            } else {
                                foreach ($con as $a => $b) {
                                    if ($a == 'Evento.data LIKE' && stripos($b, '/') !== false) {
                                        $queryData['conditions'][$key][$y][$a] = $this->String->dataFormatada("Y-m-d", $b);
                                    }
                                    if ($a == 'Evento.data_inicio LIKE' && stripos($b, '/') !== false) {
                                        $queryData['conditions'][$key][$y][$a] = $this->String->dataFormatada("Y-m-d", $b);
                                    }
                                }
                            }
                        }
                    }
                }
                // $queryData['conditions'] = array_merge($queryData['conditions'], $defaultConditions);
            }
        }

        return $queryData;

    }

    public function beforeValidate($options = array())
    {
        //trata seo_url
//		if (isset($this->data[$this->alias]['nome']) && $this->data[$this->alias]['nome'] != "") {
//			$this->data[$this->alias]['seo_url'] = 	'evento/'.strtolower(Inflector::slug($this->data[$this->alias]['nome'], '-'));
//		}

//        foreach ($this->data['EventoAtributo'] as $evento_atributo) {
//
//        }
//		if(stripos($this->data[$this->alias]['seo_url'], 'evento/') === false){
//			$this->data[$this->alias]['seo_url'] = 'evento/' . $this->data[$this->alias]['seo_url'];
//		}
    }


    public function beforeSave($options = array())
    {
        App::import("helper", "String");
        $this->String = new StringHelper(new View(null));

        $user = AuthComponent::user();
        $this->data[$this->alias]['usuario_id'] = $user['id'];

        // if (isset($this->data[$this->alias]['data']) && $this->data[$this->alias]['data'] != "") {
        // 	$this->data[$this->alias]['data'] = $this->String->dataFormatada("Y-m-d", $this->data[$this->alias]['data']);
        //       }

        if (isset($this->data[$this->alias]['intervalo']) && $this->data[$this->alias]['intervalo'] != "") {
            $datas = explode('-', $this->data[$this->alias]['intervalo']);
            $this->data[$this->alias]['data_inicio'] = $this->String->dataFormatada("Y-m-d", trim($datas[0]));
            if (isset($datas[1]) && trim($datas[1]) != "") {
                $this->data[$this->alias]['data_fim'] = $this->String->dataFormatada("Y-m-d", trim($datas[1]));
            } else {
                $this->data[$this->alias]['data_fim'] = null;
            }
        }

        //trata url
        if (isset($this->data[$this->alias]['nome']) && (isset($this->data[$this->alias]['seo_url']) && $this->data[$this->alias]['seo_url'] == "")) {
            $this->data[$this->alias]['seo_url'] = 'evento/' . strtolower(Inflector::slug($this->data[$this->alias]['nome'], '-'));
        }

        //trata seo_title
        if (isset($this->data[$this->alias]['seo_title']) && $this->data[$this->alias]['seo_title'] == "") {
            $this->data[$this->alias]['seo_title'] = $this->data[$this->alias]['nome'];
        }

        if (isset($this->data[$this->alias]['conteudo'])) {
            $conteudo = utf8_encode(html_entity_decode(strip_tags($this->data[$this->alias]['conteudo'])));
            //trata descricao_resumida
            if (isset($this->data[$this->alias]['descricao_resumida']) && $this->data[$this->alias]['descricao_resumida'] == "") {
                $this->data[$this->alias]['descricao_resumida'] = $this->String->truncate_str($conteudo, 320);
                if (strlen($conteudo) > 320) {
                    $this->data[$this->alias]['descricao_resumida'] .= "...";
                }
            }

            //trata seo_description
            if (isset($this->data[$this->alias]['seo_description']) && $this->data[$this->alias]['seo_description'] == "") {
                $this->data[$this->alias]['seo_description'] = $this->data[$this->alias]['descricao_resumida'];
            }

            //trata seo_description
            if (isset($this->data[$this->alias]['seo_description']) && $this->data[$this->alias]['seo_description'] == "") {
                $this->data[$this->alias]['seo_description'] = $this->String->truncate_str(strip_tags($this->data[$this->alias]['conteudo']), 150);
            }

            if (isset($this->data[$this->alias]['conteudo']) && $this->data[$this->alias]['conteudo'] != "") {
                $this->data[$this->alias]['conteudo'] = str_replace('src="../../app/', 'src="/app/', $this->data[$this->alias]['conteudo']);
                $this->data[$this->alias]['conteudo'] = str_replace('src="../../../app/', 'src="/app/', $this->data[$this->alias]['conteudo']);
            }
        }

        parent::beforeSave($options = array());
    }

    public function afterSave($created, $options = array())
    {

        parent::afterSave($created, $options = array());

        //begin salva o registro da URL nas rotas
        $controller = 'Eventos';
        $model = 'Evento';
        $action = 'detalhe';
        $params_id = null;
        $params_value = null;

        //salvo a rota da categoria
        if (isset($this->data[$this->alias]['seo_url'])) {
            App::import('Model', 'Rota');
            $this->Rota = new Rota();

            $rt = $this->Rota->find('first', array('conditions' => array(
                    'AND' => array(
                        'row_id' => $this->data[$this->alias]['id'],
                        'model' => $model
                    )
                )
                )
            );

            if ($rt) {
                $rota['id'] = $rt['Rota']['id'];
            } else {
                $rota['id'] = null;
            }

            $rota['controller'] = $controller;
            $rota['model'] = $model;
            $rota['action'] = $action;
            $rota['params_id'] = $params_id;
            $rota['params_value'] = $params_value;
            $rota['row_id'] = $this->data[$this->alias]['id'];
            $rota['seo_url'] = $this->data[$this->alias]['seo_url'];
            $rota['buscavel'] = true;
            $rota['buscavel_ordem'] = 3;
            $rota['sites'] = '["portal"]';

            if (isset($this->data[$this->alias]['nome'])) {
                $rota['nome'] = $this->data[$this->alias]['nome'];
            }
            if (isset($this->data[$this->alias]['conteudo'])) {
                $rota['conteudo'] = $this->data[$this->alias]['conteudo'];

                $rota['conteudo'] = strip_tags($rota['conteudo']);
            }
            if (isset($this->data[$this->alias]['seo_keywords'])) {
                $rota['seo_keywords'] = $this->data[$this->alias]['seo_keywords'];
            }
            if (isset($this->data[$this->alias]['seo_description'])) {
                $rota['seo_description'] = $this->data[$this->alias]['seo_description'];
            }
            if (isset($this->data[$this->alias]['data'])) {
                $rota['data'] = $this->data[$this->alias]['data'];
            }

            $this->Rota->save($rota);
        }
        //end salva o registro da URL nas rotas
    }

    public function afterFind($results, $primary = false)
    {
        App::import("helper", "String");
        $this->String = new StringHelper(new View(null));

        $params = Router::getParams();

        //tá no admin? então exibe só os registros da loja atual
        if (isset($params['fatorcms']) && $params['fatorcms'] == true) {
            if (!empty($results)) {
                foreach ($results as $k => &$r) {
                    if (isset($r[$this->alias])) {
                        if ($r[$this->alias]['data_inicio'] != "" && $r[$this->alias]['data_fim'] != "") {
                            @$r[$this->alias]['intervalo'] = $this->String->dataFormatada("d/m/Y", $r[$this->alias]['data_inicio']) . ' - ' . $this->String->dataFormatada("d/m/Y", $r[$this->alias]['data_fim']);
                        } elseif ($r[$this->alias]['data_inicio'] != "") {
                            @$r[$this->alias]['intervalo'] = $this->String->dataFormatada("d/m/Y", $r[$this->alias]['data_inicio']);
                        } else {
                            @$r[$this->alias]['intervalo'] = '';
                        }
                    }
                }
            }
        } else {
            foreach ($results as $k => &$r) {
                if (isset($r[$this->alias])) {

                    if ($r[$this->alias]['data_inicio'] == '0000-00-00' || empty($r[$this->alias]['data_inicio'])) {
                        $r[$this->alias]['intervalo'] = '';
                    } else {
                        if ($r[$this->alias]['data_inicio'] != "" && $r[$this->alias]['data_fim'] != "") {
                            @$r[$this->alias]['intervalo'] = $this->String->dataFormatada("d/m/Y", $r[$this->alias]['data_inicio']) . ' - ' . $this->String->dataFormatada("d/m/Y", $r[$this->alias]['data_fim']);
                        } elseif ($r[$this->alias]['data_inicio'] != "") {
                            @$r[$this->alias]['intervalo'] = $this->String->dataFormatada("d/m/Y", $r[$this->alias]['data_inicio']);
                        }
                    }

                    if (empty($r[$this->alias]['pauta'])) {
                        $r[$this->alias]['pauta'] = '';
                    }
                    if (empty($r[$this->alias]['patrocinio'])) {
                        $r[$this->alias]['patrocinio'] = '';
                    }
                    if (empty($r[$this->alias]['endereco'])) {
                        $r[$this->alias]['endereco'] = '';
                    }
                }
            }
        }

        return $results;
    }

    public function afterDelete()
    {
        parent::afterDelete();

        App::import('Model', 'Rota');
        $this->Rota = new Rota();

        $rota = $this->Rota->find('first', array('conditions' => array('Rota.model' => 'Evento', 'Rota.row_id' => $this->id)));

        if (!empty($rota)) {
            $this->Rota->delete($rota['Rota']['id'], false);
        }

        //removo vinculo na mão, para evitar recursividade
        $this->Rota->query('DELETE FROM evento_evento_tags WHERE evento_id = ' . $this->id);
    }
}