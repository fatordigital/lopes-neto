<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');
App::uses('AuthComponent', 'Controller/Component');

/**
 * EventoData Model
 *
 * @property EventoData $EventoData
 */
class EventoData extends FdEventosAppModel {
	
	public $useTable = "evento_data";
	
	
}