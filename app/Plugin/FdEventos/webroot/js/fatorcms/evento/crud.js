$(document).ready(function(){
    $("[data-slug]").on('keyup', function(){
        $('[name="data[EventoAtributo]['+$(this).data('slug')+'][seo_url]"]').val(slugify($(this).val()));
    });

	$(".count_me").characterCounter({
		limit: '160',
		renderTotal: true,
		counterFormat: '[Recomenda-se utilizar no máximo 160 caracteres no resumo do conteúdo]. %1'
	});

	$(".count_me_320").characterCounter({
		limit: '320',
		renderTotal: true,
		counterFormat: '[Recomenda-se utilizar no máximo 160 caracteres no resumo do conteúdo]. %1'
	});

	$('.date-range-picker').daterangepicker({
		locale: {
			applyLabel: 'Aplicar',
			clearLabel: 'Limpar',
			fromLabel: 'De',
			toLabel: 'Até'
		},
		format: "DD/MM/YYYY"
	});
});