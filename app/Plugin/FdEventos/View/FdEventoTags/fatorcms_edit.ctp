<?php echo $this->Html->script('FdDashboard.jquery.stringToSlug.min'); ?>
<?php echo $this->Html->script('FdEventos.fatorcms/evento_tag/crud.js'); ?>

<?php $this->Html->addCrumb('Eventos', array('controller' => 'noticias', 'action' => 'index')) ?>
<?php $this->Html->addCrumb('Tags de Eventos', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Editar Tag') ?>

    <h3>Editar Tag</h3>

<?php echo $this->Form->create('EventoTag', array('type' => 'file', 'role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
<?php echo $this->Form->input('id') ?>

<?php echo $this->Element('FdEventos.evento_tags_form') ?>

<?php echo $this->Form->end() ?>