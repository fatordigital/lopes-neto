<?php $this->Html->addCrumb(($this->params->params['tipo'] == 'evento') ? 'Eventos' : 'Acadêmico'); ?>

<div class="row">
     <h3 class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
        <?php echo ($this->params->params['tipo'] == 'evento') ? 'Eventos' : 'Acadêmico'; ?> 
        <?php echo $this->Html->link('Cadastrar Evento', array('action' => 'add', 'tipo' => $this->params->params['tipo']), array('class' => 'btn btn-info pull-right')); ?>
    </h3>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <div class="row-fluid">
                    <div class="col-sm-12 col-md-10 col-lg-11">
                        <?php echo $this->FilterForm->create(null, array('role' => 'form', 'class' => 'form-inline')) ?>
                            <div class="form-group">
                                <?php echo $this->FilterForm->input('filtro_nome', array('placeholder' => 'Filtrar por nome', 'class' => 'form-control')) ?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->FilterForm->input('filtro_conteudo', array('placeholder' => 'Filtrar por conteúdo', 'class' => 'form-control')) ?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->FilterForm->input('filtro_data', array('placeholder' => 'Filtrar por data', 'class' => 'form-control data')) ?>
                            </div>
                            <?php echo $this->FilterForm->submit('Filtrar', array('class' => 'btn btn-success', 'div' => false)) ?>
                            <?php echo $this->Html->link('Limpar Filtro', array('action' => 'index'), array('class' => 'btn btn-warning')) ?>
                        <?php echo $this->FilterForm->end() ?>
                    </div>
                    <div class="col-lg-1 visible-md visible-lg">
                       <?php echo $this->Element('FdDashboard.limit'); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="panel-body">
                <table class="table table-hover general-table">
                    <thead>
                        <tr>
                            <th style="width:60px"><?php echo $this->Paginator->sort('id', '#') ?></th>
                            <th><?php echo $this->Paginator->sort('nome') ?></th>
                            <th><?php echo $this->Paginator->sort('data', 'Data') ?></th>
							<th><?php echo $this->Paginator->sort('status') ?></th>
                            <th><?php echo $this->Paginator->sort('modified', 'Modificação') ?></th>
                            <th style="width:180px">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($eventos) > 0): ?>
                            <?php foreach($eventos AS $y => $evento): ?>
                            <tr>
                                <td><?php echo $evento['Evento']['id'] ?></td>
                                <td><?php echo $this->Idioma->extrair($evento['EventoAtributo'], 'nome') ?></td>
                                <td><?php echo $evento['Evento']['intervalo'] ?></td>
								<td>
                                    <?php if($evento['Evento']['status'] != 2): ?>
                                        <input type="checkbox" class="atualiza-status" value="<?php echo $evento['Evento']['status'] == 1 ? 0 : 1 ?>" data-id="<?php echo $evento['Evento']['id'] ?>" data-url="<?php echo $this->Html->url(array('action' => 'status')) ?>" data-on-text="Sim" data-on-color="success" data-off-text="Não" data-off-color="danger"<?php echo $evento['Evento']['status'] == 1 ? ' checked="checked"' : '' ?>>
                                    <?php else: ?>
                                        <label class="btn btn-warning">Rascunho</label>
                                    <?php endIf; ?>
                                </td>
                                <td><?php echo $this->Time->format($evento['Evento']['modified'], '%d/%m/%Y %H:%M:%S') ?></td>
                                <td>
									<?php echo $this->Html->link('<i class="fa fa-edit"></i> Editar', array('action' => 'edit', $evento['Evento']['id'], 'tipo' => $this->params->params['tipo']), array('class' => 'btn btn-primary btn-sm', 'escape' => false)) ?>
									<?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Remover', array('action' => 'delete', $evento['Evento']['id'], 'tipo' => $this->params->params['tipo']), array('class' => 'btn btn-danger btn-sm', 'escape' => false), __('Deseja mesmo remover o registro # %s?', $evento['EventoAtributo'][0]['nome'])) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="5" class="text-center">Nenhum registro encontrado.</td>
                            </tr>
                        <?php endIf; ?>
                    </tbody>
                </table>
				
				<?php
					$this->Paginator->options(
						array(
							'url'=> array(
								'controller' => 'fd_eventos',
								'action' => 'index',
								'fatorcms' => true, 
								'tipo' => $this->params->params['tipo']
							)
						)
					);
				?>

                <?php echo $this->Element('FdDashboard.paginator'); ?>
            </div>
        </section>
    </div>
</div>