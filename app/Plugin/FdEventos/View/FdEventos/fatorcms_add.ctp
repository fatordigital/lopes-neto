<?php echo $this->Html->script('FdDashboard.jquery.stringToSlug.min'); ?>
<?php
echo $this->Html->css('FdDashboard.core/tagedit/css/ui-lightness/jquery-ui-1.9.2.custom.min');
echo $this->Html->css('FdDashboard.core/tagedit/css/jquery.tagedit');
echo $this->Html->script('FdDashboard.core/tagedit/js/jquery.autoGrowInput');
echo $this->Html->script('FdDashboard.core/tagedit/js/jquery.tagedit');
?>

<?php echo $this->Html->script('FdDashboard.core/jquery-character-counter/jquery.charactercounter'); ?>

<?php
echo $this->Html->css('FdDashboard.date-time/datepicker');
echo $this->Html->css('FdDashboard.date-time/daterangepicker');
echo $this->Html->script('FdDashboard.date-time/bootstrap-datepicker.min');
echo $this->Html->script('FdDashboard.date-time/bootstrap-timepicker.min');
echo $this->Html->script('FdDashboard.date-time/moment.min');
echo $this->Html->script('FdDashboard.date-time/daterangepicker.min');
?>

<?php echo $this->Html->script('FdEventos.fatorcms/evento/crud.js'); ?>

<?php $this->Html->addCrumb('Eventos', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Cadastrar Evento') ?>

<h3>Cadastrar Evento</h3>

<?php if (isset($tags) && $tags != ""): ?>
    <script type="text/javascript">
        $(document).ready(function () {
            // Local Source
            var localJSON = <?php echo $tags; ?>;
            $('input.tag').tagedit({
                autocompleteOptions: {
                    source: localJSON
                }
            });
        });
    </script>
<?php endIf; ?>

<?php echo $this->Form->create('Evento', array('type' => 'file', 'role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
<?php echo $this->Form->input('id') ?>
<?php echo $this->Element('FdEventos.evento_form') ?>
<?php echo $this->Form->end() ?>