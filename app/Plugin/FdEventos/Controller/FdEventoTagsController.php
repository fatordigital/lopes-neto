<?php

class FdEventoTagsController extends FdEventosAppController {

	public $uses = array('FdEventos.EventoTag');

/**
 * admin_index method
 *
 * @return void
 */
	public function fatorcms_index($page = 1) {

		// Add filter
		$this->FilterResults->addFilters(
			array(
				'filter' => array(
					'OR' => array(
						'EventoTag.nome'    => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
					)
				),
				'filtro_nome' => array(
					'EventoTag.nome'     	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				),
			)
		);
		// Define conditions
		$this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

		//exportar? 
		if(isset($this->params->params['named']['acao']) && $this->params->params['named']['acao'] == "exportar"){
			$this->Reports->xls($this->EventoTag->find('all', array('conditions' => $this->FilterResults->getConditions(), 'callbacks' => false)), 'Notícias');
		}

		// Paginate
		$options['conditions'] 	= $this->FilterResults->getConditions();
		$options['order'] 		= array('EventoTag.id' => 'ASC');
		$this->paginate = $options;

		$this->EventoTag->recursive = 0;
		$this->set('evento_tags', $this->paginate());
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function fatorcms_add() {
		if ($this->request->is('post')) {
			$this->EventoTag->create();
			if ($this->EventoTag->save($this->request->data)) {
				//set caches
				$this->_resetCaches();
				$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_edit($id = null) {		
		if (!$this->EventoTag->exists($id)) {
			throw new NotFoundException(__('Registro Inválido.'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->EventoTag->save($this->request->data)) {
				//set caches
				$this->_resetCaches();
				$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');

				//$this->redirect(array('action' => 'index'));
				$this->_redirectFilter($this->Session->read('referer'));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}
		} else {
			$options = array('conditions' => array('EventoTag.' . $this->EventoTag->primaryKey => $id));
			$this->request->data = $this->EventoTag->find('first', $options);

			//tratamento para exibir paneas a url
			if($this->request->data['EventoTag']['seo_url'] != "" && stripos($this->request->data['EventoTag']['seo_url'], '/') !== FALSE){
				$explode = array_reverse(explode('/', $this->request->data['EventoTag']['seo_url']));
				$this->request->data['EventoTag']['seo_url'] = $explode[0];
			}

			$this->Session->write('referer', $this->referer());
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_delete($id = null) {
		$this->EventoTag->id = $id;
		if (!$this->EventoTag->exists()) {
            throw new NotFoundException('Registro inválido.');
		}
		$this->request->is('get');
		if ($this->EventoTag->delete()) {
			$this->_resetCaches();
			$this->Session->setFlash(__('Registro deletado.'),'FdDashboard.alerts/fatorcms_success');
			// $this->redirect(array('action' => 'index'));
			$this->_redirectFilter($this->referer());
		}
		$this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
		// $this->redirect(array('action' => 'index'));
		$this->_redirectFilter($this->referer());
	}

/**
 * fatorcms_status method
 *
 * @return void
 */
    public function fatorcms_status(){
        if (!$this->request->is('post')){
            throw new NotFoundException('Registro inválido.');
        }
        echo $this->_saveStatus('EventoTag', $this->request->data['id'], $this->request->data['value']);
        die;
    }
}