<?php

class FdEventosAppController extends AppController {

	public $evento_tipo;

	public function beforeFilter(){

		parent::beforeFilter();

		if(isset($this->params->params['tipo']) && $this->params->params['tipo'] != ''){
			$this->evento_tipo = $this->params->params['tipo'];
		}

		if($this->evento_tipo != ""){

			//BEGIN: validacao dos grupos que gerenciam eventos x calendário academicos
			$user = $this->Auth->user();
			if( ($user['grupo_id'] == 6 && $this->evento_tipo != 'academico') || ($user['grupo_id'] == 5 && $this->evento_tipo != 'evento') ){
				$this->Session->setFlash(__('Você não tem permissão para alterar esse registro'),'FdDashboard.alerts/fatorcms_danger');
				if($this->Session->read('referer') && $this->Session->read('referer') != "/"){
					$this->redirect($this->Session->read('referer'));
				}else{
					$this->redirect(array('fatorcms' => true, 'controller' => 'fd_menus', 'action' => 'index'));
				}
			}
			//END: validacao dos grupos que gerenciam eventos x calendário academicos

			if ($this->request->is('post')) {

				if(isset($this->request->data['Evento'])){
					$this->request->data['Evento']['evento_tipo'] = $this->evento_tipo;			
				}
				
				if(isset($this->request->data['filter'])){
					$this->request->data['filter']['filtro_evento_evento_tipo'] = $this->evento_tipo;
				}
			}
		}		
	}

}