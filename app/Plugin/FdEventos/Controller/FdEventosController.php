<?php

class FdEventosController extends FdEventosAppController
{

    public $uses = array('FdEventos.Evento');

    /**
     * admin_index method
     *
     * @return void
     */
    public function fatorcms_index($page = 1)
    {

        // Add filter
        $this->FilterResults->addFilters(
            array(
                'filter' => array(
                    'OR' => array(
                        'EventoAtributo.nome' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%')),
                        'EventoAtributo.conteudo' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%')),
                    )
                ),
                'filtro_nome' => array(
                    'EventoAtributo.nome' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%'))
                ),
                'filtro_conteudo' => array(
                    'EventoAtributo.conteudo' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%'))
                ),
                'filtro_data' => array(
                    'Evento.data_inicio' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%'))
                ),
            )
        );
        // Define conditions
        $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

        //exportar?
        if (isset($this->params->params['named']['acao']) && $this->params->params['named']['acao'] == "exportar") {
            $this->Reports->xls($this->Evento->find('all', array('conditions' => $this->FilterResults->getConditions(), 'callbacks' => false)), 'Eventos');
        }

        // Paginate
        if (count($this->FilterResults->getConditions()) > 0) {
            $options['conditions'] = array($this->FilterResults->getConditions(), 'Evento.evento_tipo' => $this->evento_tipo);
        } else {
            $options['conditions'] = array('Evento.evento_tipo' => $this->evento_tipo);
        }

        // Paginate
        //$options['conditions'] 	= $this->FilterResults->getConditions();
        $options['order'] = array('data_inicio' => 'ASC');
        $this->paginate = $options;

        $this->set('eventos', $this->paginate());
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function fatorcms_add()
    {
        if ($this->request->is('post')) {

            $this->Evento->create();
            if ($this->Evento->saveAll($this->request->data)) {

                //begin: salva as tags das eventos
                if (isset($this->request->data['Evento']['tags']) && count($this->request->data['Evento']['tags'])) {
                    // $this->loadModel('FdEventos.EventoTag');
                    App::import('Model', 'FdEventos.EventoTag');
                    $this->EventoTag = new EventoTag();
                    $this->loadModel('FdEventos.EventoEventoTag');
                    foreach ($this->request->data['Evento']['tags'] as $key => $tag) {

                        $tag_id = 0;
                        if (stripos($key, '-')) {
                            $explode = explode('-', $key);
                            $tag_id = $explode[0];
                        } else {
                            $new_tag['id'] = null;
                            $new_tag['nome'] = $tag;
                            $new_tag['seo_url'] = "";
                            $new_tag['seo_title'] = $new_tag['nome'];
                            $new_tag['status'] = true;
                            if ($this->EventoTag->save($new_tag)) {
                                $tag_id = $this->EventoTag->id;
                            }
                        }

                        if ($tag_id > 0) {
                            if (!$this->EventoEventoTag->find('count', array('recursive' => -1, 'conditions' => array('AND' => array('tag_id' => $tag_id, 'evento_id' => $this->Evento->id))))) {
                                $this->EventoEventoTag->save(array('id' => null, 'tag_id' => $tag_id, 'evento_id' => $this->Evento->id));
                            }
                        }
                    }
                }
                //end: salva as tags das eventos

                //set caches
                $this->_resetCaches();
                $this->Session->setFlash(__('Registro salvo com sucesso.'), 'FdDashboard.alerts/fatorcms_success');
                $this->redirect(array('action' => 'index', 'tipo' => $this->params->params['tipo']));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'FdDashboard.alerts/fatorcms_danger');
            }
        }

        //set tags de eventos
        $this->loadModel('FdEventos.EventoTag');
        $eventoTags = $this->EventoTag->find('all', array('fields' => array('EventoTag.id', 'EventoTag.nome'), 'conditions' => array('EventoTag.status' => true), 'order' => array('EventoTag.nome ASC')));
        $tags = array();
        if (count($eventoTags) > 0) {
            foreach ($eventoTags as $key => $tag) {
                $tags[] = array('id' => $tag['EventoTag']['id'], 'label' => $tag['EventoTag']['nome'], 'value' => $tag['EventoTag']['nome']);
            }
        }
        $tags = json_encode($tags);
        $this->set(compact('tags'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function fatorcms_edit($id = null)
    {
        if (!$this->Evento->exists($id)) {
            throw new NotFoundException(__('Registro Inválido.'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {

            if ($this->Evento->saveAll($this->request->data)) {

                //begin: salva as tags das eventos
                if (isset($this->request->data['Evento']['tags']) && count($this->request->data['Evento']['tags'])) {
                    // $this->loadModel('FdEventos.EventoTag');
                    App::import('Model', 'FdEventos.EventoTag');
                    $this->EventoTag = new EventoTag();
                    $this->loadModel('FdEventos.EventoEventoTag');

                    // $this->EventoEventoTag->query('DELETE FROM evento_evento_tags WHERE evento_id = ' . $this->Evento->id);

                    //quem já tava vinculado?
                    $ja_vinculados = $this->EventoEventoTag->find('all', array('recursive' => -1, 'conditions' => array('EventoEventoTag.evento_id' => $this->Evento->id)));

                    //removo os registros que estavam vinculados e não vieram selecionados. (pq? logo, o cara 'desmarcou' esse cara)
                    if (!empty($ja_vinculados)) {
                        foreach ($this->request->data['Evento']['tags'] as $key => $tag) {
                            if (stripos($key, '-')) {
                                $explode = explode('-', $key);
                                $tags[] = $explode[0];
                            }
                        }
                        foreach ($ja_vinculados as $key => $vinculo) {
                            if (!in_array($vinculo['EventoEventoTag']['tag_id'], $tags)) {
                                $this->ProdutoLoja->query("DELETE FROM evento_evento_tags WHERE tag_id = " . $vinculo['EventoEventoTag']['tag_id'] . " AND evento_id = " . $this->Evento->id);
                            }
                        }
                    }

                    foreach ($this->request->data['Evento']['tags'] as $key => $tag) {

                        $tag_id = 0;
                        if (stripos($key, '-')) {
                            $explode = explode('-', $key);
                            $tag_id = $explode[0];
                        } else {
                            $new_tag['id'] = null;
                            $new_tag['nome'] = $tag;
                            $new_tag['seo_url'] = "";
                            $new_tag['seo_title'] = $new_tag['nome'];
                            $new_tag['status'] = true;
                            if ($this->EventoTag->save($new_tag)) {
                                $tag_id = $this->EventoTag->id;
                            }
                        }

                        if ($tag_id > 0) {
                            if (!$this->EventoEventoTag->find('count', array('recursive' => -1, 'conditions' => array('AND' => array('tag_id' => $tag_id, 'evento_id' => $this->Evento->id))))) {
                                $this->EventoEventoTag->save(array('id' => null, 'tag_id' => $tag_id, 'evento_id' => $this->Evento->id));
                            }
                            // $this->EventoEventoTag->query('INSERT INTO  evento_evento_tags (evento_id, tag_id) VALUES (' . $this->Evento->id . ', ' . $tag_id . ')');
                        }
                    }
                }
                //end: salva as tags das eventos

                $this->_resetCaches();
                $this->Session->setFlash(__('Registro salvo com sucesso.'), 'FdDashboard.alerts/fatorcms_success');
                //$this->redirect(array('action' => 'index'));
                $this->_redirectFilter($this->Session->read('referer'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'), 'FdDashboard.alerts/fatorcms_danger');
            }
        } else {
            $options = array('conditions' => array('Evento.' . $this->Evento->primaryKey => $id));
            $this->request->data = $this->Evento->find('first', $options);

            $this->Session->write('referer', $this->referer());

            //formato as tags, para ficar no padrão do plugin de tags
            if (isset($this->request->data['EventoTag']) && count($this->request->data['EventoTag']) > 0) {
                foreach ($this->request->data['EventoTag'] as $key => $tag) {
                    $this->request->data['Evento']['tags'][$tag['id'] . '-a'] = $tag['nome'];
                }
            }
        }

        //tratamento para exibir paneas a url
        foreach ($this->request->data['EventoAtributo'] as $key => $item) {
            if ($this->request->data['EventoAtributo'][$key]['seo_url'] != "" && stripos($this->request->data['EventoAtributo'][$key]['seo_url'], '/') !== FALSE) {
                $explode = array_reverse(explode('/', $this->request->data['EventoAtributo'][$key]['seo_url']));
                $this->request->data['EventoAtributo'][$key]['seo_url'] = $explode[0];
            }
        }

        // set evento
        $options = array('conditions' => array('Evento.' . $this->Evento->primaryKey => $id));
        $evento = $this->Evento->find('first', $options);
        $this->set(compact('evento'));

        //set tags de eventos
        $this->loadModel('FdEventos.EventoTag');
        $eventoTags = $this->EventoTag->find('all', array('fields' => array('EventoTag.id', 'EventoTag.nome'), 'conditions' => array('EventoTag.status' => true), 'order' => array('EventoTag.nome ASC')));
        $tags = array();
        if (count($eventoTags) > 0) {
            foreach ($eventoTags as $key => $tag) {
                $tags[] = array('id' => $tag['EventoTag']['id'], 'label' => $tag['EventoTag']['nome'], 'value' => $tag['EventoTag']['nome']);
            }
        }
        $tags = json_encode($tags);
        $this->set(compact('tags'));
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function fatorcms_delete($id = null)
    {
        $this->Evento->id = $id;
        if (!$this->Evento->exists()) {
            throw new NotFoundException('Registro inválido.');
        }
        $this->request->is('get');
        if ($this->Evento->delete()) {
            $this->_resetCaches();
            $this->Session->setFlash(__('Registro deletado.'), 'FdDashboard.alerts/fatorcms_success');

            //removo o vinculo do evento com a noitica
            App::import('Model', 'FdNoticias.Noticia');
            $this->Noticia = new Noticia();
            $noticia = $this->Noticia->find('first', array('conditions' => array('Noticia.evento_id' => $id)));
            if (!empty($noticia)) {
                $this->Noticia->save(array('id' => $noticia['Noticia']['id'], 'evento_id' => null), false);
            }

            // $this->redirect(array('action' => 'index'));
            $this->_redirectFilter($this->referer());
        }
        $this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
        // $this->redirect(array('action' => 'index'));
        $this->_redirectFilter($this->referer());
    }

    /**
     * fatorcms_status method
     *
     * @return void
     */
    public function fatorcms_status()
    {
        if (!$this->request->is('post')) {
            throw new NotFoundException('Registro inválido.');
        }
        echo $this->_saveStatus('Evento', $this->request->data['id'], $this->request->data['value']);
        die;
    }
}