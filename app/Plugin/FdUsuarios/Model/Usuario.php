<?php
App::uses('AuthComponent', 'Controller/Component');

/**
 * Usuario Model
 *
 * @property Usuario $Usuario
 */

class Usuario extends FdUsuariosAppModel {

    public $actsAs = array('Acl' => array('type' => 'requester'));

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'nome' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Informe o nome de exibição',
                'required' => true,
            ),
        ),
        'email' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'O e-mail obrigatório',
                'required' => true,
            ),
            'email' => array(
                'rule' => array('email', true),
                'message' => 'Informe um e-mail válido',
            ),
            'unique' => array(
                'rule' => array('isUnique'),
                'message' => 'Este e-mail já consta em nosso banco de dados',
                'on' => 'create',
            ),
        ),
        'senha_nova' => array(
            'confirm' => array(
                'rule' => array('confirmPassword'),
                'message' => 'As senhas não conferem'
            ),
            'minimoUpdate' => array(
                'rule' => array('between', 6, 8),
                'menssage' => 'A senha deve ter entre 6 à 8 caracteres',
                'allowEmpty' => true,
                'on' => 'update'
            ),
            'preenchido' => array(
                'rule' => 'senha_esta_vazio',
                'message' => 'Campo de preenchimento obrigatório',
            ),
        ),
        'grupo_id' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'O grupo de usuário é obrigatório'
            )
        )
    );

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'Grupo' => array(
            'className' => 'Grupo',
            'foreignKey' => 'grupo_id',
        )
    );

    public function parentNode(){
        if (!$this->id && empty($this->data)){
            return null;
        }

        if (isset($this->data['Usuario']['grupo_id'])){
            $grupoId = $this->data['Usuario']['grupo_id'];
        } else {
            $grupoId = $this->field('grupo_id');
        }

        if (!$grupoId){
            return null;
        } else {
            return array('Grupo' => array('id' => $grupoId));
        }
    }

    // public function bindNode(){
    //     $data = AuthComponent::user();
    //     return array('model' => 'Grupo', 'foreign_key' => $data['Grupo']['id']);
    // }

    public function senha_esta_vazio() {
        if(isset($this->data[$this->alias]['senha_nova']) && empty($this->data[$this->alias]['senha_nova'])){
            return false;
        }
        return true;
    }

    public function confirmPassword($password = null){
        if (isset($this->data[$this->name]['senha_nova']) && isset($this->data[$this->name]['confirma']))
            return (@$this->data[$this->name]['senha_nova'] == @$this->data[$this->name]['confirma']);
        else
            return true;
    }

/**
 * beforeSave
 *
 * sobrecarga do metodo executado antes de salvar o registro
 */
    public function beforeSave($options = array()){
        parent::beforeSave();
        if (isset($this->data[$this->alias]['senha_nova']) && $this->data[$this->alias]['senha_nova'] != ""){
            $this->data[$this->alias]['senha'] = AuthComponent::password($this->data[$this->alias]['senha_nova']);
        } else {
            unset($this->data[$this->alias]['senha_nova']);
        }
        parent::beforeSave($options = array());
    }

    public function generatePAssword($length = 8){
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }
}