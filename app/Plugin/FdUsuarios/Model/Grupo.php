<?php
/**
 * Grupo Model
 *
 * @property Grupo $Grupo
 */

class Grupo extends FdUsuariosAppModel {

    public $actsAs = array('Acl' => array('type' => 'requester'));

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'nome' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'O nome do grupo é obrigatório'
            ),
        )
    );

//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
    public $hasMany = array(
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'grupo_id',
            'dependent' => false,
        )
    );

    public function parentNode(){
        return null;
    }
}