<?php

class FdGruposController extends FdUsuariosAppController {

    public $uses = array('FdUsuarios.Grupo');

/**
 * fatorcms_index method
 *
 * @return void
 */
    public function fatorcms_index(){

        // Add filter
        $this->FilterResults->addFilters(
            array(
                'filtro_nome' => array(
                    'OR' => array(
                        'Grupo.nome' => array('operator' => 'LIKE', 'value' => array('before' => '%', 'after' => '%'))
                    )
                )
            )
        );

        // Set conditions
        // $this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

        // Get user
        $user = $this->Auth->user();

        // Paginate
        $options['conditions']  = $this->FilterResults->getConditions();

        if($user['grupo_id'] != 7){
            if(is_array($options['conditions'])){
                $options['conditions'] = array_merge($options['conditions'], array('Usuario.id <>' => 7));
            }else{
                $options['conditions'] = array('Usuario.id <>' => 7);
            }
        }

        // Exportar? 
        if(isset($this->params->params['named']['acao']) && $this->params->params['named']['acao'] == "exportar"){
            $this->Reports->xls($this->Grupo->find('all', array('conditions' => $this->FilterResults->getConditions(), 'recursive' => -1, 'callbacks' => false)), 'Grupos');
        }

        // Paginate
        $grupos = $this->paginate();
        $this->set(compact('grupos'));
    }

/**
 * fatorcms_add method
 *
 * @return void
 */
    public function fatorcms_add(){
        // eh post?
        if($this->request->is('post')){
            $this->Grupo->create();
            if ($this->Grupo->save($this->request->data)){
                $this->Session->setFlash('Registro salvo com sucesso.','FdDashboard.alerts/fatorcms_success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('O registro não pode ser salvo. Verifique os campos em destaque.','FdDashboard.alerts/fatorcms_danger');
            }
        }
    }

/**
 * fatorcms_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function fatorcms_edit($id = null){
         // Get user logado
        $user = $this->Auth->user();

        if( $user['grupo_id'] != 7 && $user['id'] != $id ){
            $this->Session->setFlash(__('Você não tem permissão para alterar esse registro'),'FdDashboard.alerts/fatorcms_danger');
            $this->_redirectFilter();
        }

        $this->Grupo->id = $id;
        if (!$this->Grupo->exists()){
            throw new NotFoundException('Registro inválido.');
        }

        // eh post?
        if ($this->request->is('post') || $this->request->is('put')){
            if ($this->Grupo->save($this->request->data)){
                $this->Session->setFlash('Registro salvo com sucesso.','FdDashboard.alerts/fatorcms_success');
                // $this->redirect(array('action' => 'index'));
                $this->_redirectFilter($this->Session->read('referer'));
            } else {
                $this->Session->setFlash('O registro não pode ser salvo. Verifique os campos em destaque.','FdDashboard.alerts/fatorcms_danger');
            }
        } else {
            $options = array('conditions' => array('Grupo.' . $this->Grupo->primaryKey => $id));
            $this->request->data = $this->Grupo->find('first', $options);
            $this->Session->write('referer', $this->referer());
        }

        // set grupo
        $this->set('grupo', $this->Grupo->find('first', array('conditions' => array('Grupo.' . $this->Grupo->primaryKey => $id))));
    }

/**
 * fatorcms_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function fatorcms_delete($id = null){
        if (!$this->request->is('get')){
            throw new MethodNotAllowedException();
        }
        $this->Grupo->id = $id;
        if (!$this->Grupo->exists()){
            throw new NotFoundException('Registro inválido.');
        }
        if ($this->Grupo->delete()){
            $this->Session->setFlash('Registro deletado com sucesso.','FdDashboard.alerts/fatorcms_success');
            // $this->redirect(array('action' => 'index'));
            $this->_redirectFilter($this->referer());
        }
        $this->Session->setFlash('O registro não pode ser removido.', 'fatorcms_warning');
        // $this->redirect(array('action' => 'index'));
        $this->_redirectFilter($this->referer());
    }

/**
 * fatorcms_status method
 *
 * @return void
 */
    public function fatorcms_status(){
        if (!$this->request->is('post')){
            throw new NotFoundException('Registro inválido.');
        }
        echo $this->_saveStatus('Grupo', $this->request->data['id'], $this->request->data['value']);
        die;
    }
}