<?php

class FdUsuariosController extends FdUsuariosAppController
{

    public $uses = array('FdUsuarios.Usuario', 'FdUsuarios.Grupo');

    public function beforeFilter()
    {
        parent::beforeFilter();

        App::import('Model', 'FdUsuarios.Usuario');
        $this->Usuario = new Usuario();
    }

    /**
     * fatorcms_login method
     *
     * @return void
     */
    public function fatorcms_login()
    {
        if ($this->Auth->user()) {
            $this->Session->setFlash('Você já está logado.', 'FdDashboard.alerts/fatorcms_success');
            if (stripos($this->referer(), 'login') === false && $this->referer() != "/") {
                $this->redirect($this->referer());
            } else {
                $this->redirect(array('plugin' => 'fd_dashboard', 'controller' => 'fd_dashboard', 'fatorcms' => true));
            }
        }

        $this->layout = 'FdDashboard.login';
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->Usuario->id = $this->Auth->user('id');
                $this->Usuario->saveField('ultimo_acesso', date('Y-m-d H:i:s'));
                return $this->redirect($this->Auth->redirect());
            } else {
                $this->Session->setFlash('Usuário ou senha estão incorretos', 'FdDashboard.alerts/fatorcms_danger', array(), 'auth');
            }
        }
    }

    /**
     * fatorcms_logout method
     *
     * @return void
     */
    public function fatorcms_logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    /**
     * fatorcms_index method
     *
     * @return void
     */
    public function fatorcms_index($page = 1)
    {

        // Add filter
        $this->FilterResults->addFilters(
            array(
                'filtro_nome' => array(
                    'Usuario.nome' => array(
                        'operator' => 'LIKE',
                        'value' => array('before' => '%', 'after' => '%'),
                    ),
                ),
            )
        );

        $this->FilterResults->addFilters(
            array(
                'filtro_email' => array(
                    'Usuario.email' => array(
                        'operator' => 'LIKE',
                        'value' => array('before' => '%', 'after' => '%'),
                    ),
                ),
            )
        );

        $this->FilterResults->addFilters(
            array(
                'filtro_grupo' => array(
                    'Grupo.nome' => array(
                        'operator' => 'LIKE',
                        'value' => array('before' => '%', 'after' => '%'),
                    ),
                ),
            )
        );

        $this->FilterResults->setPaginate('page', $page);

        // Get user
        $user = $this->Auth->user();

        // Paginate
        $options['conditions'] = $this->FilterResults->getConditions();

        // Não é admin? então não listo os administradores
        if ($user['grupo_id'] != 1 && $user['grupo_id'] != 7) {
            if (is_array($options['conditions'])) {
                $options['conditions'] = array_merge($options['conditions'], array('Usuario.grupo_id <>' => 1, 'Usuario.grupo_id <>' => 7));
            } else {
                $options['conditions'] = array('Usuario.grupo_id <>' => 1, 'Usuario.grupo_id <>' => 7);
            }
        } elseif ($user['grupo_id'] != 7) {
            if (is_array($options['conditions'])) {
                $options['conditions'] = array_merge($options['conditions'], array('Usuario.grupo_id <>' => 7));
            } else {
                $options['conditions'] = array('Usuario.grupo_id <>' => 7);
            }
        }
        $this->paginate = $options;

        // Exportar?
        if (isset($this->params->params['named']['acao']) && $this->params->params['named']['acao'] == "exportar") {
            $this->Reports->xls($this->Usuario->find('all', array('conditions' => $options['conditions'], 'recursive' => -1, 'callbacks' => false)), 'Usuarios');
        }

        // Paginate
        $usuarios = $this->paginate();
        $this->set(compact('usuarios'));
    }

    /**
     * fatorcms_add method
     *
     * @return void
     */
    public function fatorcms_add()
    {
        // eh post?
        if ($this->request->is('post')) {
            $this->Usuario->create();
            if ($this->Usuario->save($this->request->data)) {
                $id = $this->Usuario->getInsertID();
                if ($this->request->data['Usuario']['enviar']) {
                    $this->sendPassword($id);
                }
                $this->Session->setFlash('Registro salvo com sucesso.', 'FdDashboard.alerts/fatorcms_success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('O registro não pode ser salvo. Verifique os campos em destaque.', 'FdDashboard.alerts/fatorcms_danger');
            }
        }

        // Set grupos
        // Get user logado
        $user = $this->Auth->user();

        // Não listo o grupo de admin se o usuario logado não for admin
        if ($user['grupo_id'] == 1) {
            $this->set('grupos', $this->Grupo->find('list', array('recursive' => -1, 'fields' => array('Grupo.id', 'Grupo.nome'), 'conditions' => array('Grupo.id <>' => 7))));
        } elseif ($user['grupo_id'] != 7 && $user['grupo_id'] != 1) {
            $this->set('grupos', $this->Grupo->find('list', array('recursive' => -1, 'fields' => array('Grupo.id', 'Grupo.nome'), 'conditions' => array('Grupo.id <>' => 1, 'Grupo.id <>' => 7))));
        } else {
            $this->set('grupos', $this->Grupo->find('list', array('recursive' => -1, 'fields' => array('Grupo.id', 'Grupo.nome'))));
        }
    }

    /**
     * fatorcms_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function fatorcms_edit($id = null)
    {

        // Get user logado
        $user = $this->Auth->user();

        // Se o usuário a ser exibo é admin, e o usuario logado não for, não deixo entrar
        if (($user['grupo_id'] != 1 && $user['grupo_id'] != 7) && $user['id'] != $id) {
            $this->Session->setFlash(__('Você não tem permissão para alterar esse registro'), 'FdDashboard.alerts/fatorcms_danger');
            $this->_redirectFilter();
        }

        if ($user['grupo_id'] != 7 && $user['id'] != $id) {
            $this->Session->setFlash(__('Você não tem permissão para alterar esse registro'), 'FdDashboard.alerts/fatorcms_danger');
            $this->_redirectFilter();
        }


        $this->Usuario->id = $id;
        if (!$this->Usuario->exists()) {
            throw new NotFoundException('Registro inválido.');
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Usuario->save($this->request->data)) {
                // if ($this->request->data['Usuario']['enviar']){
                //     $this->sendPassword($id);
                // }
                $this->Session->setFlash('Registro salvo com sucesso.', 'FdDashboard.alerts/fatorcms_success');
                //$this->redirect(array('action' => 'index'));
                $this->_redirectFilter($this->Session->read('referer'));
            } else {
                $this->Session->setFlash('O registro não pode ser salvo. Verifique os campos em destaque.', 'FdDashboard.alerts/fatorcms_danger');
            }
        } else {
            $options = array('conditions' => array('Usuario.' . $this->Usuario->primaryKey => $id));
            $this->request->data = $this->Usuario->find('first', $options);
            $this->Session->write('referer', $this->referer());
        }

        // Set usuario
        $options = array('conditions' => array('Usuario.' . $this->Usuario->primaryKey => $id));
        $usuario = $this->Usuario->find('first', $options);
        $this->set(compact('usuario'));

        // Get user logado
        $user = $this->Auth->user();

        // // Se o usuário a ser exibo é admin, e o usuario logado não for, não deixo entrar
        // if($this->request->data['Usuario']['grupo_id'] == 1 && $user['grupo_id'] != 1){
        //     $this->Session->setFlash(__('Você não tem permissão para alterar esse registro'),'FdDashboard.alerts/fatorcms_danger');
        //     $this->_redirectFilter();
        // }

        // Set grupos
        // Não listo o grupo de admin se o usuario logado não for admin
        if ($user['grupo_id'] == 1) {
            $this->set('grupos', $this->Grupo->find('list', array('recursive' => -1, 'fields' => array('Grupo.id', 'Grupo.nome'), 'conditions' => array('Grupo.id <>' => 7))));
        } elseif ($user['grupo_id'] != 7 && $user['grupo_id'] != 1) {
            $this->set('grupos', $this->Grupo->find('list', array('recursive' => -1, 'fields' => array('Grupo.id', 'Grupo.nome'), 'conditions' => array('Grupo.id <>' => 1, 'Grupo.id <>' => 7))));
        } else {
            $this->set('grupos', $this->Grupo->find('list', array('recursive' => -1, 'fields' => array('Grupo.id', 'Grupo.nome'))));
        }
    }


    /**
     * fatorcms_update_pass method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function fatorcms_update_pass($id = null)
    {

        // Get user logado
        $user = $this->Auth->user();

        $id = base64_decode($id);
        $this->Usuario->id = $id;
        if (!$this->Usuario->exists()) {
            throw new NotFoundException('Registro inválido.');
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->request->data['Usuario']['grupo_id'] = $user['grupo_id'];
            if ($this->Usuario->save($this->request->data, false)) {
                $this->Session->setFlash('Registro salvo com sucesso.', 'FdDashboard.alerts/fatorcms_success');
                //$this->redirect(array('action' => 'index'));
                $this->_redirectFilter($this->Session->read('referer'));
            } else {
                $this->Session->setFlash('O registro não pode ser salvo. Verifique os campos em destaque.', 'FdDashboard.alerts/fatorcms_danger');
            }
        } else {
            $options = array('conditions' => array('Usuario.' . $this->Usuario->primaryKey => $id));
            $this->request->data = $this->Usuario->find('first', $options);
            $this->Session->write('referer', $this->referer());
            unset($this->request->data['Usuario']['senha']);
        }

        // Set usuario
        $options = array('conditions' => array('Usuario.' . $this->Usuario->primaryKey => $id));
        $usuario = $this->Usuario->find('first', $options);
        $this->set(compact('usuario'));

        // Get user logado
        $user = $this->Auth->user();
    }

    /**
     * fatorcms_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function fatorcms_delete($id = null)
    {
        if (!$this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        $this->Usuario->id = $id;
        if (!$this->Usuario->exists()) {
            throw new NotFoundException('Registro inválido.');
        }
        if ($this->Usuario->delete()) {
            $this->Session->setFlash('Registro deletado com sucesso.', 'FdDashboard.alerts/fatorcms_success');
            // $this->redirect(array('action' => 'index'));
            $this->_redirectFilter($this->referer());
        }
        $this->Session->setFlash('O registro não pode ser removido.', 'fatorcms_warning');
        // $this->redirect(array('action' => 'index'));
        $this->_redirectFilter($this->referer());
    }

    /**
     * fatorcms_status method
     *
     * @return void
     */
    public function fatorcms_status()
    {
        if (!$this->request->is('post')) {
            throw new NotFoundException('Registro inválido.');
        }
        echo $this->_saveStatus('Usuario', $this->request->data['id'], $this->request->data['value']);
        die;
    }

    /**
     * sendPassword method
     *
     * @return void
     */
    public function sendPassword($id)
    {
        App::uses('CakeEmail', 'Network/Email');
        App::uses('HtmlHelper', 'View/Helper');

        $this->Email = new CakeEmail('smtp');
        $this->Html = new HtmlHelper(new View());

        $senha = $this->Usuario->generatePAssword();

        $mensagem = 'Olá ' . $this->request->data['Usuario']['nome'] . ',' . "\r\n\r\n";
        $mensagem .= 'Estamos enviando para o seu e-mail, a senha da Fator CMS.' . "\r\n";
        $mensagem .= 'A senha é: ' . $senha . "\r\n\r\n";
        $mensagem .= 'Para acessar a Fator CMS, acesse:' . $this->Html->url('/fatorcms', true) . "\r\n";
        $mensagem .= 'Qualquer dúvida, não deixe de entrar em contato conosco pelo email, fator@fatordigital.com.br' . "\r\n\r\n";
        $mensagem .= 'Atenciosamente,' . "\r\n";
        $mensagem .= 'Fator Digital';

        $this->Email->to($this->request->data['Usuario']['email']);
        $this->Email->replyTo('fator@fatordigital.com.br');
        $this->Email->subject('Evnvio de senha');
        $this->Email->send($mensagem);

        //$this->request->data['Usuario']['senha'] = $senha;
        $this->Usuario->id = $id;
        $this->Usuario->saveField('senha', $senha);
    }

}