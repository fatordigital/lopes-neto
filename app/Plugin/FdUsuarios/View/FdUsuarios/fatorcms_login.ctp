<?php echo $this->Form->create('', array('class' => 'form-signin')) ?>
<h2 class="form-signin-heading"><?php echo $this->Html->image('fatorcms/logo.png'); ?></h2>
<div class="login-wrap">
    <div class="user-login-info">
        <?php echo $this->Form->input('email', array('type' => 'text','class' => 'form-control', 'placeholder' => 'E-mail', 'autofocus', 'div' => false, 'label' => false)) ?>
        <?php echo $this->Form->input('senha', array('class' => 'form-control', 'placeholder' => 'Senha', 'div' => false, 'label' => false, 'type' => 'password')) ?>
    </div>
   <!--  <label class="checkbox">
       <span class="pull-right">
           <a data-toggle="modal" href="#myModal"> Esqueci minha senha</a>

       </span>
   </label> -->
    <button class="btn btn-lg btn-login btn-block" type="submit">Fazer login</button>

</div>

  <!-- Modal -->
  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Forgot Password ?</h4>
              </div>
              <div class="modal-body">
                  <p>Enter your e-mail address below to reset your password.</p>
                  <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

              </div>
              <div class="modal-footer">
                  <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                  <button class="btn btn-success" type="button">Submit</button>
              </div>
          </div>
      </div>
  </div>
  <!-- modal -->

<?php echo $this->Form->end() ?>