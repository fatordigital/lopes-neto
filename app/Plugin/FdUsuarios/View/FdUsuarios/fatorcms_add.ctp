<?php $this->Html->addCrumb('Usuários', array('action' => 'index')) ?>
<?php $this->Html->addCrumb('Cadastrar Usuário') ?>

<h3>Cadastrar Usuário</h3>

<div class="panel">
	<div class="panel-body">
		<?php echo $this->Form->create('Usuario', array('role' => 'form', 'class' => 'minimal', 'novalidate' => true)) ?>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<label>Status</label>
						<div class="icheck">
							<?php echo $this->Form->input('status', array(
																		'type' => 'radio', 
																		'options' => array(1 => 'Ativo', 0 => 'Inativo'), 
																		'default' => 1, 
																		'legend' => false, 
																		'before' => '<div class="radio">', 
																		'after' => '</div>', 
																		'separator' => '</div><div class="clearfix"></div><div class="radio">')); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('grupo_id', array('options' => $grupos, 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('nome', array('label' => 'Nome', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('email', array('label' => 'E-mail', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<?php /*
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<div class="icheck">
							<div class="checkbox single-row">
								<?php echo $this->Form->input('enviar', array('label' => 'Gerar senha automática e enviar por e-mail', 'type' => 'checkbox')) ?>
							</div>
						</div>
					</div>
				</div>
			</div> */ ?>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('senha_nova', array('label' => 'Senha de acesso', 'type'=>'password', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('confirma', array('label' => 'Confirmar senha de acesso', 'type' => 'password', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div>
			<?php /*
			<div class="row">
				<div class="col-md-6 col-lg-6">
					<div class="form-group">
						<?php echo $this->Form->input('telefone', array('label' => 'Telefone', 'class' => 'form-control')) ?>
					</div>
				</div>
			</div> */ ?>
			<?php echo $this->Form->submit('Salvar', array('class' => 'btn btn-success', 'div' => false)) ?>
			<a href="<?php echo $this->Html->Url(array('action' => 'index')); ?>" class="btn btn-default">Cancelar</a>
		<?php echo $this->Form->end() ?>
	</div>
</div>