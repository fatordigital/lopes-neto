<?php

App::uses('SiteUploadHandler', 'Lib');

class FdSitesController extends FdSitesAppController {

	public $uses = array('FdSites.Site');

/**
 * admin_index method
 *
 * @return void
 */
	public function fatorcms_index($page = 1) {

		// Add filter
		$this->FilterResults->addFilters(
			array(
				'filter' => array(
					'OR' => array(
						'Site.nome'   => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
					)
				),
				'filtro_nome' => array(
					'Site.nome'     	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				),
			)
		);

		$this->FilterResults->setPaginate('page', $page);
		// Define conditions
		$this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

		// Site
		$this->Site->recursive = 0;
		$sites = $this->paginate();

		$this->set('sites', $sites);
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function fatorcms_add() {
		if ($this->request->is('post')) {
			$this->Site->create();
			if ($this->Site->save($this->request->data)) {
				$this->_resetCaches();
				$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_edit($id = null) {
		if (!$this->Site->exists($id)) {
            throw new NotFoundException('Registro inválido.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Site->save($this->request->data)) {
				$this->_resetCaches();
				$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');

				//$this->redirect(array('action' => 'index'));
				$this->_redirectFilter($this->Session->read('referer'));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}
		} else {
			$options = array('conditions' => array('Site.' . $this->Site->primaryKey => $id));
			$this->request->data = $this->Site->find('first', $options);

			$this->Session->write('referer', $this->referer());
		}

		// set site
		$options = array('conditions' => array('Site.' . $this->Site->primaryKey => $id));
		$site = $this->Site->find('first', $options);
		$this->set(compact('site'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_delete($id = null) {
		$this->Site->id = $id;
		if (!$this->Site->exists()) {
            throw new NotFoundException('Registro inválido.');
		}
		$this->request->is('get');
		if ($this->Site->delete()) {
			$this->_resetCaches();
			$this->Session->setFlash(__('Registro deletado.'),'FdDashboard.alerts/fatorcms_success');
			// $this->redirect(array('action' => 'index'));
			$this->_redirectFilter($this->referer());
		}
		$this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
		// $this->redirect(array('action' => 'index'));
		$this->_redirectFilter($this->referer());
	}

/**
 * fatorcms_status method
 *
 * @return void
 */
    public function fatorcms_status(){
        if (!$this->request->is('post')){
            throw new NotFoundException('Registro inválido.');
        }
        echo $this->_saveStatus('Site', $this->request->data['id'], $this->request->data['value']);
		$this->_resetCaches();
        die;
    }
}