<?php
	Router::connect("/fatorcms/galerias", array('plugin' => 'fd_galerias', 'controller' => 'fd_galerias', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
    Router::connect("/fatorcms/galerias/:action/*", array('plugin' => 'fd_galerias', 'controller' => 'fd_galerias', 'prefix' => 'fatorcms', 'fatorcms' => true));
	
	Router::connect("/fatorcms/galeria_tipos", array('plugin' => 'fd_galerias', 'controller' => 'fd_galeria_tipos', 'action' => 'index', 'prefix' => 'fatorcms', 'fatorcms' => true));
    Router::connect("/fatorcms/galeria_tipos/:action/*", array('plugin' => 'fd_galerias', 'controller' => 'fd_galeria_tipos', 'prefix' => 'fatorcms', 'fatorcms' => true));