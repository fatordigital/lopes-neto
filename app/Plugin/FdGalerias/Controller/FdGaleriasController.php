<?php

App::uses('GaleriaUploadHandler', 'Lib');

class FdGaleriasController extends FdGaleriasAppController {

	public $uses = array('FdGalerias.Galeria');

/**
 * admin_index method
 *
 * @return void
 */
	public function fatorcms_index($page = 1) {

		//busco os tipos de galerias
		$this->loadModel('FdGalerias.GaleriaTipo');
		$galeria_tipos = $this->GaleriaTipo->find('list', array('fields' => array('GaleriaTipo.id', 'GaleriaTipo.nome'), 'conditions' => array('GaleriaTipo.status' => true)));

		// Add filter
		$this->FilterResults->addFilters(
			array(
				'filter' => array(
					'OR' => array(
						'Galeria.nome'   => array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' )),
					)
				),
				'filtro_nome' => array(
					'Galeria.nome'     	=> array('operator' => 'LIKE', 'value' => array( 'before' => '%', 'after'  => '%' ))
				),
				'filtro_galeria_tipo_id' => array(
					'Galeria.galeria_tipo_id'    =>  array('select' => $this->FilterResults->select('Tipo de Galeria...', $galeria_tipos))
				),
			)
		);

		$this->FilterResults->setPaginate('page', $page);
		// Define conditions
		$this->FilterResults->setPaginate('conditions', $this->FilterResults->getConditions());

		// Galeria
		$this->Galeria->recursive = 0;
		$galerias = $this->paginate();

		$this->set('galerias', $galerias);
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function fatorcms_add() {
		App::import('Model', 'FdGalerias.GaleriaImagem');
		
		if ($this->request->is('post')) {
			$this->Galeria->create();
			if ($this->Galeria->saveAll($this->request->data)) {
				$this->_resetCaches();
				$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}
		}

		// set tipos de galerias
		$galeriaTipos = $this->Galeria->GaleriaTipo->find('list', array('fields' => array('GaleriaTipo.id', 'GaleriaTipo.nome'), 'conditions' => array('GaleriaTipo.status' => true), 'order' => array('GaleriaTipo.nome ASC')));
		$this->set(compact('galeriaTipos'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_edit($id = null) {
		App::import('Model', 'FdGalerias.GaleriaImagem');

		if (!$this->Galeria->exists($id)) {
            throw new NotFoundException('Registro inválido.');
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Galeria->saveAll($this->request->data)) {
				$this->_resetCaches();
				$this->Session->setFlash(__('Registro salvo com sucesso.'),'FdDashboard.alerts/fatorcms_success');

				//$this->redirect(array('action' => 'index'));
				$this->_redirectFilter($this->Session->read('referer'));
			} else {
				$this->Session->setFlash(__('O registro não pode ser salvo. Verifique os campos em destaque.'),'FdDashboard.alerts/fatorcms_danger');
			}
		} else {
			$options = array('conditions' => array('Galeria.' . $this->Galeria->primaryKey => $id));
			$this->request->data = $this->Galeria->find('first', $options);

			$this->Session->write('referer', $this->referer());
		}

		// set galeria
		$options = array('conditions' => array('Galeria.' . $this->Galeria->primaryKey => $id));
		$galeria = $this->Galeria->find('first', $options);
		$this->set(compact('galeria'));

		// set tipos de galerias
		$galeriaTipos = $this->Galeria->GaleriaTipo->find('list', array('fields' => array('GaleriaTipo.id', 'GaleriaTipo.nome'), 'conditions' => array('GaleriaTipo.status' => true), 'order' => array('GaleriaTipo.nome ASC')));
		$this->set(compact('galeriaTipos'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_delete($id = null) {
		App::import('Model', 'FdGalerias.GaleriaImagem');
		$this->Galeria->id = $id;
		if (!$this->Galeria->exists()) {
            throw new NotFoundException('Registro inválido.');
		}
		$this->request->is('get');
		if ($this->Galeria->delete()) {
			$this->_resetCaches();
			$this->Session->setFlash(__('Registro deletado.'),'FdDashboard.alerts/fatorcms_success');
			// $this->redirect(array('action' => 'index'));
			$this->_redirectFilter($this->referer());
		}
		$this->Session->setFlash(__('Registro não pode ser deletado.'), 'fatorcms_warning');
		// $this->redirect(array('action' => 'index'));
		$this->_redirectFilter($this->referer());
	}

/**
 * fatorcms_images_save method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_images_save(){
		if(isset($this->params->data['id']) && isset($this->params->data['legenda'])){
			if($this->params->data['id'] > 0){
				App::import('Model', 'FdGalerias.GaleriaImagem');
        		$this->GaleriaImagem = new GaleriaImagem();
        		$this->GaleriaImagem->id = $this->params->data['id'];
        		$this->GaleriaImagem->save(array('legenda' => $this->params->data['legenda']), false);
        		die('1');
			}
		}else{
			die('0');
		}
	}

/**
 * fatorcms_images method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_images($galeria_id = null) {
		// $this->layout = false;
		Configure::write('debug', 0);
		App::import('Model', 'FdGalerias.GaleriaImagem');
		if (!$this->Galeria->exists($galeria_id)) {
            throw new NotFoundException('Registro inválido.');
		}

		// set campus
		$options = array('conditions' => array('Galeria.' . $this->Galeria->primaryKey => $galeria_id));
		$galeria = $this->Galeria->find('first', $options);
		$this->set(compact('galeria'));

		$this->set(compact('galeria_id'));
	}

/**
 * fatorcms_upload method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function fatorcms_upload($galeria_id = null) {
		// Configure::write('debug', 0);
		if(isset($_GET['galeria_id']) && $_GET['galeria_id'] > 0){
			$galeria_id = $_GET['galeria_id'];
		}

		$this->layout = false;
		$this->autoRender = false;
		$options['url_root'] = Router::Url('/', true);
		$options['path_id'] = $galeria_id;
		$upload_handler = new GaleriaUploadHandler($options);
	}

/**
 * fatorcms_status method
 *
 * @return void
 */
    public function fatorcms_status(){
        if (!$this->request->is('post')){
            throw new NotFoundException('Registro inválido.');
        }
        echo $this->_saveStatus('Galeria', $this->request->data['id'], $this->request->data['value']);
        die;
    }
}