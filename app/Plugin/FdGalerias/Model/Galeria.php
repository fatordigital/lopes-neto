<?php
App::uses('AppModel', 'Model');
App::uses('CakeSession', 'Model/Datasource');

class Galeria extends FdGaleriasAppModel {

	public $cursos = 1;
	public $noticias = 2;

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'nome' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo de preenchimento obrigatório.',
			),
		),
	);

//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'GaleriaImagem' => array(
			'className' => 'GaleriaImagem',
			'foreignKey' => 'galeria_id',
			// 'dependent' => false
		),
	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'GaleriaTipo' => array(
			'className' => 'GaleriaTipo',
			'foreignKey' => 'galeria_tipo_id'
		),
	);
}