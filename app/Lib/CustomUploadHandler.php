<?php
/*
 * jQuery File Upload Plugin PHP Example 5.14
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

require('UploadHandler.php');
// require('Wideimage/WideImage.php');

class CustomUploadHandler extends UploadHandler {

	protected function initialize() {
        //ClassRegistry::init("Campus");
        App::import('Model', 'FdCursos.CampusImagem');
        $this->CampusImagem = new CampusImagem();

        $this->options['script_url'] = $this->options['url_root'].'fatorcms/campi/upload/';
        $this->options['upload_dir'] = dirname($this->get_server_var('SCRIPT_FILENAME')).'/files/campi/file/'.$this->options['path_id'].'/';
        $this->options['upload_url'] = $this->options['url_root'].'files/campi/file/'.$this->options['path_id'].'/';

        parent::initialize();
    }


    //sobrecarga do metodo que obtem informacoes extra, além das infos das imagens
    protected function handle_form_data($file, $index) {
        // $file->ordem = @$_REQUEST['ordem'][$index];
        // $file->legenda = @$_REQUEST['legenda'][$index];
        $file->campus_id = @$_REQUEST['campus_id'][$index];
    }

    //sobrecarga do metodo que faz o upload, para inserir as fotos no banco, com o vinculo necessario
    protected function handle_file_upload($uploaded_file, $name, $size, $type, $error, $index = null, $content_range = null) {
        $tmp_name = $name;
        $tmp_name = explode('.', $tmp_name);
        $name = strtolower(Inflector::slug($tmp_name[0], '-'));

        $fileDir = $name . '.' . $tmp_name[1];

        $exist = $this->options['upload_dir'] . $fileDir;

        if(file_exists($exist)){
            $count = count(glob($dir . '*.*'));
            $name = $name . '-' . $count;
        }

        $file = parent::handle_file_upload(
            $uploaded_file, $name, $size, $type, $error, $index, $content_range
        );
        if (empty($file->error)) {

            $file_name = $file->name;

            $data = array(
                    'id' => null,
                    'campus_id' => $file->campus_id,
                    'file' => $file_name,
                    'dir' => $file->campus_id,
                    'type' => $file->type,
                    'size' => $file->size,
                    'legenda' => $file->legenda,
                    'created' => date('Y-m-d H-i-s'),
                    'modified' => date('Y-m-d H-i-s'),
                    );
            $this->CampusImagem->save($data);

            // $dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'files' . DS . 'campi'. DS  . 'file' . DS . $file->grade_id;
            // $imagem = $dir . DS . $file->name;

            // if(is_file($imagem)){
            //     $extract = array_reverse(explode(".", utf8_encode($file->name)));

            //     //800, 600
            //     $novo_dir   = $dir.DS.'zoom';
            //     $outFile    = $novo_dir.DS.$file->name;
            //     if(!is_dir($novo_dir)){
            //         if(mkdir($novo_dir)){
            //             // WideImage::load($imagem)->resize(800, 800, 'inside')->saveToFile($outFile);
            //             // $image = WideImage::load($outFile);
            //             // $image->resizeCanvas(800, 800, 'center', 'center')->saveToFile($outFile);
            //             WideImage::load($imagem)->resize(800, 800, 'outside')->crop('center', 'center', 800, 800)->saveToFile($outFile);
            //         }
            //     }else{
            //         // WideImage::load($imagem)->resize(800, 800, 'inside')->saveToFile($outFile);
            //         // $image = WideImage::load($outFile);
            //         // $image->resizeCanvas(800, 800, 'center', 'center')->saveToFile($outFile);
            //         WideImage::load($imagem)->resize(800, 800, 'outside')->crop('center', 'center', 800, 800)->saveToFile($outFile);
            //     }

            //     //470, 313
            //     $novo_dir   = $dir.DS.'thumb';
            //     $outFile    = $novo_dir.DS.$file->name;
            //     if(!is_dir($novo_dir)){
            //         if(mkdir($novo_dir)){
            //             // WideImage::load($imagem)->resize(447, 447, 'inside')->saveToFile($outFile);
            //             // $image = WideImage::load($outFile);
            //             // $image->resizeCanvas(447, 447, 'center', 'center')->saveToFile($outFile);
            //             WideImage::load($imagem)->resize(470, 313, 'outside')->crop('center', 'center', 470, 313)->saveToFile($outFile);
            //         }
            //     }else{
            //         // WideImage::load($imagem)->resize(447, 447, 'inside')->saveToFile($outFile);
            //         // $image = WideImage::load($outFile);
            //         // $image->resizeCanvas(447, 447, 'center', 'center')->saveToFile($outFile);
            //         WideImage::load($imagem)->resize(470, 313, 'outside')->crop('center', 'center', 470, 313)->saveToFile($outFile);
            //     }


            //     //270, 180
            //     $novo_dir   = $dir.DS.'medium';
            //     $outFile    = $novo_dir.DS.$file->name;
            //     if(!is_dir($novo_dir)){
            //         if(mkdir($novo_dir)){
            //             // WideImage::load($imagem)->resize(447, 447, 'inside')->saveToFile($outFile);
            //             // $image = WideImage::load($outFile);
            //             // $image->resizeCanvas(447, 447, 'center', 'center')->saveToFile($outFile);
            //             WideImage::load($imagem)->resize(270, 180, 'outside')->crop('center', 'center', 270, 180)->saveToFile($outFile);
            //         }
            //     }else{
            //         // WideImage::load($imagem)->resize(447, 447, 'inside')->saveToFile($outFile);
            //         // $image = WideImage::load($outFile);
            //         // $image->resizeCanvas(447, 447, 'center', 'center')->saveToFile($outFile);
            //         WideImage::load($imagem)->resize(270, 180, 'outside')->crop('center', 'center', 270, 180)->saveToFile($outFile);
            //     }

            //     //156, 104
            //     $novo_dir   = $dir.DS.'semi-thumbnail';
            //     $outFile    = $novo_dir.DS.$file->name;
            //     if(!is_dir($novo_dir)){
            //         if(mkdir($novo_dir)){
            //             // WideImage::load($imagem)->resize(257, 257, 'inside')->saveToFile($outFile);
            //             // $image = WideImage::load($outFile);
            //             // $image->resizeCanvas(257, 257, 'center', 'center')->saveToFile($outFile);
            //             WideImage::load($imagem)->resize(156, 104, 'outside')->crop('center', 'center', 156, 104)->saveToFile($outFile);
            //         }
            //     }else{
            //         // WideImage::load($imagem)->resize(257, 257, 'inside')->saveToFile($outFile);
            //         // $image = WideImage::load($outFile);
            //         // $image->resizeCanvas(257, 257, 'center', 'center')->saveToFile($outFile);
            //         WideImage::load($imagem)->resize(156, 104, 'outside')->crop('center', 'center', 156, 104)->saveToFile($outFile);
            //     }

            //     //110, 110
            //     $novo_dir   = $dir.DS.'thumbnail';
            //     $outFile    = $novo_dir.DS.$file->name;
            //     if(!is_dir($novo_dir)){
            //         if(mkdir($novo_dir)){
            //             // WideImage::load($imagem)->resize(80, 80, 'inside')->saveToFile($outFile);
            //             // $image = WideImage::load($outFile);
            //             // $image->resizeCanvas(80, 80, 'center', 'center')->saveToFile($outFile);
            //             WideImage::load($imagem)->resize(110, 110, 'outside')->crop('center', 'center', 110, 110)->saveToFile($outFile);
            //         }
            //     }else{
            //         // WideImage::load($imagem)->resize(80, 80, 'inside')->saveToFile($outFile);
            //         // $image = WideImage::load($outFile);
            //         // $image->resizeCanvas(80, 80, 'center', 'center')->saveToFile($outFile);
            //         WideImage::load($imagem)->resize(110, 110, 'outside')->crop('center', 'center', 110, 110)->saveToFile($outFile);
            //     }

            // }

            $file->id = $this->CampusImagem->id;
        }
        return $file;
    }

    //sobrecarga do metodo que retorna as infos de cada imagem
    protected function set_additional_file_properties($file) {
        parent::set_additional_file_properties($file);
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $CampusImagem = $this->CampusImagem->find('first', array('recursive' => -1, 'conditions' => array("CampusImagem.file LIKE '%$file->name%'")));
            if($CampusImagem){
                $file->id = $CampusImagem['CampusImagem']['id'];
                $file->type = $CampusImagem['CampusImagem']['type'];
                $file->legenda = $CampusImagem['CampusImagem']['legenda'];
                $file->campus_id = $CampusImagem['CampusImagem']['campus_id'];
            }
        }
    }

     //sobrecarga do metodo que retorna as infos de cada imagem
    protected function get_file_objects($iteration_method = 'get_file_object') {
        $images = parent::get_file_objects($iteration_method);
        $return_images = array();
        if(count($images) > 0){
            foreach ($images as $key => $img) {
                if(!isset($img->id)){
                    unset($images[$key]);
                }else{
                    $return_images[] = $img;
                }
            }
        }

        return $return_images;
    }

    //sobrecarga do metodo que deleta a imagem, para que a mesma seja deletada do banco também
    public function delete($print_response = true) {
        $response = parent::delete(false);
        foreach ($response as $name => $deleted) {
            if ($deleted) {
                $extract = array_reverse(explode(".", $name));

                $CampusImagem = $this->CampusImagem->find('first', array('recursive' => -1, 'conditions' => array("CampusImagem.file LIKE '%$name%'")));
                if($CampusImagem){
                    $this->CampusImagem->delete($CampusImagem['CampusImagem']['id']);

                    $dir = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS . 'files' . DS . 'campus'. DS  . 'file' . DS . $CampusImagem['CampusImagem']['campus_id'];
                    if(file_exists($dir . DS . $name)){
                        unlink($dir . DS . $name);
                    }

                    if(file_exists($dir . DS . 'zoom' . DS . $name)){
                        unlink($dir . DS . 'zoom' . DS . $name);
                    }

                    if(file_exists($dir . DS . 'thumb' . DS . $name)){
                        unlink($dir . DS . 'thumb' . DS . $name);
                    }

                    if(file_exists($dir . DS . 'medium' . DS . $name)){
                        unlink($dir . DS . 'medium' . DS . $name);
                    }

                    if(file_exists($dir . DS . 'semi-thumbnail' . DS . $name)){
                        unlink($dir . DS . 'semi-thumbnail' . DS . $name);
                    }

                    if(file_exists($dir . DS . 'thumbnail' . DS . $name)){
                        unlink($dir . DS . 'thumbnail' . DS . $name);
                    }
                }
            }
        } 
        return $this->generate_response($response, $print_response);
    }

}
